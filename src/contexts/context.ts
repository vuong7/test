import { createContext, useContext } from 'react'

import { GeneratedTheme, ThemeData, TranslationData } from './types';

export type JobhopStorybookContextProps = {
  theme: GeneratedTheme
  setTheme(theme: ThemeData): void;
  setTranslation(translation: TranslationData): void;
};

export const JobhopStorybookContext = createContext<JobhopStorybookContextProps>({} as JobhopStorybookContextProps);

export const useJobhopStorybookTheme = () => {
  const context = useContext(JobhopStorybookContext)
  if(context === undefined) {
    console.error('[useJobhopStorybookTheme] Context not defined.');
    throw new Error('useJobhopStorybookTheme must be used within a JobhopStorybookProvider')
  }
  return context.theme
}

export const useJobhopStorybookSettings = () => {
  const context = useContext(JobhopStorybookContext)
  if(context === undefined) {
    console.error('[useJobhopStorybookSettings] Context not defined.');
    throw new Error('useJobhopStorybookSettings must be used within a JobhopStorybookProvider')
  }
  return {
    setTheme: context.setTheme,
    setTranslation: context.setTranslation,
  }
}