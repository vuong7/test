import { TintShade } from "."

export type ThemeData = {
  primary: string;
  highlight: string;
}

export type GeneratedTheme = {
  primary: string;
  primaryTs: TintShade;
  
  highlight: string;
  highlightTs: TintShade;

  neutral: string;
  neutralTs: TintShade;

  system: SystemColor;
  contactTags: ContactTags;
}

type SystemColor = {
  light1: string;
  light2: string;
  light3: string;
  neutral: string;

  error: string;
  success: string;
  warning: string;
}

type ContactTags = {
  negative1: string;
  negative2: string;
  systemAdded: string;
  systemSuggestion: string;
}