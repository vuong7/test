/**
 * Please keep it sort alphabetically inside modules.
 */
export type TranslationData = {
  // ## CORE -------------------------------------------

  // ## CONTACT ----------------------------------------
  contact: {
    notes: {
      attachments: string;
      clickUpdate: string;
      cancel: string;
      createdBy: string;
      lastUpdate: string;
      recording: string;
      save: string;
      typeTitle: string;
      updateTag: string;
    };
  };
  // ## HOMEPAGE ---------------------------------------
  homepage: {
    feedHeader: {
      publishedAProduct: string;
      publishedAnInsight: string;
      publishedOn: string;
    };
  };

  // ## INSIGHT ----------------------------------------
  insight: {
    card: {
      createdBy: string;
      createdOn: string;
    };
    sliderUplinkCards: {
      storySuggestions: string;
      noCloseDate: string;
      noMaturityDate: string;
    };
    storyTags: {
      recommended: string;
      strong: string;
      ok: string;
      weak: string;
    };
    storyTellingSuggestions: {
      addNew: string;
      noFilterStories: string;
      showLess: string;
      showMore: string;
    };
  };

  // ## PRODUCT ----------------------------------------
  product: {
    addTags: {
      noTagsFound: string;
      startTagSearch: string;
      tags: string;
      addNew: string;
      addTags: string;
      chooseTags: string;
      typeSearch: string;
      selectCategory: string;
      backButton: string;
      nextButton: string;
      saveButton: string;
      selectedTags: string;
      primaryTag: string;
      primaryTagsMessage: string;
      clearAll: string;
      dropMessage: string;
      showSelected: string;
      hideSelected: string;
      hide: string;
    };
    attachments: {
      attachments: string;
      message: string;
      accepted: string;
      maxSize: string;
      addExternal: string;
      reorderMessage: string;
      yourAttachments: string;
      uploadedOn: string;
      documentType: string;
      selectType: string;
      saveButton: string;
      dragDropMessage: string;
      browseMessage: string;
      dropFiles: string;
      noAttachments: string;
      public: string;
      private: string;
      noTypes: string;
      unselected: string;
      submitFailed: string;
      invalidUrl: string;
    };
  };
  uplink: {
    shares: {
      author: string;
      complete: string;
      downloadFile: string;
      downloadTheFile: string;
      failed: string;
      viewing: string;
      accept: string;
      acceptMessage: string;
      failedFullLoad: string;
      failedFullTryAgain: string;
      failedLoad: string;
      preview: string;
      privacyPolicy: string;
      watchVideo: string;
    };
  };
};
