export type TintShade = {
  10: string;
  20: string;
  30: string;
  40: string;
  50: string;
  60: string;
  70: string;
  80: string;
  90: string;
  
  110: string;
  120: string;
  130: string;
  140: string;
  150: string;
  160: string;
  170: string;
  180: string;
  190: string;
  
}