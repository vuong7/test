import React, { useCallback, useEffect, useMemo, useState } from "react";
import { initReactI18next } from "react-i18next";
import { ThemeContext, ThemeProvider } from "styled-components";
import { tintGenerator, shadeGenerator } from "tint-shade-generator";
import i18next from "i18next";

import translationFile from "../../data/translations.json";

import { JobhopStorybookContext } from "./context";
import { GeneratedTheme, ThemeData, TintShade, TranslationData } from "./types";

const i18n = i18next.createInstance();
i18n.use(initReactI18next).init({ lng: "en", debug: false });

type Props = {
  translation?: TranslationData;
  theme?: ThemeData;
};
export const JobhopStorybookProvider: React.FC<Props> = ({
  translation,
  theme,
  children,
}) => {
  const [themeColors, setThemeColors] = useState<GeneratedTheme>();

  const generateTintShade = useCallback((hexColor: string): TintShade => {
    return {
      90: tintGenerator(hexColor, 1),
      80: tintGenerator(hexColor, 2),
      70: tintGenerator(hexColor, 3),
      60: tintGenerator(hexColor, 4),
      50: tintGenerator(hexColor, 5),
      40: tintGenerator(hexColor, 6),
      30: tintGenerator(hexColor, 7),
      20: tintGenerator(hexColor, 8),
      10: tintGenerator(hexColor, 9),
      110: shadeGenerator(hexColor, 1),
      120: shadeGenerator(hexColor, 2),
      130: shadeGenerator(hexColor, 3),
      140: shadeGenerator(hexColor, 4),
      150: shadeGenerator(hexColor, 5),
      160: shadeGenerator(hexColor, 6),
      170: shadeGenerator(hexColor, 7),
      180: shadeGenerator(hexColor, 8),
      190: shadeGenerator(hexColor, 9),
    };
  }, []);

  const setTheme = useCallback((themeData: ThemeData) => {
    setThemeColors({
      ...themeData,
      primaryTs: generateTintShade(themeData.primary),
      highlightTs: generateTintShade(themeData.highlight),
      neutral: "#574E61",
      neutralTs: generateTintShade("#574E61"),
      system: {
        light1: "#fff",
        light2: "#FAFAFC",
        light3: "#F4F3F5",
        neutral: "#5D5C94",
        error: "#E15050",
        success: "#1CC684",
        warning: "#E98C00",
      },
      contactTags: {
        negative1: "#FFE0E1",
        negative2: "#EB5757",
        systemAdded: "#EDE5F6",
        systemSuggestion: "#AA8CCC",
      },
    });
  }, []);

  const setTranslation = useCallback((translationData: TranslationData) => {
    console.log("setTranslation >>", translationData);
    i18n.addResourceBundle("en", "translation", translationData, true, true);
  }, []);

  useEffect(() => {
    setTheme(theme || { primary: "#4C2875", highlight: "#ED3995" });
  }, [theme]);

  useEffect(() => {
    setTranslation(translation || translationFile);
  }, [translation]);

  console.count("JobhopStorybookProvider PRE");
  console.log(theme);

  if (!themeColors) return null;

  console.count("JobhopStorybookProvider RENDER");

  return (
    <JobhopStorybookContext.Provider
      value={{
        theme: themeColors,
        setTheme,
        setTranslation,
      }}
    >
      <ThemeProvider theme={themeColors}>{children}</ThemeProvider>
    </JobhopStorybookContext.Provider>
  );
};

export default JobhopStorybookProvider;
