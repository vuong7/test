import styled from 'styled-components';

export const ErrorsWrapper = styled.div`
  margin-top: 10px;
`;

export const MessageWrapper = styled.div`
  display: flex;
  align-items: center;

  svg {
    margin-right: 10px;
  }
`;

type Props = {
  color: string;
};
export const Wrapper = styled.div<Props>`
  padding: 11px;
  border-radius: 8px;
  background-color: ${props => props.color};
`;
