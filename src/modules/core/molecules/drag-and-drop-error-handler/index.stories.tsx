import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { DragAndDropErrorHandler } from './index'

export default {
  title: 'Design System/DragAndDropErrorHandler',
  component: DragAndDropErrorHandler,
} as ComponentMeta<typeof DragAndDropErrorHandler>

const Template: Story = (args) => <DragAndDropErrorHandler errors={[
  { row: 1, msgs: ['CONTACT_NOT_FOUND', 'CONTACT_TYPE_NOT_FOUND']},
  { row: 2, msgs: ['CONTACT_REQUIRE_FIRST_NAME', 'CONTACT_REQUIRE_LAST_NAME']},
]} {...args} />

export const Default = Template.bind({});
