import React, { useCallback } from 'react';
import { transparentize } from 'polished';
import { useTranslation } from 'react-i18next';

import { Icon, Typography } from '../../../core/atoms';
import {useSelectColor} from '../../utils/colors'

import { ErrorsWrapper, MessageWrapper, Wrapper } from './styles';

export type FileImportError = {
  row: number;
  msgs: string[];
};

export type FileImport = {
  success: boolean;
  errors?: FileImportError[];
};

type Props = {
  errors: FileImportError[];
};
export const DragAndDropErrorHandler: React.FC<Props> = ({ errors }) => {
  const { t } = useTranslation();
  const hexColor = useSelectColor('error');
  const bgColor = transparentize(0.9, hexColor);

  const mappedErrorMsgs: any = {
    // Not Found
    CONTACT_NOT_FOUND: t('Contact not found'),
    CONTACT_TYPE_NOT_FOUND: t('Contact type not found'),

    // Required fields
    CONTACT_REQUIRE_FIRST_NAME: t('First name required'),
    CONTACT_REQUIRE_LAST_NAME: t('Last name required'),
    CONTACT_REQUIRE_CONTACT_TYPE: t('Contact type required'),

    // Invalid format
    CONTACT_PHONE_INVALID_FORMAT: t('Invalid phone format'),
    CONTACT_EMAIL_INVALID_FORMAT: t('Invalid email format'),

    // Duplicated in csv file
    CONTACT_PHONE_DUPLICATED: t('Phone duplicated'),
    CONTACT_EMAIL_DUPLICATED: t('Email duplicated'),

    // Already existed in database
    CONTACT_PHONE_NOT_AVAILABLE: t('Phone not available'),
    CONTACT_EMAIL_NOT_AVAILABLE: t('Email not available'),
  };

  const findTagOnMsg = useCallback(
    msg => {
      let message = msg;
      Object.keys(mappedErrorMsgs).map(errorTag => {
        message = message.replace(new RegExp(errorTag, 'g'), mappedErrorMsgs[errorTag]);
      });
      return message;
    },
    [mappedErrorMsgs],
  );

  return (
    <Wrapper color={bgColor}>
      <MessageWrapper>
        <Icon type="alert-circle" size="sm" customColor={hexColor} />
        <Typography customColor={hexColor}>{t('{{count}} errors occurred', { count: errors.length })}:</Typography>
      </MessageWrapper>
      <ErrorsWrapper>
        {errors.map(error => (
          <Typography>
            {t('Row')} {error.row}: {error.msgs.map(msg => mappedErrorMsgs[msg] || findTagOnMsg(msg)).join(', ')}
          </Typography>
        ))}
      </ErrorsWrapper>
    </Wrapper>
  );
};

export default DragAndDropErrorHandler;
