import React from 'react';
import { useTranslation } from 'react-i18next';

import { DownloadBottomIcon } from '../../atoms/icon-group';

import { UpsButtonExport } from './styles';

type Props = {
  onClick(): void;
};
export const ButtonExport: React.FC<Props> = ({ onClick }) => {
  const { t } = useTranslation();
  return (
    <UpsButtonExport color="primary" outline onClick={onClick} width={112}>
      <DownloadBottomIcon />
      <span>{t('Export')}</span>
    </UpsButtonExport>
  );
};

export default ButtonExport;
