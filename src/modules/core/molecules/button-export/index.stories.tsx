import React from 'react'
import { action } from '@storybook/addon-actions';
import { ComponentMeta, Story } from '@storybook/react';

import { ButtonExport } from './index'

export default {
  title: 'Design System/ButtonExport',
  component: ButtonExport,
} as ComponentMeta<typeof ButtonExport>

const Template: Story = (args) => <ButtonExport onClick={action('clicked')} {...args} />

export const Default = Template.bind({});
