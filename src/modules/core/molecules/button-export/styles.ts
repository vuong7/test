import styled from 'styled-components';
import { UpsButton } from '../../atoms';

export const UpsButtonExport = styled(UpsButton)`
  svg {
    margin-right: 3px;
  }

  span {
    color: #84768e !important;
  }
  border-color: #84768e !important;
`;
