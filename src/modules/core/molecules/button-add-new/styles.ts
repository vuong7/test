import styled from 'styled-components';
import { UpsButton } from '../../atoms';

export const Button = styled(UpsButton)`
  svg {
    width: 10px !important;
    height: 10px;
    margin-right: 3px;
  }
`;
