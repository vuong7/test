import React from 'react';
import { useTranslation } from 'react-i18next';

import { PlusIcon } from '../../atoms/icon-group';

import { Button } from './styles';

type Props = {
  onClick(): void;
};
export const ButtonAddNew: React.FC<Props> = ({ onClick }) => {
  const { t } = useTranslation();
  return (
    <Button color="primary" onClick={onClick} width={88}>
      <PlusIcon />
      <span>{t('ADD NEW')}</span>
    </Button>
  );
};

export default ButtonAddNew;
