import React from 'react'
import { action } from '@storybook/addon-actions';
import { ComponentMeta, Story } from '@storybook/react';

import { ButtonAddNew } from './index'

export default {
  title: 'Design System/ButtonAddNew',
  component: ButtonAddNew,
} as ComponentMeta<typeof ButtonAddNew>

const Template: Story = (args) => <ButtonAddNew onClick={action('clicked')} {...args} />

export const Default = Template.bind({});
