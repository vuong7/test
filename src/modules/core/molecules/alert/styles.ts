import styled from 'styled-components';

type Props = {
  color: string;
};
export const Wrapper = styled.div<Props>`
  padding: 11px;
  border-radius: 8px;
  background-color: ${props => props.color};
  display: flex;
  align-items: center;

  svg {
    margin-right: 10px;
  }
`;
