import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Alert } from './index'

export default {
  title: 'Design System/Alert',
  component: Alert,
} as ComponentMeta<typeof Alert>

const Template: Story = (args) => <Alert {...args} />

export const Default = Template.bind({});
Default.args = { variant: 'error', hasIcon:true}

export const Attention = Template.bind({});
Attention.args = { variant: 'warning', hasIcon:true}

export const Success = Template.bind({});
Success.args = { variant: 'success', hasIcon:true}