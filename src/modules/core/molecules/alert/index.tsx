import React from 'react';
import { transparentize } from 'polished';

import { Icon, IconsAvailable } from '../../atoms/icon';
import { Typography, TypographyVariant} from '../../atoms/typography'
import { useSelectColor } from '../../utils/colors';
import { Wrapper } from './styles';

const mapIcon: Record<AlertVariant, IconsAvailable> = {
  error: 'alert-circle',
  warning: 'social-email-rounded', // TODO not defined yet
  // info: 'social-email-rounded', // TODO not defined yet
  success: 'check-circle',
};

type AlertVariant = 'warning' | 'error' | 'success' /*| 'info'*/;

type Props = {
  variant?: AlertVariant;
  typography?: TypographyVariant;
  hasIcon?: boolean;
};
export const Alert: React.FC<Props> = ({ variant = 'warning', typography = 'p', hasIcon = false, children }) => {
  const hexColor = useSelectColor(variant);
  const bgColor = transparentize(0.9, hexColor);
  return (
    <Wrapper color={bgColor}>
      {hasIcon && <Icon type={mapIcon[variant]} size="sm" customColor={hexColor} />}
      <Typography customColor={hexColor} variant={typography}>
        {children}
      </Typography>
    </Wrapper>
  );
};

export default Alert;
