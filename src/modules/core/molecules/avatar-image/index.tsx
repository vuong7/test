import React, { useState } from "react";
import { Typography } from "../../atoms/typography";

import { InitialsWrapper, TextWrapper, ImageWrapper } from "./styles";

type Sizes = "xs" | "sm" | "md" | "lg" | "xl" | "xxl" | "customSize";

const sizes: Record<Sizes, number> = {
  xs: 8,
  sm: 16,
  md: 24,
  lg: 32,
  xl: 48,
  xxl: 96,
  customSize: 0,
};

type Props = {
  url?: string;
  size?: Sizes;
  customSize?: number;
  variant?: "squared" | "rounded" | "circle";
  name?: string;
};

export const AvatarImage: React.FC<Props> = ({
  url,
  size = "customSize",
  customSize = 0,
  variant = "circle",
  name,
}) => {
  const [hasImageFailed, setHasImageFailed] = useState<boolean>(false);
  const [hasImageLoaded, setHasImageLoaded] = useState<boolean>(false);
  const imageSize = size === "customSize" ? customSize : sizes[size];

  const getInitials = () => {
    if (name) {
      const namesArray = name
        ?.replace(/[^A-Za-zÀ-ü ]/g, "")
        .trim()
        .split(" ");
      const initials =
        namesArray.length > 1
          ? `${namesArray[0][0]}${namesArray[namesArray.length - 1][0]}`
          : `${namesArray[0][0]}`;
      return initials;
    } else {
      return "";
    }
  };

  return (
    <InitialsWrapper
      url={url}
      size={imageSize}
      variant={variant}
      hasImageFailed={hasImageFailed}
    >
      {!hasImageFailed && (
        <ImageWrapper
          src={url || ""}
          alt=" "
          onError={() => setHasImageFailed(true)}
          onLoad={() => setHasImageLoaded(true)}
          variant={variant}
          size={imageSize}
          hasImageLoaded={hasImageLoaded}
        />
      )}
      {hasImageFailed && (
        <TextWrapper>
          <Typography variant="h4" color="neutral/60">
            {getInitials()}
          </Typography>
        </TextWrapper>
      )}
    </InitialsWrapper>
  );
};

export default AvatarImage;
