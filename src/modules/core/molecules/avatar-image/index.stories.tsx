import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AvatarImage } from "./index";

export default {
  title: "Design System/AvatarImage",
  component: AvatarImage,
} as ComponentMeta<typeof AvatarImage>;

const Template: Story = (args) => <AvatarImage {...args} />;

export const Default = Template.bind({});
Default.args = {
  url: "https://www.w3schools.com/howto/img_avatar.png",
  name: "Test Name Last",
  customSize: 42,
};

export const OnlyName = Template.bind({});
OnlyName.args = {
  name: "-- Test Nam7e~   Last~",
  customSize: 42,
};

export const BrokenUrl = Template.bind({});
BrokenUrl.args = {
  url: "fail",
  name: "Test Name Last",
  customSize: 42,
};

export const EmptyUrl = Template.bind({});
EmptyUrl.args = {
  url: "",
  name: "Test Name Last",
  customSize: 42,
};
