import styled, { css } from "styled-components";
// import AvatarDefault from '../../../../app/components/images/AvatarDefault.png';

const Variants = {
  squared: css``,

  rounded: css`
    border-radius: 10px;
  `,

  circle: css`
    border-radius: 50%;
  `,
};

type WrapperProps = {
  url?: string;
  size: number;
  variant: "squared" | "rounded" | "circle";
  hasImageFailed?: boolean;
  hasImageLoaded?: boolean;
};

export const InitialsWrapper = styled.div<WrapperProps>`
  width: ${(props) => props.size}px;
  height: ${(props) => props.size}px;
  min-width: ${(props) => props.size}px;
  min-height: ${(props) => props.size}px;

  background-color: ${(props) =>
    props.hasImageFailed ? props.theme.neutralTs[10] : "none"};

  ${(props) => Variants[props.variant]}
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TextWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ImageWrapper = styled.img<WrapperProps>`
  width: 100%;
  height: 100%;

  ${(props) => Variants[props.variant]}

  opacity: ${(props) => (props.hasImageLoaded ? "1" : 0)}
`;
