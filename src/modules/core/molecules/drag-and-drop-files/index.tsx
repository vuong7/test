import React, { useCallback, useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { useTranslation } from "react-i18next";

import { Button, Icon, IconButton, LinearProgress, Spacer } from "../../atoms";
import { FileImport, FileImportError } from "../drag-and-drop-error-handler";
import { Alert, DragAndDropErrorHandler } from "../../molecules";
import {
  DragNDropWrapper,
  FilesWrapper,
  FileRow,
  FileInput,
  Wrapper,
  NoteFileInput,
  LineDivision,
  NoteFileRow,
  Filler,
  TypeIconContainer,
} from "./styles";

type FileStatus =
  | "AWAITING"
  | "VALIDATING"
  | "VALID"
  | "INVALID"
  | "UPLOADING"
  | "DONE"
  | "ERROR";
type DropFile = {
  status: FileStatus;
  errors?: FileImportError[];
  file: File;
};

type Props = {
  accept?: string | string[];
  maxFiles?: number;
  onChange(files: File[]): void;
  onValidate?(file: File): Promise<FileImport>;
  variant?: "default" | "notes";
  updateNoteFiles?: (files: File[]) => void;
};
export const DragAndDropFiles: React.FC<Props> = ({
  accept,
  maxFiles,
  onChange,
  onValidate,
  variant = "default",
  updateNoteFiles,
}) => {
  const { t } = useTranslation();
  const [files, setFiles] = useState<DropFile[]>([]);

  if (variant === "default") {
    const onDrop = useCallback(
      (acceptedFiles: File[], fileRejections) => {
        if (!fileRejections || fileRejections.length === 0) {
          setFiles(
            acceptedFiles.map((file) => ({
              status: onValidate ? "AWAITING" : "VALID",
              file,
            }))
          );
        }
      },
      [onValidate]
    );

    const updateFileStatus = useCallback(
      (index: number, status: FileStatus, errors?: FileImportError[]) => {
        setFiles(
          files.map((file, fileIndex) => {
            if (fileIndex === index) {
              return { ...file, status, errors };
            }
            return file;
          })
        );
      },
      [files]
    );

    const handleDeleteFile = useCallback(
      (index) => {
        setFiles([...files.slice(0, index), ...files.slice(index + 1)]);
      },
      [files]
    );

    const handleOnValidate = useCallback(
      async (index) => {
        if (onValidate) {
          updateFileStatus(index, "VALIDATING");
          try {
            const response = await onValidate(files[index].file);
            if (response.success) {
              updateFileStatus(index, "VALID");
            } else {
              updateFileStatus(index, "INVALID", response.errors);
            }
          } catch {
            updateFileStatus(index, "INVALID");
          }
        }
      },
      [files, onValidate, updateFileStatus]
    );

    useEffect(() => {
      const validFiles = files
        .filter(({ status }) => status === "VALID")
        .map(({ file }) => file);
      onChange(validFiles);
    }, [files, onChange]);

    const { getRootProps, getInputProps, isDragActive, isDragAccept } =
      useDropzone({
        onDrop,
        accept,
        maxFiles,
        multiple: maxFiles ? maxFiles > 1 : true,
      });
    return (
      <Wrapper>
        {!files || files.length === 0 ? (
          <DragNDropWrapper
            {...getRootProps()}
            isDragActive={isDragActive}
            isDragAccept={isDragAccept}
          >
            <input {...getInputProps()} />
            <Icon
              type={
                isDragActive && !isDragAccept
                  ? "alert-triangle"
                  : "upload-cloud"
              }
              customColor="#786C86"
              size="md"
            />
            {isDragActive ? (
              isDragAccept ? (
                <div>{t("Drop the files here...")}</div>
              ) : (
                <div>{t("File not accepted")}</div>
              )
            ) : (
              <div>
                {t("Drag and drop or")} <span>{t("browse your files")}</span>
              </div>
            )}
          </DragNDropWrapper>
        ) : (
          <FilesWrapper>
            {files.map((item, index) => (
              <>
                <FileRow>
                  <FileInput>
                    <span>{item.file.name}</span>
                    <IconButton
                      onClick={() => handleDeleteFile(index)}
                      type="close-circle"
                      size="md"
                      customColor="#786C86"
                    />
                  </FileInput>
                  {onValidate && item.status === "AWAITING" && (
                    <Button
                      type="button"
                      color="primary"
                      // variant="outlined"
                      style={{ width: 136, marginLeft: 8 }}
                      onClick={() => handleOnValidate(index)}
                    >
                      {t("Validate")}
                    </Button>
                  )}
                </FileRow>
                {onValidate && item.status !== "AWAITING" && <Spacer y={10} />}
                {onValidate && item.status === "VALIDATING" && (
                  <LinearProgress />
                )}
                {onValidate && item.status === "VALID" && (
                  <Alert variant="success" hasIcon>
                    {t(
                      "Validation with success, your file is ready to import."
                    )}
                  </Alert>
                )}
                {onValidate && item.status === "INVALID" && item.errors && (
                  <DragAndDropErrorHandler errors={item.errors} />
                )}
              </>
            ))}
          </FilesWrapper>
        )}
      </Wrapper>
    );
  } else {
    const [noteFiles, setNoteFiles] = useState<DropFile[]>([]);
    const onDrop = useCallback(
      (acceptedFiles: File[], fileRejections) => {
        if (!fileRejections || fileRejections.length === 0) {
          setFiles(
            acceptedFiles.map((file) => ({
              status: onValidate ? "AWAITING" : "VALID",
              file,
            }))
          );
        }
      },
      [onValidate]
    );

    const updateFileStatus = useCallback(
      (index: number, status: FileStatus, errors?: FileImportError[]) => {
        setFiles(
          files.map((file, fileIndex) => {
            if (fileIndex === index) {
              return { ...file, status, errors };
            }
            return file;
          })
        );
      },
      [files]
    );

    const handleDeleteFile = useCallback(
      (index) => {
        setFiles([...files.slice(0, index), ...files.slice(index + 1)]);
        setNoteFiles([
          ...noteFiles.slice(0, index),
          ...noteFiles.slice(index + 1),
        ]);
      },
      [files]
    );

    const handleOnValidate = useCallback(
      async (index) => {
        if (onValidate) {
          updateFileStatus(index, "VALIDATING");
          try {
            const response = await onValidate(files[index].file);
            if (response.success) {
              updateFileStatus(index, "VALID");
            } else {
              updateFileStatus(index, "INVALID", response.errors);
            }
          } catch {
            updateFileStatus(index, "INVALID");
          }
        }
      },
      [files, onValidate, updateFileStatus]
    );

    useEffect(() => {
      const validFiles = files
        .filter(({ status }) => status === "VALID")
        .map(({ file }) => file);
      onChange(validFiles);
    }, [files, onChange]);

    const { getRootProps, getInputProps, isDragActive, isDragAccept } =
      useDropzone({
        onDrop,
        accept,
        maxFiles,
        multiple: maxFiles ? maxFiles > 1 : true,
      });
    useEffect(() => {
      files.forEach((file, index) => handleOnValidate(index));
    }, [files.length]);
    useEffect(() => {
      const newList = [
        ...noteFiles,
        ...files.filter((file) =>
          noteFiles.every((noteFile) => file.file !== noteFile.file)
        ),
      ];
      setNoteFiles(newList);
    }, [files]);

    useEffect(() => {
      if (updateNoteFiles) {
        updateNoteFiles(noteFiles.map((noteFile) => noteFile.file));
      }
    }, [noteFiles]);
    return (
      <Wrapper>
        <DragNDropWrapper
          {...getRootProps()}
          isDragActive={isDragActive}
          isDragAccept={isDragAccept}
        >
          <input
            {...getInputProps({ onClick: (event: any) => console.log(event) })}
          />

          {isDragActive ? (
            isDragAccept ? (
              <div>{t("Drop the files here...")}</div>
            ) : (
              <div>{t("File not accepted")}</div>
            )
          ) : (
            <div>
              {t("Drag and drop or")} <span>{t("browse your files")}</span>
            </div>
          )}
        </DragNDropWrapper>

        <FilesWrapper>
          {noteFiles.map((item, index) => (
            <>
              <Spacer y={8} />
              <NoteFileRow>
                <Icon type="menu" size="sm" color="neutral/40" />
                <Spacer x={8} />
                <NoteFileInput>
                  {item.file.type.startsWith("image") && (
                    <>
                      <img src={URL.createObjectURL(item.file)} />
                      <Spacer x={8} />
                      <TypeIconContainer>
                        <Icon type="image" size="sm" color="system/light1" />
                      </TypeIconContainer>
                    </>
                  )}
                  {item.file.type.startsWith("video") && (
                    <>
                      <video src={URL.createObjectURL(item.file)} />
                      <Spacer x={8} />
                      <TypeIconContainer>
                        <Icon type="video" size="sm" color="system/light1" />
                      </TypeIconContainer>
                    </>
                  )}
                  <span>{item.file.name}</span>
                </NoteFileInput>
                <Filler />
                <Spacer x={8} />
                <LineDivision />
                <Spacer x={12} />
                <IconButton
                  onClick={() => handleDeleteFile(index)}
                  type="close-circle"
                  size="md"
                  customColor="#786C86"
                />
              </NoteFileRow>
              {onValidate && item.status !== "AWAITING" && <Spacer y={10} />}
              {onValidate && item.status === "VALIDATING" && <LinearProgress />}
              {onValidate && item.status === "VALID" && (
                <Alert variant="success" hasIcon>
                  {t("Validation with success, your file is ready to import.")}
                </Alert>
              )}
              {onValidate && item.status === "INVALID" && item.errors && (
                <DragAndDropErrorHandler errors={item.errors} />
              )}
            </>
          ))}
        </FilesWrapper>
      </Wrapper>
    );
  }
};

export default DragAndDropFiles;
