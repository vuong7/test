import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { DragAndDropFiles } from "./index";

export default {
  title: "Design System/DragAndDropFiles",
  component: DragAndDropFiles,
} as ComponentMeta<typeof DragAndDropFiles>;

const Template: Story = (args) => (
  <DragAndDropFiles
    onChange={function (files: File[]): void {
      throw new Error("Function not implemented.");
    }}
    {...args}
  />
);

export const Default = Template.bind({});

export const NoteDrops = Template.bind({});
NoteDrops.args = { variant: "notes" };
