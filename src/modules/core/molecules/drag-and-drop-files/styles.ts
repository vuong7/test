import styled from "styled-components";

type DragNDropWrapperProps = {
  isDragActive: boolean;
  isDragAccept: boolean;
};
export const DragNDropWrapper = styled.div<DragNDropWrapperProps>`
  background: #eeecf0;
  border: 1px dashed
    ${(props) =>
      props.isDragActive
        ? props.isDragAccept
          ? props.theme.highlight
          : "#E15050"
        : "#786c86"};
  border-radius: 8px;
  padding: 28px 0px;
  text-align: center;

  cursor: ${(props) =>
    props.isDragActive && !props.isDragAccept ? "not-allowed" : "inherit"};

  div {
    margin-top: 2px;
    color: #574e61;
    font-size: 14px;

    span {
      color: ${(props) => props.theme.primary};
      font-weight: 600;
      cursor: pointer;
    }
  }
`;

export const FilesWrapper = styled.div``;

export const FileRow = styled.div`
  display: grid;
  grid-template-columns: auto min-content;
`;

export const FileInput = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 40px;
  background: #fafafc;
  border-radius: 8px;
  padding: 8px 12px;

  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  color: #574e61;

  > span {
    max-width: 340px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }
`;

export const Wrapper = styled.div``;

export const NoteFileInput = styled.div`
  display: flex;

  align-items: center;
  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  color: #574e61;

  position: relative;

  img,
  video {
    width: 63px;
    height: 42px;
    object-fit: cover;
  }

  > span {
    /* max-width: 340px; */
    flex-grow: 1;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: wrap;
  }
`;

export const LineDivision = styled.div`
  border-left: 1px solid ${(props) => props.theme.neutralTs[10]};

  height: 31px;
  box-sizing: border-box;
`;

export const NoteFileRow = styled.div`
  display: flex;
  align-items: center;
  height: 40px;
  background: #fafafc;
  border-radius: 8px;
  padding: 8px 12px;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const TypeIconContainer = styled.div`
  position: absolute;

  width: 63px;
  height: 42px;
  z-index: 1;
  background: linear-gradient(
    60.45deg,
    rgba(0, 0, 0, 0.68) 4.45%,
    rgba(0, 0, 0, 0) 97.85%
  );
  border-radius: 4px;

  svg {
    position: absolute;
    top: 22px;
    left: 7px;
  }
`;
