import React from 'react'
import { action } from '@storybook/addon-actions';
import { ComponentMeta, Story } from '@storybook/react';

import { ButtonEdit } from './index'

export default {
  title: 'Design System/ButtonEdit',
  component: ButtonEdit,
} as ComponentMeta<typeof ButtonEdit>

const Template: Story = (args) => <ButtonEdit onClick={action('clicked')} {...args} />

export const Default = Template.bind({});
