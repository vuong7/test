import styled from 'styled-components';
import { UpsButton } from '../../atoms';

export const Button = styled(UpsButton)`
  svg {
    margin-right: 8px;
  }

  span {
    font-weight: 600 !important;
    font-size: 14px !important;
    line-height: 14px !important;
    letter-spacing: 0.02em !important;
    text-transform: uppercase !important;
    margin-left: 0px !important;
  }
`;
