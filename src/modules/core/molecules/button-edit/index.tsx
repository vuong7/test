import React from 'react';
import { useTranslation } from 'react-i18next';

import { Button } from './styles';
import { Icon } from '../../atoms/icon';

type Props = {
  onClick(): void;
};
export const ButtonEdit: React.FC<Props> = ({ onClick }) => {
  const { t } = useTranslation();
  return (
    <Button color="primary" onClick={onClick} width={88}>
      <Icon type="edit" color="system/light1" size="sm" />
      <span>{t('Edit')}</span>
    </Button>
  );
};

export default ButtonEdit;
