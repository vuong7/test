import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Typography, TypographyProps } from "..";

export default {
  title: "Design System/Typography",
  component: Typography,
} as ComponentMeta<typeof Typography>;

const Template: Story<TypographyProps> = (args) => (
  <Typography {...args}>Example of typography text</Typography>
);

export const Default = Template.bind({});

export const BigNumbers = Template.bind({});
BigNumbers.args = { variant: "big-numbers" };

export const Header1 = Template.bind({});
Header1.args = { variant: "h1" };

export const Header2 = Template.bind({});
Header2.args = { variant: "h2" };

export const Header3 = Template.bind({});
Header3.args = { variant: "h3" };

export const Header4 = Template.bind({});
Header4.args = { variant: "h4" };

export const InputValidation = Template.bind({});
InputValidation.args = { variant: "input-validation" };

export const Label = Template.bind({});
Label.args = { variant: "label" };

export const Paragraph = Template.bind({});
Paragraph.args = { variant: "p" };

export const ParagraphArticles = Template.bind({});
ParagraphArticles.args = { variant: "p-articles" };

export const ParagraphSemiBold = Template.bind({});
ParagraphSemiBold.args = { variant: "p-semi-bold" };

export const ParagraphSmall = Template.bind({});
ParagraphSmall.args = { variant: "p-small" };

export const SmallTitles = Template.bind({});
SmallTitles.args = { variant: "small-titles" };

export const Titles = Template.bind({});
Titles.args = { variant: "titles" };
