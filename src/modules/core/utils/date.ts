export const formatFullDateAndTime = (updatedAt: Date): string => {
    return `${updatedAt.getDate()} ${updatedAt.toLocaleString("default", { month: "short" })} ${updatedAt.getFullYear()} ${updatedAt.getHours()}:${updatedAt.getMinutes()}`;
}