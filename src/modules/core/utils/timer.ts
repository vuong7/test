export const formatTimerMinutesSeconds = (seconds: number) => {
  if (seconds <= 0 || isNaN(seconds) || seconds === Infinity) {
    return "0:00";
  } else {
    return `${Math.floor(seconds / 60)}:${
      Math.floor(seconds % 60) >= 10
        ? Math.floor(seconds % 60)
        : Math.floor(seconds % 60) <= 0
        ? "00"
        : `0${Math.floor(seconds % 60)}`
    }`;
  }
};
