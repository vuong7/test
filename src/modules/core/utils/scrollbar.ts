export const ScrollbarStyled = `
    ::-webkit-scrollbar {
      width: 6px;
      height: 6px;
      background: white;
      border-radius: 8px;
    }

    ::-webkit-scrollbar-track {
      background: white;
      border-radius: 8px;
    }

    ::-webkit-scrollbar-thumb {
      background: rgba(29,31,33,0.6) !important;
      border-radius: 8px;
    }
  `;
