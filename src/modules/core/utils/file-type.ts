export const getFileType = (fileExtension: string): string => {
  if (fileExtension === "EXTERNAL_LINK") {
    return "EXTERNAL_LINK";
  }

  if (["doc", "docx"].includes(fileExtension)) {
    return "DOCUMENT";
  } else if (["pdf"].includes(fileExtension)) {
    return "PDF";
  } else if (["xls", "xlsx"].includes(fileExtension)) {
    return "SPREADSHEET";
  } else if (["zip", "rar"].includes(fileExtension)) {
    return "COMPRESSED";
  } else if (["jpg", "jpeg", "png"].includes(fileExtension)) {
    return "IMAGE";
  } else if (["gif"].includes(fileExtension)) {
    return "IMAGE_ANIMATED";
  } else if (
    ["3gp", "mpg", "mpeg", "mpe", "mp4", "avi", "mov"].includes(fileExtension)
  ) {
    return "VIDEO";
  } else if (["wav", "mp3"].includes(fileExtension)) {
    return "AUDIO";
  } else if (["csv"].includes(fileExtension)) {
    return "CSV";
  }
  return "UNKNOWN";
};
