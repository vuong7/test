import { useContext, useEffect, useState } from "react";
import { useJobhopStorybookTheme } from "../../../contexts";

export type AllowedColor =
  | 'customColor'
  | 'primary'
  | 'primary/10'
  | 'primary/20'
  | 'primary/30'
  | 'primary/40'
  | 'primary/50'
  | 'primary/60'
  | 'primary/70'
  | 'primary/80'
  | 'primary/90'
  | 'primary/110'
  | 'primary/120'
  | 'primary/130'
  | 'primary/140'
  | 'primary/150'
  | 'primary/160'
  | 'primary/170'
  | 'primary/180'
  | 'primary/190'
  | 'highlight'
  | 'highlight/10'
  | 'highlight/20'
  | 'highlight/30'
  | 'highlight/40'
  | 'highlight/50'
  | 'highlight/60'
  | 'highlight/70'
  | 'highlight/80'
  | 'highlight/90'
  | 'highlight/110'
  | 'highlight/120'
  | 'highlight/130'
  | 'highlight/140'
  | 'highlight/150'
  | 'highlight/160'
  | 'highlight/170'
  | 'highlight/180'
  | 'highlight/190'
  | 'neutral'
  | 'neutral/10'
  | 'neutral/20'
  | 'neutral/30'
  | 'neutral/40'
  | 'neutral/50'
  | 'neutral/60'
  | 'neutral/70'
  | 'neutral/80'
  | 'neutral/90'
  | 'neutral/110'
  | 'neutral/120'
  | 'neutral/130'
  | 'neutral/140'
  | 'neutral/150'
  | 'neutral/160'
  | 'neutral/170'
  | 'neutral/180'
  | 'neutral/190'
  | 'error'
  | 'success'
  | 'warning'
  | 'system/light1'
  | 'system/light2'
  | 'system/light3'
  | 'system/neutral';

export type ColorProps = {
  color?: AllowedColor;
  customColor?: string;
};

export const useSelectColor = (color: AllowedColor = 'customColor', customColor: string = '#574E61'): string => {
  const branding = useJobhopStorybookTheme();

  const colors: Record<AllowedColor, string> = {
    'customColor': customColor,
    'primary': branding.primary,
    'primary/10': branding.primaryTs[10],
    'primary/20': branding.primaryTs[20],
    'primary/30': branding.primaryTs[30],
    'primary/40': branding.primaryTs[40],
    'primary/50': branding.primaryTs[50],
    'primary/60': branding.primaryTs[60],
    'primary/70': branding.primaryTs[70],
    'primary/80': branding.primaryTs[80],
    'primary/90': branding.primaryTs[90],
    'primary/110': branding.primaryTs[110],
    'primary/120': branding.primaryTs[120],
    'primary/130': branding.primaryTs[130],
    'primary/140': branding.primaryTs[140],
    'primary/150': branding.primaryTs[150],
    'primary/160': branding.primaryTs[160],
    'primary/170': branding.primaryTs[170],
    'primary/180': branding.primaryTs[180],
    'primary/190': branding.primaryTs[190],
    'highlight': branding.highlight,
    'highlight/10': branding.highlightTs[10],
    'highlight/20': branding.highlightTs[20],
    'highlight/30': branding.highlightTs[30],
    'highlight/40': branding.highlightTs[40],
    'highlight/50': branding.highlightTs[50],
    'highlight/60': branding.highlightTs[60],
    'highlight/70': branding.highlightTs[70],
    'highlight/80': branding.highlightTs[80],
    'highlight/90': branding.highlightTs[90],
    'highlight/110': branding.highlightTs[110],
    'highlight/120': branding.highlightTs[120],
    'highlight/130': branding.highlightTs[130],
    'highlight/140': branding.highlightTs[140],
    'highlight/150': branding.highlightTs[150],
    'highlight/160': branding.highlightTs[160],
    'highlight/170': branding.highlightTs[170],
    'highlight/180': branding.highlightTs[180],
    'highlight/190': branding.highlightTs[190],
    'neutral': branding.neutral,
    'neutral/10': branding.neutralTs[10],
    'neutral/20': branding.neutralTs[20],
    'neutral/30': branding.neutralTs[30],
    'neutral/40': branding.neutralTs[40],
    'neutral/50': branding.neutralTs[50],
    'neutral/60': branding.neutralTs[60],
    'neutral/70': branding.neutralTs[70],
    'neutral/80': branding.neutralTs[80],
    'neutral/90': branding.neutralTs[90],
    'neutral/110': branding.neutralTs[110],
    'neutral/120': branding.neutralTs[120],
    'neutral/130': branding.neutralTs[130],
    'neutral/140': branding.neutralTs[140],
    'neutral/150': branding.neutralTs[150],
    'neutral/160': branding.neutralTs[160],
    'neutral/170': branding.neutralTs[170],
    'neutral/180': branding.neutralTs[180],
    'neutral/190': branding.neutralTs[190],
    'error': branding.system.error,
    'success': branding.system.success,
    'warning': branding.system.warning,
    'system/light1': branding.system.light1,
    'system/light2': branding.system.light2,
    'system/light3': branding.system.light3,
    'system/neutral': branding.system.neutral,
  }
  return colors[color];
};