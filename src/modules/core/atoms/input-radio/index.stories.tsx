import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InputRadio } from './index'

export default {
  title: 'Design System/InputRadio',
  component: InputRadio,
} as ComponentMeta<typeof InputRadio>

const Template: Story= (args) => <InputRadio {...args} />

export const Default = Template.bind({});



