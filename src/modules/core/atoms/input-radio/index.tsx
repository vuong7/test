import React from 'react';

import { Checkmark, Input, Wrapper } from './styles';

export const InputRadio = React.forwardRef<HTMLInputElement>((props, forwardedRef) => (
  <Wrapper>
    <Input {...props} ref={forwardedRef} />
    <Checkmark />
  </Wrapper>
));

export default InputRadio;
