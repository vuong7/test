import styled from 'styled-components';

export const Checkmark = styled.span`
  display: block;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  border: 1px solid rgba(132, 118, 142, 0.5);
  opacity: 0.8;
`;

export const Input = styled.input`
  opacity: 0;
  width: 0px;
  height: 0px;
  float: right;

  &:focus {
    border-color: ${props => props.theme.highlight};
  }

  &:focus + span:last-child {
    box-shadow: 0 0 0 0.05em #fff, 0 0 0.15em 0.1em ${props => props.theme.highlight};
  }

  &:checked + span:last-child {
    background-color: ${props => props.theme?.primary};
    background-image: url("data:image/svg+xml,%0A%3Csvg width='9' height='8' viewBox='0 0 9 8' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M8.24286 1.15L3.275 7.525C3.23571 7.56429 3.17143 7.65 3.09286 7.65C3.01071 7.65 2.95714 7.59286 2.91071 7.54643C2.86429 7.5 0.0928571 4.83571 0.0928571 4.83571L0.0392857 4.78214C0.0178571 4.75 0 4.71071 0 4.66786C0 4.625 0.0178571 4.58571 0.0392857 4.55357C0.0535714 4.53929 0.0642857 4.52857 0.0785714 4.51071C0.353571 4.22143 0.910714 3.63571 0.946428 3.6C0.992857 3.55357 1.03214 3.49286 1.11786 3.49286C1.20714 3.49286 1.26429 3.56786 1.30714 3.61071C1.35 3.65357 2.91429 5.15714 2.91429 5.15714L6.88929 0.05C6.925 0.0214286 6.96786 0 7.01429 0C7.06071 0 7.10357 0.0178571 7.13929 0.0464286L8.23214 0.907143C8.26071 0.942857 8.27857 0.985714 8.27857 1.03214C8.28214 1.07857 8.26428 1.11786 8.24286 1.15Z' fill='white'/%3E%3C/svg%3E%0A");
    background-repeat: no-repeat;
    background-position: center center;
    border: 0px;
    opacity: 1;
  }
`;

export const Wrapper = styled.label`
  margin: 0;
  grid-area: input;
`;
