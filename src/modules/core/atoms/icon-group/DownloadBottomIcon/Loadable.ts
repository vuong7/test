/**
 *
 * Asynchronously loads the component for DownloadBottomIcon
 *
 */

import { lazyLoad } from '../../../utils/loadable';

export const DownloadBottomIcon = lazyLoad(
  () => import('./index'),
  module => module.DownloadBottomIcon,
);
