/**
 *
 * PlusIcon
 *
 */
import React from 'react';

// interface Props {
//   md?: any;
//   sm?: any;
//   xs?: any;
//   active?: any;
//   bold?: boolean;
//   color?: string;
// }

export function PlusIcon({ sm, active, bold, color }: any) {

  if (bold) {
    return (
      <svg
        width="11"
        height="12"
        viewBox="0 0 11 12"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fill-rule="evenodd"
          clip-rule="evenodd"
          d="M5.30435 5.78261V1H5.69565V5.78261H10V6.21739H5.69565V11H5.30435V6.21739H1V5.78261H5.30435Z"
          fill="white"
          stroke="#4C2875"
        />
      </svg>
    );
  }

  if (sm) {
    return (
      <svg
        width="11"
        height="11"
        viewBox="0 0 11 11"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M5.30435 5.30435V1H5.69565V5.30435H10V5.69565H5.69565V10H5.30435V5.69565H1V5.30435H5.30435Z"
          fill="white"
          stroke={
            active
              ? "#4C2875"
              : "#4C2875"
          }
          strokeOpacity="0.8"
          strokeWidth="0.5"
        />
      </svg>
    );
  }

  return (
    <svg
      width="12"
      height="12"
      viewBox="0 0 12 12"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.78261 5.78261V1H6.21739V5.78261H11V6.21739H6.21739V11H5.78261V6.21739H1V5.78261H5.78261Z"
        fill={color || "white"}
        stroke={color || "white"}
      />
    </svg>
  );
}
