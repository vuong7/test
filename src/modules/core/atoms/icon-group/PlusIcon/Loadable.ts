/**
 *
 * Asynchronously loads the component for PlusIcon
 *
 */

import { lazyLoad } from '../../../utils/loadable';

export const PlusIcon = lazyLoad(
  () => import('./index'),
  module => module.PlusIcon,
);
