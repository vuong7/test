import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Textarea } from './index'

export default {
  title: 'Design System/Textarea',
  component: Textarea,
} as ComponentMeta<typeof Textarea>

const Template: Story= (args) => <Textarea {...args} />

export const Default = Template.bind({});



