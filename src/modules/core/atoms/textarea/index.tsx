import React from 'react';

import { InputStyled } from './styles';

type Props = React.InputHTMLAttributes<HTMLTextAreaElement> & {
  isError?: boolean;
  rows?: number;
};

export const Textarea = React.forwardRef<HTMLTextAreaElement, Props>(({ isError = false, ...props }, forwardedRef) => (
  <InputStyled isError={isError} ref={forwardedRef} {...props} />
));

export default Textarea;
