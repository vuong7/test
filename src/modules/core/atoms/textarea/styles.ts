import styled from 'styled-components';

type Props = {
  isError: boolean;
};
export const InputStyled = styled.textarea<Props>`
  width: 100%;
  border: 1px solid ${props => (props.isError ? '#E15050' : 'rgba(205, 208, 227, 0.6)')};
  border-radius: 8px;
  padding: 12px;

  font-weight: normal;
  font-size: 14px;
  line-height: 100%;
  color: #574e61;
  /* height: 40px; */

  &:focus {
    border-color: ${props => props.theme.highlight};
  }

  ::placeholder {
    color: #786c86;
  }
`;
