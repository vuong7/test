import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Icon, IconProps } from './index'

export default {
  title: 'Design System/Icon',
  component: Icon,
} as ComponentMeta<typeof Icon>

const Template: Story<IconProps> = (args) => <Icon {...args}>Example of Icon text</Icon>

export const Default = Template.bind({});
Default.args = { type: 'alert-circle' }

