import React from 'react';

import { ColorProps, useSelectColor } from '../../utils/colors';

import { IconTypes, IconSvgProps, types } from './svgs';

export type IconProps = ColorProps & {
  type: IconTypes;
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | 'xxl';
};

export const Icon: React.FC<IconProps> = ({ customColor = '#000', color, type, size = 'md' }) => {
  const Component: React.FC<IconSvgProps> = types[type];

  const sizes = {
    xs: 8,
    sm: 16,
    md: 24,
    lg: 32,
    xl: 48,
    xxl: 96,
  };

  return <Component color={useSelectColor(color, customColor)} size={sizes[size]} />;
};

export default Icon;

export type IconsAvailable = IconTypes;
