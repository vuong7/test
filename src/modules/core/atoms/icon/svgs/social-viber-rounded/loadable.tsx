import { lazyLoad } from '../../../../utils/loadable'

export const SocialViberRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
