import { ActivityIcon } from "./activity/loadable";
import { AlertCircleIcon } from "./alert-circle/loadable";
import { AlertTriangleIcon } from "./alert-triangle/loadable";
import { AlignLeftIcon } from "./align-left/loadable";
import { ArrowDownIcon } from "./arrow-down/loadable";
import { ArrowLeftIcon } from "./arrow-left/loadable";
import { ArrowRightIcon } from "./arrow-right/loadable";
import { ArrowRightFullIcon } from "./arrow-right-full/loadable";
import { ArrowUpIcon } from "./arrow-up/loadable";
import { AttachmentIcon } from "./attachment/loadable";
import { AudioIcon } from "./audio/loadable";
import { BarChartIcon } from "./bar-chart/loadable";
import { BoxIcon } from "./box/loadable";
import { BriefcaseIcon } from "./briefcase/loadable";
import { CalendarIcon } from "./calendar/loadable";
import { CellphoneIcon } from "./cellphone/loadable";
import { ClockIcon } from "./clock/loadable";
import { CloseIcon } from "./close/loadable";
import { CloseCircleIcon } from "./close-circle/loadable";
import { CheckIcon } from "./check/loadable";
import { CheckCircleIcon } from "./check-circle/loadable";
import { CompressedIcon } from "./compressed/loadable";
import { CsvIcon } from "./csv/loadable";
import { CubeIcon } from "./cube/loadable";
import { DownloadIcon } from "./download/loadable";
import { DocumentIcon } from "./document/loadable";
import { DocumentUnknownIcon } from "./document-unknown/loadable";
import { EditIcon } from "./edit/loadable";
import { ExternalUrlIcon } from "./external-url/loadable";
import { EyeIcon } from "./eye/loadable";
import { GlobeIcon } from "./globe/loadable";
import { GraphMoneyIcon } from "./graph-money/loadable";
import { HelpCircleIcon } from "./help-circle/loadable";
import { ImageIcon } from "./image/loadable";
import { ImageLightIcon } from "./image-light/loadable";
import { InternalUrlIcon } from "./internal-url/loadable";
import { LinkIcon } from "./link/loadable";
import { LinkedinIcon } from "./linkedin/loadable";
import { LockIcon } from "./lock/loadable";
import { MailIcon } from "./mail/loadable";
import { MailRegularIcon } from "./mail-regular/loadable";
import { MaximizeIcon } from "./maximize/loadable";
import { MenuIcon } from "./menu/loadable";
import { MicIcon } from "./mic/loadable";
import { MonitorIcon } from "./monitor/loadable";
import { MoreVerticalIcon } from "./more-vertical/loadable";
import { MousePointerIcon } from "./mouse-pointer/loadable";
import { NoteIcon } from "./note/loadable";
import { PauseIcon } from "./pause/loadable";
import { PdfIcon } from "./pdf/loadable";
import { PdfLightIcon } from "./pdf-light/loadable";
import { PersonAddIcon } from "./person-add/loadable";
import { PlayIcon } from "./play/loadable";
import { PlusIcon } from "./plus/loadable";
import { PrimaryIcon } from "./primary/loadable";
import { ProductIcon } from "./product/loadable";
import { SearchIcon } from "./search/loadable";
import { ShareIcon } from "./share/loadable";
import { SlidersIcon } from "./sliders/loadable";
import { SocialEmailRoundedIcon } from "./social-email-rounded/loadable";
import { SocialFacebookRoundedIcon } from "./social-facebook-rounded/loadable";
import { SocialInstagramRoundedIcon } from "./social-instagram-rounded/loadable";
import { SocialLineRoundedIcon } from "./social-line-rounded/loadable";
import { SocialLinkedInRoundedIcon } from "./social-linkedin-rounded/loadable";
import { SocialPhoneRoundedIcon } from "./social-phone-rounded/loadable";
import { SocialPinterestRoundedIcon } from "./social-pinterest-rounded/loadable";
import { SocialSignalRoundedIcon } from "./social-signal-rounded/loadable";
import { SocialSkypeRoundedIcon } from "./social-skype-rounded/loadable";
import { SocialSmsRoundedIcon } from "./social-sms-rounded/loadable";
import { SocialTelegramRoundedIcon } from "./social-telegram-rounded/loadable";
import { SocialTwitterRoundedIcon } from "./social-twitter-rounded/loadable";
import { SocialViberRoundedIcon } from "./social-viber-rounded/loadable";
import { SocialWeChatRoundedIcon } from "./social-wechat-rounded/loadable";
import { SocialWhatsAppRoundedIcon } from "./social-whatsapp-rounded/loadable";
import { SocialYoutubeRoundedIcon } from "./social-youtube-rounded/loadable";
import { SocialZaloRoundedIcon } from "./social-zalo-rounded/loadable";
import { SpreadsheetIcon } from "./spreadsheet/loadable";
import { StarIcon } from "./star/loadable";
import { StarFilledIcon } from "./star-filled/loadable";
import { SunIcon } from "./sun/loadable";
import { TagIcon } from "./tag/loadable";
import { TelegramIcon } from "./telegram/loadable";
import { TerminalIcon } from "./terminal/loadable";
import { ThumbsDownIcon } from "./thumbs-down/loadable";
import { ThumbsUpIcon } from "./thumbs-up/loadable";
import { TrashIcon } from "./trash/loadable";
import { TrendingDownIcon } from "./trending-down/loadable";
import { TrendingUpIcon } from "./trending-up/loadable";
import { UnknownIcon } from "./unknown/loadable";
import { UploadCloudIcon } from "./upload-cloud/loadable";
import { UserIcon } from "./user/loadable";
import { UserEngagementIcon } from "./user-engagement/loadable";
import { UserProfileIcon } from "./user-profile/loadable";
import { UsersIcon } from "./users/loadable";
import { UsersFilledIcon } from "./users-filled/loadable";
import { UsersTwoIcon } from "./users-two/loadable";
import { UplinkIcon } from "./uplink/loadable";
import { VideoIcon } from "./video/loadable";
import { VideoLightIcon } from "./video-light/loadable";
import { WhatsappIcon } from "./whatsapp/loadable";
import { ZoomInIcon } from "./zoom-in/loadable";
import { ZoomOutIcon } from "./zoom-out/loadable";

export type IconSvgProps = {
  color: string;
  size: number;
};

export type IconTypes =
  | "activity"
  | "alert-circle"
  | "alert-triangle"
  | "align-left"
  | "arrow-down"
  | "arrow-left"
  | "arrow-right"
  | "arrow-right-full"
  | "arrow-up"
  | "attachment"
  | "audio"
  | "bar-chart"
  | "box"
  | "briefcase"
  | "calendar"
  | "cellphone"
  | "clock"
  | "close"
  | "close-circle"
  | "compressed"
  | "document"
  | "document-unknown"
  | "check"
  | "check-circle"
  | "compressed"
  | "csv"
  | "cube"
  | "document"
  | "document-unknown"
  | "download"
  | "edit"
  | "external-url"
  | "eye"
  | "globe"
  | "graph-money"
  | "help-circle"
  | "image"
  | "image-light"
  | "internal-url"
  | "link"
  | "linkedin"
  | "lock"
  | "mail"
  | "mail-regular"
  | "maximize"
  | "menu"
  | "mic"
  | "monitor"
  | "more-vertical"
  | "mouse-pointer"
  | "note"
  | "pause"
  | "pdf"
  | "pdf-light"
  | "person-add"
  | "play"
  | "plus"
  | "primary"
  | "product"
  | "search"
  | "share"
  | "sliders"
  | "social-email-rounded"
  | "social-facebook-rounded"
  | "social-instagram-rounded"
  | "social-line-rounded"
  | "social-linkedin-rounded"
  | "social-phone-rounded"
  | "social-pinterest-rounded"
  | "social-signal-rounded"
  | "social-skype-rounded"
  | "social-sms-rounded"
  | "social-telegram-rounded"
  | "social-twitter-rounded"
  | "social-viber-rounded"
  | "social-wechat-rounded"
  | "social-whatsapp-rounded"
  | "social-youtube-rounded"
  | "social-zalo-rounded"
  | "spreadsheet"
  | "star"
  | "star-filled"
  | "sun"
  | "tag"
  | "telegram"
  | "terminal"
  | "thumbs-down"
  | "thumbs-up"
  | "trash"
  | "trending-down"
  | "trending-up"
  | "unknown"
  | "uplink"
  | "upload-cloud"
  | "user"
  | "user-engagement"
  | "user-profile"
  | "users"
  | "users-filled"
  | "users-two"
  | "video"
  | "video-light"
  | "whatsapp"
  | "zoom-in"
  | "zoom-out";

export const types: Record<IconTypes, React.FC<IconSvgProps>> = {
  activity: ActivityIcon,
  "alert-circle": AlertCircleIcon,
  "alert-triangle": AlertTriangleIcon,
  "align-left": AlignLeftIcon,
  "arrow-down": ArrowDownIcon,
  "arrow-left": ArrowLeftIcon,
  "arrow-right": ArrowRightIcon,
  "arrow-right-full": ArrowRightFullIcon,
  "arrow-up": ArrowUpIcon,
  attachment: AttachmentIcon,
  audio: AudioIcon,
  "bar-chart": BarChartIcon,
  box: BoxIcon,
  briefcase: BriefcaseIcon,
  calendar: CalendarIcon,
  cellphone: CellphoneIcon,
  clock: ClockIcon,
  close: CloseIcon,
  "close-circle": CloseCircleIcon,
  check: CheckIcon,
  "check-circle": CheckCircleIcon,
  compressed: CompressedIcon,
  csv: CsvIcon,
  cube: CubeIcon,
  document: DocumentIcon,
  "document-unknown": DocumentUnknownIcon,
  download: DownloadIcon,
  edit: EditIcon,
  "external-url": ExternalUrlIcon,
  eye: EyeIcon,
  globe: GlobeIcon,
  "graph-money": GraphMoneyIcon,
  image: ImageIcon,
  "image-light": ImageLightIcon,
  "internal-url": InternalUrlIcon,
  link: LinkIcon,
  linkedin: LinkedinIcon,
  lock: LockIcon,
  mail: MailIcon,
  "mail-regular": MailRegularIcon,
  maximize: MaximizeIcon,
  menu: MenuIcon,
  mic: MicIcon,
  monitor: MonitorIcon,
  "more-vertical": MoreVerticalIcon,
  "mouse-pointer": MousePointerIcon,
  note: NoteIcon,
  "help-circle": HelpCircleIcon,
  pause: PauseIcon,
  pdf: PdfIcon,
  "pdf-light": PdfLightIcon,
  "person-add": PersonAddIcon,
  play: PlayIcon,
  plus: PlusIcon,
  primary: PrimaryIcon,
  product: ProductIcon,
  search: SearchIcon,
  share: ShareIcon,
  sliders: SlidersIcon,
  "social-email-rounded": SocialEmailRoundedIcon,
  "social-facebook-rounded": SocialFacebookRoundedIcon,
  "social-instagram-rounded": SocialInstagramRoundedIcon,
  "social-line-rounded": SocialLineRoundedIcon,
  "social-linkedin-rounded": SocialLinkedInRoundedIcon,
  "social-phone-rounded": SocialPhoneRoundedIcon,
  "social-pinterest-rounded": SocialPinterestRoundedIcon,
  "social-signal-rounded": SocialSignalRoundedIcon,
  "social-skype-rounded": SocialSkypeRoundedIcon,
  "social-sms-rounded": SocialSmsRoundedIcon,
  "social-telegram-rounded": SocialTelegramRoundedIcon,
  "social-twitter-rounded": SocialTwitterRoundedIcon,
  "social-viber-rounded": SocialViberRoundedIcon,
  "social-wechat-rounded": SocialWeChatRoundedIcon,
  "social-whatsapp-rounded": SocialWhatsAppRoundedIcon,
  "social-youtube-rounded": SocialYoutubeRoundedIcon,
  "social-zalo-rounded": SocialZaloRoundedIcon,
  spreadsheet: SpreadsheetIcon,
  star: StarIcon,
  "star-filled": StarFilledIcon,
  sun: SunIcon,
  tag: TagIcon,
  telegram: TelegramIcon,
  terminal: TerminalIcon,
  "thumbs-down": ThumbsDownIcon,
  "thumbs-up": ThumbsUpIcon,
  trash: TrashIcon,
  "trending-down": TrendingDownIcon,
  "trending-up": TrendingUpIcon,
  unknown: UnknownIcon,
  uplink: UplinkIcon,
  "upload-cloud": UploadCloudIcon,
  user: UserIcon,
  "user-engagement": UserEngagementIcon,
  "user-profile": UserProfileIcon,
  users: UsersIcon,
  "users-filled": UsersFilledIcon,
  "users-two": UsersTwoIcon,
  video: VideoIcon,
  "video-light": VideoLightIcon,
  whatsapp: WhatsappIcon,
  "zoom-in": ZoomInIcon,
  "zoom-out": ZoomOutIcon,
};
