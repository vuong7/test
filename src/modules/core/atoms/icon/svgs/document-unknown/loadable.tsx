import { lazyLoad } from '../../../../utils/loadable'

export const DocumentUnknownIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
