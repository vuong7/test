import React from 'react';
import { IconSvgProps } from '..';

const MenuIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 8H14" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M2 4H14" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M2 12H14" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default MenuIcon;
