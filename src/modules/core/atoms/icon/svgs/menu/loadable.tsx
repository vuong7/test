import { lazyLoad } from '../../../../utils/loadable'

export const MenuIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
