import { lazyLoad } from '../../../../utils/loadable'

export const MonitorIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
