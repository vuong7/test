import { lazyLoad } from "../../../../utils/loadable";

export const UsersTwoIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
