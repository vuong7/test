import { lazyLoad } from '../../../../utils/loadable'

export const CheckIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
