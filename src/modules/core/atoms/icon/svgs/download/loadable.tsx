import { lazyLoad } from '../../../../utils/loadable'

export const DownloadIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
