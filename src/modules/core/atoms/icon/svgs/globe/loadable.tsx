import { lazyLoad } from '../../../../utils/loadable'

export const GlobeIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
