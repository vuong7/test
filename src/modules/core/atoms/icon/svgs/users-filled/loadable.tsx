import { lazyLoad } from '../../../../utils/loadable'

export const UsersFilledIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
