import React from 'react';
import { IconSvgProps } from '..';

const CellphoneIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M11.3334 1.33325L4.66671 1.33325C3.93033 1.33325 3.33337 1.93021 3.33337 2.66659L3.33337 13.3333C3.33337 14.0696 3.93033 14.6666 4.66671 14.6666H11.3334C12.0698 14.6666 12.6667 14.0696 12.6667 13.3333L12.6667 2.66659C12.6667 1.93021 12.0698 1.33325 11.3334 1.33325Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8 12H8.00667" stroke={color} stroke-width="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default CellphoneIcon;
