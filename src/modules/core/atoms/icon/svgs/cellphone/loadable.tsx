import { lazyLoad } from '../../../../utils/loadable'

export const CellphoneIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
