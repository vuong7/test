import { lazyLoad } from '../../../../utils/loadable'

export const SocialZaloRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
