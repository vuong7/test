import React from "react";
import { IconSvgProps } from "..";

const MicIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_4465_43610)">
        <path
          d="M8 1.1665C7.46957 1.1665 6.96086 1.37722 6.58579 1.75229C6.21071 2.12736 6 2.63607 6 3.1665V8.49984C6 9.03027 6.21071 9.53898 6.58579 9.91405C6.96086 10.2891 7.46957 10.4998 8 10.4998C8.53043 10.4998 9.03914 10.2891 9.41421 9.91405C9.78929 9.53898 10 9.03027 10 8.49984V3.1665C10 2.63607 9.78929 2.12736 9.41421 1.75229C9.03914 1.37722 8.53043 1.1665 8 1.1665V1.1665Z"
          stroke={color}
          strokeWidth="1.13"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12.6668 7.1665V8.49984C12.6668 9.73751 12.1752 10.9245 11.3 11.7997C10.4248 12.6748 9.23784 13.1665 8.00016 13.1665C6.76249 13.1665 5.5755 12.6748 4.70033 11.7997C3.82516 10.9245 3.3335 9.73751 3.3335 8.49984V7.1665"
          stroke={color}
          strokeWidth="1.13"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M8 13.1665V15.8332"
          stroke={color}
          strokeWidth="1.13"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M5.3335 15.8335H10.6668"
          stroke={color}
          strokeWidth="1.13"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0_4465_43610">
          <rect
            width="16"
            height="16"
            fill="white"
            transform="translate(0 0.5)"
          />
        </clipPath>
      </defs>
    </svg>
  );
};

export default MicIcon;
