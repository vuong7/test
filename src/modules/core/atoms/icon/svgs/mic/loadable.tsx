import { lazyLoad } from "../../../../utils/loadable";

export const MicIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
