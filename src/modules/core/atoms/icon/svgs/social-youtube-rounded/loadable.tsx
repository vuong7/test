import { lazyLoad } from '../../../../utils/loadable'

export const SocialYoutubeRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
