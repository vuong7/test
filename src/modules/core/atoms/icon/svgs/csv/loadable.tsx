import { lazyLoad } from "../../../../utils/loadable";

export const CsvIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
