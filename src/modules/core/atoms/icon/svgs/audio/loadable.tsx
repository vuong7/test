import { lazyLoad } from '../../../../utils/loadable'

export const AudioIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
