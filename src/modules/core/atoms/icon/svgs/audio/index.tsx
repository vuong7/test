import React from 'react';
import { IconSvgProps } from '..';

const AudioIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M4 9.5V14.5" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M20 9.5V14.5" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M8 6V18" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M16 6V18" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M12 3V21" stroke={color} strokeWidth="2" strokeLinecap="round" />
    </svg>
  );
};

export default AudioIcon;
