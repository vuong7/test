import { lazyLoad } from '../../../../utils/loadable'

export const SocialWeChatRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
