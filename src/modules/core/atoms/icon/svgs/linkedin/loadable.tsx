import { lazyLoad } from "../../../../utils/loadable";

export const LinkedinIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
