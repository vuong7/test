import { lazyLoad } from "../../../../utils/loadable";

export const ArrowRightFullIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
