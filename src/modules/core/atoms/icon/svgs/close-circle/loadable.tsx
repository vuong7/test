import { lazyLoad } from '../../../../utils/loadable'

export const CloseCircleIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
