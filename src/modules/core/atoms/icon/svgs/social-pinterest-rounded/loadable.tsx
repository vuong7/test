import { lazyLoad } from '../../../../utils/loadable'

export const SocialPinterestRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
