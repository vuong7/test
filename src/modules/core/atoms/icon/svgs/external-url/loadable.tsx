import { lazyLoad } from '../../../../utils/loadable'

export const ExternalUrlIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
