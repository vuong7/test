import React from 'react';
import { IconSvgProps } from '..';

const ExternalUrlIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M12 9.25553V13.2555C12 13.6092 11.8595 13.9483 11.6095 14.1983C11.3594 14.4484 11.0203 14.5889 10.6667 14.5889H3.33333C2.97971 14.5889 2.64057 14.4484 2.39052 14.1983C2.14048 13.9483 2 13.6092 2 13.2555V5.9222C2 5.56858 2.14048 5.22944 2.39052 4.97939C2.64057 4.72934 2.97971 4.58887 3.33333 4.58887H7.33333"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M10 2.58887H14V6.58887" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M6.66663 9.9222L14 2.58887" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default ExternalUrlIcon;
