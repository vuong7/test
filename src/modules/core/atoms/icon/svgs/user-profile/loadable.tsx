import { lazyLoad } from '../../../../utils/loadable'

export const UserProfileIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
