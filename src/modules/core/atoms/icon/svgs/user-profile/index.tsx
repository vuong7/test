import React from 'react';
import { IconSvgProps } from '..';

const UserProfileIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 37 37" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M30.8329 32.375V29.2917C30.8329 27.6562 30.1832 26.0877 29.0267 24.9312C27.8703 23.7747 26.3018 23.125 24.6663 23.125H12.3329C10.6974 23.125 9.12891 23.7747 7.97243 24.9312C6.81596 26.0877 6.16626 27.6562 6.16626 29.2917V32.375"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M18.5004 16.9583C21.9062 16.9583 24.6671 14.1974 24.6671 10.7917C24.6671 7.38591 21.9062 4.625 18.5004 4.625C15.0947 4.625 12.3337 7.38591 12.3337 10.7917C12.3337 14.1974 15.0947 16.9583 18.5004 16.9583Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default UserProfileIcon;
