import { lazyLoad } from '../../../../utils/loadable'

export const SocialLinkedInRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
