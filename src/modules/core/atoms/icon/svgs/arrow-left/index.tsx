import React from 'react';
import { IconSvgProps } from '..';

const ArrowLeftIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M10 12L6 8L10 4" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default ArrowLeftIcon;
