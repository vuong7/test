import { lazyLoad } from '../../../../utils/loadable'

export const ArrowLeftIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
