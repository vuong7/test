import { lazyLoad } from '../../../../utils/loadable'

export const ThumbsUpIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
