import { lazyLoad } from '../../../../utils/loadable'

export const ThumbsDownIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
