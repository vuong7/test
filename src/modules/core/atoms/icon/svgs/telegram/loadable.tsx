import { lazyLoad } from "../../../../utils/loadable";

export const TelegramIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
