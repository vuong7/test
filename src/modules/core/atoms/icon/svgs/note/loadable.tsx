import { lazyLoad } from "../../../../utils/loadable";

export const NoteIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
