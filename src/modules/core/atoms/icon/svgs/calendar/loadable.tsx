import { lazyLoad } from '../../../../utils/loadable'

export const CalendarIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
