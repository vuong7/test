import { lazyLoad } from '../../../../utils/loadable'

export const AlertTriangleIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
