import React from 'react';
import { IconSvgProps } from '..';

const AlertTriangleIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M6.86001 2.57298L1.21335 11.9996C1.09693 12.2013 1.03533 12.4298 1.03467 12.6627C1.03402 12.8955 1.09434 13.1244 1.20963 13.3267C1.32492 13.5289 1.49116 13.6975 1.69182 13.8155C1.89247 13.9336 2.12055 13.9971 2.35335 13.9996H13.6467C13.8795 13.9971 14.1076 13.9336 14.3082 13.8155C14.5089 13.6975 14.6751 13.5289 14.7904 13.3267C14.9057 13.1244 14.966 12.8955 14.9654 12.6627C14.9647 12.4298 14.9031 12.2013 14.7867 11.9996L9.14001 2.57298C9.02117 2.37705 8.85383 2.21506 8.65414 2.10264C8.45446 1.99021 8.22917 1.93115 8.00001 1.93115C7.77086 1.93115 7.54557 1.99021 7.34588 2.10264C7.1462 2.21506 6.97886 2.37705 6.86001 2.57298V2.57298Z"
        stroke={color}
        strokeWidth="1.5"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8 6V8.66667" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M8 11.3335H8.00667" stroke={color} strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default AlertTriangleIcon;
