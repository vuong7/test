import React from 'react';
import { IconSvgProps } from '..';

const AlignLeftIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M11.8333 6.66675H2.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M14.5 4H2.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M14.5 9.33325H2.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M11.8333 12H2.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default AlignLeftIcon;
