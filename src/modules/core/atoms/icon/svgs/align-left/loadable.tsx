import { lazyLoad } from '../../../../utils/loadable'

export const AlignLeftIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
