import { lazyLoad } from "../../../../utils/loadable";

export const PlayIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
