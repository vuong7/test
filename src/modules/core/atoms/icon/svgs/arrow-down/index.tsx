import React from 'react';
import { IconSvgProps } from '..';

const ArrowDownIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M4 7L8 11L12 7" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default ArrowDownIcon;
