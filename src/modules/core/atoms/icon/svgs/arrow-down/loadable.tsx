import { lazyLoad } from '../../../../utils/loadable'

export const ArrowDownIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
