import React from 'react';
import { IconSvgProps } from '..';

const BoxIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M14.0316 11.1667V5.83333C14.0313 5.59951 13.9706 5.36987 13.8554 5.16743C13.7403 4.965 13.5747 4.7969 13.3754 4.67999L8.78235 2.01333C8.58286 1.8963 8.35656 1.83469 8.1262 1.83469C7.89584 1.83469 7.66954 1.8963 7.47005 2.01333L2.87698 4.67999C2.67768 4.7969 2.51214 4.965 2.39698 5.16743C2.28181 5.36987 2.22106 5.59951 2.22083 5.83333V11.1667C2.22106 11.4005 2.28181 11.6301 2.39698 11.8326C2.51214 12.035 2.67768 12.2031 2.87698 12.32L7.47005 14.9867C7.66954 15.1037 7.89584 15.1653 8.1262 15.1653C8.35656 15.1653 8.58286 15.1037 8.78235 14.9867L13.3754 12.32C13.5747 12.2031 13.7403 12.035 13.8554 11.8326C13.9706 11.6301 14.0313 11.4005 14.0316 11.1667Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.39789 5.13989L8.1261 8.50656L13.8543 5.13989"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8.1261 15.22V8.50001" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default BoxIcon;
