import { lazyLoad } from '../../../../utils/loadable'

export const BoxIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
