import { lazyLoad } from '../../../../utils/loadable'

export const SocialFacebookRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
