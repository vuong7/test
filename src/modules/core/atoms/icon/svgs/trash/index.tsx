import React from 'react';
import { IconSvgProps } from '..';

const TrashIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2 4H3.33333H14" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M5.33325 3.99986V2.66652C5.33325 2.3129 5.47373 1.97376 5.72378 1.72372C5.97383 1.47367 6.31296 1.33319 6.66659 1.33319H9.33325C9.68687 1.33319 10.026 1.47367 10.2761 1.72372C10.5261 1.97376 10.6666 2.3129 10.6666 2.66652V3.99986M12.6666 3.99986V13.3332C12.6666 13.6868 12.5261 14.026 12.2761 14.276C12.026 14.526 11.6869 14.6665 11.3333 14.6665H4.66659C4.31296 14.6665 3.97382 14.526 3.72378 14.276C3.47373 14.026 3.33325 13.6868 3.33325 13.3332V3.99986H12.6666Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default TrashIcon;
