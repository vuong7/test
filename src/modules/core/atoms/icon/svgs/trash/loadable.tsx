import { lazyLoad } from '../../../../utils/loadable'

export const TrashIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
