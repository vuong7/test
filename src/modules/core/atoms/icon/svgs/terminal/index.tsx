import React from "react";
import { IconSvgProps } from "..";

const TerminalIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M2.5 4.25842L7 8.25842L2.5 12.2584"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M7.49951 12.2584H13.4995"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default TerminalIcon;
