import { lazyLoad } from "../../../../utils/loadable";

export const TerminalIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
