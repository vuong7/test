import { lazyLoad } from '../../../../utils/loadable'

export const LinkIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
