import { lazyLoad } from "../../../../utils/loadable";

export const UnknownIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
