import React from 'react';
import { IconSvgProps } from '..';

const UploadCloudIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M12 12V21" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M20.39 18.39C21.3653 17.8583 22.1358 17.0169 22.5798 15.9987C23.0239 14.9804 23.1162 13.8433 22.8422 12.7667C22.5682 11.6902 21.9435 10.7356 21.0666 10.0535C20.1898 9.37143 19.1108 9.00078 18 9.00004H16.74C16.4373 7.82929 15.8731 6.74238 15.0899 5.82104C14.3067 4.8997 13.3248 4.1679 12.2181 3.68065C11.1113 3.19341 9.90851 2.9634 8.70008 3.00793C7.49164 3.05245 6.30903 3.37035 5.24114 3.93771C4.17325 4.50508 3.24787 5.30715 2.53458 6.28363C1.82129 7.26011 1.33865 8.38558 1.12294 9.57544C0.90723 10.7653 0.964065 11.9886 1.28917 13.1533C1.61428 14.318 2.1992 15.3939 2.99996 16.3"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M16 16L12 12L8 16" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default UploadCloudIcon;
