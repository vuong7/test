import { lazyLoad } from '../../../../utils/loadable'

export const UploadCloudIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
