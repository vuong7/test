import { lazyLoad } from '../../../../utils/loadable'

export const SocialEmailRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
