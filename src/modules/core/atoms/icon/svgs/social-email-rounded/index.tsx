import React from 'react';
import { IconSvgProps } from '..';

const SocialEmailRoundedIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="16" cy="16" r="16" fill={color} />
      <path
        d="M9.33366 9.33331H22.667C23.5837 9.33331 24.3337 10.0833 24.3337 11V21C24.3337 21.9166 23.5837 22.6666 22.667 22.6666H9.33366C8.41699 22.6666 7.66699 21.9166 7.66699 21V11C7.66699 10.0833 8.41699 9.33331 9.33366 9.33331Z"
        stroke="#786C86"
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M24.3337 11L16.0003 16.8333L7.66699 11"
        stroke="#786C86"
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SocialEmailRoundedIcon;
