import { lazyLoad } from '../../../../utils/loadable'

export const SocialInstagramRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
