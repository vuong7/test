import { lazyLoad } from '../../../../utils/loadable'

export const CheckCircleIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
