import { lazyLoad } from "../../../../utils/loadable";

export const ZoomOutIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
