import React from "react";
import { IconSvgProps } from "..";

const ImageLightIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M12.6667 2H3.33333C2.59695 2 2 2.59695 2 3.33333V12.6667C2 13.403 2.59695 14 3.33333 14H12.6667C13.403 14 14 13.403 14 12.6667V3.33333C14 2.59695 13.403 2 12.6667 2Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M5.6665 6.6665C6.21879 6.6665 6.6665 6.21879 6.6665 5.6665C6.6665 5.11422 6.21879 4.6665 5.6665 4.6665C5.11422 4.6665 4.6665 5.11422 4.6665 5.6665C4.6665 6.21879 5.11422 6.6665 5.6665 6.6665Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.0002 9.99984L10.6668 6.6665L3.3335 13.9998"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ImageLightIcon;
