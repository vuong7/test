import { lazyLoad } from "../../../../utils/loadable";

export const ImageLightIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
