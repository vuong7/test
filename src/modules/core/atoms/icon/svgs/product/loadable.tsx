import { lazyLoad } from '../../../../utils/loadable'

export const ProductIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
