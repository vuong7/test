import React from 'react';
import { IconSvgProps } from '..';

const ProductIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M13.7793 11.1664V5.83311C13.779 5.59929 13.7183 5.36965 13.6031 5.16721C13.4879 4.96478 13.3224 4.79668 13.1231 4.67977L8.53003 2.01311C8.33054 1.89608 8.10424 1.83447 7.87388 1.83447C7.64352 1.83447 7.41722 1.89608 7.21773 2.01311L2.62466 4.67977C2.42536 4.79668 2.25982 4.96478 2.14466 5.16721C2.02949 5.36965 1.96874 5.59929 1.96851 5.83311V11.1664C1.96874 11.4003 2.02949 11.6299 2.14466 11.8323C2.25982 12.0348 2.42536 12.2029 2.62466 12.3198L7.21773 14.9864C7.41722 15.1035 7.64352 15.1651 7.87388 15.1651C8.10424 15.1651 8.33054 15.1035 8.53003 14.9864L13.1231 12.3198C13.3224 12.2029 13.4879 12.0348 13.6031 11.8323C13.7183 11.6299 13.779 11.4003 13.7793 11.1664Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2.14551 5.14014L7.87372 8.5068L13.6019 5.14014"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M7.87378 15.22V8.5" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default ProductIcon;
