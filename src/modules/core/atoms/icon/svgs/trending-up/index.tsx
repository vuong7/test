import React from 'react';
import { IconSvgProps } from '..';

const TrendingUpIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0_1045_49663)">
        <path
          d="M11.5 3L6.75 7.75L4.25 5.25L0.5 9"
          stroke={color}
          strokeWidth="1.125"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M8.5 3H11.5V6" stroke={color} strokeWidth="1.125" strokeLinecap="round" strokeLinejoin="round" />
      </g>
      <defs>
        <clipPath id="clip0_1045_49663">
          <rect width={size} height={size} fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default TrendingUpIcon;
