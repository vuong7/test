import { lazyLoad } from '../../../../utils/loadable'

export const TrendingUpIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
