import { lazyLoad } from '../../../../utils/loadable'

export const LockIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
