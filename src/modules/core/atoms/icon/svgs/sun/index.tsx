import React from 'react';
import { IconSvgProps } from '..';

const SunIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M7.99996 11.3334C9.84091 11.3334 11.3333 9.84103 11.3333 8.00008C11.3333 6.15913 9.84091 4.66675 7.99996 4.66675C6.15901 4.66675 4.66663 6.15913 4.66663 8.00008C4.66663 9.84103 6.15901 11.3334 7.99996 11.3334Z"
        stroke={color}
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8 0.666748V2.00008" stroke={color} strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M8 14V15.3333" stroke={color} strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M2.81335 2.81323L3.76002 3.7599"
        stroke={color}
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.24 12.24L13.1867 13.1867"
        stroke={color}
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M0.666626 8H1.99996" stroke={color} strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M14 8H15.3333" stroke={color} strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M2.81335 13.1867L3.76002 12.24"
        stroke={color}
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.24 3.7599L13.1867 2.81323"
        stroke={color}
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SunIcon;
