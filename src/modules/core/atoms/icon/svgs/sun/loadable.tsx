import { lazyLoad } from '../../../../utils/loadable'

export const SunIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
