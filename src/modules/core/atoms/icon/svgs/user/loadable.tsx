import { lazyLoad } from '../../../../utils/loadable'

export const UserIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
