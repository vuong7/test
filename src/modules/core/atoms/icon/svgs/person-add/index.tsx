import React from "react";
import { IconSvgProps } from "..";

const PersonAddIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.20892 12.801C11.4181 12.801 13.2089 11.0101 13.2089 8.80096C13.2089 6.59183 11.4181 4.80096 9.20892 4.80096C6.99978 4.80096 5.20892 6.59183 5.20892 8.80096C5.20892 11.0101 6.99978 12.801 9.20892 12.801Z"
        fill={color}
      />
      <path
        d="M9.20892 14.801C6.53892 14.801 1.20892 16.141 1.20892 18.801V19.801C1.20892 20.351 1.65892 20.801 2.20892 20.801H16.2089C16.7589 20.801 17.2089 20.351 17.2089 19.801V18.801C17.2089 16.141 11.8789 14.801 9.20892 14.801Z"
        fill={color}
      />
      <path
        d="M20.2089 10.801V7.80096H18.2089V10.801H15.2089V12.801H18.2089V15.801H20.2089V12.801H23.2089V10.801H20.2089Z"
        fill={color}
      />
    </svg>
  );
};

export default PersonAddIcon;
