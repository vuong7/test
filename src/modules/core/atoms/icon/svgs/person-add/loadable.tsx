import { lazyLoad } from "../../../../utils/loadable";

export const PersonAddIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
