import { lazyLoad } from '../../../../utils/loadable'

export const CompressedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
