import { lazyLoad } from '../../../../utils/loadable'

export const SocialSmsRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
