import React from 'react';
import { IconSvgProps } from '..';

const SocialSmsRoundedIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="16" cy="16" r="16" fill={color} />
      <path
        d="M23.5 18.5C23.5 18.942 23.3244 19.366 23.0118 19.6785C22.6993 19.9911 22.2754 20.1667 21.8333 20.1667H11.8333L8.5 23.5V10.1667C8.5 9.72464 8.67559 9.30072 8.98816 8.98816C9.30072 8.67559 9.72464 8.5 10.1667 8.5H21.8333C22.2754 8.5 22.6993 8.67559 23.0118 8.98816C23.3244 9.30072 23.5 9.72464 23.5 10.1667V18.5Z"
        stroke="#786C86"
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default SocialSmsRoundedIcon;
