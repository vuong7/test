import { lazyLoad } from '../../../../utils/loadable'

export const StarIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
