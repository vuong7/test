import React from 'react';
import { IconSvgProps } from '..';

const StartIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M8.00004 2.46294L9.61169 5.72796C9.68446 5.87538 9.82505 5.97761 9.98773 6.00139L13.5925 6.52828L10.9845 9.06847C10.8666 9.18334 10.8127 9.3489 10.8406 9.51117L11.456 13.0991L8.23277 11.4041C8.08708 11.3275 7.91301 11.3275 7.76732 11.4041L4.54413 13.0991L5.15951 9.51117C5.18734 9.3489 5.13351 9.18334 5.01557 9.06847L2.40758 6.52828L6.01236 6.00139C6.17503 5.97761 6.31562 5.87538 6.38839 5.72796L8.00004 2.46294Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default StartIcon;
