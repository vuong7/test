import React from 'react';
import { IconSvgProps } from '..';

const BarChartIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M11.9999 13.3334V6.66675"
        stroke={color}
        strokeWidth="1.125"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8 13.3334V2.66675" stroke={color} strokeWidth="1.125" strokeLinecap="round" strokeLinejoin="round" />
      <path
        d="M3.99988 13.3333V9.33325"
        stroke={color}
        strokeWidth="1.125"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default BarChartIcon;
