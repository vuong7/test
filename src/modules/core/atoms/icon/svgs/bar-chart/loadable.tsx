import { lazyLoad } from '../../../../utils/loadable'

export const BarChartIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
