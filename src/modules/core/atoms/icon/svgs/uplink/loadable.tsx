import { lazyLoad } from '../../../../utils/loadable'

export const UplinkIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
