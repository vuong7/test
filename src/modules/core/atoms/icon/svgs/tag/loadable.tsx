import { lazyLoad } from '../../../../utils/loadable'

export const TagIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
