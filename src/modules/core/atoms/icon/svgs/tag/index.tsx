import React from 'react';
import { IconSvgProps } from '..';

const TagIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M27.4527 17.8797L17.8927 27.4397C17.645 27.6877 17.3509 27.8843 17.0272 28.0185C16.7035 28.1527 16.3565 28.2218 16.006 28.2218C15.6556 28.2218 15.3086 28.1527 14.9848 28.0185C14.6611 27.8843 14.367 27.6877 14.1194 27.4397L2.66602 15.9997V2.66638H15.9994L27.4527 14.1197C27.9494 14.6193 28.2281 15.2952 28.2281 15.9997C28.2281 16.7042 27.9494 17.3801 27.4527 17.8797V17.8797Z"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M9.33398 9.33362H9.34732" stroke={color} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default TagIcon;
