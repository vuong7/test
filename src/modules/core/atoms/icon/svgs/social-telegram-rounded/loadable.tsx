import { lazyLoad } from '../../../../utils/loadable'

export const SocialTelegramRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
