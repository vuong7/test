import { lazyLoad } from "../../../../utils/loadable";

export const StarFilledIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
