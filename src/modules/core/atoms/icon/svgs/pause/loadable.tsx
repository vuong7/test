import { lazyLoad } from "../../../../utils/loadable";

export const PauseIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
