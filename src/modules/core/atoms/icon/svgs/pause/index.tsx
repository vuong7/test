import React from "react";
import { IconSvgProps } from "..";

const PauseIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 25 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.8896 4.38599H6.88965V20.386H10.8896V4.38599Z"
        fill={color}
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M19.3896 4.38599C19.3896 4.10984 19.1658 3.88599 18.8896 3.88599H14.8896C14.6135 3.88599 14.3896 4.10984 14.3896 4.38599V20.386C14.3896 20.6621 14.6135 20.886 14.8896 20.886H18.8896C19.1658 20.886 19.3896 20.6621 19.3896 20.386V4.38599Z"
        fill={color}
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default PauseIcon;
