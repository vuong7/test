import { lazyLoad } from '../../../../utils/loadable'

export const VideoIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
