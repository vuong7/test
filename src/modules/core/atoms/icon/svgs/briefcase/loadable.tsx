import { lazyLoad } from '../../../../utils/loadable'

export const BriefcaseIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
