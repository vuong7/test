import React from 'react';
import { IconSvgProps } from '..';

const BriefcaseIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M13.3334 5.25549L2.66671 5.25549C1.93033 5.25549 1.33337 5.85245 1.33337 6.58883L1.33337 13.2555C1.33337 13.9919 1.93033 14.5888 2.66671 14.5888L13.3334 14.5888C14.0698 14.5888 14.6667 13.9919 14.6667 13.2555V6.58883C14.6667 5.85245 14.0698 5.25549 13.3334 5.25549Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M10.6667 14.5889V3.9222C10.6667 3.56858 10.5262 3.22944 10.2762 2.97939C10.0261 2.72934 9.68699 2.58887 9.33337 2.58887L6.66671 2.58887C6.31309 2.58887 5.97395 2.72934 5.7239 2.97939C5.47385 3.22944 5.33337 3.56858 5.33337 3.9222L5.33337 14.5889"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default BriefcaseIcon;
