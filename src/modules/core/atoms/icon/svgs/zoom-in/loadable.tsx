import { lazyLoad } from "../../../../utils/loadable";

export const ZoomInIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
