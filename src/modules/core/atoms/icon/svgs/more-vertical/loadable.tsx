import { lazyLoad } from "../../../../utils/loadable";

export const MoreVerticalIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
