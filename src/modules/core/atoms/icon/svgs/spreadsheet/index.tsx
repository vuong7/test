import React from 'react';
import { IconSvgProps } from '..';

const SpreadsheetIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1" width="22" height="22" rx="1" stroke={color} strokeWidth="2" />
      <rect x="15" y="1" width="8" height="22" rx="1" stroke={color} strokeWidth="2" />
      <path d="M1.5 8H22.5" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M15 13H22" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path d="M15 18H22" stroke={color} strokeWidth="2" strokeLinecap="round" />
    </svg>
  );
};

export default SpreadsheetIcon;
