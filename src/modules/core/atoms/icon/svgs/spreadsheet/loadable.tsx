import { lazyLoad } from '../../../../utils/loadable'

export const SpreadsheetIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
