import { lazyLoad } from '../../../../utils/loadable'

export const AlertCircleIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
