import { lazyLoad } from '../../../../utils/loadable'

export const SocialLineRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
