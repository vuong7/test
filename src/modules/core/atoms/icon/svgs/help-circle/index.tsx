import React from 'react';
import { IconSvgProps } from '..';

const HelpCircleIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M7.99998 14.6667C11.6819 14.6667 14.6666 11.6819 14.6666 8.00004C14.6666 4.31814 11.6819 1.33337 7.99998 1.33337C4.31808 1.33337 1.33331 4.31814 1.33331 8.00004C1.33331 11.6819 4.31808 14.6667 7.99998 14.6667Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.06 5.99998C6.21673 5.55443 6.5261 5.17872 6.9333 4.9394C7.3405 4.70009 7.81926 4.61261 8.28478 4.69246C8.7503 4.77231 9.17254 5.01433 9.47672 5.37567C9.78089 5.737 9.94737 6.19433 9.94666 6.66665C9.94666 7.99998 7.94666 8.66665 7.94666 8.66665"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M8 11.3334H8.00667" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default HelpCircleIcon;
