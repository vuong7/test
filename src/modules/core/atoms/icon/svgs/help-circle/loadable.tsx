import { lazyLoad } from '../../../../utils/loadable'

export const HelpCircleIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
