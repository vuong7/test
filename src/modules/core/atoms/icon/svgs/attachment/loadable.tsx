import { lazyLoad } from '../../../../utils/loadable'

export const AttachmentIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
