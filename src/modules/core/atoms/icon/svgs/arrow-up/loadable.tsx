import { lazyLoad } from '../../../../utils/loadable'

export const ArrowUpIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
