import React from 'react';
import { IconSvgProps } from '..';

const DocumentIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clip-path="url(#clip0_1_8753)">
        <path
          d="M9.83329 2.20776H4.49996C4.14634 2.20776 3.8072 2.34824 3.55715 2.59829C3.3071 2.84834 3.16663 3.18748 3.16663 3.5411V14.2078C3.16663 14.5614 3.3071 14.9005 3.55715 15.1506C3.8072 15.4006 4.14634 15.5411 4.49996 15.5411H12.5C12.8536 15.5411 13.1927 15.4006 13.4428 15.1506C13.6928 14.9005 13.8333 14.5614 13.8333 14.2078V6.20776L9.83329 2.20776Z"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M11.1667 12.2078H5.83337" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
        <path d="M11.1667 9.54126H5.83337" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
        <path d="M7.16671 6.87451H6.50004H5.83337" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
        <path d="M9.83331 2.20776V6.20776H13.8333" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      </g>
      <defs>
        <clipPath id="clip0_1_8753">
          <rect width="16" height="16" fill="white" transform="translate(0.5 0.874512)" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default DocumentIcon;
