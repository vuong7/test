import { lazyLoad } from '../../../../utils/loadable'

export const DocumentIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
