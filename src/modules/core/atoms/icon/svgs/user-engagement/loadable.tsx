import { lazyLoad } from '../../../../utils/loadable'

export const UserEngagementIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
