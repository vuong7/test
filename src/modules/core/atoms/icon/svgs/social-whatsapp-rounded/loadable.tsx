import { lazyLoad } from '../../../../utils/loadable'

export const SocialWhatsAppRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
