import React from "react";
import { IconSvgProps } from "..";

const PrimaryIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.1875 0C3.76922 0 0.1875 3.58172 0.1875 8C0.1875 12.4183 3.76922 16 8.1875 16C12.6058 16 16.1875 12.4183 16.1875 8C16.1875 3.58172 12.6058 0 8.1875 0ZM7.1913 12.004H5.4873V4H9.23131C10.9713 4 11.9193 5.176 11.9193 6.58C11.9193 7.972 10.9593 9.136 9.23131 9.136H7.1913V12.004ZM9.00331 7.672C9.67531 7.672 10.1793 7.24 10.1793 6.568C10.1793 5.908 9.67531 5.464 9.00331 5.464H7.1913V7.672H9.00331Z"
        fill={color}
      />
    </svg>
  );
};

export default PrimaryIcon;
