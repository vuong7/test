import { lazyLoad } from "../../../../utils/loadable";

export const PrimaryIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
