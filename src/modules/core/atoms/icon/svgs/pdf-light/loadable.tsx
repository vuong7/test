import { lazyLoad } from "../../../../utils/loadable";

export const PdfLightIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
