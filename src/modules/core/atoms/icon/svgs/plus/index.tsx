import React from 'react';
import { IconSvgProps } from '..';

const PlusIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M8 3.33319V12.6665" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      <path d="M3.33337 8H12.6667" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default PlusIcon;
