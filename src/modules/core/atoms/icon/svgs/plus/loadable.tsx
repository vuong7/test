import { lazyLoad } from '../../../../utils/loadable'

export const PlusIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
