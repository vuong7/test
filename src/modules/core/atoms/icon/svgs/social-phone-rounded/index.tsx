import React from 'react';
import { IconSvgProps } from '..';

const SocialPhoneRoundedIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="16" cy="16" r="16" fill={color} />
      <path
        d="M20.167 7.66669H11.8337C10.9132 7.66669 10.167 8.41288 10.167 9.33335V22.6667C10.167 23.5872 10.9132 24.3334 11.8337 24.3334H20.167C21.0875 24.3334 21.8337 23.5872 21.8337 22.6667V9.33335C21.8337 8.41288 21.0875 7.66669 20.167 7.66669Z"
        stroke="#786C86"
        strokeWidth="1.25"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M16 21H16.0083" stroke="#786C86" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
    </svg>
  );
};

export default SocialPhoneRoundedIcon;
