import { lazyLoad } from '../../../../utils/loadable'

export const SocialPhoneRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
