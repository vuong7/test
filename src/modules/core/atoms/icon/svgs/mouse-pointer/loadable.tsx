import { lazyLoad } from '../../../../utils/loadable'

export const MousePointerIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
