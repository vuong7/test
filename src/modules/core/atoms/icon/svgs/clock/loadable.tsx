import { lazyLoad } from '../../../../utils/loadable'

export const ClockIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
