import React from 'react';
import { IconSvgProps } from '..';

const InternalUrlIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M7.00008 13.3333L6.99964 10.0004L3.66675 10"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2 14.6668L6.66667 10.3335"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.66667 14.6667H14C14.1768 14.6667 14.3464 14.5964 14.4714 14.4714C14.5964 14.3464 14.6667 14.1768 14.6667 14V2.66667C14.6667 2.48986 14.5964 2.32029 14.4714 2.19526C14.3464 2.07024 14.1768 2 14 2H2.66667C2.48986 2 2.32029 2.07024 2.19526 2.19526C2.07024 2.32029 2 2.48986 2 2.66667V7.33333"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default InternalUrlIcon;
