import { lazyLoad } from '../../../../utils/loadable'

export const InternalUrlIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
