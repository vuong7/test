/**
 *
 * Asynchronously loads the component for EyeIcon
 *
 */

import { lazyLoad } from '../../../../utils/loadable'

export const EyeIcon = lazyLoad(
  () => import('./index'),
  module => module.EyeIcon,
);
