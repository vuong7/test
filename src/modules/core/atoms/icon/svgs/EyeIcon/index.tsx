/**
 *
 * EyeIcon
 *
 */
import React, { memo } from 'react';
// import { selectBranding } from 'app/containers/Global/selectors';
// import { useSelector } from 'react-redux';

// interface Props {
//   active?: any;
//   sm?: any;
//   md?: any;
//   xs?: any;
//   color?: any;
//   disabled?: boolean;
// }

export const EyeIcon = memo(
  ({ active, sm, md, xs, color: defaultColor, disabled }: any) => {
    // const branding = useSelector(selectBranding);

    const color = '#4C2875'

    if (disabled) {
      return (
        <svg
          width="19"
          height="16"
          viewBox="0 0 19 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g opacity="0.5">
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M9.5 14.4726C5.4625 14.4726 1.9 11.0616 0 7.65057C1.9 4.32079 5.4625 1.4783 9.5 1.4783C13.5375 1.4783 17.1 4.32079 19 7.65057C17.2583 11.224 13.6958 14.4726 9.5 14.4726ZM0.95 7.65057C1.58333 8.70636 4.90833 13.6604 9.5 13.6604C14.4875 13.6604 17.4958 8.70636 18.1292 7.65057C17.4958 6.676 14.4875 2.29044 9.5 2.29044C4.67083 2.29044 1.58333 6.676 0.95 7.65057ZM9.5 3.91472C11.7167 3.91472 13.4583 5.70143 13.4583 7.97543C13.4583 10.2494 11.7167 12.0361 9.5 12.0361C7.28333 12.0361 5.54167 10.2494 5.54167 7.97543C5.54167 5.70143 7.28333 3.91472 9.5 3.91472ZM9.5 4.72687C11.2417 4.72687 12.6667 6.18872 12.6667 7.97543C12.6667 9.76214 11.2417 11.224 9.5 11.224C7.75833 11.224 6.33333 9.76214 6.33333 7.97543C6.33333 6.18872 7.75833 4.72687 9.5 4.72687Z"
              fill={defaultColor}
            />
            <line
              x1="3.71716"
              y1="14.8593"
              x2="17.8593"
              y2="0.717173"
              stroke={defaultColor}
              stroke-width="0.8"
            />
          </g>
        </svg>
      );
    }

    if (active) {
      return (
        <svg
          width="20"
          height="15"
          viewBox="0 0 20 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M9.5 14.3304C5.4625 14.3304 1.9 10.9194 0 7.50845C1.9 4.17867 5.4625 1.33618 9.5 1.33618C13.5375 1.33618 17.1 4.17867 19 7.50845C17.2583 11.0819 13.6958 14.3304 9.5 14.3304ZM0.95 7.50845C1.58333 8.56423 4.90833 13.5183 9.5 13.5183C14.4875 13.5183 17.4958 8.56423 18.1292 7.50845C17.4958 6.53388 14.4875 2.14832 9.5 2.14832C4.67083 2.14832 1.58333 6.53388 0.95 7.50845ZM9.5 3.7726C11.7167 3.7726 13.4583 5.55931 13.4583 7.83331C13.4583 10.1073 11.7167 11.894 9.5 11.894C7.28333 11.894 5.54167 10.1073 5.54167 7.83331C5.54167 5.55931 7.28333 3.7726 9.5 3.7726ZM9.5 4.58474C11.2417 4.58474 12.6667 6.0466 12.6667 7.83331C12.6667 9.62002 11.2417 11.0819 9.5 11.0819C7.75833 11.0819 6.33333 9.62002 6.33333 7.83331C6.33333 6.0466 7.75833 4.58474 9.5 4.58474Z"
            fill={color}
          />
          <line
            x1="2.71716"
            y1="14.7172"
            x2="16.8593"
            y2="0.575022"
            stroke="white"
            strokeWidth="0.8"
          />
          <line
            x1="4.71716"
            y1="14.7172"
            x2="18.8593"
            y2="0.575022"
            stroke="white"
            strokeWidth="0.8"
          />
          <line
            x1="3.71716"
            y1="14.7172"
            x2="17.8593"
            y2="0.575022"
            stroke={color}
            strokeWidth="0.8"
          />
          <line
            x1="3.71716"
            y1="14.7172"
            x2="17.8593"
            y2="0.575022"
            stroke={color}
            strokeWidth="0.8"
          />
        </svg>
      );
    }

    if (xs) {
      return (
        <svg
          width="12"
          height="9"
          viewBox="0 0 12 9"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6 8.50902C3.45 8.50902 1.2 6.35471 0 4.2004C1.2 2.09738 3.45 0.302124 6 0.302124C8.55 0.302124 10.8 2.09738 12 4.2004C10.9 6.4573 8.65 8.50902 6 8.50902ZM0.6 4.2004C1 4.86721 3.1 7.99609 6 7.99609C9.15 7.99609 11.05 4.86721 11.45 4.2004C11.05 3.58488 9.15 0.815055 6 0.815055C2.95 0.815055 1 3.58488 0.6 4.2004ZM6 1.84092C7.4 1.84092 8.5 2.96937 8.5 4.40557C8.5 5.84178 7.4 6.97023 6 6.97023C4.6 6.97023 3.5 5.84178 3.5 4.40557C3.5 2.96937 4.6 1.84092 6 1.84092ZM6 2.35385C7.1 2.35385 8 3.27712 8 4.40557C8 5.53402 7.1 6.4573 6 6.4573C4.9 6.4573 4 5.53402 4 4.40557C4 3.27712 4.9 2.35385 6 2.35385Z"
            fill={color}
          />
        </svg>
      );
    }

    if (md) {
      return (
        <svg
          width="18"
          height="13"
          viewBox="0 0 18 13"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M9 12.3103C5.175 12.3103 1.8 9.07888 0 5.84741C1.8 2.69289 5.175 0 9 0C12.825 0 16.2 2.69289 18 5.84741C16.35 9.23276 12.975 12.3103 9 12.3103ZM0.9 5.84741C1.5 6.84763 4.65 11.5409 9 11.5409C13.725 11.5409 16.575 6.84763 17.175 5.84741C16.575 4.92414 13.725 0.769397 9 0.769397C4.425 0.769397 1.5 4.92414 0.9 5.84741ZM9 2.30819C11.1 2.30819 12.75 4.00086 12.75 6.15517C12.75 8.30948 11.1 10.0022 9 10.0022C6.9 10.0022 5.25 8.30948 5.25 6.15517C5.25 4.00086 6.9 2.30819 9 2.30819ZM9 3.07759C10.65 3.07759 12 4.4625 12 6.15517C12 7.84785 10.65 9.23276 9 9.23276C7.35 9.23276 6 7.84785 6 6.15517C6 4.4625 7.35 3.07759 9 3.07759Z"
            fill={color}
            // fill-opacity="0.6"
          />
        </svg>
      );
    }

    if (sm) {
      return (
        <svg
          width="12"
          height="9"
          viewBox="0 0 12 9"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M6 8.50902C3.45 8.50902 1.2 6.35471 0 4.2004C1.2 2.09738 3.45 0.302124 6 0.302124C8.55 0.302124 10.8 2.09738 12 4.2004C10.9 6.4573 8.65 8.50902 6 8.50902ZM0.6 4.2004C1 4.86721 3.1 7.99609 6 7.99609C9.15 7.99609 11.05 4.86721 11.45 4.2004C11.05 3.58488 9.15 0.815055 6 0.815055C2.95 0.815055 1 3.58488 0.6 4.2004ZM6 1.84092C7.4 1.84092 8.5 2.96937 8.5 4.40557C8.5 5.84178 7.4 6.97023 6 6.97023C4.6 6.97023 3.5 5.84178 3.5 4.40557C3.5 2.96937 4.6 1.84092 6 1.84092ZM6 2.35385C7.1 2.35385 8 3.27712 8 4.40557C8 5.53402 7.1 6.4573 6 6.4573C4.9 6.4573 4 5.53402 4 4.40557C4 3.27712 4.9 2.35385 6 2.35385Z"
            fill={color}
          />
        </svg>
      );
    }

    return (
      <svg
        width="19"
        height="14"
        viewBox="0 0 19 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M9.5 13.3304C5.4625 13.3304 1.9 9.91944 0 6.50845C1.9 3.17867 5.4625 0.336182 9.5 0.336182C13.5375 0.336182 17.1 3.17867 19 6.50845C17.2583 10.0819 13.6958 13.3304 9.5 13.3304ZM0.95 6.50845C1.58333 7.56423 4.90833 12.5183 9.5 12.5183C14.4875 12.5183 17.4958 7.56423 18.1292 6.50845C17.4958 5.53388 14.4875 1.14832 9.5 1.14832C4.67083 1.14832 1.58333 5.53388 0.95 6.50845ZM9.5 2.7726C11.7167 2.7726 13.4583 4.55931 13.4583 6.83331C13.4583 9.1073 11.7167 10.894 9.5 10.894C7.28333 10.894 5.54167 9.1073 5.54167 6.83331C5.54167 4.55931 7.28333 2.7726 9.5 2.7726ZM9.5 3.58474C11.2417 3.58474 12.6667 5.0466 12.6667 6.83331C12.6667 8.62002 11.2417 10.0819 9.5 10.0819C7.75833 10.0819 6.33333 8.62002 6.33333 6.83331C6.33333 5.0466 7.75833 3.58474 9.5 3.58474Z"
          fill={color}
        />
      </svg>
    );
  },
);
