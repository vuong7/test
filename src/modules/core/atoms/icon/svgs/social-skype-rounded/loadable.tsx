import { lazyLoad } from '../../../../utils/loadable'

export const SocialSkypeRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
