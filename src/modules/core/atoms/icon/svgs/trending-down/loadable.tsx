import { lazyLoad } from '../../../../utils/loadable'

export const TrendingDownIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
