import React from 'react';
import { IconSvgProps } from '..';

const TrendingDownIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0_1045_49700)">
        <path
          d="M11.5 9L6.75 4.25L4.25 6.75L0.5 3"
          stroke={color}
          strokeWidth="1.125"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M8.5 9H11.5V6" stroke={color} strokeWidth="1.125" strokeLinecap="round" strokeLinejoin="round" />
      </g>
      <defs>
        <clipPath id="clip0_1045_49700">
          <rect width={size} height={size} fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default TrendingDownIcon;
