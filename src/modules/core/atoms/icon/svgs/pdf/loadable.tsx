import { lazyLoad } from '../../../../utils/loadable'

export const PdfIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
