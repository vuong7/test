import React from "react";
import { IconSvgProps } from "..";

const MaximizeIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10 2.5H14V6.5"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6 14.5H2V10.5"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.0002 2.5L9.3335 7.16667"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M2 14.5002L6.66667 9.8335"
        stroke={color}
        strokeWidth="1.13"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default MaximizeIcon;
