import { lazyLoad } from "../../../../utils/loadable";

export const MaximizeIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
