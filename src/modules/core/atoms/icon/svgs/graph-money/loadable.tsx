import { lazyLoad } from '../../../../utils/loadable'

export const GraphMoneyIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
