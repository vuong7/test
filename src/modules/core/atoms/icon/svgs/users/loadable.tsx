import { lazyLoad } from '../../../../utils/loadable'

export const UsersIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
