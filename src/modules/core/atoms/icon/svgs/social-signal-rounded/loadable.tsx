import { lazyLoad } from '../../../../utils/loadable'

export const SocialSignalRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
