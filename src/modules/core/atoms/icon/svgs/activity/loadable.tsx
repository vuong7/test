import { lazyLoad } from "../../../../utils/loadable";

export const ActivityIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
