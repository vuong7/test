import React from "react";
import { IconSvgProps } from "..";

const ActivityIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 17 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_1759_54446)">
        <path
          d="M15.4774 8.25842H12.8107L10.8107 14.2584L6.81071 2.25842L4.81071 8.25842H2.14404"
          stroke={color}
          strokeWidth="1.13"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0_1759_54446">
          <rect
            width="16"
            height="16"
            fill="white"
            transform="translate(0.811035 0.258423)"
          />
        </clipPath>
      </defs>
    </svg>
  );
};

export default ActivityIcon;
