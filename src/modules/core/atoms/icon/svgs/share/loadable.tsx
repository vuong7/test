import { lazyLoad } from '../../../../utils/loadable'

export const ShareIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
