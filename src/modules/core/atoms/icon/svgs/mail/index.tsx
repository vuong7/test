import React from 'react';
import { IconSvgProps } from '..';

const MailIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M2.66671 2.86304L13.3334 2.86304C14.0667 2.86304 14.6667 3.46304 14.6667 4.19637L14.6667 12.1964C14.6667 12.9297 14.0667 13.5297 13.3334 13.5297L2.66671 13.5297C1.93337 13.5297 1.33337 12.9297 1.33337 12.1964L1.33337 4.19637C1.33337 3.46304 1.93337 2.86304 2.66671 2.86304Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.6667 4.19629L8.00004 8.86296L1.33337 4.19629"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default MailIcon;
