import { lazyLoad } from '../../../../utils/loadable'

export const MailIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
