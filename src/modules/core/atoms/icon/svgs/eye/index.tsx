import React from 'react';
import { IconSvgProps } from '..';

const EyeIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M0.666626 7.99984C0.666626 7.99984 3.33329 2.6665 7.99996 2.6665C12.6666 2.6665 15.3333 7.99984 15.3333 7.99984C15.3333 7.99984 12.6666 13.3332 7.99996 13.3332C3.33329 13.3332 0.666626 7.99984 0.666626 7.99984Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8 10C9.10457 10 10 9.10457 10 8C10 6.89543 9.10457 6 8 6C6.89543 6 6 6.89543 6 8C6 9.10457 6.89543 10 8 10Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default EyeIcon;
