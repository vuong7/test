import { lazyLoad } from '../../../../utils/loadable'

export const EyeIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
