import React from 'react';
import { IconSvgProps } from '..';

const EditIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.3906 2.27618C10.9113 1.75548 11.7555 1.75548 12.2762 2.27618L13.0572 3.05723C13.5779 3.57793 13.5779 4.42215 13.0572 4.94285L5.7239 12.2762C5.47385 12.5262 5.13471 12.6667 4.78109 12.6667L2.66671 12.6667L2.66671 10.5523C2.66671 10.1987 2.80718 9.85957 3.05723 9.60952L10.3906 2.27618Z"
        stroke={color}
      />
      <path d="M1.33337 15.3334H10.6667" stroke={color} strokeLinecap="round" />
      <path d="M9.33337 3.33337L12 6.00004" stroke={color} />
    </svg>
  );
};

export default EditIcon;
