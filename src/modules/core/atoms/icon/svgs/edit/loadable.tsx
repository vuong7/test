import { lazyLoad } from '../../../../utils/loadable'

export const EditIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
