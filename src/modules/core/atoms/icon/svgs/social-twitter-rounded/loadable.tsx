import { lazyLoad } from '../../../../utils/loadable'

export const SocialTwitterRoundedIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
