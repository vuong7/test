import { lazyLoad } from "../../../../utils/loadable";

export const SlidersIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
