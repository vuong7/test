import { lazyLoad } from "../../../../utils/loadable";

export const MailRegularIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
