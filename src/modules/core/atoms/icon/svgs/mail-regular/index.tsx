import React from "react";
import { IconSvgProps } from "..";

const MailRegularIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M21.9934 6.18903C21.9934 5.11275 21.1128 4.23215 20.0365 4.23215H4.38144C3.30516 4.23215 2.42456 5.11275 2.42456 6.18903V17.9303C2.42456 19.0066 3.30516 19.8872 4.38144 19.8872H20.0365C21.1128 19.8872 21.9934 19.0066 21.9934 17.9303V6.18903ZM20.0365 6.18903L12.209 11.0715L4.38144 6.18903H20.0365ZM20.0365 17.9303H4.38144V8.14592L12.209 13.0381L20.0365 8.14592V17.9303Z"
        fill={color}
      />
    </svg>
  );
};

export default MailRegularIcon;
