import { lazyLoad } from "../../../../utils/loadable";

export const VideoLightIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
