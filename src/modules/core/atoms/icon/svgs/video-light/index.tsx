import React from "react";
import { IconSvgProps } from "..";

const VideoLightIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg
      width={size}
      height={size}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15.3332 4.6665L10.6665 7.99984L15.3332 11.3332V4.6665Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M9.33317 3.3335H1.99984C1.26346 3.3335 0.666504 3.93045 0.666504 4.66683V11.3335C0.666504 12.0699 1.26346 12.6668 1.99984 12.6668H9.33317C10.0696 12.6668 10.6665 12.0699 10.6665 11.3335V4.66683C10.6665 3.93045 10.0696 3.3335 9.33317 3.3335Z"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default VideoLightIcon;
