import { lazyLoad } from '../../../../utils/loadable'

export const CubeIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
