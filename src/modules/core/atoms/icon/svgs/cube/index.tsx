import React from 'react';
import { IconSvgProps } from '..';

const CubeIcon: React.FC<IconSvgProps> = ({ color, size }) => {
  return (
    <svg width={size} height={size} viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <g clip-path="url(#clip0_1_8759)">
        <path
          d="M14.5001 11.5412V6.20786C14.4998 5.97404 14.4381 5.7444 14.3211 5.54197C14.2041 5.33954 14.0359 5.17144 13.8334 5.05453L9.16673 2.38786C8.96404 2.27084 8.73411 2.20923 8.50006 2.20923C8.26601 2.20923 8.03609 2.27084 7.83339 2.38786L3.16673 5.05453C2.96424 5.17144 2.79605 5.33954 2.67904 5.54197C2.56202 5.7444 2.5003 5.97404 2.50006 6.20786V11.5412C2.5003 11.775 2.56202 12.0047 2.67904 12.2071C2.79605 12.4095 2.96424 12.5776 3.16673 12.6945L7.83339 15.3612C8.03609 15.4782 8.26601 15.5398 8.50006 15.5398C8.73411 15.5398 8.96404 15.4782 9.16673 15.3612L13.8334 12.6945C14.0359 12.5776 14.2041 12.4095 14.3211 12.2071C14.4381 12.0047 14.4998 11.775 14.5001 11.5412Z"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M2.67993 5.5144L8.49993 8.88107L14.3199 5.5144"
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path d="M8.49994 15.5945V8.87451" stroke={color} strokeLinecap="round" strokeLinejoin="round" />
      </g>
      <defs>
        <clipPath id="clip0_1_8759">
          <rect width="16" height="16" fill="white" transform="translate(0.5 0.874512)" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default CubeIcon;
