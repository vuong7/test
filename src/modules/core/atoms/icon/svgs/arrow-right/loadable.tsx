import { lazyLoad } from "../../../../utils/loadable";

export const ArrowRightIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
