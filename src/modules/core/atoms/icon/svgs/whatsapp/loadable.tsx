import { lazyLoad } from "../../../../utils/loadable";

export const WhatsappIcon = lazyLoad(
  () => import("./index"),
  (module) => module.default
);
