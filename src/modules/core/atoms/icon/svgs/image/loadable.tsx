import { lazyLoad } from '../../../../utils/loadable'

export const ImageIcon = lazyLoad(
  () => import('./index'),
  module => module.default,
);
