import styled from 'styled-components';

type Props = {
  customColor: string;
  removeMargin: boolean;
};
export const Wrapper = styled.div<Props>`
  grid-area: error;
  font-weight: normal;
  font-size: 12px;
  line-height: 100%;
  color: ${props => props.customColor};

  margin-top: ${props => (props.removeMargin ? '-8px' : '4px')};

  display: flex;
  align-items: center;

  svg {
    margin-right: 8px;
  }
`;
