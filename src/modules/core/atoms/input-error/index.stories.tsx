import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InputError } from './index'

export default {
  title: 'Design System/InputError',
  component: InputError,
} as ComponentMeta<typeof InputError>

const Template: Story= (args) => <InputError {...args}>Example of Input Error</InputError>

export const Default = Template.bind({});



