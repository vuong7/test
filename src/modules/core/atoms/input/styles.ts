import styled, { css } from "styled-components";

type Props = {
  isError: boolean;
  isWarning: boolean;
};
export const InputStyled = styled.input<Props>`
  width: 100%;
  border: 1px solid
    ${(props) => {
      if (props.isError) {
        if (props.isWarning) {
          return "#E47D00";
        }
        return "#E15050";
      }
      return "rgba(205, 208, 227, 0.6)";
    }};

  border-radius: 8px;
  padding: 12px;

  font-weight: normal;
  font-size: 14px;
  line-height: 100%;
  color: #574e61;
  height: 40px;
  box-sizing: border-box;

  &:focus {
    outline: none;
    ${(props) =>
      !props.isError &&
      css`
        border-color: ${(props) => props.theme.highlight};
      `}
  }

  ::placeholder {
    color: #786c86;
  }
`;
