import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Input } from './index'

export default {
  title: 'Design System/Input',
  component: Input,
} as ComponentMeta<typeof Input>

const Template: Story= (args) => <Input {...args} />

export const Default = Template.bind({});

export const Error = Template.bind({});
Error.args = { isError:true, isWarning:false }

export const Warning = Template.bind({});
Error.args = { isError:true, isWarning:true }


