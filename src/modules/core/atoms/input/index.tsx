import React from 'react';

import { InputStyled } from './styles';

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  isError?: boolean;
  isWarning?: boolean;
};

export const Input = React.forwardRef<HTMLInputElement, Props>(
  ({ isError = false, isWarning = false, ...props }, forwardedRef) => (
    <InputStyled isError={isError} isWarning={isWarning} ref={forwardedRef} {...props} />
  ),
);

export default Input;
