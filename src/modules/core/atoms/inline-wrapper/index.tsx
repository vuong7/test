import React from "react";

import { Wrapper } from "./styles";

export type InlineWrapperProps = {
  justifyContent?:
    | "flex-start"
    | "flex-end"
    | "center"
    | "space-around"
    | "space-evenly"
    | "space-between";
  alignItems?: "center" | "flex-start" | "flex-end" | "stretch" | "baseline";
};

export const InlineWrapper: React.FC<InlineWrapperProps> = ({
  children,
  justifyContent = "flex-start",
  alignItems = "center",
}) => (
  <Wrapper justifyContent={justifyContent} alignItems={alignItems}>
    {children}
  </Wrapper>
);

export default InlineWrapper;
