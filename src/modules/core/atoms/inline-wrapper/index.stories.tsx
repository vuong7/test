import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InlineWrapper } from './index'

export default {
  title: 'Design System/InlineWrapper',
  component: InlineWrapper,
} as ComponentMeta<typeof InlineWrapper>

const Template: Story= (args) => <InlineWrapper {...args}>Example of InlineWrapper</InlineWrapper>

export const Default = Template.bind({});


