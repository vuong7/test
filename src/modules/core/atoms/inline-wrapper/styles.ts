import styled from "styled-components";

type Props = {
  justifyContent?:
    | "flex-start"
    | "flex-end"
    | "center"
    | "space-around"
    | "space-evenly"
    | "space-between";
  alignItems?: "center" | "flex-start" | "flex-end" | "stretch" | "baseline";
};

export const Wrapper = styled.div<Props>`
  display: flex;
  align-items: ${(props) => props.alignItems};
  justify-content: ${(props) => props.justifyContent};
`;
