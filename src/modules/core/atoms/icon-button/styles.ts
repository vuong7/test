import styled from 'styled-components';

export const Button = styled.button`
  border: 0px;
  background: inherit;

  &:focus {
    box-shadow: 0 0 0 0.05em #fff, 0 0 0.15em 0.1em ${props => props.theme.highlight};
  }
`;
