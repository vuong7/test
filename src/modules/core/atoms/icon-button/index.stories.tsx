import React from 'react'
import { action } from '@storybook/addon-actions';
import { ComponentMeta, Story } from '@storybook/react';

import { IconButton } from './index'

export default {
  title: 'Design System/IconButton',
  component: IconButton,
} as ComponentMeta<typeof IconButton>

const Template: Story = (args) => <IconButton type='search' onClick={action('clicked')} {...args}>Example of IconButton text</IconButton>

export const Default = Template.bind({});

