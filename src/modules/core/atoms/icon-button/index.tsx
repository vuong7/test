import React from 'react';

import { Icon, IconProps } from '../icon/index';

import { Button } from './styles';

type Props = IconProps & {
  buttonType?: 'button' | 'submit' | 'reset' | undefined;
  onClick(): void;
};

export const IconButton: React.FC<Props> = ({ buttonType = 'button', onClick, ...iconProps }) => {
  return (
    <Button type={buttonType} onClick={onClick}>
      <Icon {...iconProps} />
    </Button>
  );
};

export default IconButton;
