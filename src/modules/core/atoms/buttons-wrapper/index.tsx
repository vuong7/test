import React from 'react';

import { Wrapper } from './styles';

export const ButtonsWrapper: React.FC = ({ children }) => {
  return <Wrapper>{children}</Wrapper>;
};

export default ButtonsWrapper;
