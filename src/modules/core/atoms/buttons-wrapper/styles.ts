import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  > * {
    margin-right: 16px;

    &:last-child {
      margin-right: 0px;
    }
  }
`;
