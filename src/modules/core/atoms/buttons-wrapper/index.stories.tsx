import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { ButtonsWrapper } from './index'

export default {
  title: 'Design System/ButtonsWrapper',
  component: ButtonsWrapper,
} as ComponentMeta<typeof ButtonsWrapper>

const Template: Story = (args) => <ButtonsWrapper {...args}>Example of ButtonsWrapper</ButtonsWrapper>

export const Default = Template.bind({});
