import React from 'react';
import 'flagpack/dist/flagpack.css';

// import { Container } from './styles';

type Props = {
  country: string;
};
export const Flag: React.FC<Props> = ({ country }) => {
  return <span className={`fp fp-rounded ${country.toLowerCase()}`}></span>;
};

export default Flag;
