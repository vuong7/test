import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Flag } from './index'

export default {
  title: 'Design System/Flag',
  component: Flag,
} as ComponentMeta<typeof Flag>

const Template: Story = (args) => <Flag country={'TH'} {...args}>Example of Flag</Flag>

export const Default = Template.bind({});
