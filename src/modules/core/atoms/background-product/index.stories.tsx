import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { BackgroundProduct } from './index'

export default {
  title: 'Design System/BackgroundProduct',
  component: BackgroundProduct,
} as ComponentMeta<typeof BackgroundProduct>

const Template: Story = (args) => <BackgroundProduct color={'#4C2875'} {...args}>Example of BackgroundProduct</BackgroundProduct>

export const Default = Template.bind({});
