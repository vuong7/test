import React, { useState } from 'react';
// import { useSelector } from 'react-redux';

import { EyeIcon } from '../icon/svgs/EyeIcon';
// import { selectBranding } from 'app/containers/Global/selectors';

import { Input } from '../input';

import { Icon, Wrapper } from './styles';

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  isError?: boolean;
};

export const InputPassword = React.forwardRef<HTMLInputElement, Props>(
  ({ isError = false, ...props }, forwardedRef) => {
    const [showPassword, setShowPassword] = useState(false);
    // const branding = useSelector(selectBranding);

    return (
      <Wrapper>
        <Input isError={isError} ref={forwardedRef} {...props} type={showPassword ? 'text' : 'password'} />
        <Icon onClick={() => setShowPassword(!showPassword)}>
          <EyeIcon active={showPassword} color='#4C2875' />
        </Icon>
      </Wrapper>
    );
  },
);

export default InputPassword;
