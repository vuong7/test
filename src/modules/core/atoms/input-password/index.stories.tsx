import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InputPassword } from './index'

export default {
  title: 'Design System/InputPassword',
  component: InputPassword,
} as ComponentMeta<typeof InputPassword>

const Template: Story= (args) => <InputPassword {...args} />

export const Default = Template.bind({});



