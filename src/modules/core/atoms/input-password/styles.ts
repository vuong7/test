import styled from 'styled-components';

export const Icon = styled.div`
  position: absolute;
  right: 10px;
  top: 50%;
  cursor: pointer;
  transform: translateY(-50%);
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  input {
    padding-right: 40px;
  }
`;
