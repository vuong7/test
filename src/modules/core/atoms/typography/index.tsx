import React, { FC } from "react";

import { ColorProps, useSelectColor } from "../../utils/colors";

import {
  Header1,
  Header2,
  Header3,
  Header4,
  Label,
  InputValidation,
  Titles,
  Paragraph,
  ParagraphArticles,
  ParagraphSemiBold,
  ParagraphSmall,
  ParagraphSmallStrong,
  SmallTitles,
  BigNumbers,
} from "./styles";

export type TypographyVariant =
  | "h1"
  | "h2"
  | "h3"
  | "h4"
  | "label"
  | "input-validation"
  | "titles"
  | "p"
  | "p-articles"
  | "p-semi-bold"
  | "p-small"
  | "p-small-strong"
  | "small-titles"
  | "big-numbers";

const mappedStyles: Record<TypographyVariant, any> = {
  h1: Header1,
  h2: Header2,
  h3: Header3,
  h4: Header4,
  label: Label,
  "input-validation": InputValidation,
  titles: Titles,
  p: Paragraph,
  "p-articles": ParagraphArticles,
  "p-semi-bold": ParagraphSemiBold,
  "p-small": ParagraphSmall,
  "p-small-strong": ParagraphSmallStrong,
  "small-titles": SmallTitles,
  "big-numbers": BigNumbers,
};

export type TypographyProps = ColorProps & {
  variant?: TypographyVariant;
};
export const Typography: FC<TypographyProps> = ({
  color = "customColor",
  customColor = "#574E61",
  variant = "p",
  children,
}) => {
  const hexColor = useSelectColor(color, customColor);
  const Component = mappedStyles[variant];
  return <Component color={hexColor}>{children}</Component>;
};

export default Typography;
