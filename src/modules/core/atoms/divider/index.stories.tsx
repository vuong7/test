import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Divider } from './index'

export default {
  title: 'Design System/Divider',
  component: Divider,
} as ComponentMeta<typeof Divider>

const Template: Story = (args) => <Divider {...args}>Example of Divider</Divider>

export const Default = Template.bind({});
