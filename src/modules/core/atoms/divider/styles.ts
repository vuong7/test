import styled from 'styled-components';

type LineProps = {
  marginVertical?: number;
  marginHorizontal?: number;
};
export const Line = styled.div<LineProps>`
  border-top: 1px solid rgba(205, 208, 227, 0.6);
  margin: ${props => `${props.marginVertical}px`} ${props => `${props.marginHorizontal}px`};
  /* opacity: 0.14; */
`;
