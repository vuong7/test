import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from ".";

export default {
  title: 'Design System/Button',
  component: Button,
};

export const Default = () => <Button onClick={action('clicked')}>Dummy - Default Button Example</Button>;
export const Primary = () => <Button primary onClick={action('clicked')}>Dummy - Primary Button Example</Button>;