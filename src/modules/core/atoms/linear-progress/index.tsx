import React from 'react';

import { Wrapper } from './styles';

export const LinearProgress: React.FC = () => {
  return (
    <Wrapper>
      <progress className="pure-material-progress-linear" />
    </Wrapper>
  );
};

export default LinearProgress;
