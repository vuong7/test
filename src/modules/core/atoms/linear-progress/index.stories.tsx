import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { LinearProgress } from './index'

export default {
  title: 'Design System/LinearProgress',
  component: LinearProgress,
} as ComponentMeta<typeof LinearProgress>

const Template: Story= (args) => <LinearProgress {...args} />

export const Default = Template.bind({});



