import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { Spacer } from './index'

export default {
  title: 'Design System/Spacer',
  component: Spacer,
} as ComponentMeta<typeof Spacer>

const Template: Story= (args) => <Spacer {...args}/>

export const Default = Template.bind({});



