import styled from 'styled-components';

export const Checkmark = styled.span`
  display: block;
  width: 20px;
  height: 20px;
  border-radius: 4px;
  border: 1px solid rgba(132, 118, 142, 0.5);
  opacity: 0.8;
`;

export const Input = styled.input`
  opacity: 0;
  width: 0px;
  height: 0px;
  float: right;

  &:focus {
    border-color: ${props => props.theme.highlight};
  }

  &:focus + span:last-child {
    box-shadow: 0 0 0 0.05em #fff, 0 0 0.15em 0.1em ${props => props.theme.highlight};
  }

  &:checked + span:last-child {
    background-color: ${props => props.theme?.primary};
    background-image: url("data:image/svg+xml,%0A%3Csvg width='11' height='10' viewBox='0 0 11 10' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M10.6216 1.4375L4.28896 9.40625C4.23888 9.45536 4.15693 9.5625 4.05678 9.5625C3.95207 9.5625 3.88378 9.49107 3.8246 9.43304C3.76541 9.375 0.232624 6.04464 0.232624 6.04464L0.164336 5.97768C0.137021 5.9375 0.114258 5.88839 0.114258 5.83482C0.114258 5.78125 0.137021 5.73214 0.164336 5.69196C0.182546 5.67411 0.196204 5.66071 0.214414 5.63839C0.564961 5.27679 1.27516 4.54464 1.32069 4.5C1.37987 4.44196 1.42995 4.36607 1.53921 4.36607C1.65302 4.36607 1.72586 4.45982 1.7805 4.51339C1.83513 4.56696 3.82915 6.44643 3.82915 6.44643L8.89615 0.0625C8.94168 0.0267857 8.99631 0 9.05549 0C9.11467 0 9.1693 0.0223214 9.21483 0.0580357L10.6079 1.13393C10.6443 1.17857 10.6671 1.23214 10.6671 1.29018C10.6716 1.34821 10.6489 1.39732 10.6216 1.4375Z' fill='white'/%3E%3C/svg%3E%0A");
    background-repeat: no-repeat;
    background-position: center center;
    border: 0px;
    opacity: 1;
  }
`;

export const Wrapper = styled.label`
  margin: 0;
  grid-area: input;
`;
