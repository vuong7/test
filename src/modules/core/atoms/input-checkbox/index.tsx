import React from 'react';

import { Checkmark, Input, Wrapper } from './styles';

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  checked?: boolean;
};
export const InputCheckbox = React.forwardRef<HTMLInputElement, Props>((props, forwardedRef) => (
  <Wrapper>
    <Input type="checkbox" {...props} ref={forwardedRef} />
    <Checkmark />
  </Wrapper>
));

export default InputCheckbox;
