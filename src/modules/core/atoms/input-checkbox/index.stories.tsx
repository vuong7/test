import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InputCheckbox } from './index'

export default {
  title: 'Design System/InputCheckbox',
  component: InputCheckbox,
} as ComponentMeta<typeof InputCheckbox>

const Template: Story= (args) => <InputCheckbox {...args} />

export const Default = Template.bind({});



