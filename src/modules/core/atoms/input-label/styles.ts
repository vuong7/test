import styled from "styled-components";

export const Wrapper = styled.div`
  font-weight: normal;
  font-size: 16px;
  line-height: 145%;
  color: #574E61;
`