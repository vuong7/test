import React from 'react';

import { Typography } from '../typography/index';

export const InputLabel: React.FC = ({ children }) => <Typography variant="label">{children}</Typography>;

export default InputLabel;
