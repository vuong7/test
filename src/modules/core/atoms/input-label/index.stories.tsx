import React from 'react'
import { ComponentMeta, Story } from '@storybook/react';

import { InputLabel } from './index'

export default {
  title: 'Design System/InputLabel',
  component: InputLabel,
} as ComponentMeta<typeof InputLabel>

const Template: Story= (args) => <InputLabel {...args}>Example of Input Label</InputLabel>

export const Default = Template.bind({});



