import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MiniPreview, MiniPreviewProps } from "..";

export default {
  title: "Uplink/MiniPreview",
  component: MiniPreview,
} as ComponentMeta<typeof MiniPreview>;

const Template: Story<MiniPreviewProps> = (args) => (
  <MiniPreview {...args}>Example</MiniPreview>
);

export const Image = Template.bind({});
Image.args = {
  extension: "jpeg",
  thumbnailUrl: "http://cdn.statcdn.com/Infographic/images/normal/26812.jpeg",
};

export const Video = Template.bind({});
Video.args = {
  extension: "mp4",
  thumbnailUrl:
    "https://arpideas.com/wp-content/uploads/2020/06/Office-365_d315f31a6d119c1eaad0703e44296e1d.png",
};

export const Spreadsheet = Template.bind({});
Spreadsheet.args = {
  extension: "xls",
};
