import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { UplinkVideo, UplinkVideoProps } from "..";

export default {
  title: "Uplink/UplinkVideo",
  component: UplinkVideo,
} as ComponentMeta<typeof UplinkVideo>;

const Template: Story<UplinkVideoProps> = (args) => (
  <UplinkVideo {...args}>Example</UplinkVideo>
);

export const Default = Template.bind({});
Default.args = {
  url: "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4",
  onDuration: (duration: number) => console.log(duration),
  onProgress: (total: number, last: number) =>
    console.log(`total: ${total} last: ${last}`),
};
