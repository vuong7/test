import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { FullPreviewFailed, FullPreviewFailedProps } from "..";

export default {
  title: "Uplink/FullPreviewFailed",
  component: FullPreviewFailed,
} as ComponentMeta<typeof FullPreviewFailed>;

const Template: Story<FullPreviewFailedProps> = (args) => (
  <FullPreviewFailed {...args}>Example</FullPreviewFailed>
);

export const Default = Template.bind({});
Default.args = {};
