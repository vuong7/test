import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { UplinkAudio, UplinkAudioProps } from "..";

export default {
  title: "Uplink/UplinkAudio",
  component: UplinkAudio,
} as ComponentMeta<typeof UplinkAudio>;

const Template: Story<UplinkAudioProps> = (args) => (
  <UplinkAudio {...args}>Default</UplinkAudio>
);

export const Default = Template.bind({});
Default.args = {
  url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3",
  onListen: (played: number, total: number) => console.log(`played =====> ${played} total =====> ${total}`),
};
