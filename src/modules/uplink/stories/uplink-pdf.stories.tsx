import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { UplinkPdf, UplinkPdfProps } from "..";

export default {
  title: "Uplink/UplinkPdf",
  component: UplinkPdf,
} as ComponentMeta<typeof UplinkPdf>;

const Template: Story<UplinkPdfProps> = (args) => (
  <UplinkPdf {...args}>Example</UplinkPdf>
);

export const Default = Template.bind({});
Default.args = {
  url: "https://jobhop-stg-upload.s3.amazonaws.com/67377a354791ae62fe31bae37a8eadb5.pdf?AWSAccessKeyId=ASIAWHSPCVVYQITTYLV4&Signature=DRxRaRBH%2FFERQ2G%2BGd2RSNp3arA%3D&x-amz-security-token=IQoJb3JpZ2luX2VjEFEaDmFwLXNvdXRoZWFzdC0xIkcwRQIhAK57P7v1ehGEmNWM7E5ZuBqkx9B62RbhHqheb%2Fe4b%2Fw6AiAurBGi4KTOVkdJ7X3qq0hzJCp2LmiKMJKT4pvqZhOCUyqdAghKEAEaDDQyODU4ODgzODI1NyIMMNkxQSwlfyfK0HhtKvoBqeTB9dWYlW814eZ0rmpPL1Sy0pM9szp4mINhNndg9Qzkwh5vDw2uRGWbZ9EzeHOZYXs4QtKy%2B5pesW%2BYk0HD24pvXKiSRuURiFG7g%2FIG6pb7J0bPV82O%2BxJxWL5ifXLARz5OOyCc7Kc2DMg5jDINdztRa33ukVMbMOLC1YdunDT84mkgEbJE0CRdhXwASlj2chHHpVQ3Cfublkyly%2F52%2F5FBWjaXLJjdBA9skIIeacy1uP5%2F%2BnMIWO4eyOnSUY4iY9wEsxCSFLGHzZv%2BUSW3nIpD2LFJLmuydJX%2BmUTssy2nsbogmbh%2FiO2ugGiynSecbTWHG7atxLMtsTDxut6UBjqaASg%2BKLQyVinJB%2FxBMdsQZ3XkC62fURppuv11%2F0R16y6WEyt8T7S4xtP%2BGv8HZFnnt5oInv%2FEDMLJzhfZjwSHFRJISb0ex68XP%2FslHojEeLOWa5nHTUjc4o6QLrcHoSM1Xlkt3Qm2jRrBKxiGup33gOnhxMz7gRxQ70WdlN8KP%2BXYE2dabPCC2oMqBe7%2F2GTHV0j5qly29DoUYgY%3D&Expires=1654107308",
  onPdfLoad: (totalPages: number) => console.log(totalPages),
  onChangePage: (pagesRead: number[]) => console.log(pagesRead),
};
