import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AcceptLegalNotices, AcceptLegalNoticesProps } from "..";

export default {
  title: "Uplink/AcceptLegalNotices",
  component: AcceptLegalNotices,
} as ComponentMeta<typeof AcceptLegalNotices>;

const Template: Story<AcceptLegalNoticesProps> = (args) => (
  <AcceptLegalNotices {...args}>Example</AcceptLegalNotices>
);

export const Default = Template.bind({});
Default.args = {
  onOpenLegalNotices: () => console.log("legal notices opened"),
  onSubmit: () => console.log("accepted"),
};
