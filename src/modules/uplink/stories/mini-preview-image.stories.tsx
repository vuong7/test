import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MiniPreviewImage, MiniPreviewImageProps } from "..";

export default {
  title: "Uplink/MiniPreviewImage",
  component: MiniPreviewImage,
} as ComponentMeta<typeof MiniPreviewImage>;

const Template: Story<MiniPreviewImageProps> = (args) => (
  <MiniPreviewImage {...args}>Example</MiniPreviewImage>
);

export const Default = Template.bind({});
Default.args = {
  url: "http://cdn.statcdn.com/Infographic/images/normal/26812.jpeg",
};
