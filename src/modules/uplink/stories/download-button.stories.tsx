import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { DownloadButton, DownloadButtonProps } from "..";

export default {
  title: "Uplink/DownloadButton",
  component: DownloadButton,
} as ComponentMeta<typeof DownloadButton>;

const Template: Story<DownloadButtonProps> = (args) => (
  <DownloadButton {...args}>Example</DownloadButton>
);

export const Default = Template.bind({});
Default.args = {
  onClickDownload: () => console.log("download"),
};
