import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AttachmentTitle, AttachmentTitleProps } from "..";

export default {
  title: "Uplink/AttachmentTitle",
  component: AttachmentTitle,
} as ComponentMeta<typeof AttachmentTitle>;

const Template: Story<AttachmentTitleProps> = (args) => (
  <AttachmentTitle {...args}>Example</AttachmentTitle>
);

export const Default = Template.bind({});
Default.args = {
  fileName: "Our-company-introduction-Spring-2022.mp4",
  size: "54.3MB",
};

export const ShortTitle = Template.bind({});
ShortTitle.args = {
  fileName: "Introduction.jpg",
  size: "54.3MB",
};

export const BigTitle = Template.bind({});
BigTitle.args = {
  fileName:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore officiis debitis expedita a officia nobis reprehenderit facere accusamus, quasi omnis recusandae unde animi, dolor corporis delectus aperiam exercitationem. Nam..mp4",
  size: "54.3MB",
};
