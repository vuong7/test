import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MultipleAttachmentsCarousel, MultipleAttachmentsCarouselProps } from "../molecules";

export default {
  title: "Uplink/MultipleAttachmentsCarousel",
  component: MultipleAttachmentsCarousel,
} as ComponentMeta<typeof MultipleAttachmentsCarousel>;

const Template: Story<MultipleAttachmentsCarouselProps> = (args) => (
  <MultipleAttachmentsCarousel {...args}>Example</MultipleAttachmentsCarousel>
);

export const Default = Template.bind({});
Default.args = {
  data: [
    { image: '/images/attachment.png', title: 'Our company introduction - Spring 2022' },
    { image: '/images/attachment.png', title: 'Our company introduction - Spring 2022' }
  ],
  onItemClicked: (item: any) => console.log('onItemClicked =====>', item)
};
