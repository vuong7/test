import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { PreviewHeader, PreviewHeaderProps } from "..";

export default {
  title: "Uplink/PreviewHeader",
  component: PreviewHeader,
} as ComponentMeta<typeof PreviewHeader>;

const Template: Story<PreviewHeaderProps> = (args) => (
  <PreviewHeader {...args}>Example</PreviewHeader>
);

export const Default = Template.bind({});
Default.args = {
  fileName: "Our-company-introduction-Spring-2022.mp4",
  size: "54.3MB",
  onClickDownload: () => console.log("download"),
};

export const BigTitle = Template.bind({});
BigTitle.args = {
  fileName:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore officiis expedita a officia nobis reprehenderit facere accusamus, quasi.mp4",
  size: "54.3MB",
  onClickDownload: () => console.log("download"),
};

export const OverflowTitle = Template.bind({});
OverflowTitle.args = {
  fileName:
    "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti tempore officiis debitis expedita a officia nobis reprehenderit facere accusamus, quasi omnis recusandae unde animi, dolor corporis delectus aperiam exercitationem. Nam..mp4",
  size: "54.3MB",
  onClickDownload: () => console.log("download"),
};
