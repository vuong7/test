import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Author } from "..";

import { AuthorProps } from "../../../";

export default {
  title: "Uplink/Author",
  component: Author,
} as ComponentMeta<typeof Author>;

const Template: Story<AuthorProps> = (args) => (
  <Author {...args}>Example of Nav Tags text</Author>
);

export const Default = Template.bind({});
Default.args = {
  avatarUrl: "https://www.w3schools.com/howto/img_avatar.png",
  name: "Dominic Gamble",
};

export const MoreText = Template.bind({});
MoreText.args = {
  name: "Dominic Gamble Albert White",
};
