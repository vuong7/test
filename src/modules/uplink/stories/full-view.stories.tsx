import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { FullView, FullViewProps } from "..";

export default {
  title: "Uplink/FullView",
  component: FullView,
} as ComponentMeta<typeof FullView>;

const Template: Story<FullViewProps> = (args) => (
  <FullView {...args}>Example</FullView>
);

export const Image = Template.bind({});
Image.args = {
  url: "http://cdn.statcdn.com/Infographic/images/normal/26812.jpeg",
  fileName: "introduction-Spring-2022.jpg",
  size: "1.1MB",
  onCloseFullView: () => console.log("closed"),
};

export const Video = Template.bind({});
Video.args = {
  url: "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4",
  fileName: "introduction-Spring-2022.mp4",
  size: "55.5MB",
  onCloseFullView: () => console.log("closed"),
  onDuration: (duration: number) => console.log(duration),
  onProgress: (total: number, last: number) => console.log(`total =====> ${total} last =====> ${last}`),
};

export const Audio = Template.bind({});
Audio.args = {
  url: "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-3.mp3",
  fileName: "SoundHelix-Song-3.mp3",
  size: "33.3MB",
  onCloseFullView: () => console.log("closed"),
  onListen: (played: number, total: number) => console.log(`played =====> ${played} total =====> ${total}`),
};

export const Pdf = Template.bind({});
Pdf.args = {
  url: "https://jobhop-stg-upload.s3.amazonaws.com/bc14f5614b4b29c69f0407f89be400fc.pdf?AWSAccessKeyId=ASIAWHSPCVVYUMRAPANV&Signature=CNPh5fV9ptl%2Bb%2Byzw7cNxCvQVb4%3D&x-amz-security-token=IQoJb3JpZ2luX2VjEB4aDmFwLXNvdXRoZWFzdC0xIkcwRQIhAKSP5StYYZrcKXeITE5YuuoKixXUMM2qhRWsl%2BK3rd4CAiBU9vVmZOEBR4HPKVU73RM0LX1WFCvDTlcKV493cuEICyqmAgj3%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAEaDDQyODU4ODgzODI1NyIMyqE%2BnXv7ZR0tUUNRKvoBOgEHD%2FfN1e0cZQhk5IYdr%2BcimikZUyCReFtKeFAdWnZhPOTBTLvvGwi0eoO3I7IVua5YKN18%2BZmFG6h8elP0v6tWAaoowSRSBrx4m997xqPnhldV6Oy8h4w%2Flky7h6vI1S3UsuQZWgnXLTX7Po3s6Uo2i4rtE9f%2BTmuKoaaQ69bCWRzjC2zv2YWdeKkq8GxkKfmu8SKtYgujTYZsrtMN92VBaJRFH%2BoBEc9ygz5ni1HplqiLGaZrjd28sfGcu4XAwr%2B%2FbLz3cFD0gkeDOrilCuz%2F6pz%2BjOV4tdPPCuMZM9nOtotJ9X%2BJ%2FJGn%2FuTidvPx1BhqJGEO0NG0JTDr7ZqUBjqaAfInhhtHXpmaIYYGB%2BgvkW%2Bane7FT%2Fad2FphdeDj%2B2lbcOXue7ZPDtzj1sKt6YCckyqw2hLOkQb4I1ks9Hqb6rm2q1DGg3zUnt5vQ9ahKmv9NlFeHkdnKOzpI1HajGH%2BGIYAhwE93Sfy%2FUdVO9IIPz1ulnNE16ylsToYmae3uhACyddl7lRvzryIG%2FUwomk4gm8RmYeSN2B44oA%3D&Expires=1652999423",
  fileName: "introduction-Spring-2022.pdf",
  size: "22.2MB",
  onCloseFullView: () => console.log("closed"),
  onPdfLoad: (totalPages: number) => console.log(totalPages),
  onChangePage: (pagesRead: number[]) => console.log(pagesRead),
};

export const File = Template.bind({});
File.args = {
  url: "",
  fileName: "introduction-Spring-2022.xlsx",
  size: "11.1MB",
  onCloseFullView: () => console.log("closed"),
};
