import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { TapConnectButton, TapConnectButtonProps } from "../molecules";

export default {
  title: "Uplink/TapConnectButton",
  component: TapConnectButton,
} as ComponentMeta<typeof TapConnectButton>;

const Template: Story<TapConnectButtonProps> = (args) => (
  <TapConnectButton {...args}>Example</TapConnectButton>
);

export const Default = Template.bind({});
Default.args = {
  show: false,
  expanded: false,
  buttons: [
    { icon: 'whatsapp', label: 'Send Whatsapp Message' },
    { icon: 'telegram', label: 'Send Telegram Message' },
    { icon: 'linkedin', label: 'Connect on LinkedIn' },
    { icon: 'mail', label: 'Send email' },
    { icon: 'person-add', label: 'Add to Contacts' },
  ],
  onTapConnect: () => console.log('onTapConnect =====>'),
  onTapButton: (label: string) => console.log('onTapButton =====>', label)
};
