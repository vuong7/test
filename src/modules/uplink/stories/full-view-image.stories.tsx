import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { FullViewImage, FullViewImageProps } from "..";

export default {
  title: "Uplink/FullViewImage",
  component: FullViewImage,
} as ComponentMeta<typeof FullViewImage>;

const Template: Story<FullViewImageProps> = (args) => (
  <FullViewImage {...args}>Example</FullViewImage>
);

export const Default = Template.bind({});
Default.args = {
  url: "http://cdn.statcdn.com/Infographic/images/normal/26812.jpeg",
};
