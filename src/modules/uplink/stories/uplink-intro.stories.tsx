import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { UplinkIntro, UplinkIntroProps } from "..";

export default {
  title: "Uplink/UplinkIntro",
  component: UplinkIntro,
} as ComponentMeta<typeof UplinkIntro>;

const Template: Story<UplinkIntroProps> = (args) => (
  <UplinkIntro {...args}>Example</UplinkIntro>
);

export const Default = Template.bind({});
Default.args = {
  name: "Test User",
  fileName: "introduction-Spring-2022.mp4",
  size: "54.3MB",
  onClickDownload: () => console.log("download"),
  thumbnailUrl:
    "https://arpideas.com/wp-content/uploads/2020/06/Office-365_d315f31a6d119c1eaad0703e44296e1d.png",
  onOpenLegalNotices: () => console.log("open legal notices"),
  onClickViewAttachment: () => console.log("accepted - view attachment"),
};
