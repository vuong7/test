import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MiniPreviewFailed, MiniPreviewFailedProps } from "..";

export default {
  title: "Uplink/MiniPreviewFailed",
  component: MiniPreviewFailed,
} as ComponentMeta<typeof MiniPreviewFailed>;

const Template: Story<MiniPreviewFailedProps> = (args) => (
  <MiniPreviewFailed {...args}>Example</MiniPreviewFailed>
);

export const Default = Template.bind({});
Default.args = {};
