import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MiniPreviewFiles, MiniPreviewFilesProps } from "..";

export default {
  title: "Uplink/MiniPreviewFiles",
  component: MiniPreviewFiles,
} as ComponentMeta<typeof MiniPreviewFiles>;

const Template: Story<MiniPreviewFilesProps> = (args) => (
  <MiniPreviewFiles {...args}>Example</MiniPreviewFiles>
);

export const Default = Template.bind({});
Default.args = {
  extension: "xlsx",
};
