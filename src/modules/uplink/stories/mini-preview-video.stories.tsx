import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MiniPreviewVideo, MiniPreviewVideoProps } from "..";

export default {
  title: "Uplink/MiniPreviewVideo",
  component: MiniPreviewVideo,
} as ComponentMeta<typeof MiniPreviewVideo>;

const Template: Story<MiniPreviewVideoProps> = (args) => (
  <MiniPreviewVideo {...args}>Example</MiniPreviewVideo>
);

export const Default = Template.bind({});
Default.args = {
  thumbnailUrl:
    "https://arpideas.com/wp-content/uploads/2020/06/Office-365_d315f31a6d119c1eaad0703e44296e1d.png",
};
