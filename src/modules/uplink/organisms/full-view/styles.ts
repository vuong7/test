import styled from "styled-components";


export const Container = styled.div`
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  position: relative;

  display: flex;
  flex-direction: column;
  
  @media only screen and (max-width: 768px) {
    background: #000000;
    flex-direction: column-reverse;
  }
`;

export const CloseContainer = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  z-index: 2;

  &:hover {
    cursor: pointer;
  }

  @media only screen and (min-width: 768px) {
    display: none;
  }
`;

export const ContentWrapper = styled.div`
  flex: 1;
  overflow: hidden;
`;
