import React, { FC, useCallback } from "react";

import { Container, CloseContainer, ContentWrapper } from "./styles";

import {
  PreviewHeader,
  UplinkVideo,
  UplinkAudio,
  UplinkPdf,
  FullViewImage,
  MiniPreviewFiles,
} from "../../";
import { Icon } from "../../..";
import { getFileType } from "../../../core";


export type FullViewProps = {
  url: string;
  fileName: string;
  size: string;
  downloading: boolean;
  videoRef?: any;
  onClickDownload(): void;
  onCloseFullView(): void;
  onReady?(): void;
  onDuration?(duration: number): void;
  onProgress?(total: number, last: number): void;
  onListen?(played: number, total: number): void;
  onPdfLoad?(totalPages: number): void;
  onChangePage?(pagesRead: number[]): void;
};

export const FullView: FC<FullViewProps> = ({
  url,
  fileName,
  size,
  downloading,
  onClickDownload,
  onCloseFullView,
  videoRef,
  onReady,
  onDuration,
  onProgress,
  onListen,
  onPdfLoad,
  onChangePage,
}) => {
  const extension = fileName.slice(fileName.lastIndexOf(".") + 1);
  const fileType = getFileType(extension);

  const renderFullPreviewType = useCallback(() => {
    switch (fileType) {
      case "IMAGE":
        return <FullViewImage url={url} />;
      case "VIDEO":
        return (
          <UplinkVideo
            videoRef={videoRef}
            url={url}
            onReady={onReady as () => void}
            onDuration={onDuration as () => void}
            onProgress={onProgress as (total: number, last: number) => void}
          />
        );
      case "AUDIO":
        return (
          <UplinkAudio
            url={url}
            onListen={onListen as (played: number, total: number) => void}
          />
        );
      case "PDF":
        return (
          <UplinkPdf
            url={url}
            onPdfLoad={onPdfLoad as (totalPages: number) => void}
            onChangePage={onChangePage as (pagesRead: number[]) => void}
          />
        );
      default:
        return <MiniPreviewFiles extension={extension} />;
    }
  }, []);

  return (
    <Container>
      <CloseContainer onClick={onCloseFullView}>
        <Icon type="close" size="lg" color="system/light1" />
      </CloseContainer>
      <PreviewHeader
        fileName={fileName}
        size={size}
        downloading={downloading}
        onClickDownload={onClickDownload}
        onClose={onCloseFullView}
      />
      <ContentWrapper>{renderFullPreviewType()}</ContentWrapper>
    </Container>
  );
};

export default FullView;
