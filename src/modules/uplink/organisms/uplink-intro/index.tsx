import React, { FC } from "react";

import { Container, InfoContainer, DownloadButtonWrapper } from "./styles";

import {
  Author,
  AttachmentTitle,
  DownloadButton,
  MiniPreview,
  AuthorProps,
  AttachmentTitleProps,
  DownloadButtonProps,
  MiniPreviewProps,
} from "../../";


export type UplinkIntroProps = AuthorProps &
  AttachmentTitleProps &
  DownloadButtonProps &
  MiniPreviewProps;

export const UplinkIntro: FC<UplinkIntroProps> = ({
  avatarUrl,
  name,
  fileName,
  size,
  downloading,
  onClickDownload,
  thumbnailUrl,
  onAccept,
  onOpenLegalNotices,
  hasAccepted,
  onClickViewAttachment,
}) => {
  const extension = fileName.slice(fileName.lastIndexOf(".") + 1);

  return (
    <Container>
      <InfoContainer>
        <Author avatarUrl={avatarUrl} name={name} />
        <AttachmentTitle fileName={fileName} size={size} />
        {hasAccepted && (
          <DownloadButtonWrapper>
            <DownloadButton
              onClickDownload={onClickDownload}
              downloading={downloading}
            />
          </DownloadButtonWrapper>
        )}
      </InfoContainer>
      <MiniPreview
        hasAccepted={hasAccepted}
        extension={extension}
        thumbnailUrl={thumbnailUrl}
        onAccept={onAccept}
        onClickViewAttachment={onClickViewAttachment}
        onOpenLegalNotices={onOpenLegalNotices}
      />
    </Container>
  );
};

export default UplinkIntro;
