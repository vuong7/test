import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 32px;
  column-gap: 37px;
  width: 100%;
  height: 100%;

  @media only screen and (min-width: 768px) {
    flex-direction: row;
  }
`;

export const InfoContainer = styled.div``;

export const DownloadButtonWrapper = styled.div`
  margin-top: 10px;

  @media only screen and (min-width: 768px) {
    margin-top: 20px;
  }
`;
