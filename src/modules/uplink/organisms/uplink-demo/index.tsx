import React, { FC, useState } from "react";

import { Container, MiniViewWrapper } from "./styles";

import { UplinkIntro, FullView, UplinkIntroProps, FullViewProps } from "../";
import { getFileType } from "../../../core";


export type UplinkDemoProps = UplinkIntroProps & FullViewProps;

export const UplinkDemo: FC<UplinkDemoProps> = ({
  avatarUrl,
  name,
  fileName,
  size,
  downloading,
  onClickDownload,
  thumbnailUrl,
  onOpenLegalNotices,
  hasAccepted,
  onAccept,
  onClickViewAttachment,
  url,
  onCloseFullView,
  onDuration,
  onProgress,
  onPdfLoad,
  onChangePage,
}) => {
  const [hasCurrentAccepted, setHasCurrentAccepted] = useState<boolean>(
    !!hasAccepted
  );
  const [isFullView, setIsFullView] = useState<boolean>(false);

  const extension = fileName.slice(fileName.lastIndexOf(".") + 1);
  const fileType = getFileType(extension);

  const handleAccept = () => {
    setHasCurrentAccepted(true);
    if (fileType === "VIDEO" || fileType === "IMAGE" || fileType === "PDF") {
      setIsFullView(true);
    }
  };

  const handleCloseFullView = () => {
    setIsFullView(false);
  };
  return (
    <Container>
      {!isFullView ? (
        <MiniViewWrapper>
          <UplinkIntro
            avatarUrl={avatarUrl}
            name={name}
            fileName={fileName}
            size={size}
            onClickDownload={onClickDownload}
            downloading={downloading}
            thumbnailUrl={thumbnailUrl}
            onOpenLegalNotices={onOpenLegalNotices}
            hasAccepted={hasCurrentAccepted}
            onAccept={onAccept}
            onClickViewAttachment={handleAccept}
            extension={extension}
          />
        </MiniViewWrapper>
      ) : (
        <FullView
          url={url}
          fileName={fileName}
          size={size}
          downloading={downloading}
          onCloseFullView={handleCloseFullView}
          onClickDownload={onClickDownload}
          onDuration={onDuration}
          onProgress={onProgress}
          onPdfLoad={onPdfLoad}
          onChangePage={onChangePage}
        />
      )}
    </Container>
  );
};

export default UplinkDemo;
