export type IUplinkOverviewTypeAudio = {
  type: 'AUDIO';
  value: string; // file.
  options: {
    includeAsAttachment: boolean;
    engagingTitle?: string;
    engagingDescription?: string;
    audioTitle: string;
    audioShortDescription?: string;
    cover?: string;
  };
};

export type IUplinkOverviewTypeAttachmentShortcut = {
  type: 'ATTACHMENT_SHORTCUT';
  value: string; // attachment id
  options: {
    cover?: string;
  };
};

export type IUplinkOverviewTypeDivider = {
  type: 'DIVIDER';
  value: string; // style
  options: {
    size: number; // default 1
  };
};

export type IUplinkOverviewTypeImage = {
  type: 'IMAGE';
  value: string; // file.
  options: {
    includeAsAttachment: boolean;
  };
};

export type IUplinkOverviewTypeImageSlider = {
  type: 'IMAGE_SLIDER';
  value: {
    file: string;
    caption?: string;
  }[]; // list of image objects.
  options: {
    includeAsAttachment: boolean;
  };
};

export type IUplinkOverviewTypeQuote = {
  type: 'QUOTE';
  value: string;
};

export type IUplinkOverviewTypeTable = {
  type: 'TABLE';
  value: string[][]; // rows
  options: {
    title?: string;
    columns: {
      name: string;
      size: number; // column size in percentage
    }[];
    showSource: boolean;
    source?: string;
  };
};

export type IUplinkOverviewTypeText = {
  type: 'TEXT';
  value: string;
};

export type IUplinkOverviewTypeVideo = {
  type: 'VIDEO';
  value: string; // file or url
  options: {
    includeAsAttachment?: boolean;
    source: 'MEDIA_UPLOAD' | 'YOUTUBE' | 'VIMEO';
    cover?: string;
  };
};

type IUplinkOverviewSection =
  | IUplinkOverviewTypeAudio
  | IUplinkOverviewTypeAttachmentShortcut
  | IUplinkOverviewTypeDivider
  | IUplinkOverviewTypeImage
  | IUplinkOverviewTypeImageSlider
  | IUplinkOverviewTypeQuote
  | IUplinkOverviewTypeTable
  | IUplinkOverviewTypeText
  | IUplinkOverviewTypeVideo;

export type IUplinkOverview = {
  id?: string;
  order?: number;
} & IUplinkOverviewSection;

export type IUplinkOverviewType =
  | 'AUDIO'
  | 'ATTACHMENT_SHORTCUT'
  | 'DIVIDER'
  | 'IMAGE'
  | 'IMAGE_SLIDER'
  | 'QUOTE'
  | 'TABLE'
  | 'TEXT'
  | 'VIDEO';
