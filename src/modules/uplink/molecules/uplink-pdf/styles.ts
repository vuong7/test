import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const InnerContainer = styled.div<{ height: number }>`
  width: 100%;
  height: ${props => props.height}px;
  padding: 10px;
  overflow: auto;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  @media (max-width: 768px) {
    padding-top: 60px;
  }
`;

export const PageControl = styled.div`
  width: 100%;
  height: 45px;
  background-color: ${(props) => props.theme.neutral};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const NumbersContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  padding: 4px;
  border: 2px solid ${(props) => props.theme.system.light3}30;
  border-radius: 10px;

  &:hover {
    cursor: pointer;
  }
`;

export const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;
