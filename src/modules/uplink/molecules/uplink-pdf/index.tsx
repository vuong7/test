import React, { FC, useState, useEffect, useRef } from "react";
import { Document, Page } from "react-pdf/dist/esm/entry.webpack";
import { PDFDocumentProxy } from "pdfjs-dist/types/src/display/api";

import {
  Container,
  InnerContainer,
  PageControl,
  NumbersContainer,
  IconContainer,
  LoadingContainer,
} from "./styles";

import { FullPreviewFailed } from "../";
import { Typography, Spacer, Icon, DotLoader } from "../../..";

export type UplinkPdfProps = {
  url: any;
  onPdfLoad(totalPages: number): void;
  onChangePage(pagesRead: number[]): void;
};


export const UplinkPdf: FC<UplinkPdfProps> = ({
  url,
  onPdfLoad,
  onChangePage,
}) => {

  let containerRef = useRef<HTMLDivElement>(null);
  const [innerHeight, setInnerHeight] = useState<number>(0);
  const [pageWidth, setPageWidth] = useState<number>(0);
  const [pageHeight, setPageHeight] = useState<number>(0);

  const [numPages, setNumPages] = useState<number>(1);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const [pagesRead, setPagesRead] = useState<number[]>([]);
  const [scale, setScale] = useState<number>(1);

  const [hasFailed, setHasFailed] = useState<boolean>(false);
  const [hasLoaded, setHasLoaded] = useState<boolean>(false);


  const onDocumentLoadSuccess = (pdf: PDFDocumentProxy) => {
    const numPages = pdf.numPages;
    onPdfLoad(numPages);
    setNumPages(numPages);
    setPageNumber(1);
    setPagesRead([1]);
    setHasLoaded(true);
  };

  const handlePage = (number: number) => {
    const newPage = pageNumber + number;
    if (newPage > 0 && newPage <= numPages) {
      setPageNumber(newPage);
      setPagesRead(pagesRead.includes(newPage) ? pagesRead : [...pagesRead, newPage]);
    }
  };

  const handleScale = (value: number) => {
    if (value > 0) {
      if (scale < 3) {
        setScale(scale + value);
      }
    } else {
      if (scale > 1) {
        setScale(scale + value);
      }
    }
  }

  const handleInnerHeight = () => {
    if (containerRef.current) {
      setInnerHeight(containerRef.current.clientHeight - 45);
      setPageWidth(window.innerWidth - 20);
      setPageHeight(containerRef.current.clientHeight - 105)
    }
  }


  useEffect(() => {
    onChangePage(pagesRead);
  }, [pagesRead]);

  useEffect(() => {
    handleInnerHeight();
  }, [containerRef.current]);

  useEffect(() => {
    window.addEventListener("resize", handleInnerHeight);
    return () => window.removeEventListener("resize", handleInnerHeight);
  }, []);


  return (
    <Container ref={containerRef}>
      {hasFailed ?
        <FullPreviewFailed/>
        :
        containerRef.current && <>
          <InnerContainer height={innerHeight}>
            <Document
              file={url}
              options={{workerSrc: "/pdf.worker.js"}}
              noData={() => <FullPreviewFailed/>}
              onSourceError={() => setHasFailed(true)}
              onLoadError={() => setHasFailed(true)}
              onLoadSuccess={onDocumentLoadSuccess}
              loading={() => (
                <LoadingContainer>
                  <DotLoader/>
                </LoadingContainer>
              )}>
              <Page
                width={pageWidth >= pageHeight ? undefined : pageWidth}
                height={pageWidth >= pageHeight ? pageHeight : undefined}
                pageNumber={pageNumber}
                scale={scale}
                renderAnnotationLayer={false}
              />
            </Document>
          </InnerContainer>
          <PageControl>
            {hasLoaded && <>
              <IconContainer onClick={() => handleScale(-1)}>
                  <Icon type="zoom-out" color="system/light1"/>
              </IconContainer>
              <Spacer x={10}/>
              <IconContainer onClick={() => handlePage(-1)}>
                  <Icon type="arrow-left" color="system/light1"/>
              </IconContainer>
              <Spacer x={30}/>
              <NumbersContainer>
                  <Typography variant="big-numbers" color="system/light1">
                    {pageNumber} / {numPages}
                  </Typography>
              </NumbersContainer>
              <Spacer x={30}/>
              <IconContainer onClick={() => handlePage(1)}>
                  <Icon type="arrow-right" color="system/light1"/>
              </IconContainer>
              <Spacer x={10}/>
              <IconContainer onClick={() => handleScale(1)}>
                  <Icon type="zoom-in" color="system/light1"/>
              </IconContainer>
            </>}
          </PageControl>
        </>
      }
    </Container>
  );
};

export default UplinkPdf;
