import styled, { css } from 'styled-components';

export const ActionButtons = styled.div`
  display: none;
  justify-content: center;
  align-items: center;

  position: absolute;
  top: -28px;
  left: 50%;
  background: ${props => props.theme.brandingColors.primary};
  border-radius: 8px 8px 0px 0px;
  height: 28px;
  transform: translateX(-50%);
  padding: 0px 12px;

  z-index: 9;

  > span {
    cursor: pointer;
    margin-left: 8px;

    &:first-child {
      margin-left: 0px;
    }
  }
`;

export const EditButton = styled.div`
  display: none;
  justify-content: center;
  align-items: center;

  position: absolute;
  top: -11px;
  right: -11px;
  border-radius: 50%;
  border: 2px solid ${props => props.theme.brandingColors.primary};
  background: #fff;
  padding: 4px;

  z-index: 9;
  cursor: pointer;
`;

const WrapperHover = css`
  margin: -8px;
  padding: 6px;
  border: 2px solid ${props => props.theme.brandingColors.primary};
  border-radius: 8px;

  .action-buttons {
    display: flex;
  }
  .edit-button {
    display: flex;
  }
`;

type WrapperProps = {
  isDragging?: boolean;
};
export const Wrapper = styled.div<WrapperProps>`
  position: relative;

  &:hover {
    ${WrapperHover}
  }

  ${props => props.isDragging && WrapperHover}
`;
