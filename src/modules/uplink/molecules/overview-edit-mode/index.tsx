import React, { memo } from 'react';

import { Icon, Spacer } from '../../../core';

import { IUplinkOverview } from '../..';

import { ActionButtons, EditButton, Wrapper } from './styles';
import { AddOverview } from '..';

type Props = {
  onAdd?(overview: IUplinkOverview): void;
  onChangeOrder?(): void;
  onDelete?(): void;
  onEdit?(): void;
};
export const OverviewEditMode: React.FC<Props> = memo(({ onAdd, onChangeOrder, onDelete, onEdit, children }) => {
  return (
    <Wrapper>
      {(onAdd || onChangeOrder || onDelete) && (
        <ActionButtons className="action-buttons">
          {onAdd && (
            <AddOverview
              onAdd={onAdd}
              button={
                <span>
                  <Icon type="plus" size="sm" customColor="#fff" />
                </span>
              }
            />
          )}
          {onChangeOrder && (
            <span>
              <Icon type="menu" size="sm" customColor="#fff" />
            </span>
          )}
          {onDelete && (
            <span onClick={onDelete}>
              <Icon type="close-circle" size="sm" customColor="#fff" />
            </span>
          )}
        </ActionButtons>
      )}
      {onEdit && (
        <EditButton className="edit-button" onClick={onEdit}>
          <Icon type="edit" size="sm" customColor="#786C86" />
        </EditButton>
      )}
      {children}
    </Wrapper>
  );
});

export default OverviewEditMode;
