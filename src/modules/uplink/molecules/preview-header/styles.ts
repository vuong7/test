import styled from "styled-components";

type ViewingTextProps = {
  hasOverflow?: boolean;
};

type TitleTextProps = {
  hasOverflow?: boolean;
};

type FileTextProps = {
  hasOverflow?: boolean;
};


export const Container = styled.div`
  background: #2E1846;
  box-sizing: border-box;
  width: 100%;
  height: 100px;

  display: flex;
  align-items: center;

  padding: 16px;

  @media only screen and (min-width: 768px) {
    padding: 18.5px 31px;
    height: 80px;
  }
`;

export const TextWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  height: 70px;

  @media only screen and (min-width: 768px) {
    flex-direction: row;
    flex-wrap: no-wrap;
    align-items: flex-end;
    justify-content: flex-start;
    column-gap: 5px;
    height: 50px;
  }
`;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ViewingText = styled.div<ViewingTextProps>`
  font-weight: 600;
  font-size: 12px;
  line-height: 12px;
  letter-spacing: 0.1em;
  text-transform: uppercase;

  width: 100%;

  @media only screen and (min-width: 768px) {
    font-weight: 400;
    font-size: ${(props) => (props.hasOverflow ? "14px" : "16px")};
    line-height: 19px;
  }
`;

export const TitleText = styled.div<TitleTextProps>`
  font-weight: 700;

  font-size: 14px;
  line-height: 110%;
  box-sizing: border-box;
  margin: 2px 0px 4px;

  overflow: hidden;
  text-overflow: ellipsis;
  /* width: 100%; */
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 3;

  @media only screen and (min-width: 768px) {
    font-size: ${(props) => (props.hasOverflow ? "15px" : "23px")};
    -webkit-line-clamp: ${(props) => (props.hasOverflow ? 2 : 1)};
  }
`;

export const FileText = styled.div<FileTextProps>`
  font-weight: 400;
  font-size: 12px;
  line-height: 110%;
  min-width: fit-content;
  white-space: nowrap;
  margin: 0px;

  @media only screen and (min-width: 768px) {
    font-size: ${(props) => (props.hasOverflow ? "14px" : "16px")};

    margin: 2px 0px 5px;
  }
`;

export const CloseButton=styled.div`
  
  margin-left: 16px;
  display: none;
  
  &:hover {
    cursor: pointer;
  }

  @media only screen and (min-width: 768px) {
    display: block;
  }
`
