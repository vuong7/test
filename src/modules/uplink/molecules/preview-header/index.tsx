import React, { FC, useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  TextWrapper,
  TitleWrapper,
  ViewingText,
  TitleText,
  FileText,
  CloseButton
} from "./styles";

import { DownloadButton } from "../../";
import { Typography, Spacer, Icon } from "../../..";


export type PreviewHeaderProps = {
  fileName: string;
  size: string;
  downloading: boolean;
  onClickDownload(): void;
  onClose(): void;
};

export const PreviewHeader: FC<PreviewHeaderProps> = ({
  fileName,
  size,
  downloading,
  onClickDownload,
  onClose
}) => {

  const { t } = useTranslation();

  const title = fileName.slice(0, fileName.lastIndexOf("."));
  const extension = fileName.slice(fileName.lastIndexOf("."));

  const contentRef = useRef<HTMLHeadingElement>(null);
  const [hasOverflow, setHasOverflow] = useState<boolean>(false);

  useEffect(() => {
    if (!!contentRef.current && !hasOverflow) {
      setHasOverflow(
        contentRef.current.clientHeight + 3 < contentRef.current.scrollHeight ||
          contentRef.current.clientWidth + 3 < contentRef.current.scrollWidth
      );
    }
  }, [contentRef.current]);

  return (
    <Container>
      <TextWrapper>
        <TitleWrapper>
          <Typography color="neutral/40">
            <ViewingText hasOverflow={hasOverflow}>
              {t("uplink.shares.viewing")}
            </ViewingText>
          </Typography>
          <Typography color="system/light3">
            <TitleText ref={contentRef} hasOverflow={hasOverflow}>
              {title}
            </TitleText>
          </Typography>
        </TitleWrapper>
        <Typography color="neutral/50">
          <FileText
            hasOverflow={hasOverflow}
          >{`${extension}, ${size}`}</FileText>
        </Typography>
      </TextWrapper>
      <Spacer x={12} />
      <DownloadButton
        onClickDownload={onClickDownload}
        downloading={downloading}
      />
      <CloseButton onClick={onClose}>
        <Icon type='close' size='lg' color='system/light1' />
      </CloseButton>
    </Container>
  );
};

export default PreviewHeader;
