import React, { FC } from "react";
import { Container, MessageWrapper, MessageText } from "./styles";

import { Typography, Spacer, Icon, AvatarImage, InlineWrapper } from "../../..";

import { useTranslation } from "react-i18next";

export type AuthorProps = {
  avatarUrl?: string;
  name: string;
};

export const Author: FC<AuthorProps> = ({ avatarUrl, name }) => {
  const { t } = useTranslation();

  return (
    <Container>
      <AvatarImage url={avatarUrl} customSize={42} name={name} />
      <Spacer x={12} />
      <MessageWrapper>
        <Typography color="neutral/10">
          <MessageText>{t("uplink.shares.author", [name])}</MessageText>
        </Typography>
      </MessageWrapper>
    </Container>
  );
};

export default Author;
