import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;

  width: 332px;
  height: 50px;

  box-sizing: border-box;

  background: rgba(0, 0, 0, 0.21);
  border-radius: 64px;
  padding: 4px 16px 4px 6px;
`;

export const MessageWrapper = styled.div`
  /* min-height: 100%; */
  display: flex;
  align-items: center;
  justify-content: flex-start;

  * {
    text-transform: none;
    letter-spacing: 0;
  }
`;

export const MessageText = styled.div`
  font-weight: 600;
  font-size: 16px;
  line-height: 16px;
`;
