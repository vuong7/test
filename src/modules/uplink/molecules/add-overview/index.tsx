import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { hideAll } from 'tippy.js';
import { v4 } from 'uuid';
import Tippy from '@tippyjs/react'; // tooltop, popover

import { Icon, Spacer, Typography } from '../../../core';

import { IUplinkOverview, IUplinkOverviewType } from '../..';

import { Item, Wrapper } from './styles';

type Props = {
  button: JSX.Element;
  onAdd(overview: IUplinkOverview): void;
};
export const AddOverview: React.FC<Props> = ({ button, onAdd }) => {
  const { t } = useTranslation();

  const handleOnAdd = useCallback(
    (type: IUplinkOverviewType) => {
      hideAll();
      switch (type) {
        case 'ATTACHMENT_SHORTCUT':
          return onAdd({
            id: v4(),
            type: 'ATTACHMENT_SHORTCUT',
            value: '',
            options: {
              cover: undefined,
            },
          });
        case 'IMAGE':
          return onAdd({
            id: v4(),
            type: 'IMAGE',
            value: '',
            options: {
              includeAsAttachment: false,
            },
          });
        case 'TEXT':
          return onAdd({
            id: v4(),
            type: 'TEXT',
            value: '',
          });
        case 'VIDEO':
          return onAdd({
            id: v4(),
            type: 'VIDEO',
            value: '',
            options: {
              source: 'YOUTUBE',
            },
          });
      }
    },
    [onAdd],
  );

  const items = useMemo(
    () => [
      <Item onClick={() => handleOnAdd('ATTACHMENT_SHORTCUT')} key="ATTACHMENT_SHORTCUT">
        <Icon type="attachment" size="sm" customColor="#786C86" />
        <Spacer y={8} />
        <Typography variant="titles">{t('Attachment Shortcut')}</Typography>
      </Item>,
      <Item onClick={() => handleOnAdd('IMAGE')} key="IMAGE">
        <Icon type="image" size="sm" customColor="#786C86" />
        <Spacer y={8} />
        <Typography variant="titles">{t('Image')}</Typography>
      </Item>,
      <Item onClick={() => handleOnAdd('TEXT')} key="TEXT">
        <Icon type="align-left" size="sm" customColor="#786C86" />
        <Spacer y={8} />
        <Typography variant="titles">{t('Text')}</Typography>
      </Item>,
      <Item onClick={() => handleOnAdd('VIDEO')} key="VIDEO">
        <Icon type="video" size="sm" customColor="#786C86" />
        <Spacer y={8} />
        <Typography variant="titles">{t('Video')}</Typography>
      </Item>,
    ],
    [handleOnAdd, t],
  );

  return (
    <Tippy
      content={<Wrapper items={items.length}>{items}</Wrapper>}
      animation="perspective-extreme"
      allowHTML
      trigger="click"
      interactive
      placement="bottom"
      // visible={true}
      maxWidth="none"
    >
      {button}
    </Tippy>
  );
};

export default AddOverview;