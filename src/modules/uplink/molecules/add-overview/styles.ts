import styled from 'styled-components';

export const Item = styled.div`
  /* width: 142px; */
  height: 66px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  cursor: pointer;
`;

type WrapperProps = {
  items: number;
};
export const Wrapper = styled.div<WrapperProps>`
  display: grid;
  grid-template-columns: repeat(
    ${props => {
      if (props.items < 5) return props.items;
      const items = Math.ceil(props.items / 2);
      return items > 5 ? 5 : items;
    }},
    142px
  );
`;
