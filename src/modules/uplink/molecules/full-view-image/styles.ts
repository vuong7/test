import styled from "styled-components";

type ImageWrapperProps = {
  hasImageFailed?: boolean;
  hasImageLoaded?: boolean;
};

export const Container = styled.div`
  box-sizing: border-box;
  position: relative;

  box-sizing: border-box;
  width: 100%;
  height: 100%;
  max-height: 100%;
`;

export const ImageWrapper = styled.img<ImageWrapperProps>`
  width: 100%;
  height: 100%;

  object-fit: contain;

  opacity: ${(props) => (props.hasImageLoaded ? "1" : 0)};
`;
