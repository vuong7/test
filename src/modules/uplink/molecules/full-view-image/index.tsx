import React, { FC, useState } from "react";
import { Container, ImageWrapper } from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";
import { FullPreviewFailed } from "../";

export type FullViewImageProps = {
  url: string;
};

export const FullViewImage: FC<FullViewImageProps> = ({ url }) => {
  const [hasImageFailed, setHasImageFailed] = useState<boolean>(false);
  const [hasImageLoaded, setHasImageLoaded] = useState<boolean>(false);

  return (
    <Container>
      {!hasImageFailed ? (
        <>
          <ImageWrapper
            src={url || ""}
            alt=" "
            onError={() => setHasImageFailed(true)}
            onLoad={() => setHasImageLoaded(true)}
            hasImageLoaded={hasImageLoaded}
          />
        </>
      ) : (
        <FullPreviewFailed />
      )}
    </Container>
  );
};

export default FullViewImage;
