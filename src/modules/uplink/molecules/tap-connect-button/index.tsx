import React, { FC } from "react";
import { useTranslation } from "react-i18next";

import { Container, ConnectButton, ListButtonsContainer, ListButton } from "./styles";

import { Typography, Spacer, Icon } from "../../..";


export type TapConnectButtonProps = {
  show: boolean;
  expanded: boolean;
  buttons: any[];
  onTapConnect(): void;
  onTapButton(label: string): void;
}


export const TapConnectButton: FC<TapConnectButtonProps> = ({
  show,
  expanded,
  buttons,
  onTapConnect,
  onTapButton
}) => {

  const { t } = useTranslation();

  return (
    <Container>
      <ConnectButton show={show} expanded={expanded} onClick={onTapConnect}>
        <Typography variant="titles" color="system/light1">
          {t('Tap to connect')}
        </Typography>
      </ConnectButton>
      <ListButtonsContainer expanded={expanded} height={50 * buttons.length}>
        {buttons.map((item, index) => (
          <ListButton key={index} onClick={() => onTapButton(item.label)}>
            <Icon type={item.icon} size='md' color='system/light1' />
            <Spacer x={12} />
            <Typography variant="titles" color="system/light1">
              {t(item.label)}
            </Typography>
          </ListButton>
        ))}
      </ListButtonsContainer>
    </Container>
  )
}
