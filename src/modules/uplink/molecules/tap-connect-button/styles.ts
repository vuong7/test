import styled from "styled-components";


export const Container = styled.div`
`;

export const ConnectButton = styled.div<{ show: boolean, expanded: boolean }>`
  height: ${props => (props.show ? '42px' : '0px')};
  background: #ed3995;
  border-radius: ${props => (props.expanded ? '8px 8px 0px 0px' : '8px')};
  box-shadow: 0px 8px 28px rgba(27, 209, 93, 0.1);
  overflow: hidden;
  transition: height 0.75s cubic-bezier(0.42, 0, 0.58, 1) 0s;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ListButtonsContainer = styled.div<{ expanded: boolean, height: number }>`
  height: ${props => (props.expanded ? `${props.height}px` : '0px')};
  background: #ed399588;
  border-radius: 0px 0px 8px 8px;
  overflow: hidden;
  transition: height 0.75s cubic-bezier(0.42, 0, 0.58, 1) 0s;
`;

export const ListButton = styled.div`
  height: 50px;
  background: #ed399588;
  box-shadow: 0px 8px 28px rgba(27, 209, 93, 0.1);
  padding-left: 30px;
  display: flex;
  align-items: center;
`;
