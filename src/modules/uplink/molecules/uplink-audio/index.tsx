import React, { FC, useState } from "react";
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';

import { Container } from "./styles";

import FullPreviewFailed from "../full-view-failed";


export type UplinkAudioProps = {
  url: string;
  onListen(played: number, total: number): void;
};


export const UplinkAudio: FC<UplinkAudioProps> = ({ url, onListen }) => {

  const [loadFailed, setLoadFailed] = useState<boolean>(false);

  const handleListen = (event: any) => {
    onListen(Math.floor(event.target.currentTime), Math.floor(event.target.duration));
  };


  return (
    <Container>
      {!loadFailed ? (
        <AudioPlayer
          autoPlay
          style={{ width: 1000 }}
          src={url}
          onError={() => setLoadFailed(true)}
          onListen={handleListen}
        />
      ) : (
        <FullPreviewFailed/>
      )}
    </Container>
  );
};

export default UplinkAudio;
