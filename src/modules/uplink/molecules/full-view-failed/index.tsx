import React, { FC } from "react";
import { Container, FailMessage } from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";

import { useTranslation, Trans } from "react-i18next";

export type FullPreviewFailedProps = {};

export const FullPreviewFailed: FC<FullPreviewFailedProps> = ({}) => {
  const { t } = useTranslation();

  const downloadTheFile = t("uplink.shares.downloadTheFile");

  return (
    <Container>
      <Icon type="alert-circle" color="neutral/10" size="lg" />
      <Spacer y={20} />
      <Typography color="system/light1">
        <FailMessage>
          {t("uplink.shares.failedFullLoad")}
          <br />
          <Trans
            i18nKey="uplink.shares.failedFullTryAgain"
            values={{ downloadTheFile: downloadTheFile }}
          >
            Please try again later or
            <span>{{ downloadTheFile }}</span>
          </Trans>
        </FailMessage>
      </Typography>
    </Container>
  );
};

export default FullPreviewFailed;
