import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  max-height: 100%;
  background-color: rgba(0, 0, 0, 0.33);

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const FailMessage = styled.div`
  text-align: center;
  font-size: 16px;
  line-height: 22px;
  letter-spacing: 0.01em;

  span {
    font-weight: 600;
    color: ${(props) => props.theme.highlight};
    white-space: nowrap;
  }

  @media only screen and (min-width: 768px) {
    font-size: 24px;
    line-height: 33px;
  }
`;
