import React, { FC, useEffect, useLayoutEffect, useRef, useState } from "react";
import ReactPlayer from "react-player";

import { Container } from "./styles";

import { FullPreviewFailed } from "../";


export type UplinkVideoProps = {
  url: string;
  videoRef: any
  onReady(): void;
  onDuration(duration: number): void;
  onProgress(total: number, last: number): void;
};

export const UplinkVideo: FC<UplinkVideoProps> = ({
  url,
  videoRef,
  onReady,
  onDuration,
  onProgress,
}) => {

  let containerRef = useRef<HTMLDivElement>(null);
  const [playerHeight, setPlayerHeight] = useState<number>(0);

  const [lastWatchedSecond, setLastWatchedSecond] = useState<number>(0);
  const [totalSecondsWatched, setTotalSecondsWatched] = useState<number>(0);
  const [hasFailed, setHasFailed] = useState<boolean>(false);


  const handleProgress = (playedSeconds: number) => {
    const newLast =
      playedSeconds > lastWatchedSecond ? playedSeconds : lastWatchedSecond;
    const newTotal = totalSecondsWatched + 1;
    setLastWatchedSecond(newLast);
    setTotalSecondsWatched(newTotal);
    onProgress(newTotal, newLast);
  };

  const handlePlayerHeight = () => {
    if (containerRef.current) {
      setPlayerHeight(containerRef.current.clientHeight);
    }
  }

  useLayoutEffect(() => {
    window.addEventListener('resize', handlePlayerHeight);
    handlePlayerHeight();
    return () => window.removeEventListener('resize', handlePlayerHeight);
  }, []);

  useEffect(() => {
    handlePlayerHeight();
  }, [containerRef.current]);

  useEffect(() => {
    window.addEventListener("resize", handlePlayerHeight);
    return () => window.removeEventListener("resize", handlePlayerHeight);
  }, []);


  return (
    <Container ref={containerRef}>
      {!hasFailed ? (
        <ReactPlayer
          ref={videoRef}
          width={"100%"}
          height={playerHeight}
          url={url}
          playing={true}
          controls
          onReady={onReady}
          onProgress={(state) => handleProgress(state.playedSeconds)}
          onDuration={(duration) => onDuration(duration)}
          onError={(error) => setHasFailed(true)}
        />
      ) : (
        <FullPreviewFailed />
      )}
    </Container>
  );
};

export default UplinkVideo;
