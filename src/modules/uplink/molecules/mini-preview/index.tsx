import React, { FC, useCallback } from "react";

import { Container, AcceptWrapper } from "./styles";

import { getFileType } from "../../../core";
import {
  MiniPreviewVideo,
  MiniPreviewImage,
  MiniPreviewFiles,
  AcceptLegalNotices,
} from "../../";

export type MiniPreviewProps = {
  hasAccepted: boolean;
  extension: string;
  thumbnailUrl?: string;
  onAccept(): void;
  onClickViewAttachment(): void;
  onOpenLegalNotices(): void;
};


export const MiniPreview: FC<MiniPreviewProps> = ({
  hasAccepted,
  extension,
  thumbnailUrl,
  onAccept,
  onClickViewAttachment,
  onOpenLegalNotices,
}) => {

  const fileType = getFileType(extension);

  const renderMiniPreviewType = useCallback(() => {
    switch (fileType) {
      case "VIDEO":
        return <MiniPreviewVideo thumbnailUrl={thumbnailUrl || ""} onClickViewAttachment={onClickViewAttachment} />;
      case "IMAGE":
        return <MiniPreviewImage url={thumbnailUrl || ""} onClickViewAttachment={onClickViewAttachment} />;
      default:
        return <MiniPreviewFiles extension={extension} />;
    }
  }, [extension, thumbnailUrl]);

  return (
    <Container>
      {renderMiniPreviewType()}
      {!hasAccepted && (
        <AcceptWrapper>
          <AcceptLegalNotices
            onOpenLegalNotices={onOpenLegalNotices}
            onSubmit={onAccept}
          />
        </AcceptWrapper>
      )}
    </Container>
  );
};

export default MiniPreview;
