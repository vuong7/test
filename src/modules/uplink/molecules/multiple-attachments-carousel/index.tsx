import React, { FC } from "react";
import { useTranslation } from 'react-i18next';

import {
  Container,
  HorizontalScrollContainer,
  AttachmentItem,
  AttachmentItemImageWrapper,
  AttachmentItemImage,
  AttachmentItemTitle,
} from "./styles";

import { Typography } from "../../../core";


export type MultipleAttachmentsCarouselProps = {
  data: any[];
  onItemClicked(item: any): void;
}


export const MultipleAttachmentsCarousel: FC<MultipleAttachmentsCarouselProps> = ({
  data,
  onItemClicked
}) => {

  const { t } = useTranslation();

  return (
    <Container>
      <Typography variant="titles" color="system/light1">
        {t('Sharing with you')}
      </Typography>
      <HorizontalScrollContainer>
        {data.map((item, index) => (
          <AttachmentItem key={index} onClick={() => onItemClicked(item)}>
            <AttachmentItemImageWrapper>
              <AttachmentItemImage src={item.image} />
            </AttachmentItemImageWrapper>
            <AttachmentItemTitle>{t(item.title)}</AttachmentItemTitle>
          </AttachmentItem>
        ))}
      </HorizontalScrollContainer>
    </Container>
  )
}
