import styled from "styled-components";


export const Container = styled.div`
`;

export const HorizontalScrollContainer = styled.div`
  width: 100%;
  height: 224px;
  margin-top: 10px;
  overflow-x: scroll;
  white-space: nowrap;
`;

export const AttachmentItem = styled.div`
  width: 276px;
  height: 222px;
  border: 1px solid rgba(255, 255, 255, 0.21);
  border-radius: 8px;
  box-sizing: border-box;
  margin-right: 16px;
  overflow: hidden;
  display: inline-block;
`;

export const AttachmentItemImageWrapper = styled.div`
  width: 276px;
  height: 162px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AttachmentItemImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const AttachmentItemTitle = styled.div`
  width: 276px;
  height: 60px;
  background: #1d1f2155;
  border-radius: 8px;
  color: #ffffff;
  display: flex;
  align-items: center;
  justify-content: center;
`;
