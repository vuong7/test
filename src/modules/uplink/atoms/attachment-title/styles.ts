import styled from "styled-components";

type ContainerProps = {
  isLoading?: boolean;
  hasFinalHeight?: boolean;
};

type TitleWrapperProps = {
  fontSize: number;
};

export const Container = styled.div<ContainerProps>`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
  max-width: 363px;
  height: ${(props) => (props.hasFinalHeight ? "fit-content" : "206px")};
  max-height: 206px;
  overflow-y: hidden;

  opacity: ${(props) => (props.isLoading ? "0" : "1")};
`;

export const TitleWrapper = styled.div<TitleWrapperProps>`
  font-weight: 700;
  font-size: ${(props) => props.fontSize}px;
  line-height: 110%;
  color: ${(props) => props.theme.system.light3};
`;

export const AdditionalInfo = styled.div`
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;
  color: ${(props) => props.theme.neutralTs[30]};
`;
