import React, { FC, useState, useEffect, useRef } from "react";
import { Container, TitleWrapper, AdditionalInfo } from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";

export type AttachmentTitleProps = {
  fileName: string;
  size: string;
};

export const AttachmentTitle: FC<AttachmentTitleProps> = ({
  fileName,
  size,
}) => {
  const [fontSize, setFontSize] = useState<number>(54);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const contentRef = useRef<HTMLHeadingElement>(null);
  const [hasFinalHeight, setHasFinalHeight] = useState<boolean>(false);

  const title = fileName.slice(0, fileName.lastIndexOf("."));
  const extension = fileName.slice(fileName.lastIndexOf("."));

  useEffect(() => {
    if (
      contentRef.current
        ? contentRef.current?.scrollHeight > contentRef.current?.clientHeight
        : false
    ) {
      setFontSize(fontSize - 2);
    } else {
      setIsLoading(false);
      setHasFinalHeight(true);
    }
  }, [fontSize]);

  return (
    <Container
      ref={contentRef}
      isLoading={isLoading}
      hasFinalHeight={hasFinalHeight}
    >
      <TitleWrapper fontSize={fontSize}>{title}</TitleWrapper>
      <Spacer y={10} />
      <AdditionalInfo>{`${extension}, ${size}`}</AdditionalInfo>
    </Container>
  );
};

export default AttachmentTitle;
