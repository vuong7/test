import styled from "styled-components";

type ThumbnailWrapperProps = {
  hasImageFailed?: boolean;
  hasImageLoaded?: boolean;
};

export const Container = styled.div`
  box-sizing: border-box;
  position: relative;

  box-sizing: border-box;
  width: 100%;
  height: 100%;
  max-height: 100%;
`;

export const ThumbnailWrapper = styled.img<ThumbnailWrapperProps>`
  width: 100%;
  height: 100%;

  object-fit: contain;
  filter: brightness(27%);

  opacity: ${(props) => (props.hasImageLoaded ? "1" : 0)};
`;

export const PreviewLayer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const PreviewContainer = styled.div`
  width: 185px;
  height: 70px;

  background: rgba(0, 0, 0, 0.32);
  border-radius: 96px;
  box-sizing: border-box;
  display: flex;
  align-items: center;

  padding: 14px 32px 14px 14px;
`;

export const IconContainer = styled.div`
  box-sizing: border-box;
  width: 42px;
  height: 42px;
  border: 1px solid ${(props) => props.theme.system.light1};
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const PreviewText = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 14px;
  letter-spacing: 0.01em;
`;
