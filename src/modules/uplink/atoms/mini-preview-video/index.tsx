import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  ThumbnailWrapper,
  PreviewLayer,
  PreviewContainer,
  IconContainer,
  PreviewText,
} from "./styles";

import { Typography, Spacer, Icon } from "../../..";
import { MiniPreviewFailed } from "..";

export type MiniPreviewVideoProps = {
  thumbnailUrl: string;
  onClickViewAttachment: () => void;
};


export const MiniPreviewVideo: FC<MiniPreviewVideoProps> = ({ thumbnailUrl, onClickViewAttachment }) => {

  const { t } = useTranslation();

  const [hasImageFailed, setHasImageFailed] = useState<boolean>(false);
  const [hasImageLoaded, setHasImageLoaded] = useState<boolean>(false);

  return (
    <Container>
      {!hasImageFailed ? (
        <>
          <ThumbnailWrapper
            src={thumbnailUrl || ""}
            alt=" "
            onError={() => setHasImageFailed(true)}
            onLoad={() => setHasImageLoaded(true)}
            hasImageLoaded={hasImageLoaded}
            onContextMenu={(evt) => evt.preventDefault()}
          />
          <PreviewLayer onClick={onClickViewAttachment}>
            <PreviewContainer>
              <IconContainer>
                <Icon type="play" color="system/light1" />
              </IconContainer>
              <Spacer x={18} />
              <Typography color="system/light1">
                <PreviewText>{t("uplink.shares.watchVideo")}</PreviewText>
              </Typography>
            </PreviewContainer>
          </PreviewLayer>
        </>
      ) : (
        <MiniPreviewFailed />
      )}
    </Container>
  );
};

export default MiniPreviewVideo;
