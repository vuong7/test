import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  position: relative;
  width: 215px;
  height: 185px;

  background-color: ${(props) => props.theme.system.light1};
  border-radius: 14px;

  /* background-blend-mode: overlay; */
`;

export const Content = styled.div`
  z-index: 1;
  background-color: ${(props) => props.theme.system.light1};
  width: 100%;
  height: 100%;
  position: relative;
  box-sizing: border-box;
  border-radius: 14px;
  padding: 19px;

  &:hover {
    cursor: pointer;
  }
`;

export const Shadow = styled.div`
  position: absolute;
  top: 22px;
  left: 21px;
  width: 100%;
  height: 100%;
  background-color: rgba(205, 208, 227, 0.6);
  border-radius: 14px;
`;

export const FileType = styled.div`
  font-weight: 600;
  font-size: 23px;
  line-height: 23px;
`;

export const FileExtension = styled.div`
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;
`;
