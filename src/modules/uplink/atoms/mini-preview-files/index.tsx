import React, { FC } from "react";
import { Container, Content, Shadow, FileType, FileExtension } from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";
import { getFileType } from "../../../core/utils";
import { IconTypes } from "../../../core/atoms/icon/svgs";

export type MiniPreviewFilesProps = {
  extension: string;
};

export const MiniPreviewFiles: FC<MiniPreviewFilesProps> = ({ extension }) => {
  const fileType = `${getFileType(extension)[0].toUpperCase()}${getFileType(
    extension
  )
    .slice(1)
    .toLowerCase()}`;
  const getExtensionIcon = () => {
    const mappedTypes: Record<string, string> = {
      AUDIO: "audio",
      COMPRESSED: "compressed",
      CSV: "csv",
      DOCUMENT: "document",
      SPREADSHEET: "spreadsheet",
      PDF: "pdf",
      UNKNOWN: "unknown",
    };

    if (extension) {
      const type = getFileType(extension);
      return mappedTypes[type] || "unkown";
    }

    return mappedTypes.UNKNOWN;
  };

  return (
    <Container>
      <Content>
        <Icon
          type={(getExtensionIcon() as IconTypes) || "unknown"}
          color="neutral/80"
          size="lg"
        />
        <Spacer y={36} />
        <Typography color="neutral">
          <FileType>{fileType}</FileType>
        </Typography>
        <Spacer y={11} />
        <Typography color="neutral/80">
          <FileExtension>{`.${extension}`}</FileExtension>
        </Typography>
      </Content>

      <Shadow />
    </Container>
  );
};

export default MiniPreviewFiles;
