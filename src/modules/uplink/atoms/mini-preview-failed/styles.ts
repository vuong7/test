import styled from "styled-components";

export const Container = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 100%;
  max-height: 100%;
  background-color: rgba(0, 0, 0, 0.33);

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const PreviewFailed = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 14px;
  letter-spacing: 0.01em;
`;
