import React, { FC } from "react";
import { Container, PreviewFailed } from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";

import { useTranslation } from "react-i18next";

export type MiniPreviewFailedProps = {};

export const MiniPreviewFailed: FC<MiniPreviewFailedProps> = ({}) => {
  const { t } = useTranslation();

  return (
    <Container>
      <Icon type="alert-circle" color="neutral/10" />
      <Spacer y={15} />
      <Typography color="system/light1">
        <PreviewFailed>{t("uplink.shares.failedLoad")}</PreviewFailed>
      </Typography>
    </Container>
  );
};

export default MiniPreviewFailed;
