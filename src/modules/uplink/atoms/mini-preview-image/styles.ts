import styled from "styled-components";

type ImageWrapperProps = {
  hasImageFailed?: boolean;
  hasImageLoaded?: boolean;
};

export const Container = styled.div`
  box-sizing: border-box;
  position: relative;

  box-sizing: border-box;
  width: 100%;
  height: 100%;
  max-height: 100%;
`;

export const ImageWrapper = styled.img<ImageWrapperProps>`
  width: 100%;
  height: 100%;

  object-fit: cover;
  object-position: 0 0;

  opacity: ${(props) => (props.hasImageLoaded ? "1" : 0)};
`;

export const PreviewLayer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  background-color: rgba(0, 0, 0, 0.73);

  display: flex;
  justify-content: center;
  align-items: center;

  &:not(:hover) * {
    display: none;
  }

  &:hover {
    cursor: pointer;
  }
`;

export const PreviewContainer = styled.div`
  width: 204px;
  height: 70px;

  background: rgba(0, 0, 0, 0.32);
  border-radius: 96px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
`;

export const PreviewText = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 14px;
  letter-spacing: 0.01em;
`;
