import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  ImageWrapper,
  PreviewLayer,
  PreviewContainer,
  PreviewText,
} from "./styles";

import { Typography, Spacer, Icon } from "../../..";
import { MiniPreviewFailed } from "../";

export type MiniPreviewImageProps = {
  url: string;
  onClickViewAttachment: () => void;
};


export const MiniPreviewImage: FC<MiniPreviewImageProps> = ({ url, onClickViewAttachment }) => {

  const { t } = useTranslation();

  const [hasImageFailed, setHasImageFailed] = useState<boolean>(false);
  const [hasImageLoaded, setHasImageLoaded] = useState<boolean>(false);

  return (
    <Container>
      {!hasImageFailed ? (
        <>
          <ImageWrapper
            src={url || ""}
            alt=" "
            onError={() => setHasImageFailed(true)}
            onLoad={() => setHasImageLoaded(true)}
            hasImageLoaded={hasImageLoaded}
            onContextMenu={(evt) => evt.preventDefault()}
          />
          <PreviewLayer onClick={onClickViewAttachment}>
            <PreviewContainer>
              <Spacer x={65} />
              <Typography color="system/light1">
                <PreviewText>{t("uplink.shares.preview")}</PreviewText>
              </Typography>
              <Spacer x={14} />
              <Icon type="eye" color="system/light1" />
            </PreviewContainer>
          </PreviewLayer>
        </>
      ) : (
        <MiniPreviewFailed />
      )}
    </Container>
  );
};

export default MiniPreviewImage;
