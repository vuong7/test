import React, { FC, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { SpinningCircles } from 'react-loading-icons';

import { Container } from "./styles";

import { Typography, Spacer, Icon } from "../../..";


export type DownloadButtonProps = {
  downloading: boolean
  onClickDownload(): void;
};

export const DownloadButton: FC<DownloadButtonProps> = ({
  downloading,
  onClickDownload,
}) => {

  const { t } = useTranslation();

  const [isMobile, setIsMobile] = useState(false);

  const handleDownload = () => {
    onClickDownload();
  };

  const handleWindowSizeChange = () => {
    if (window.innerWidth <= 600) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  }

  useEffect(() => {
    handleWindowSizeChange();
    window.addEventListener('resize', handleWindowSizeChange);
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange);
    }
  }, []);


  return (
    <Container onClick={handleDownload}>
      <Typography variant='h4' color='system/light1'>
        {isMobile ? t('Download') : t('Download file')}
      </Typography>
      <Spacer x={8} y={0}/>
      {downloading ?
        <SpinningCircles width={24} height={24} stroke='#ffffff' />
        :
        <Icon type='download' size='md' color='system/light1' />
      }
    </Container>
  );
};

export default DownloadButton;
