import styled from "styled-components";


export const Container = styled.div`
  max-width: fit-content;
  min-width: 139px;
  min-height: 42px;
  background: #ED3995;
  border-radius: 8px;
  box-sizing: border-box;
  position: relative;
  overflow: hidden;

  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 9px;

  &:hover {
    cursor: pointer;
  }

  @media only screen and (min-width: 768px) {
    min-width: 168px;
  }
`;
