import styled from "styled-components";

export const Container = styled.div`
  width: 288px;
  height: 110px;
  background-color: ${(props) => props.theme.system.light1};
  border-radius: 9px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 13px;
  box-sizing: border-box;
  row-gap: 10px;

  text-align: center;

  @media only screen and (min-width: 768px) {
    width: 486px;
    height: 60px;
    flex-direction: row;
    justify-content: center;

    span {
      white-space: auto;
    }
  }
`;

export const AcceptButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 9px;

  width: 103px;
  box-sizing: border-box;

  background-color: ${(props) => props.theme.highlight};
  border-radius: 8px;

  &:hover {
    cursor: pointer;
  }
`;

export const PrivacyPolicyMessage = styled.div`
  font-weight: 400;
  font-size: 18px;
  line-height: 110%;

  span {
    font-weight: 600;
    color: ${(props) => props.theme.highlight};
    white-space: pre;
  }

  span:hover {
    cursor: pointer;
  }
`;

export const AcceptText = styled.div`
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  text-transform: uppercase;
`;
