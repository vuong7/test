import React, { FC } from "react";
import {
  Container,
  AcceptButton,
  PrivacyPolicyMessage,
  AcceptText,
} from "./styles";

import { Typography, Spacer, Icon, InlineWrapper } from "../../..";

import { useTranslation, Trans } from "react-i18next";

export type AcceptLegalNoticesProps = {
  onOpenLegalNotices(): void;
  onSubmit(): void;
};

export const AcceptLegalNotices: FC<AcceptLegalNoticesProps> = ({
  onOpenLegalNotices,
  onSubmit,
}) => {
  const { t } = useTranslation();

  const privacyPolicy = t("uplink.shares.privacyPolicy");

  return (
    <Container onClick={(evt) => evt.stopPropagation()}>
      <InlineWrapper>
        <Typography>
          <PrivacyPolicyMessage>
            <Trans
              i18nKey="uplink.shares.acceptMessage"
              values={{ privacyPolicy: privacyPolicy }}
            >
              Please accept our
              <span onClick={onOpenLegalNotices}>{{ privacyPolicy }}</span> to
              proceed
            </Trans>
          </PrivacyPolicyMessage>
        </Typography>
        <Spacer x={10} />
      </InlineWrapper>
      <AcceptButton onClick={onSubmit}>
        <Typography color="system/light1">
          <AcceptText>{t("uplink.shares.accept")}</AcceptText>
        </Typography>
      </AcceptButton>
    </Container>
  );
};

export default AcceptLegalNotices;
