import React, { FC, useState } from "react";

import { Container, Menu, IconContainer, MenuItem } from "./styles";

import { Icon, Typography, Spacer, useComponentVisible } from "../../../";
import { useTranslation } from "react-i18next";

export type DropDownMenuProps = {
  onUpdate?: () => void;
  onDelete?: () => void;
};

export const DropDownMenu: FC<DropDownMenuProps> = ({ onUpdate, onDelete }) => {
  const [isDropped, setIsDropped] = useState<boolean>(false);
  const { t } = useTranslation();

  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  return (
    <Container
      ref={ref}
      onClick={(evt) => {
        evt.stopPropagation();
        setIsComponentVisible(!isComponentVisible);
      }}
    >
      <IconContainer>
        <Icon type="more-vertical" size="sm" color="neutral/80" />
      </IconContainer>
      {isComponentVisible && (
        <>
          <Menu>
            <MenuItem onClick={onUpdate}>
              <Typography variant="titles" color="neutral/80">
                {t("Edit")}
              </Typography>
            </MenuItem>

            <MenuItem onClick={onDelete}>
              <Typography variant="titles" color="neutral/80">
                {t("Delete")}
              </Typography>
            </MenuItem>
          </Menu>
        </>
      )}
    </Container>
  );
};

export default DropDownMenu;
