import styled from "styled-components";

import { ScrollbarStyled } from "../../../core/utils/scrollbar";

export const Container = styled.div`
  position: relative;
  /* width: 100%; */
  display: flex;
  justify-content: flex-end;
  box-sizing: border-box;
  border-radius: 8px;

  &:hover {
    cursor: pointer;
  }
`;

export const Menu = styled.div`
  position: absolute;
  top: 22px;
  right: 0px;
  /* padding: 18px; */
  box-sizing: border-box;
  outline: none;
  background-color: ${(props) => props.theme.system.light1};

  box-shadow: 0px 13px 48px #aaa2b4;
  border-radius: 8px;
  z-index: 1;
  border: 1px solid rgba(205, 208, 227, 0.6);
  overflow-y: auto;
  max-height: 400px;
  ${ScrollbarStyled}
`;

export const IconContainer = styled.div`
  /* z-index: 2; */
`;

export const MenuItem = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 12px 28px 12px 18px;
  display: flex;
  align-items: center;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid #ffffff00;
  outline: none;
  box-sizing: border-box;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: clip;
  white-space: nowrap;

  &:first-child {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
  }
  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.contactTags.systemAdded};
  }
`;
