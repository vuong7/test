import styled from "styled-components";

type VolumeBarProps = {
  volume: number;
};

export const Container = styled.div`
  background-color: ${(props) => props.theme.system.light3};
  border-radius: 16px;
  padding: 23px 12px 24px;
  box-sizing: border-box;
  width: 100%;
`;

export const IconsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 40px;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;

export const IconBorder = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 6px;
  border: 1px solid ${(props) => props.theme.primary};
  border-radius: 50%;

  &:hover {
    cursor: pointer;
  }
`;

export const IconFilled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 8px;
  background-color: ${(props) => props.theme.primary};
  border: 1px solid ${(props) => props.theme.primary};
  border-radius: 50%;
  box-sizing: border-box;
  width: 38px;
  height: 38px;
  &:hover {
    cursor: pointer;
  }
`;

export const DurationContainer = styled.div`
  border: 1px solid ${(props) => props.theme.neutralTs[30]};
  box-sizing: border-box;
  border-radius: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 6px 12px;
`;

export const VolumeContainer = styled.div`
  height: 31px;
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  overflow-x: hidden;
`;

export const VolumeCol = styled.div<VolumeBarProps>`
  background-color: ${(props) => props.theme.system.neutral};
  width: 2px;
  min-width: 2px;
  height: ${(props) => props.volume}%;
  max-height: 100%;
  min-height: 1px;
  margin-left: 8px;
`;
