import React, { FC, useState, useEffect } from "react";

import {
  Container,
  IconsWrapper,
  IconContainer,
  IconBorder,
  IconFilled,
  DurationContainer,
  VolumeContainer,
  VolumeCol,
} from "./styles";

import { AudioPlayer } from "../";

import {
  InlineWrapper,
  Spacer,
  Icon,
  Typography,
  formatTimerMinutesSeconds,
} from "../../../";
import { useTranslation } from "react-i18next";

import { v4 } from "uuid";

import MicRecorder from "mic-recorder";

type PreviewAudioData = {
  url: string;
  uuidKey: string;
  duration: number;
};

type VolumeData = {
  relativeVolume: number;
  uuidKey: string;
};

export type RecordingProps = {
  onSubmit(): void;
  onClickDelete(): void;
  uploadFile(audio: Blob): void;
  shouldStopRecord?: boolean;
};

export const Recording: FC<RecordingProps> = ({
  onSubmit,
  onClickDelete,
  uploadFile,
  shouldStopRecord,
}) => {
  const { t } = useTranslation();

  const [isRecording, setIsRecording] = useState(true);
  const [recorder, setRecorder] = useState<null | MicRecorder>(null);

  const [previewAudioList, setPreviewAudioList] = useState<PreviewAudioData[]>(
    []
  );
  // const [audioList, setAudioList] = useState<Blob[]>([])

  const [timer, setTimer] = useState<number>(0);

  const [currentAudioIndex, setCurrentAudioIndex] = useState<number>(0);

  const [analyserControl, setAnalyserControl] = useState<null | AnalyserNode>(
    null
  );
  const [sourceStream, setSourceStream] =
    useState<MediaStreamAudioSourceNode>();

  const [currentVolume, setCurrentVolume] = useState<number>(0);
  const [volumeHistory, setVolumeHistory] = useState<VolumeData[]>([]);
  const [maxVolume, setMaxVolume] = useState<number>(0);
  const [minVolume, setMinVolume] = useState<number>(0);

  const [initial, setInital] = useState<number>(0);

  const requestRecorder = async () => {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    const result = new MicRecorder({
      bitRate: 128,
      encoder: "wav",
      sampleRate: 44100,
    });
    return result;
  };

  useEffect(() => {
    if (recorder === null) {
      if (isRecording) {
        requestRecorder()
          .then((value) => setRecorder(value))
          .catch((err) => console.error);
      }
      return;
    }

    if (isRecording) {
      recorder.start();
      setInital(Date.now() / 1000);
    } else {
      recorder
        .stop()
        .getAudio()
        .then(([buffer, blob]) => {
          setPreviewAudioList([
            ...previewAudioList,
            {
              url: URL.createObjectURL(blob),
              uuidKey: v4(),
              duration: Date.now() / 1000 - initial,
            },
          ]);
          uploadFile(blob);
        })
        .catch((e) => {
          alert("We could not retrieve your message");
          console.log(e);
        });
    }

    // const handleData = (e: any) => {
    //   const blob = new Blob([e.data], { type: "audio/mp3" });

    //   setPreviewAudioList([
    //     ...previewAudioList,
    //     {
    //       url: URL.createObjectURL(blob),
    //       uuidKey: v4(),
    //       duration: Date.now() / 1000 - initial,
    //     },
    //   ]);
    //   uploadFile(blob);
    // };
  }, [recorder, isRecording]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (initial > 0) {
        setTimer(timer + 1);
      }
    }, 1000);
    if (!isRecording) {
      setTimer(0);
      clearInterval(interval);
    }

    return () => {
      clearInterval(interval);
    };
  }, [isRecording, timer, initial]);

  const handleSubmit = () => {
    onSubmit();
  };

  const handleDelete = () => {
    setIsRecording(false);
    onClickDelete();
  };

  const micRequest = async () => {
    const audioStream = await navigator.mediaDevices.getUserMedia({
      audio: {
        echoCancellation: true,
      },
    });
    return audioStream;
  };

  const volumeRegister = (audioStream: MediaStream) => {
    const audioContext = new AudioContext();
    const audioSource = audioContext.createMediaStreamSource(audioStream);
    const analyser = audioContext.createAnalyser();
    analyser.fftSize = 1024;
    analyser.minDecibels = -127;
    analyser.maxDecibels = 0;
    analyser.smoothingTimeConstant = 0.4;
    setSourceStream(audioSource);

    setAnalyserControl(analyser);
  };

  useEffect(() => {
    micRequest()
      .then((stream) => volumeRegister(stream))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      sourceStream?.connect(analyserControl as AnalyserNode);
      const volumes = new Uint8Array(analyserControl?.frequencyBinCount || 0);
      analyserControl?.getByteFrequencyData(volumes);
      const volumeSum = volumes.reduce((acm, cv) => acm + cv, 0);
      setCurrentVolume(volumeSum);
    }, 75);
    if (!isRecording) {
      setVolumeHistory([]);

      clearInterval(interval);
    }

    return () => clearInterval(interval);
  }, [isRecording, analyserControl]);

  useEffect(() => {
    const min =
      minVolume > currentVolume || minVolume === 0 ? currentVolume : minVolume;
    const max = maxVolume < currentVolume ? currentVolume : maxVolume;

    const relative = max > 0 ? ((currentVolume - min) * 100) / (max - min) : 50;

    setMaxVolume(max);
    setMinVolume(min);
    if (volumeHistory.length < 100) {
      setVolumeHistory([
        ...volumeHistory,
        { relativeVolume: relative, uuidKey: v4() },
      ]);
    } else {
      setVolumeHistory([
        ...volumeHistory.slice(1, 101),
        { relativeVolume: relative, uuidKey: v4() },
      ]);
    }
  }, [currentVolume]);

  useEffect(() => {
    if (shouldStopRecord) {
      setIsRecording(false);
    }
  }, [shouldStopRecord]);

  return (
    <Container>
      <InlineWrapper>
        <Typography variant="h1" color="primary">
          {t("contact.notes.recording")}
        </Typography>
      </InlineWrapper>
      <Spacer y={16} />
      {previewAudioList.map((audio, index) => (
        <div
          key={`container-${audio.uuidKey}-${audio.url}`}
          onClick={() => setCurrentAudioIndex(index)}
        >
          <AudioPlayer
            key={`audio-${audio.uuidKey}-${audio.url}`}
            audioSource={audio.url}
            isOtherPlaying={currentAudioIndex === index ? false : true}
            // durationPreview={audio.duration}
          />
          <Spacer y={4} />
        </div>
      ))}
      <Spacer y={19} />
      {isRecording && (
        <>
          <VolumeContainer>
            {volumeHistory.map((history) => (
              <VolumeCol
                key={history.uuidKey}
                volume={history.relativeVolume}
              />
            ))}
          </VolumeContainer>

          <Spacer y={13} />
        </>
      )}
      <IconsWrapper>
        <IconContainer onClick={handleDelete}>
          <Icon type="trash" color="neutral/70" />
        </IconContainer>
        {isRecording ? (
          <DurationContainer>
            <Typography variant="p">
              {formatTimerMinutesSeconds(timer)}
            </Typography>
          </DurationContainer>
        ) : (
          <IconContainer onClick={() => setIsRecording(true)}>
            <Icon type="mic" color="error" />
          </IconContainer>
        )}
        {isRecording ? (
          <IconBorder onClick={() => setIsRecording(false)}>
            <Icon type="arrow-right-full" color="primary" />
          </IconBorder>
        ) : (
          <IconFilled onClick={handleSubmit}>
            <Icon type="check" color="system/light1" />
          </IconFilled>
        )}
      </IconsWrapper>
    </Container>
  );
};

export default Recording;
