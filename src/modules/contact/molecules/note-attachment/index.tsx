import React, { FC, useState, useEffect } from "react";
import {
  Container,
  Wrapper,
  Filler,
  DragIcon,
  ThumNailContainer,
  FileName,
  IconContainer,
  IconOptionContainer,
  NameInput,
  EditContainer,
} from "./styles";

import { Typography, Spacer, Icon, formatFullDateAndTime } from "../../..";

import { NoteAttachmentData } from "../../";

import { IconTypes } from "../../../core/atoms/icon/svgs";
import { getFileType } from "../../../core/utils";

import { useTranslation } from "react-i18next";

export type NoteAttachmentProps = {
  attachment: NoteAttachmentData;
  isEditMode?: boolean;
  onClickDelete?(): void;
  onClickRename?(newName: string, isFile: boolean): void;
  hasSubmitFailed?: boolean;
};

export const NoteAttachment: FC<NoteAttachmentProps> = ({
  attachment,
  isEditMode,
  onClickDelete,
  onClickRename,
  hasSubmitFailed,
}) => {
  const { t } = useTranslation();

  const [fileName, setFileName] = useState<string>(
    attachment.fileType !== "EXTERNAL_LINK" || attachment.fileType === undefined
      ? attachment.name || ""
      : attachment.externalUrl || ""
  );
  const [uploadedAt, setUploadedAt] = useState<string>(
    formatFullDateAndTime(attachment.uploadedAt)
  );

  const [newName, setNewName] = useState<string>(
    attachment.fileType !== "EXTERNAL_LINK" || attachment.fileType === undefined
      ? attachment.name || ""
      : attachment.externalUrl || ""
  );
  const [isEditingName, setIsEditingName] = useState<boolean>(
    attachment.externalUrl === "" ? true : false
  );
  const [isNameInvalid, setIsNameInvalid] = useState<boolean>(false);

  useEffect(() => {
    setFileName(
      attachment.fileType !== "EXTERNAL_LINK" ||
        attachment.fileType === undefined
        ? attachment.name || ""
        : attachment.externalUrl || ""
    );
    setNewName(
      attachment.fileType !== "EXTERNAL_LINK" ||
        attachment.fileType === undefined
        ? attachment.name || newName
        : attachment.externalUrl || newName
    );
    setIsEditingName(attachment.externalUrl === "" ? true : false);
    setUploadedAt(formatFullDateAndTime(attachment.uploadedAt));
  }, [attachment]);

  const handleRename = () => {
    const isFile =
      attachment.fileType !== "EXTERNAL_LINK" ||
      attachment.fileType === undefined;
    if (!isFile) {
      try {
        let url = new URL(newName);
        setIsEditingName(false);
        setIsNameInvalid(false);
      } catch {
        setIsNameInvalid(true);
      }
      if (onClickRename) {
        onClickRename(newName, isFile);
      }
    } else if (newName !== "") {
      setIsEditingName(false);
      const extension = fileName.slice(fileName.lastIndexOf("."));
      const newNameExtension = newName.endsWith(extension)
        ? newName
        : `${newName}${extension}`;
      if (onClickRename) {
        onClickRename(newNameExtension, isFile);
      }
    } else {
      setIsEditingName(false);
      if (onClickRename) {
        onClickRename(fileName, isFile);
      }
    }
  };

  useEffect(() => {
    if (hasSubmitFailed) {
      handleRename();
    }
  }, [hasSubmitFailed]);

  const getExtensionIcon = () => {
    const mappedTypes: Record<string, string> = {
      VIDEO: "video-light",
      IMAGE: "image-light",
      PDF: "pdf-light",
      EXTERNAL_LINK: "external-url",
      UNKNOWN: "attachment",
    };

    if (attachment.name) {
      const type = getFileType(
        attachment.name.slice(attachment.name.lastIndexOf(".") + 1)
      );
      return mappedTypes[type] || "attachment";
    }

    return "attachment";
  };

  return (
    <Container>
      <Wrapper>
        <Spacer x={4} />
        <Typography variant="p-small">
          {t("product.attachments.uploadedOn", [
            uploadedAt.slice(0, uploadedAt.lastIndexOf(" ")),
          ])}
        </Typography>
        <Filler />

        <Spacer x={12} />
      </Wrapper>
      <Spacer y={8} />
      <Wrapper>
        {isEditMode && (
          <>
            <DragIcon>
              <Icon type="menu" size="sm" color="neutral/40" />
            </DragIcon>
            <Spacer x={8} />
          </>
        )}

        <ThumNailContainer>
          <Icon
            type={(getExtensionIcon() as IconTypes) || "attachment"}
            color="neutral/90"
          />
        </ThumNailContainer>
        <Spacer x={8} />
        {isEditingName ? (
          <NameInput
            value={newName}
            onChange={(evt) => setNewName(evt.target.value)}
            isError={isNameInvalid}
            onKeyDown={(evt) => (evt.key === "Enter" ? handleRename() : null)}
            onBlur={handleRename}
            autoFocus
            draggable={true}
            onDragStart={(evt) => {
              evt.stopPropagation();
              evt.preventDefault();
            }}
          />
        ) : (
          <>
            <FileName>
              <Typography variant="titles" color="neutral">
                {fileName}
              </Typography>
              {isEditMode && (
                <>
                  <Spacer x={8} />
                  <EditContainer onClick={() => setIsEditingName(true)}>
                    <Icon type="edit" size="sm" color="neutral/80" />
                  </EditContainer>
                </>
              )}
            </FileName>
          </>
        )}
        <Spacer x={8} />

        {isEditMode && (
          <>
            {isEditingName && newName !== "" ? (
              <IconOptionContainer onClick={() => handleRename()}>
                <Icon type="check" color="neutral/80" />
              </IconOptionContainer>
            ) : (
              <IconOptionContainer onClick={onClickDelete}>
                <Icon type="close-circle" color="neutral/80" />
              </IconOptionContainer>
            )}
          </>
        )}
      </Wrapper>
    </Container>
  );
};

export default NoteAttachment;
