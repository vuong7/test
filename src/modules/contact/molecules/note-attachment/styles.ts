import styled from "styled-components";

import { ScrollbarStyled } from "../../../core/utils/scrollbar";
import { Input } from "../../../core/atoms/input/index";

type SelectedTypeProps = {
  isError?: boolean;
};

export const Container = styled.div`
  background-color: ${(props) => props.theme.system.light2};
  border-radius: 8px;
  border: 1px solid #00000000;

  width: 100%;

  box-sizing: border-box;
  position: relative;
  padding: 10px 3px 12px 8px;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  max-width: 100%;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const DragIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: grab;
  }
`;

export const ThumNailContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 42px;
  min-width: 63px;
  width: 63px;
  background-color: ${(props) => props.theme.system.light1};

  border: 1px solid ${(props) => props.theme.neutralTs[10]};
  box-sizing: border-box;
  border-radius: 4px;
  position: relative;
`;

export const FileName = styled.div`
  display: flex;
  align-items: flex-start;
  flex-grow: 1;
  height: 36px;
  overflow-wrap: anywhere;
  white-space: wrap;
  overflow: hidden;
  text-overflow: ellipsis;
  box-sizing: border-box;
  width: fit-content;
  flex-grow: 1;

  &:hover :last-child {
    opacity: 1;
  }
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const IconOptionContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 31px;
  padding: 0px 12px;
  box-sizing: border-box;
  border-left: 1px solid ${(props) => props.theme.neutralTs[10]};
  position: relative;

  &:hover {
    cursor: pointer;
  }
`;

export const SelectedType = styled.div<SelectedTypeProps>`
  border: 1px solid
    ${(props) =>
      props.isError ? props.theme.system.error : props.theme.system.neutral};
  padding: 2px 8px;
  height: 20px;
  box-sizing: border-box;
  border-radius: 40px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  white-space: nowrap;

  &:hover {
    cursor: pointer;
  }
`;

export const NameInput = styled(Input)`
  box-sizing: border-box;
  flex-grow: 1;
`;

export const EditContainer = styled.div`
  opacity: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
    opacity: 1;
  }
`;
