import React, { FC, useState, useEffect } from "react";

import { AttachmentContainer } from "./styles";

import { Typography, Icon, Spacer } from "../../..";

export type AttachmentsCountProps = {
  count: number;
  showAttachmentFunction: () => void;
  isPreviewAlwaysShown?: boolean;
};

export const AttachmentsCount: FC<AttachmentsCountProps> = ({
  count,
  showAttachmentFunction,
  isPreviewAlwaysShown,
}) => {
  const [isHover, setIsHover] = useState<boolean>(false);
  const [isActive, setIsActive] = useState<boolean>(true);

  const handleActive = () => {
    if (!isPreviewAlwaysShown) {
      setIsActive(!isActive);
    }
  };

  useEffect(() => {
    if (isPreviewAlwaysShown) {
      setIsActive(true);
    }
  }, []);

  return (
    <AttachmentContainer
      onClick={() => {
        showAttachmentFunction();
        handleActive();
      }}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      isActive={isActive}
    >
      <Icon
        type="attachment"
        size="sm"
        color={isHover || isActive ? "system/light1" : "neutral"}
      />
      <Spacer x={9.71} />
      <Typography
        variant={isHover || isActive ? "p-small-strong" : "p-semi-bold"}
        color={isHover || isActive ? "system/light1" : "neutral"}
      >
        {count}
      </Typography>
    </AttachmentContainer>
  );
};

export default AttachmentsCount;
