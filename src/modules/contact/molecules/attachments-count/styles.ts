import styled from "styled-components";

type Props = {
  isActive?: boolean;
};

export const AttachmentContainer = styled.div<Props>`
  background-color: ${(props) =>
    props.isActive ? props.theme.neutralTs[50] : props.theme.neutralTs[10]};
  box-sizing: border-box;
  display: flex;
  align-items: center;

  padding: 2px 4px 2px 8px;
  width: 52px;
  height: 23px;
  border-radius: 32px;
  transition: 0.4s;

  &:hover {
    cursor: pointer;
    background-color: ${(props) => props.theme.neutralTs[50]};
    transition: 0.4s;
  }
`;
