import React, { FC, useState, useEffect } from "react";
import { Container } from "./styles";

import { InputText, InputSelection } from "../../";

import { Typography, InlineWrapper, Spacer } from "../../..";
import { useTranslation } from "react-i18next";

export type SearchTagsProps = {
  handleSearch: (name: string, category: string) => void;
  choices: string[];
};

export const SearchTags: FC<SearchTagsProps> = ({ handleSearch, choices }) => {
  const { t } = useTranslation();

  const [tagName, setTagName] = useState<string>("");
  const [tagCategory, setTagCategory] = useState<string>("");

  useEffect(() => {
    handleSearch(tagName, tagCategory);
  }, [tagName, tagCategory]);

  return (
    <Container>
      <InputText
        handleText={setTagName}
        placeholder="Type to search for a tag"
        icon="search"
      />
      <Spacer x={12} />
      <InputSelection
        handleSelection={setTagCategory}
        placeholder="All categories"
        choices={choices}
      />
    </Container>
  );
};

export default SearchTags;
