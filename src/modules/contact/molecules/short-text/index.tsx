import React, { FC, useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import { Typography } from "../../../";

import { Wrapper, TextBox, ShowMoreText } from "./styles";

export type ShortTextProps = {
  content?: string;
  maxLength?: number;
  maxLines?: number;
  html?: string;
};

export const ShortText: FC<ShortTextProps> = ({
  content,
  maxLength = 156,
  maxLines = 5,
  html,
}) => {
  const { t } = useTranslation();
  const [isTextBig, setIsTextBig] = useState<boolean>(true);
  const [isShowMore, setIsShowMore] = useState<boolean>(false);
  const contentRef = useRef<HTMLHeadingElement>(null);

  useEffect(() => {
    if (content ? content.length > maxLength : false) {
      setIsTextBig(true);
    } else if (content) {
      setIsTextBig(false);
    }
  }, [content]);

  const handleShow = () => {
    if (isTextBig) {
      setIsShowMore(!isShowMore);
    }
  };

  useEffect(() => {
    if (
      contentRef.current
        ? contentRef.current?.scrollHeight !== contentRef.current?.offsetHeight
        : false
    ) {
      setIsTextBig(true);
    } else if (contentRef.current) {
      setIsTextBig(false);
    }
  }, []);

  return (
    <Wrapper hasContent={!!content}>
      <Typography>
        {content ? (
          <>
            {isShowMore ? content : content.slice(0, maxLength)}
            {isTextBig && !isShowMore && "..."}
            {isTextBig && (
              <span onClick={() => handleShow()}>
                {isShowMore ? t(" Show less") : t(" Show more")}
              </span>
            )}
          </>
        ) : (
          <>
            <TextBox
              ref={contentRef}
              isShort={isTextBig && !isShowMore}
              maxLines={maxLines}
              dangerouslySetInnerHTML={{ __html: html || "" }}
            ></TextBox>
            {isTextBig && (
              <ShowMoreText onClick={() => handleShow()}>
                {isShowMore ? t("Show less") : t("...Show more")}
              </ShowMoreText>
            )}
          </>
        )}
      </Typography>
    </Wrapper>
  );
};

export default ShortText;
