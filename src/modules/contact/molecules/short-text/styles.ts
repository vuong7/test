import styled from "styled-components";

type WrapperProps = {
  hasContent?: boolean;
};

type TextBoxProps = {
  isShort?: boolean;
  maxLines: number;
};

export const Wrapper = styled.div<WrapperProps>`
  width: 100%;
  box-sizing: border-box;

  ${(props) =>
    props.hasContent
      ? `& p {
    float: left;
  }

  & span {
    color: ${props.theme.primary};
    font-weight: 700;
  }
  & span:hover {
    text-decoration: underline;
    cursor: pointer;
    font-weight: 700;
  }

  &:after,
  :before {
    content: " ";
    clear: both;
    display: table;
  }`
      : ""}
`;

export const TextBox = styled.div<TextBoxProps>`
  ${(props) =>
    props.isShort
      ? `display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: ${props.maxLines}; overflow: hidden;`
      : ""}
`;

export const ShowMoreText = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;

  color: ${(props) => props.theme.primary};
  font-weight: 700;

  &:hover {
    text-decoration: underline;
    cursor: pointer;
    font-weight: 700;
  }
`;
