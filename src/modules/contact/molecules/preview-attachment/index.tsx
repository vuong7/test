import React, { FC, useState } from "react";

import {
  FilesWrapper,
  FileRow,
  FileInput,
  Wrapper,
  LineDivision,
} from "./styles";

import { IconButton, Spacer, Typography } from "../../../";

export type PreviewAttachmentsProps = {
  file: string;
};

export const PreviewAttachments: FC<PreviewAttachmentsProps> = ({ file }) => {
  const [isError, setIsError] = useState<boolean>(false);

  return (
    <FilesWrapper>
      <FileRow>
        <FileInput isError={isError}>
          {!isError && (
            <>
              <img src={file} onError={() => setIsError(true)} />
              <Spacer x={8} />
            </>
          )}
          {isError && (
            <>
              <video src={file} />
              <Spacer x={8} />
            </>
          )}
          <Typography variant="titles" color="neutral">
            {file.toString().substring(file.toString().lastIndexOf("/") + 1)}
          </Typography>
        </FileInput>
      </FileRow>
    </FilesWrapper>
  );
};

export default PreviewAttachments;
