import styled from "styled-components";

type Props = {
  isError?: boolean;
};

export const FilesWrapper = styled.div``;

export const FileRow = styled.div`
  display: grid;
  grid-template-columns: auto min-content;
`;

export const Wrapper = styled.div``;

export const FileInput = styled.div<Props>`
  display: flex;
  align-items: center;
  height: 40px;
  background: #fafafc;
  border-radius: 8px;
  padding: 8px 12px;

  font-weight: 600;
  font-size: 14px;
  line-height: 100%;
  color: #574e61;

  img,
  video {
    width: 63px;
    height: 42px;
    object-fit: cover;
    border-radius: 4px;
  }

  > span {
    /* max-width: 340px; */
    flex-grow: 1;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: wrap;
  }
`;

export const LineDivision = styled.div`
  border-left: 1px solid ${(props) => props.theme.neutralTs[10]};

  height: 31px;
  box-sizing: border-box;
`;
