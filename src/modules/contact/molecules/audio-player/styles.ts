import styled from "styled-components";

export const Container = styled.div`
  background-color: ${(props) => props.theme.system.light1};
  border-radius: 8px;
  display: flex;
  align-items: center;
  padding: 11px 12px;
  box-sizing: border-box;
  width: 100%;
  height: 54px;
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const AudioProgressContainer = styled.div`
  border-top: 2px dotted ${(props) => props.theme.neutralTs[50]};
  width: 100%;
  height: 0px;
  position: relative;

  input {
    width: 100%;
    height: 40px;
    position: absolute;
    top: -23px;
    left: 0px;
    -webkit-appearance: none;
    background-color: #ffffff00;
  }
  input::-webkit-slider-thumb {
    -webkit-appearance: none;
    width: 10px;
    height: 10px;
    background-color: ${(props) => props.theme.highlight};
    border-radius: 50%;
  }
`;

export const LineDivision = styled.div`
  border-left: 1px solid ${(props) => props.theme.neutralTs[10]};

  height: 31px;
  box-sizing: border-box;
`;

export const DurationContainer = styled.div`
  min-width: 31px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
