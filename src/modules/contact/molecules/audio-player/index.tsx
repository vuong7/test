import React, { FC, useState, useEffect, useRef } from "react";

import {
  Container,
  AudioProgressContainer,
  IconContainer,
  LineDivision,
  DurationContainer,
} from "./styles";

import { Icon, Typography, Spacer, formatTimerMinutesSeconds } from "../../..";

export type AudioPlayerProps = {
  audioSource: string;
  isOtherPlaying: boolean;
  deleteFunction?: (audio: string) => void;
  durationPreview?: number;
};

export const AudioPlayer: FC<AudioPlayerProps> = ({
  audioSource,
  isOtherPlaying,
  deleteFunction,
  durationPreview = 0,
}) => {
  const audioElement = new Audio(audioSource);

  const audioRef = useRef(audioElement);

  const [isPlaying, setIsPlaying] = useState(false);
  const [duration, setDuration] = useState<number>(durationPreview);

  const [trackProgress, setTrackProgress] = useState(0);

  const [isProgressChanging, setIsProgressChanging] = useState<boolean>(false);

  const intervalRef = useRef();

  useEffect(() => {
    return () => {
      audioRef.current.pause();
      clearInterval(intervalRef.current);
    };
  }, []);

  const onLoadedMetadata = () => {
    if (audioRef.current) {
      const currentDuration =
        audioRef.current.duration === Infinity
          ? durationPreview
          : audioRef.current.duration;
      setDuration(
        isNaN(currentDuration) || currentDuration === Infinity
          ? durationPreview
          : currentDuration
      );
    } else {
      setDuration(durationPreview);
    }
  };

  const handlePlay = () => {
    if (!isPlaying) {
      audioRef.current.play();
      setIsPlaying(true);
    } else {
      audioRef.current.pause();
      setIsPlaying(false);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setTrackProgress(audioRef.current.currentTime);

      if (audioRef.current.ended) {
        const currentDuration =
          audioRef.current.duration === Infinity
            ? durationPreview
            : audioRef.current.duration;
        setDuration(isNaN(currentDuration) ? 0 : currentDuration);
        setTrackProgress(0);
        setIsPlaying(false);
        audioRef.current.pause();
      }
    }, 50);
    if (!isPlaying) {
      clearInterval(interval);
    }

    return () => clearInterval(interval);
  }, [isPlaying, isProgressChanging]);

  useEffect(() => {
    const currentDuration =
      audioRef.current.duration === Infinity
        ? durationPreview
        : audioRef.current.duration;
    setDuration(
      isNaN(currentDuration) || currentDuration - trackProgress < 0
        ? 0
        : currentDuration - trackProgress
    );
  }, [trackProgress]);

  const handleProgress = (newProgress: number) => {
    setTrackProgress(newProgress);
    setIsProgressChanging(!isProgressChanging);
    audioRef.current.play();
    setIsPlaying(true);
    audioRef.current.currentTime = newProgress;
  };

  useEffect(() => {
    if (isOtherPlaying) {
      audioRef.current.pause();
      setIsPlaying(false);
    }
  }, [isOtherPlaying]);

  return (
    <>
      <Container>
        <IconContainer
          onClick={() => {
            handlePlay();
          }}
        >
          <Icon type={!isPlaying ? "play" : "pause"} color="neutral/70" />
        </IconContainer>
        <Spacer x={8} />
        <AudioProgressContainer>
          <input
            type="range"
            step="0.01"
            min="0"
            max={
              isNaN(audioRef.current.duration)
                ? 0
                : audioRef.current.duration === Infinity
                ? durationPreview
                : audioRef.current.duration
            }
            value={trackProgress}
            onChange={(evt) => handleProgress(Number(evt.target.value))}
          />
        </AudioProgressContainer>
        <Spacer x={10} />
        <DurationContainer>
          <Typography variant="p" color="neutral">
            {formatTimerMinutesSeconds(duration)}
          </Typography>
        </DurationContainer>

        {deleteFunction && (
          <>
            <Spacer x={14} />
            <LineDivision />
            <Spacer x={18} />
            <IconContainer onClick={() => deleteFunction(audioSource)}>
              <Icon type="close-circle" color="neutral/80" />
            </IconContainer>
          </>
        )}
      </Container>

      <audio
        ref={audioRef}
        preload="auto"
        onLoadedMetadata={onLoadedMetadata}
        src={audioSource}
      >
        <source src={audioSource} type="audio" />
      </audio>
    </>
  );
};

export default AudioPlayer;
