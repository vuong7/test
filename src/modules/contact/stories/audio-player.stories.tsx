import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AudioPlayer, AudioPlayerProps } from "..";

export default {
  title: "Contact/AudioPlayer",
  component: AudioPlayer,
} as ComponentMeta<typeof AudioPlayer>;

const Template: Story<AudioPlayerProps> = (args) => (
  <AudioPlayer
    audioSource="http://codeskulptor-demos.commondatastorage.googleapis.com/descent/background%20music.mp3"
    {...args}
  >
    Example of AudioPlayer
  </AudioPlayer>
);

export const Default = Template.bind({});

export const Delete = Template.bind({});
Delete.args = {
  deleteFunction: () => console.log("delete"),
};
