import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { MultiAudio, MultiAudioProps } from "..";

export default {
  title: "Contact/MultiAudio",
  component: MultiAudio,
} as ComponentMeta<typeof MultiAudio>;

const Template: Story<MultiAudioProps> = (args) => (
  <MultiAudio {...args}>Example of MultiAudio</MultiAudio>
);

export const Default = Template.bind({});
Default.args = {
  audios: [
    "http://codeskulptor-demos.commondatastorage.googleapis.com/descent/background%20music.mp3",
    "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
  ],
};
