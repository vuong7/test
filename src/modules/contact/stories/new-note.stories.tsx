import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { NewNote, NewNoteProps } from "..";

import { Textarea } from "../../";

export default {
  title: "Contact/NewNote",
  component: NewNote,
} as ComponentMeta<typeof NewNote>;

const Template: Story<NewNoteProps> = (args) => (
  <NewNote {...args}>Example of NewNote</NewNote>
);

export const Default = Template.bind({});
Default.args = {
  textInput: <Textarea />,
  hasAttachment: true,
  hasRecord: true,
  onClickUpdateTags: () => console.log("updated"),
  onCancel: () => console.log("cancel"),
  onSave: (note) => console.log(note),
  onOpenRecord: () => console.log("open record"),
};
