import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { TagMessage, TagMessageProps } from "..";

export default {
  title: "Contact/TagMessage",
  component: TagMessage,
} as ComponentMeta<typeof TagMessage>;

const Template: Story<TagMessageProps> = (args) => (
  <TagMessage {...args}>Example of Nav Tags text</TagMessage>
);

export const Default = Template.bind({});

export const Negative = Template.bind({});
Negative.args = {
  isNegative: true,
};
