import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { ButtonSaveCancel, ButtonSaveCancelProps } from "..";

export default {
  title: "Contact/ButtonSaveCancel",
  component: ButtonSaveCancel,
} as ComponentMeta<typeof ButtonSaveCancel>;

const Template: Story<ButtonSaveCancelProps> = (args) => (
  <ButtonSaveCancel {...args}></ButtonSaveCancel>
);

export const Save = Template.bind({});
Save.args = { variant: "save" };

export const Cancel = Template.bind({});
Cancel.args = { variant: "cancel" };
