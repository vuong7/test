import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { PreviewAttachments, PreviewAttachmentsProps } from "..";

export default {
  title: "Contact/PreviewAttachments",
  component: PreviewAttachments,
} as ComponentMeta<typeof PreviewAttachments>;

const Template: Story<PreviewAttachmentsProps> = (args) => (
  <PreviewAttachments {...args}>
    Example of PreviewAttachments
  </PreviewAttachments>
);

export const Default = Template.bind({});
Default.args = {
  file: "https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_170,w_170,f_auto,b_white,q_auto:eco,dpr_1/n4i3nejj7rygnhh5dqxn",
};

export const Video = Template.bind({});
Video.args = {
  file: "https://vod-progressive.akamaized.net/exp=1648143068~acl=%2Fvimeo-prod-skyfire-std-us%2F01%2F3988%2F20%2F519943720%2F2424471569.mp4~hmac=8585a8c29287a460de79087915bfbe306e7687ff4c3c05219b6a5d8faba5035e/vimeo-prod-skyfire-std-us/01/3988/20/519943720/2424471569.mp4?filename=pexels-artem-podrez-7049271.mp4",
};
