import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { ContactTag, ContactTagProps } from "..";

export default {
  title: "Contact/ContactTag",
  component: ContactTag,
} as ComponentMeta<typeof ContactTag>;

const Template: Story<ContactTagProps> = (args) => (
  <ContactTag {...args}>Example of Nav Tags text</ContactTag>
);

export const Default = Template.bind({});
Default.args = {
  isAdded: false,
  name: "Business",
  addTag: () => console.log("added"),
  removeTag: () => console.log("removed"),
};

export const Added = Template.bind({});
Added.args = {
  isAdded: true,
  name: "Alternative",
  addTag: () => console.log("added"),
  removeTag: () => console.log("removed"),
};
