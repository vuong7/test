import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { NavTags, NavTagsProps } from "..";

export default {
  title: "Contact/NavTags",
  component: NavTags,
} as ComponentMeta<typeof NavTags>;

const Template: Story<NavTagsProps> = (args) => (
  <NavTags {...args}>Example of Nav Tags text</NavTags>
);

export const Default = Template.bind({});
Default.args = {
  showPositive: () => console.log("positive"),
  showNegative: () => console.log("negative"),
};
