import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { DropDownMenu, DropDownMenuProps } from "..";

export default {
  title: "Contact/DropDownMenu",
  component: DropDownMenu,
} as ComponentMeta<typeof DropDownMenu>;

const Template: Story<DropDownMenuProps> = (args) => (
  <DropDownMenu {...args}>Example of Drop Down Menu</DropDownMenu>
);

export const Default = Template.bind({});
Default.args = {
  onUpdate: () => console.log("edit"),
  onDelete: () => console.log("delete"),
};
