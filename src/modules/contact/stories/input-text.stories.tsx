import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { InputText, InputTextProps } from "..";

export default {
  title: "Contact/InputText",
  component: InputText,
} as ComponentMeta<typeof InputText>;

const Template: Story<InputTextProps> = (args) => (
  <InputText {...args}>Example of Nav Tags text</InputText>
);

export const Default = Template.bind({});
Default.args = {
  handleText: (text: string) => console.log(`text: ${text}`),
};
