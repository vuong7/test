import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { NoteAttachment, NoteAttachmentProps } from "../";

export default {
  title: "Contact/NoteAttachment",
  component: NoteAttachment,
} as ComponentMeta<typeof NoteAttachment>;

const Template: Story<NoteAttachmentProps> = (args) => (
  <NoteAttachment {...args}>Example</NoteAttachment>
);

export const Default = Template.bind({});
Default.args = {
  attachment: {
    file: {
      name: "Global insurance market lorem ipsum dolor.mp4",
      type: "video/mp4",
    },
    name: "Global insurance market lorem ipsum dolor.mp4",
    fileType: "FILE",
    uploadedAt: new Date(),
  },
};

export const ViewMode = Template.bind({});
ViewMode.args = {
  attachment: {
    name: "Global insurance market lorem ipsum dolor",
    fileType: "FILE",
    uploadedAt: new Date(),
  },
};
