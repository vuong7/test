import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AttachmentsCount, AttachmentsCountProps } from "..";

export default {
  title: "Contact/AttachmentsCount",
  component: AttachmentsCount,
} as ComponentMeta<typeof AttachmentsCount>;

const Template: Story<AttachmentsCountProps> = (args) => (
  <AttachmentsCount
    count={2}
    {...args}
    showAttachmentFunction={() => console.log("show")}
  >
    Example of AttachmentsCount
  </AttachmentsCount>
);

export const Default = Template.bind({});
Default.args = {
  count: 2,
};
