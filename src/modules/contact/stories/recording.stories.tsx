import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Recording, RecordingProps } from "..";

export default {
  title: "Contact/Recording",
  component: Recording,
} as ComponentMeta<typeof Recording>;

const Template: Story<RecordingProps> = (args) => (
  <Recording {...args}></Recording>
);

export const Default = Template.bind({});
Default.args = {
  onSubmit: () => console.log("submit"),
  onClickDelete: () => console.log("delete"),
  uploadFile: (audio: Blob) => console.log(audio),
};
