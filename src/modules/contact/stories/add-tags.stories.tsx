import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AddTags, AddTagsProps } from "..";

export default {
  title: "Contact/AddTags",
  component: AddTags,
} as ComponentMeta<typeof AddTags>;

const Template: Story<AddTagsProps> = (args) => (
  <AddTags {...args}>Example of Nav Tags text</AddTags>
);

export const Default = Template.bind({});
Default.args = {
  contactTags: [
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
    { name: "Vietnam", markedAs: "unmarked", category: "region" },
    { name: "Alternative", markedAs: "negative", category: "business" },
    { name: "Business", markedAs: "positive", category: "business" },
  ],
  cancelFunction: () => console.log("cancel"),
  saveFunction: (newTagList) => console.log(newTagList),
};
