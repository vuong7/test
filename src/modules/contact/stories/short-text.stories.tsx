import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { ShortText, ShortTextProps } from "..";

export default {
  title: "Contact/ShortText",
  component: ShortText,
} as ComponentMeta<typeof ShortText>;

const Template: Story<ShortTextProps> = (args) => (
  <ShortText {...args}></ShortText>
);

export const Default = Template.bind({});
Default.args = {
  content:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor id mi arcu, eget pellentesque id. Ipsum in interdum congue massa orci. Aliquet luctus nisl, pharetra et. Scelerisque venenatis turpis arcu ultrices nisi, pellentesque lacus consectetur malesuada. Sit faucibus faucibus turpis ac nulla eget ornare. Sed non ullamcorper consequat in sagittis a tempor hendrerit pulvinar. Nunc sit diam lorem egestas cursus eget elit id donec.",
};

export const SmallText = Template.bind({});
SmallText.args = {
  content:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor id mi arcu, eget pellentesque id. Ipsum in interdum congue massa orci.",
};

export const Html = Template.bind({});
Html.args = {
  html: "<p><b>Note Text</b></p><p><i>more note text in here</i></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sedexercitationem voluptas velit voluptate voluptatum illo unde  voluptatibus excepturi, molestias officia molestiae deserunt quia rerum!Maiores quis necessitatibus eius praesentium perspiciatis!</p><p>Lorem</p>",
  maxLines: 3,
};

export const SmallHtml = Template.bind({});
SmallHtml.args = {
  html: "<p>Note Text</p><p>Small text</p>",
};
