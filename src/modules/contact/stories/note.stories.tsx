import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Note, NoteProps } from "..";

export default {
  title: "Contact/Note",
  component: Note,
} as ComponentMeta<typeof Note>;

const Template: Story<NoteProps> = (args) => (
  <div>
    <Note {...args}>Example of Note</Note>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  title: "Note title here",
  createdBy: {
    avatar:
      "https://isaojose.com.br/wp-content/uploads/2020/12/blank-profile-picture-mystery-man-avatar-973460.jpg",
    name: "Test User",
  },
  attachments: [
    {
      name: "Global insurance market lorem ipsum dolor.mp4",
      fileType: "FILE",
      uploadedAt: new Date(),
    },
    {
      name: "Global insurance market lorem ipsum dolor.png",
      fileType: "FILE",
      uploadedAt: new Date(),
    },
  ],
  audios: [
    "http://codeskulptor-demos.commondatastorage.googleapis.com/descent/background%20music.mp3",
    "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
  ],
  updatedAt: new Date(),
  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor id mi arcu, eget pellentesque id. Ipsum in interdum congue massa orci. Aliquet luctus nisl, pharetra et. Scelerisque venenatis turpis arcu ultrices nisi, pellentesque lacus consectetur malesuada. Sit faucibus faucibus turpis ac nulla eget ornare. Sed non ullamcorper consequat in sagittis a tempor hendrerit pulvinar. Nunc sit diam lorem egestas cursus eget elit id donec.",
  onUpdate: () => console.log("edit"),
  onDelete: () => console.log("delete"),
};

export const HTMLText = Template.bind({});
HTMLText.args = {
  title: "Note title here",
  createdBy: {
    avatar:
      "https://isaojose.com.br/wp-content/uploads/2020/12/blank-profile-picture-mystery-man-avatar-973460.jpg",
    name: "Test User",
  },
  attachments: [
    {
      name: "Global insurance market lorem ipsum dolor.mp4",
      fileType: "FILE",
      uploadedAt: new Date(),
    },
    {
      name: "Global insurance market lorem ipsum dolor.png",
      fileType: "FILE",
      uploadedAt: new Date(),
    },
  ],
  audios: [
    "http://codeskulptor-demos.commondatastorage.googleapis.com/descent/background%20music.mp3",
    "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3",
  ],
  updatedAt: new Date(),
  text: "<p><b>Note Text</b></p><p><i>more note text in here</i></p><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sedexercitationem voluptas velit voluptate voluptatum illo unde  voluptatibus excepturi, molestias officia molestiae deserunt quia rerum!Maiores quis necessitatibus eius praesentium perspiciatis!</p>",
  onUpdate: () => console.log("edit"),
  onDelete: () => console.log("delete"),
};

export const HTMLSmallText = Template.bind({});
HTMLSmallText.args = {
  title: "Note title here",
  createdBy: {
    avatar:
      "https://isaojose.com.br/wp-content/uploads/2020/12/blank-profile-picture-mystery-man-avatar-973460.jpg",
    name: "Test User",
  },
  updatedAt: new Date(),
  // text: "<p><b>Note Text</b></p><p>Small note text</p>",
  onUpdate: () => console.log("edit"),
  onDelete: () => console.log("delete"),
};
