import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { SearchTags, SearchTagsProps } from "..";

export default {
  title: "Contact/SearchTags",
  component: SearchTags,
} as ComponentMeta<typeof SearchTags>;

const Template: Story<SearchTagsProps> = (args) => (
  <SearchTags {...args}>Example of Nav Tags text</SearchTags>
);

export const Default = Template.bind({});
Default.args = {
  handleSearch: (name: string, category: string) =>
    console.log(`name: ${name} category: ${category}`),
};
