import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { InputSelection, InputSelectionProps } from "..";

export default {
  title: "Contact/InputSelection",
  component: InputSelection,
} as ComponentMeta<typeof InputSelection>;

const Template: Story<InputSelectionProps> = (args) => (
  <InputSelection {...args}>Example of Nav Tags text</InputSelection>
);

export const Default = Template.bind({});
Default.args = {
  handleSelection: (selected: string) => console.log(`selected: ${selected}`),
  placeholder: "All categories",
  choices: ["category 1", "category 2", "category 3"],
};
