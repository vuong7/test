import React, { FC, useState } from "react";
import { Container, IconContainer, NameInput } from "./styles";

import { Typography, InlineWrapper, Spacer, Icon } from "../../..";
import { useTranslation } from "react-i18next";

import { IconTypes } from "src/modules/core/atoms/icon/svgs";

export type InputTextProps = {
  handleText: (text: string) => void;
  placeholder?: string;
  icon?: IconTypes;
};

export const InputText: FC<InputTextProps> = ({
  handleText,
  placeholder = "Type for search",
  icon,
}) => {
  const { t } = useTranslation();

  const [text, setText] = useState<string>("");

  return (
    <Container>
      {icon && (
        <IconContainer>
          <Icon type={icon} color="neutral/90" size="sm" />
        </IconContainer>
      )}
      <Typography variant="label" color="neutral/80">
        <NameInput
          placeholder={placeholder}
          onChange={(evt) => handleText(evt.target.value)}
          isIcon={!!icon}
        />
      </Typography>
    </Container>
  );
};

export default InputText;
