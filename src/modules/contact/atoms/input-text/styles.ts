import styled from "styled-components";

type Props = {
  isIcon?: boolean;
};

export const Container = styled.div`
  position: relative;
  display: flex;
  align-items: center;
`;

export const IconContainer = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 50%;
  left: 12px;
  transform: translateY(-50%);
`;

export const NameInput = styled.input<Props>`
  font-weight: inherit;
  font-size: inherit;
  line-height: inherit;
  color: inherit;
  box-sizing: border-box;
  padding-left: ${(props) => (props.isIcon ? "38px" : "12px")};
  padding-top: 12px;
  padding-right: 12px;
  padding-bottom: 12px;
  border: 1px solid ${(props) => props.theme.neutral[20]};
  border-radius: 8px;
  outline: none;
  background-color: ${(props) => props.theme.system.light1};

  &:focus {
    color: ${(props) => props.theme.neutral};
    transition: 0.4s;
  }
  &:not(:placeholder-shown) {
    color: ${(props) => props.theme.neutral};
    border: 1px solid ${(props) => props.theme.neutral[20]};
  }
`;
