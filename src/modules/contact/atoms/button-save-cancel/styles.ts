import styled from "styled-components";

type ButtonProps = {
  isDisabled?: boolean;
};

export const SaveButton = styled.button<ButtonProps>`
  background-color: ${(props) =>
    props.isDisabled ? props.theme.neutralTs[40] : props.theme.primary};

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 8px 32px;
  border: 1px solid #ffffff00;
  border-radius: 8px;
  box-sizing: border-box;
  text-align: center;
  text-transform: uppercase;

  &:hover {
    cursor: pointer;
    opacity: 0.8;
  }
`;

export const CancelButton = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 8px 32px;
  border: 1px solid #ffffff00;
  border-radius: 8px;
  box-sizing: border-box;
  background-color: #ffffff00;
  text-transform: uppercase;

  &:hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;
