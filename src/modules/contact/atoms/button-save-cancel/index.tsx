import React, { FC } from "react";

import { SaveButton, CancelButton } from "./styles";

import { Typography } from "../../../";
import { useTranslation } from "react-i18next";

export type ButtonSaveCancelVariant = "save" | "cancel";

const mappedStyles: Record<ButtonSaveCancelVariant, any> = {
  save: SaveButton,
  cancel: CancelButton,
};

export type ButtonSaveCancelProps = {
  variant: ButtonSaveCancelVariant;
  onClick(): void;
  isDisabled?: boolean;
};

export const ButtonSaveCancel: FC<ButtonSaveCancelProps> = ({
  variant,
  onClick,
  isDisabled,
  children,
}) => {
  const { t } = useTranslation();

  const Component = mappedStyles[variant];
  return (
    <Component
      onClick={onClick}
      disabled={!!isDisabled}
      isDisabled={!!isDisabled}
    >
      <Typography
        variant="p-semi-bold"
        color={variant === "save" ? "system/light1" : "primary"}
      >
        {children}
      </Typography>
    </Component>
  );
};

export default ButtonSaveCancel;
