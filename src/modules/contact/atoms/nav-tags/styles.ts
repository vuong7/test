import styled from "styled-components";

type Props = {
  isActive: boolean;
};

export const Container = styled.div``;

export const TagContainer = styled.div<Props>`
  padding-bottom: 4px;
  border-bottom: 2px solid
    ${(props) => (props.isActive ? props.theme.primary : "#FFFFFF00")};

  &:hover {
    cursor: pointer;
  }
`;
