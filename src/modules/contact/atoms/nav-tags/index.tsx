import React, { FC, useState } from "react";
import { Container, TagContainer } from "./styles";

import { Typography, InlineWrapper, Spacer } from "../../../core";
import { useTranslation } from "react-i18next";

export type NavTagsProps = {
  showPositive: () => void;
  showNegative: () => void;
};

export const NavTags: FC<NavTagsProps> = ({ showPositive, showNegative }) => {
  const { t } = useTranslation();

  const [isPositive, setIsPositive] = useState<boolean>(true);

  return (
    <Container>
      <InlineWrapper>
        <TagContainer
          onClick={() => {
            setIsPositive(true);
            showPositive();
          }}
          isActive={isPositive}
        >
          <Typography
            variant="h4"
            color={isPositive ? "primary" : "neutral/70"}
          >
            Positive Tags
          </Typography>
        </TagContainer>
        <Spacer x={26} />
        <TagContainer
          onClick={() => {
            setIsPositive(false);
            showNegative();
          }}
          isActive={!isPositive}
        >
          <Typography
            variant="h4"
            color={!isPositive ? "primary" : "neutral/70"}
          >
            Negative Tags
          </Typography>
        </TagContainer>
      </InlineWrapper>
    </Container>
  );
};

export default NavTags;
