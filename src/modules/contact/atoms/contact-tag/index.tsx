import React, { FC, useState, useEffect } from "react";
import { Container, TagContainer, IconContainer } from "./styles";

import { Typography, Spacer, Icon } from "../../../core";
import { useTranslation } from "react-i18next";

import { TagInfo } from "../../";

export type ContactTagProps = {
  tag: TagInfo;
  isPositive: boolean;
  handleTag: (tag: TagInfo) => void;
};

export const ContactTag: FC<ContactTagProps> = ({
  tag,
  isPositive,
  handleTag,
}) => {
  const [isOn, setIsOn] = useState<boolean>(false);

  useEffect(() => {
    setIsOn(
      (isOn) =>
        (tag.markedAs === "positive" && isPositive) ||
        (tag.markedAs === "negative" && !isPositive)
    );
  }, [isPositive]);

  const addHandler = () => {
    setIsOn(!isOn);
  };

  return (
    <Container>
      <TagContainer
        isOn={isOn}
        isPositive={isPositive}
        onClick={() => {
          addHandler();
          handleTag(tag);
        }}
      >
        {isPositive ? (
          <Typography
            variant="titles"
            color={isOn ? "system/light1" : "system/neutral"}
          >
            {tag.name}
          </Typography>
        ) : (
          <Typography variant="h3">
            <span className="negative-tag">{tag.name}</span>
          </Typography>
        )}
        <Spacer x={8} />
        <IconContainer>
          {isPositive ? (
            <Icon
              type={isOn ? "check" : "plus"}
              color={isOn ? "system/light1" : "system/neutral"}
              size="sm"
            />
          ) : (
            <Icon
              type={isOn ? "check" : "plus"}
              customColor="#EB5757"
              size="sm"
            />
          )}
        </IconContainer>
      </TagContainer>
    </Container>
  );
};

export default ContactTag;
