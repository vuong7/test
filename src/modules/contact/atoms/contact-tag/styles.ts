import styled from "styled-components";

type Props = {
  isOn: boolean;
  isPositive: boolean;
};

export const Container = styled.div`
  display: inline-block;
  margin-bottom: 12px;
`;

export const TagContainer = styled.div<Props>`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 7px 8px;
  background-color: ${(props) =>
    props.isOn && props.isPositive
      ? props.theme.system.neutral
      : !props.isOn && props.isPositive
      ? props.theme.system.light1
      : props.isOn && !props.isPositive
      ? props.theme.contactTags.negative1
      : props.theme.system.light1};
  border: 1px solid
    ${(props) =>
      props.isPositive
        ? props.theme.system.neutral
        : props.theme.contactTags.negative2};
  border-radius: 100px;

  &:hover {
    cursor: pointer;
  }

  .negative-tag {
    color: ${(props) => props.theme.contactTags.negative2};
  }
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
