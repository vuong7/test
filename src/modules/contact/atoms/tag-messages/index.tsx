import React, { FC, useState } from "react";
import { Container } from "./styles";

import { Typography, InlineWrapper, Spacer, Alert } from "../../../core";
import { useTranslation } from "react-i18next";

export type TagMessageProps = {
  isPositive: boolean;
};

export const TagMessage: FC<TagMessageProps> = ({ isPositive }) => {
  const { t } = useTranslation();

  return (
    <Container>
      <InlineWrapper>
        <Typography variant="p" color="neutral">
          {t(
            "Are there any other tags that you would like to give the contact? You will be able to add more tags at any moment."
          )}
        </Typography>
      </InlineWrapper>
      <Spacer y={6} />

      {isPositive ? (
        <Alert typography="h3">
          {t(
            "Tags are important because they are used to suggest sales matches"
          )}
        </Alert>
      ) : (
        <Alert variant="error" typography="titles">
          {t(
            "We use this information to calculate what does NOT match with the contact. "
          )}
        </Alert>
      )}
    </Container>
  );
};

export default TagMessage;
