import styled from "styled-components";
import { ScrollbarStyled } from "../../../";

type SelectInputProps = {
  isSelecting?: boolean;
};

export const Container = styled.div`
  position: relative;
  height: 40px;
  box-sizing: border-box;
  /* overflow-y: hidden; */
`;

export const IconContainer = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  right: 12px;
  top: 50%;
  transform: translateY(-50%);
`;

export const SelectInput = styled.div<SelectInputProps>`
  font-weight: inherit;
  font-size: inherit;
  line-height: inherit;
  color: inherit;
  padding: 12px;
  padding-right: 38px;
  outline: none;
  border: 1px solid
    ${(props) =>
      props.isSelecting ? props.theme.highlight : "rgba(205, 208, 227, 0.6)"};
  border-radius: 8px;
  -webkit-appearance: none;
  background-color: ${(props) => props.theme.system.light1};

  display: flex;
  align-items: center;

  width: 100%;
  height: 40px;
  box-sizing: border-box;

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  outline: none;

  &:hover {
    cursor: pointer;
  }

  &:focus {
    outline: none;
    border: 1px solid ${(props) => props.theme.highlight};
  }
`;

export const Option = styled.div`
  font-weight: inherit;
  font-size: inherit;
  line-height: inherit;
  color: inherit;
  padding: 12px;
  outline: none;

  height: 40px;
  box-sizing: border-box;
  border: 1px solid #ffffff00;
  background-color: ${(props) => props.theme.system.light1};

  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover {
    background-color: ${(props) => props.theme.contactTags.systemAdded};
  }
`;

export const OptionsContainer = styled.div`
  overflow-y: auto;
  max-height: 400px;

  outline: none;
  border: 1px solid rgba(205, 208, 227, 0.6);
  border-radius: 8px;

  ${ScrollbarStyled}
`;
