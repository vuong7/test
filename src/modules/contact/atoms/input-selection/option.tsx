import React, { FC, useState } from "react";
import { Container, IconContainer, SelectInput, Option } from "./styles";

import { Typography, InlineWrapper, Spacer, Icon } from "../../..";
import { useTranslation } from "react-i18next";

export type SelectionOptionProps = {
  value: string;
  onChange(value: string): void;
};

export const SelectionOption: FC<SelectionOptionProps> = ({
  value,
  onChange,
}) => {
  const { t } = useTranslation();

  const handleClick = (evt: React.MouseEvent) => {
    evt.stopPropagation();
    onChange(value);
  };

  return (
    <Option onClick={(evt) => handleClick(evt)}>
      <Typography variant="label" color="neutral">
        {value}
      </Typography>
    </Option>
  );
};

export default SelectionOption;
