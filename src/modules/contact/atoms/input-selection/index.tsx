import React, { FC, useState } from "react";
import {
  Container,
  IconContainer,
  SelectInput,
  OptionsContainer,
} from "./styles";
import SelectionOption from "./option";
import { useComponentVisible } from "../../../";

import { Typography, Spacer, Icon } from "../../..";
import { useTranslation } from "react-i18next";

export type InputSelectionProps = {
  handleSelection: (selected: string) => void;
  placeholder?: string;
  choices: string[];
};

export const InputSelection: FC<InputSelectionProps> = ({
  handleSelection,
  placeholder,
  choices,
}) => {
  const { t } = useTranslation();

  const [currentValue, setCurrentValue] = useState<string>("");

  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const handleOption = (value: string) => {
    handleSelection(value);
    setIsComponentVisible(false);
    setCurrentValue(value);
  };

  return (
    <Container ref={ref}>
      <SelectInput
        onClick={(evt) => {
          evt.stopPropagation();
          setIsComponentVisible(!isComponentVisible);
        }}
        isSelecting={isComponentVisible}
      >
        <Typography variant="label" color="neutral/80">
          {currentValue !== "" ? currentValue : placeholder}
        </Typography>
        <IconContainer>
          <Icon type="arrow-down" color="neutral/80" size="sm" />
        </IconContainer>
      </SelectInput>
      <Spacer y={4} />
      {isComponentVisible && choices.length > 0 && (
        <OptionsContainer>
          {choices
            .filter((option, index, array) => array.indexOf(option) === index)
            .map((option, index) => (
              <SelectionOption
                key={index}
                value={option}
                onChange={handleOption}
              >
                {option}
              </SelectionOption>
            ))}
        </OptionsContainer>
      )}
    </Container>
  );
};

export default InputSelection;
