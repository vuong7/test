import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  background-color: ${(props) => props.theme.system.light3};
  border: 1px solid ${(props) => props.theme.neutralTs[20]};
  border-radius: 8px;
  padding: 16px;
  box-sizing: border-box;
`;

export const CornerIcons = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;
`;

export const ExpandContainer = styled.div`
  &:hover {
    cursor: pointer;
  }
`;

export const NoteInfo = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const UserInfo = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
