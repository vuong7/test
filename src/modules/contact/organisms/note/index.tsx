import React, { FC, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  CornerIcons,
  ExpandContainer,
  NoteInfo,
  UserInfo,
} from "./styles";

import {
  ShortText,
  DropDownMenu,
  AttachmentsCount,
  MultiAudio,
  PreviewAttachments,
  NoteAttachment,
  NoteAttachmentData,
} from "../../";

import {
  Typography,
  Icon,
  InlineWrapper,
  Spacer,
  AvatarImage,
  formatFullDateAndTime,
} from "../../../";

type NoteCreatedByProps = {
  name: string;
  avatar?: string;
};

export type NoteProps = {
  title: string;
  text?: string;
  createdBy: NoteCreatedByProps;
  updatedAt: Date;
  audios?: string[];
  attachments?: NoteAttachmentData[];
  isExpandable?: boolean;
  onUpdate?: () => void;
  onDelete?: () => void;
  onExpand?(): void;
};

export const Note: FC<NoteProps> = ({
  title,
  text,
  createdBy,
  updatedAt,
  audios,
  attachments = [],
  isExpandable,
  onUpdate,
  onDelete,
  onExpand,
}) => {
  const { t } = useTranslation();

  const [isPreviewShown, setIsPreviewShown] = useState<boolean>(true);

  return (
    <Container>
      <InlineWrapper>
        <Icon type="note" size="sm" color="neutral/80" />
        <Spacer x={10} />
        <Typography variant="titles">{title}</Typography>
        <CornerIcons>
          {isExpandable && (
            <ExpandContainer onClick={onExpand}>
              <Icon type="maximize" size="sm" color="neutral/80" />
            </ExpandContainer>
          )}

          <DropDownMenu onUpdate={onUpdate} onDelete={onDelete} />
        </CornerIcons>
      </InlineWrapper>
      <Spacer y={4} />
      {audios && audios.length > 0 && <MultiAudio audios={audios} />}

      {text && (
        <>
          <InlineWrapper>
            <ShortText html={text}></ShortText>
          </InlineWrapper>
          <Spacer y={4} />
        </>
      )}
      <Spacer y={10} />
      <NoteInfo>
        <UserInfo>
          <AvatarImage
            url={createdBy.avatar}
            customSize={28}
            name={createdBy.name}
          ></AvatarImage>
          <Spacer x={4} />
          <Typography variant="p-small" color="neutral/80">
            {t("contact.notes.lastUpdate", [formatFullDateAndTime(updatedAt)])}
            <br />
            {t("contact.notes.createdBy", [createdBy.name])}
          </Typography>
        </UserInfo>
        {attachments.length > 0 && (
          <AttachmentsCount
            count={attachments.length}
            showAttachmentFunction={() => setIsPreviewShown(!isPreviewShown)}
          />
        )}
      </NoteInfo>
      {isPreviewShown &&
        attachments.map((attachment, index) => (
          <>
            <Spacer key={`spacer-${index}`} y={14} />
            <NoteAttachment
              key={`attachment-${index}`}
              attachment={attachment}
            />
          </>
        ))}
    </Container>
  );
};

export default Note;
