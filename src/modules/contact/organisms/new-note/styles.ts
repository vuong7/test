import styled from "styled-components";

type CardContainerProps = {
  isDragged?: boolean;
  isMovingUp?: boolean;
  isMovingDown?: boolean;
  distance?: number;
};

export const Container = styled.div`
  width: 100%;
  background-color: ${(props) => props.theme.system.light3};
  border: 1px solid ${(props) => props.theme.neutralTs[20]};
  border-radius: 8px;
  padding: 16px;
  box-sizing: border-box;
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const InputContainer = styled.div`
  display: flex;
  align-items: center;
  flex-grow: 1;

  * {
    width: 90%;
  }
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const TitleInput = styled.input`
  width: 100%;
  color: inherit;
  font-weight: inherit;
  font-size: inherit;
  line-height: inherit;
  border: 1px solid #ffffff00;
  outline: none;
  transition: 0.2s;
  padding-bottom: 2.71px;
  background-color: #ffffff00;

  &::placeholder {
    color: inherit;
    font-weight: inherit;
    font-size: inherit;
    line-height: inherit;
    border: 1px solid #ffffff00;
  }

  &:focus {
    color: ${(props) => props.theme.neutral};
    transition: 0.4s;
    border: 1px solid #ffffff00;
    border-bottom: 1px solid ${(props) => props.theme.highlight};
  }
  &:not(:placeholder-shown) {
    color: ${(props) => props.theme.neutral};
  }
`;

export const UpdateTags = styled.div`
  display: flex;
  align-items: center;
  padding: 12px 4px 12px 16px;
  background-color: ${(props) => props.theme.system.light2};
  border-radius: 64px;

  &:hover {
    cursor: pointer;
  }

  &:hover span {
    text-decoration: underline;
  }
`;

export const AttachmentsTitleAndCount = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 23px;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const CardContainer = styled.div<CardContainerProps>`
  position: relative;
  opacity: ${(props) => (props.isDragged ? 0 : 1)};

  ${(props) =>
    props.isMovingDown
      ? `transform: translateY(-${props.distance}px); transition: transform 0.5s;`
      : ""}
  ${(props) =>
    props.isMovingUp
      ? `transform: translateY(${props.distance}px); transition: transform 0.5s;`
      : ""}
`;

export const FieldWrapper = styled.div`
  position: relative;
  height: 96px;
`;

export const FieldControl = styled.div`
  width: 100%;
  height: 80%;
  position: absolute;
  top: 70%;
  left: 0;
  background-color: #00000000;
  transform: translateY(-50%);
  z-index: 1;
`;
