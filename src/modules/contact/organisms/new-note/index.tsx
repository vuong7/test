import React, { FC, useState, useEffect, useCallback } from "react";

import {
  Container,
  TitleContainer,
  InputContainer,
  IconContainer,
  TitleInput,
  UpdateTags,
  AttachmentsTitleAndCount,
  ButtonsContainer,
  CardContainer,
  FieldWrapper,
  FieldControl,
} from "./styles";

import {
  AttachmentsCount,
  MultiAudio,
  ButtonSaveCancel,
  NoteAttachment,
} from "../../";

import { Typography, Icon, InlineWrapper, Spacer, DropArea } from "../../../";
import { useTranslation } from "react-i18next";
import { v4 } from "uuid";

export type NoteAttachmentData = {
  id?: string;
  file?: File;
  name?: string;
  fileType?: string;
  externalUrl?: string;
  uploadedAt: Date;
  uuidKey?: string;
};

type AudioData = {
  id?: string;
  url: string;
};

type NoteData = {
  title?: string;
  audioList?: AudioData[];
  attachments?: NoteAttachmentData[];
};

export type NewNoteProps = {
  hasAttachment?: boolean;
  hasRecord?: boolean;
  isExpandable?: boolean;
  noteInfo?: NoteData;
  textInput: JSX.Element;
  onCancel(): void;
  onSave(Data: NoteData): void;
  onClickUpdateTags?(): void;
  onOpenRecord?(): void;
  onExpand?(): void;
  onChangeNoteInfo?(newNoteInfo: NoteData): void;
};

export const NewNote: FC<NewNoteProps> = ({
  hasAttachment = true,
  hasRecord = true,
  isExpandable,
  noteInfo,
  textInput,
  onCancel,
  onSave,
  onClickUpdateTags,
  onOpenRecord,
  onExpand,
  onChangeNoteInfo,
}) => {
  const { t } = useTranslation();

  const [noteTitle, setNoteTitle] = useState<string>("");
  // const [noteText, setNoteText] = useState<string>();
  const [audioList, setAudioList] = useState<AudioData[]>([]);
  const [attachments, setAttachments] = useState<NoteAttachmentData[]>([]);

  const [draggedIndex, setDraggedIndex] = useState<number>(0);
  const [dropIndex, setDropIndex] = useState<number>(0);
  const [isValidDrop, setIsValidDrop] = useState<boolean>(false);

  const [isDragging, setIsDragging] = useState<boolean>(false);
  const [movingIndexes, setMovingIndexes] = useState<number[]>([]);

  const [previewAudioList, setPreviewAudioList] = useState<string[]>([]);

  useEffect(() => {
    if (noteInfo) {
      setNoteTitle(noteInfo.title || noteTitle);
      setAudioList(noteInfo.audioList || audioList);
      setAttachments(noteInfo.attachments || attachments);
    }
  }, [noteInfo]);

  useEffect(() => {
    if (onChangeNoteInfo) {
      onChangeNoteInfo({
        title: noteTitle,
        audioList: audioList,
        attachments: attachments,
      });
    }
  }, [noteTitle, audioList, attachments]);

  const deleteAudio = (audioToDelete: string) => {
    setAudioList([...audioList.filter((audio) => audio.url !== audioToDelete)]);
  };

  const handleSave = () => {
    onSave({
      title: noteTitle,
      audioList: audioList,
      attachments: attachments.map((attachment) => {
        const { uuidKey, ...data } = attachment;
        return { ...data };
      }),
    });
  };

  const handleDropFile = (files: File[]) => {
    setAttachments([
      ...attachments,
      ...files.map((file) => {
        return {
          file: file,
          uploadedAt: new Date(),
          name: file.name,
          fileType: "FILE",
          uuidKey: v4(),
        };
      }),
    ]);
  };

  const handleRename = (
    newName: string,
    indexToChange: number,
    isFile: boolean
  ) => {
    if (isFile) {
      let attachmentToChange = attachments.find(
        (attachments, index) => index === indexToChange
      );
      const changedFile = attachmentToChange?.file
        ? new File([attachmentToChange?.file as File], newName, {
            type: attachmentToChange.file?.type,
          })
        : undefined;
      setAttachments(
        attachments.map((attachment, index) =>
          index === indexToChange
            ? { ...attachment, file: changedFile, name: newName }
            : attachment
        )
      );
    } else {
      setAttachments(
        attachments.map((attachment, index) =>
          index === indexToChange
            ? { ...attachment, externalUrl: newName }
            : attachment
        )
      );
    }
  };

  const removeAttachment = (indexToRemove: number) => {
    setAttachments(
      attachments.filter((attachment, index) => index !== indexToRemove)
    );
  };

  const handleDragStart = useCallback((originIndex: number) => {
    setDraggedIndex(originIndex);
    setDropIndex(originIndex);
    setIsDragging(true);
    setMovingIndexes([]);
  }, []);

  const handleDragOver = useCallback(
    (evt: React.DragEvent, destinyIndex: number) => {
      evt.stopPropagation();
      setIsValidDrop(true);
      setDropIndex(destinyIndex);
    },
    []
  );

  const handleDragEnd = useCallback(() => {
    setMovingIndexes([]);
    setIsDragging(false);
    if (isValidDrop) {
      const [draggedAttachment] = attachments.splice(draggedIndex, 1);
      attachments.splice(dropIndex, 0, draggedAttachment);
      setAttachments([...attachments]);
    }
    setDraggedIndex(0);
    setDropIndex(0);
  }, [isValidDrop, draggedIndex, dropIndex]);

  useEffect(() => {
    if (isDragging) {
      setMovingIndexes([draggedIndex, dropIndex]);
    }
  }, [isDragging, draggedIndex, dropIndex]);

  useEffect(() => {
    setPreviewAudioList(audioList.map((audio) => audio.url));
  }, [audioList]);

  return (
    <Container onMouseLeave={() => setIsValidDrop(false)}>
      <TitleContainer>
        <Icon type="note" size="sm" color="neutral/80" />
        <Spacer x={8} />
        <InputContainer>
          <Typography variant="titles" color="neutral/40">
            <TitleInput
              placeholder={t("contact.notes.typeTitle")}
              value={noteTitle}
              onChange={(evt) => setNoteTitle(evt.target.value)}
            />
          </Typography>
        </InputContainer>

        {isExpandable && (
          <>
            <Spacer x={10} />
            <IconContainer onClick={onExpand}>
              <Icon type="maximize" size="sm" color="neutral/80" />
            </IconContainer>
          </>
        )}

        {hasRecord && (
          <>
            <Spacer x={10} />
            <IconContainer onClick={onOpenRecord}>
              <Icon type="mic" size="sm" color="primary" />
            </IconContainer>
          </>
        )}
      </TitleContainer>
      {audioList.length > 0 && (
        <>
          <Spacer y={8} />
          <MultiAudio audios={previewAudioList} deleteAudio={deleteAudio} />
        </>
      )}
      <Spacer y={audioList.length > 0 ? 8 : 10} />

      {textInput}
      <Spacer y={8} />
      {onClickUpdateTags && (
        <>
          <UpdateTags onClick={() => onClickUpdateTags()}>
            <Spacer x={4} />
            <Icon type="tag" color="primary" />
            <Spacer x={14} y={24} />
            <div>
              <Typography variant="p" color="neutral">
                {t("contact.notes.updateTag")}
              </Typography>
              <Typography variant="p-semi-bold" color="primary">
                <span>{t("contact.notes.clickUpdate")}</span>
              </Typography>
            </div>
          </UpdateTags>
          <Spacer y={8} />{" "}
        </>
      )}

      {hasAttachment && (
        <>
          <AttachmentsTitleAndCount>
            <Typography variant="small-titles" color="primary">
              {t("contact.notes.attachments")}
            </Typography>
            {attachments.length > 0 && (
              <AttachmentsCount
                count={attachments.length}
                showAttachmentFunction={() => null}
                isPreviewAlwaysShown
              />
            )}
          </AttachmentsTitleAndCount>
          <Spacer y={8} />
          <DropArea onDrop={handleDropFile} variant="small" />
          <Spacer y={4} />
          {attachments.map((attachment, index) => (
            <FieldWrapper
              key={`wrapper-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
              draggable={true}
              onDragStart={() => handleDragStart(index)}
              onDragEnd={handleDragEnd}
            >
              <CardContainer
                key={`container-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
                isDragged={isDragging && movingIndexes[0] === index}
                isMovingUp={
                  isDragging &&
                  index !== movingIndexes[0] &&
                  movingIndexes[0] > movingIndexes[1] &&
                  index >= movingIndexes[1] &&
                  index < movingIndexes[0]
                }
                isMovingDown={
                  isDragging &&
                  index !== movingIndexes[0] &&
                  movingIndexes[0] < movingIndexes[1] &&
                  index <= movingIndexes[1] &&
                  index > movingIndexes[0]
                }
                distance={96}
              >
                <Spacer y={4} />
                <NoteAttachment
                  key={`attachment-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
                  attachment={attachment}
                  isEditMode={true}
                  onClickRename={(newName: string, isFile: boolean) =>
                    handleRename(newName, index, isFile)
                  }
                  onClickDelete={() => removeAttachment(index)}
                />
                <Spacer y={4} />
              </CardContainer>
              {isDragging && (
                <FieldControl
                  onDragOver={(evt) => handleDragOver(evt, index)}
                />
              )}
            </FieldWrapper>
          ))}
          <Spacer y={4} />
        </>
      )}
      <ButtonsContainer>
        <ButtonSaveCancel variant="cancel" onClick={onCancel}>
          {t("contact.notes.cancel")}
        </ButtonSaveCancel>
        <Spacer x={8} />
        <ButtonSaveCancel variant="save" onClick={handleSave}>
          {t("contact.notes.save")}
        </ButtonSaveCancel>
      </ButtonsContainer>
    </Container>
  );
};

export default NewNote;
