import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  height: 510px;
`;

export const CleanContainer = styled.div`
  position: relative;
  &:hover {
    cursor: pointer;
  }
  /* &:hover::after {
    position: absolute;
    bottom: 0;
    content: "clean";
    width: 50px;
  } */
`;

export const TagsContainer = styled.div`
  overflow-y: scroll;
`;

export const ButtonsContainer = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
`;
