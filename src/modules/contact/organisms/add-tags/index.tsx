import React, { FC, useState, useEffect } from "react";
import {
  Container,
  CleanContainer,
  TagsContainer,
  ButtonsContainer,
} from "./styles";

import { NavTags, TagMessage, SearchTags, ContactTag } from "../../";

import { Typography, InlineWrapper, Spacer, Icon } from "../../..";
import { useTranslation } from "react-i18next";
import _ from "lodash";

export type TagInfo = {
  name: string;
  category: string;
  markedAs: "positive" | "negative" | "unmarked";
};

export type AddTagsProps = {
  contactTags: TagInfo[];
  cancelFunction: () => void;
  saveFunction: (newTagList: TagInfo[]) => void;
};

export const AddTags: FC<AddTagsProps> = ({ contactTags }) => {
  const { t } = useTranslation();

  const [isPositive, setIsPositive] = useState<boolean>(true);
  const [filterName, setFilterName] = useState<string>("");
  const [filterCategory, setFilterCategory] = useState<string>("");
  const [changedTagsList, setChangedTagsList] = useState<TagInfo[]>([]);
  const [isClean, setIsClean] = useState<boolean>(true);
  const [isSearching, setIsSearching] = useState<boolean>(false);

  useEffect(() => {
    setChangedTagsList(contactTags);
  }, []);

  const positiveNav = () => {
    setIsPositive(true);
  };
  const negativeNav = () => {
    setIsPositive(false);
  };

  const handleSearch = (nameInput: string, categoryInput: string) => {
    setFilterName(nameInput);
    setFilterCategory(categoryInput);
  };

  const handleTag = (changingTag: TagInfo) => {
    let newInfo = { ...changingTag };

    if (isPositive) {
      newInfo.markedAs =
        newInfo.markedAs === "positive" ? "unmarked" : "positive";
    } else {
      newInfo.markedAs =
        newInfo.markedAs === "negative" ? "unmarked" : "negative";
    }
    setChangedTagsList([
      ...changedTagsList.map((tag, index, array) =>
        _.isEqual(tag, changingTag) ? newInfo : tag
      ),
    ]);
  };

  useEffect(() => {
    setIsSearching(!isSearching);
  }, [filterName, filterCategory]);

  return (
    <Container>
      <InlineWrapper>
        <NavTags
          showPositive={positiveNav}
          showNegative={negativeNav}
        ></NavTags>
      </InlineWrapper>
      <Spacer y={23} />
      <InlineWrapper>
        <TagMessage isPositive={isPositive}></TagMessage>
      </InlineWrapper>
      <Spacer y={16} />
      <InlineWrapper justifyContent="center">
        <SearchTags
          key={`search${isClean}`}
          choices={contactTags
            .map((tag) => tag.category)
            .filter(
              (category, index, array) => array.indexOf(category) === index
            )}
          handleSearch={handleSearch}
        ></SearchTags>
        <Spacer x={12} />
        <CleanContainer onClick={() => setIsClean(!isClean)}>
          <Icon type="close-circle" color="neutral/80" />
        </CleanContainer>
      </InlineWrapper>
      <Spacer y={13.5} />

      <TagsContainer key={`tags${isSearching}`}>
        {changedTagsList
          .filter(
            (tag) =>
              tag.name.toLowerCase().includes(filterName.toLowerCase()) ||
              filterName === ""
          )
          .filter(
            (tag) => tag.category === filterCategory || filterCategory === ""
          )
          .map((tag) => (
            <>
              <ContactTag
                tag={tag}
                isPositive={isPositive}
                handleTag={handleTag}
              />
              <Spacer x={12} />
            </>
          ))}
      </TagsContainer>
      <Spacer y={9.5} />
      <ButtonsContainer>
        <button>Cancel</button>
        <button>Save</button>
      </ButtonsContainer>
      <Spacer y={30} />
    </Container>
  );
};

export default AddTags;
