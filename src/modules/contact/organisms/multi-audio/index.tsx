import React, { FC, useState, useEffect } from "react";

import { Container } from "./styles";

import { AudioPlayer } from "../../";

import { Spacer } from "../../../";

export type MultiAudioProps = {
  audios: string[];
  deleteAudio?: (audio: string) => void;
};

export const MultiAudio: FC<MultiAudioProps> = ({ audios, deleteAudio }) => {
  const [currentAudioIndex, setCurrentAudioIndex] = useState<number>(0);
  const [audioList, setAudioList] = useState<string[]>([]);

  useEffect(() => {
    setAudioList(audios);
  }, [audios]);

  return (
    <Container>
      {audioList.map((audio, index) => (
        <>
          <div
            key={`container-${audio}-${index}`}
            onClick={() => setCurrentAudioIndex(index)}
          >
            <AudioPlayer
              key={`audio-${audio}-${index}`}
              audioSource={audio}
              isOtherPlaying={currentAudioIndex === index ? false : true}
              deleteFunction={deleteAudio || undefined}
            />
          </div>
          <Spacer y={4} />
        </>
      ))}
    </Container>
  );
};

export default MultiAudio;
