import React, { FC } from "react";

import { Container, ImageContainer } from "./styles";

import { FeedHeader } from "../../";

import { Icon, Typography, InlineWrapper, Spacer, ShortText } from "../../../";

export type FeedData = {
  createdBy: string;
  createdAt?: Date;
  category?: string;
  title: string;
  content?: string;
  thumbnail?: string;
};

export type FeedProps = {
  feedData: FeedData;
  isProduct?: boolean;
  avatarImage?: string;
  onClickOpenUrl?: () => void;
};

export const Feed: FC<FeedProps> = ({
  feedData,
  isProduct = true,
  avatarImage,
  onClickOpenUrl,
}) => {
  return (
    <>
      <Container>
        <FeedHeader
          isProduct={isProduct}
          createdBy={feedData.createdBy}
          createdAt={feedData.createdAt}
          category={feedData.category}
          avatarImage={avatarImage}
          onClickOpenUrl={onClickOpenUrl}
        />
        <Spacer y={8} />
        <Typography variant="h3" color="neutral">
          {feedData.title}
        </Typography>
        <Spacer y={8} />
        {feedData.content && (
          <ShortText content={feedData.content} maxLength={280} />
        )}
        {feedData.thumbnail && (
          <>
            <Spacer y={16} />
            <ImageContainer src={feedData.thumbnail} />
          </>
        )}
      </Container>
    </>
  );
};

export default Feed;
