import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  padding: 16px;
  box-sizing: border-box;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid rgba(205, 208, 227, 0.7);
  border-radius: 8px;
`;

export const ImageContainer = styled.img`
  width: 100%;
  box-sizing: border-box;
  border: 2px solid rgba(205, 208, 227, 0.8);
  border-radius: 8px;
`;
