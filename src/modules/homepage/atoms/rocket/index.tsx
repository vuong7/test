import React, { FC } from "react";

type Props = {
  color?: string;
};

export const Rocket: FC<Props> = ({ color }) => {
  return (
    <svg
      width="99"
      height="96"
      viewBox="0 0 99 96"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M65.9106 46.6591C49.7842 60.4367 54.9661 83.1959 46.0929 90.7767L45.4884 91.2931C44.4972 92.14 42.9728 92.0203 42.1259 91.029L30.2486 77.1269L65.9106 46.6591Z"
        fill="url(#paint0_linear_1759_54238)"
      />
      <path
        d="M53.5173 32.1525C37.3908 45.9301 15.7198 37.2584 6.84657 44.8392L6.24213 45.3556C5.25085 46.2025 5.13111 47.7268 5.97802 48.7181L17.8553 62.6203L53.5173 32.1525Z"
        fill="url(#paint1_linear_1759_54238)"
      />
      <path
        opacity="0.7"
        d="M24.6568 69.3572L36.7456 59.0292"
        stroke="white"
        strokeWidth="0.142215"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M31.7864 86.2697L35.4131 83.1713"
        stroke="url(#paint2_linear_1759_54238)"
        strokeWidth="0.142215"
        strokeMiterlimit="10"
      />
      <path
        d="M62.8531 71.2294L75.5464 60.3849"
        stroke="url(#paint3_linear_1759_54238)"
        strokeWidth="0.142215"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M67.3605 56.9227L86.7026 40.3978"
        stroke="url(#paint4_linear_1759_54238)"
        strokeWidth="0.142215"
        strokeMiterlimit="10"
        strokeLinecap="round"
      />
      <path
        d="M9.06476 59.6743L12.6914 56.5759"
        stroke="url(#paint5_linear_1759_54238)"
        strokeWidth="0.142215"
        strokeMiterlimit="10"
      />
      <path
        d="M23.0076 75.9942L17.8436 69.9498C17.2652 69.2728 17.3427 68.2865 18.0196 67.7081L19.2285 66.6753L26.4582 75.1375L25.2493 76.1703C24.5723 76.7486 23.586 76.6712 23.0076 75.9942Z"
        fill="url(#paint6_linear_1759_54238)"
      />
      <path
        d="M78.8282 25.2088C79.012 24.5916 78.8648 23.8809 78.4517 23.3973C78.0179 22.8896 77.3595 22.6574 76.7213 22.7426C76.7213 22.7426 63.1883 23.8901 53.5172 32.1525L17.8552 62.6203C17.1782 63.1987 17.1008 64.1851 17.6791 64.862L28.0072 76.9508C28.5856 77.6278 29.5719 77.7053 30.2489 77.1269L65.9109 46.6591C75.5819 38.3967 78.8282 25.2088 78.8282 25.2088ZM67.6107 38.5149C67.1623 38.4797 66.8525 38.117 66.8877 37.6687C66.9863 36.4133 66.5847 35.2089 65.7585 34.2418C64.9116 33.2505 63.7398 32.6623 62.4603 32.5843C62.2361 32.5667 62.0638 32.463 61.9192 32.2937C61.7953 32.1487 61.699 31.9381 61.7166 31.7139C61.7518 31.2656 62.1145 30.9557 62.5629 30.9909C64.2907 31.1041 65.8519 31.9034 66.9673 33.209C68.0621 34.4904 68.5907 36.0882 68.4811 37.7713C68.4459 38.2196 68.059 38.5501 67.6107 38.5149Z"
        fill="url(#paint7_linear_1759_54238)"
      />
      <path
        d="M49.288 47.813C49.2894 47.7788 49.2088 47.6568 49.2088 47.6568C49.1596 47.604 49.0763 47.5503 48.9915 47.5307C48.9068 47.5112 48.8384 47.5092 48.7522 47.5238L39.9996 49.4078L40.7866 50.2517L48.348 48.6239L48.3822 48.6249C48.3822 48.6249 49.2755 48.1205 49.288 47.813Z"
        fill="url(#paint8_linear_1759_54238)"
      />
      <path
        d="M49.3262 48.1391C49.3467 48.0542 49.3495 47.9858 49.3359 47.8999C49.3223 47.814 49.2909 47.7446 49.2253 47.6743C49.1762 47.6216 49.2089 47.6567 49.2089 47.6567L48.106 48.6852L46.0328 55.8775L46.8198 56.7213L49.3262 48.1391Z"
        fill="url(#paint9_linear_1759_54238)"
      />
      <path
        d="M46.5854 50.3003C46.5868 50.2661 46.5048 50.1782 46.5048 50.1782C46.4557 50.1255 46.3723 50.0717 46.2875 50.0522C46.2028 50.0326 46.1344 50.0306 46.0482 50.0452L42.9233 50.7242L43.7102 51.5681L45.6782 51.1463C45.6782 51.1463 46.5893 50.6254 46.5854 50.3003Z"
        fill="url(#paint10_linear_1759_54238)"
      />
      <path
        d="M46.6057 50.643C46.6263 50.5581 46.6291 50.4897 46.6155 50.4038C46.6019 50.3179 46.5705 50.2485 46.5049 50.1782L45.4717 51.1745L44.9234 52.8691L45.7104 53.713L46.6057 50.643Z"
        fill="url(#paint11_linear_1759_54238)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1759_54238"
          x1="45.7521"
          y1="54.7049"
          x2="61.6923"
          y2="103.932"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1759_54238"
          x1="42.4228"
          y1="50.8076"
          x2="-3.71238"
          y2="27.3771"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1759_54238"
          x1="nan"
          y1="nan"
          x2="nan"
          y2="nan"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" stopOpacity="0.7" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1759_54238"
          x1="nan"
          y1="nan"
          x2="nan"
          y2="nan"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="0.5" stopColor="white" stopOpacity="0.3" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_1759_54238"
          x1="nan"
          y1="nan"
          x2="nan"
          y2="nan"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="0.5" stopColor="white" stopOpacity="0.3" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_1759_54238"
          x1="nan"
          y1="nan"
          x2="nan"
          y2="nan"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" stopOpacity="0.7" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_1759_54238"
          x1="22.6839"
          y1="71.0426"
          x2="18.1137"
          y2="74.9471"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint7_linear_1759_54238"
          x1="-6.15145"
          y1="95.6779"
          x2="84.8717"
          y2="17.9124"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint8_linear_1759_54238"
          x1="48.0546"
          y1="51.5137"
          x2="36.3788"
          y2="48.3899"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint9_linear_1759_54238"
          x1="45.5025"
          y1="48.7767"
          x2="47.9669"
          y2="60.191"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint10_linear_1759_54238"
          x1="46.2455"
          y1="51.8223"
          x2="41.3003"
          y2="50.8498"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint11_linear_1759_54238"
          x1="45.0758"
          y1="50.5679"
          x2="45.9017"
          y2="55.1509"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Rocket;
