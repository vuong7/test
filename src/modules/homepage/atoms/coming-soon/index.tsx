import React, { FC } from "react";

import { Typography } from "../../../";

import { Container } from "./styles";

import { useTranslation } from "react-i18next";

export const ComingSoon: FC = ({}) => {
  const { t } = useTranslation();
  return (
    <Container>
      <Typography variant="p-semi-bold" color="system/light1">
        {t("coming soon")}
      </Typography>
    </Container>
  );
};

export default ComingSoon;
