import styled from "styled-components";

export const Container = styled.div`
  background-color: ${(props) => props.theme.highlight};
  padding: 4px 8px;
  border-radius: 36px;
  display: flex;
  justify-content: center;
  align-items: center;
  white-space: nowrap;
`;
