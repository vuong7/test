import React, { FC } from "react";

import { ColorProps, useSelectColor } from "../../../";

export const SpaceBackground: FC = ({}) => {
  const darkPrimary = useSelectColor("primary/120");
  const LightPrimary = useSelectColor("primary/80");
  return (
    <svg
      width="133"
      height="133"
      viewBox="0 0 133 133"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_1785_52714)">
        <circle cx="66.1499" cy="66.7205" r="66" fill="#C4C4C4" />
        <circle
          cx="66.1499"
          cy="66.7205"
          r="66"
          fill="url(#paint0_linear_1785_52714)"
        />
        <path
          d="M46.009 36.4625C50.3593 41.5545 58.0139 42.1558 63.1059 37.8054C68.1979 33.4551 68.7991 25.8005 64.4488 20.7085C60.0984 15.6165 52.4439 15.0153 47.3519 19.3656C42.2599 23.716 41.6587 31.3705 46.009 36.4625Z"
          fill="url(#paint1_linear_1785_52714)"
        />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M105.068 119.764C103.525 118.407 102.06 116.923 100.686 115.315C85.0504 97.0141 87.2113 69.5028 105.513 53.8671C112.15 48.1961 120 44.8662 128.055 43.7984C130.678 50.8998 132.11 58.5779 132.11 66.5905C132.11 88.4386 121.46 107.799 105.068 119.764Z"
          fill="url(#paint2_linear_1785_52714)"
        />
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_1785_52714"
          x1="34.4524"
          y1="12.9141"
          x2="66.1499"
          y2="132.72"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor={darkPrimary} />
          <stop offset="1" stopColor={LightPrimary} />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1785_52714"
          x1="54.9233"
          y1="33.9571"
          x2="58.8392"
          y2="-34.8676"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1785_52714"
          x1="132.724"
          y1="106.31"
          x2="146.799"
          y2="-141.054"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0" />
          <stop offset="1" stopColor="white" />
        </linearGradient>
        <clipPath id="clip0_1785_52714">
          <rect width="133" height="133" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default SpaceBackground;
