import React, { FC, useState, useEffect } from "react";
import { StatContainer, ComingSoon, NoActivity } from "./styles";

import { Typography, Icon, InlineWrapper, Spacer } from "../../../core";
import { useTranslation } from "react-i18next";

export type StatItemProps = {
  title: string;
  statData?: {
    current: number;
    previous: number;
  };
  isDataTime?: boolean;
  tag?: FC;
  isHigherBetter?: boolean;
};

export const StatItem: FC<StatItemProps> = ({
  title,
  statData,
  isDataTime = false,
  isHigherBetter = true,
  tag,
}) => {
  const isNewFeature = statData ? false : true;
  const isActivityToReport =
    (statData?.current !== 0 && statData?.current !== undefined) ||
    (statData?.previous !== 0 && statData?.previous !== undefined);

  const isPercentualValid = statData?.previous ? true : false;

  const [current, setCurrent] = useState<number>(120);
  const [previous, setPrevious] = useState<number>(90);
  const [percentual, setPercentual] = useState<number>(33);
  const [isPositive, setIsPositive] = useState<boolean>(true);

  const { t } = useTranslation();

  useEffect(() => {
    if (statData) {
      setCurrent(statData.current);
      setPrevious(statData.previous);
    }
  }, [statData]);

  useEffect(() => {
    setPercentual(Math.round((current / previous - 1) * 100));
    setIsPositive(isHigherBetter ? current >= previous : current <= previous);
  }, [current, previous, isHigherBetter]);

  return (
    <StatContainer>
      {isNewFeature && (
        <ComingSoon>
          <Spacer y={28.26} />
          <InlineWrapper justifyContent="center">
            <Icon type="terminal" color="neutral/80" size="sm" />
          </InlineWrapper>
          <Spacer y={6} />
          <InlineWrapper justifyContent="center">
            <Typography variant="p-small-strong" color="neutral">
              {t("Coming soon")}
            </Typography>
          </InlineWrapper>
          <Spacer y={21.5} />
          <InlineWrapper>
            <Spacer x={12} />
            <Typography variant="p-small">{title}</Typography>
          </InlineWrapper>
        </ComingSoon>
      )}
      {!isActivityToReport && !isNewFeature && (
        <NoActivity>
          <Spacer y={28.26} />
          <InlineWrapper justifyContent="center">
            <Icon type="activity" color="neutral/80" size="sm" />
          </InlineWrapper>
          <Spacer y={6} />
          <InlineWrapper justifyContent="center">
            <Typography variant="p-small-strong">
              {t("No activity to report yet")}
            </Typography>
          </InlineWrapper>
          <Spacer y={21.5} />
          <InlineWrapper>
            <Spacer x={12} />
            <Typography variant="p-small">{title}</Typography>
          </InlineWrapper>
        </NoActivity>
      )}
      <InlineWrapper justifyContent="flex-end">
        {tag}
        <Spacer y={20}></Spacer>
      </InlineWrapper>
      {!isDataTime ? (
        <>
          <Typography variant="big-numbers">{current}</Typography>
          <Spacer y={3} />
          <InlineWrapper>
            <Spacer y={20.3} />
            {(isPercentualValid || isNewFeature || !isActivityToReport) && (
              <>
                <Icon
                  type={percentual >= 0 ? "trending-up" : "trending-down"}
                  color={isPositive ? "success" : "error"}
                  size="sm"
                />
                <Spacer x={4.67} />
                <Typography color={isPositive ? "success" : "error"}>
                  {percentual}%
                </Typography>
                <Spacer x={5} />
                <Typography color="neutral/60">
                  {t("from")} {previous}
                </Typography>
              </>
            )}
          </InlineWrapper>
        </>
      ) : (
        <>
          <InlineWrapper alignItems="baseline">
            <Typography variant="big-numbers">
              {Math.floor(current / 60)}
            </Typography>
            <Typography variant="h3">h</Typography>
            <Spacer x={4} />
            <Typography variant="big-numbers">
              {`${current % 60}`.length > 1 ? current : `0${current % 60}`}
            </Typography>
            <Typography variant="h3">min</Typography>
          </InlineWrapper>
          <Spacer y={3} />
          <InlineWrapper>
            <Spacer y={20.3} />
            {isPercentualValid && (
              <>
                <Icon
                  type={percentual >= 0 ? "trending-up" : "trending-down"}
                  color={isPositive ? "success" : "error"}
                  size="sm"
                />
                <Spacer x={4.67} />
                <Typography color={isPositive ? "success" : "error"}>
                  {percentual}%
                </Typography>
                <Spacer x={5} />
                <Typography color="neutral/60">
                  {t("from")}{" "}
                  {`${Math.floor(previous / 60)}h ${
                    `${previous % 60}`.length > 1
                      ? previous % 60
                      : `0${previous % 60}`
                  }min`}
                </Typography>
              </>
            )}
          </InlineWrapper>
        </>
      )}
      <Spacer y={3} />
      <Typography variant="p-small">
        {!isNewFeature && isActivityToReport && title}
      </Typography>{" "}
    </StatContainer>
  );
};
