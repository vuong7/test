import styled from "styled-components";

export const StatContainer = styled.div`
  border: 1px solid ${(props) => props.theme.neutralTs[20]};
  box-sizing: border-box;
  border-radius: 8px;
  width: 100%;
  height: 111.875px;
  background-color: ${(props) => props.theme.system.light2};
  position: relative;
  padding: 12px;
`;

export const ComingSoon = styled.div`
  position: absolute;
  background-color: #ffffff00;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 8px;
  backdrop-filter: blur(4px);
  z-index: 1;
`;

export const NoActivity = styled.div`
  position: absolute;
  background-color: ${(props) => props.theme.system.light2};
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 8px;
  z-index: 1;
`;
