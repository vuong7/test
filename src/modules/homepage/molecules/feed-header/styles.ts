import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const Label = styled.div`
  padding: 5px 8px 4px;
  border: 1px solid ${(props) => props.theme.neutralTs[60]};
  box-sizing: border-box;
  border-radius: 40px;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;
