import React, { FC } from "react";

import { Container, Filler, Label, IconContainer } from "./styles";

import {
  Icon,
  Typography,
  InlineWrapper,
  Spacer,
  AvatarImage,
} from "../../../";

import { useTranslation } from "react-i18next";

export type FeedHeaderProps = {
  isProduct?: boolean;
  createdBy: string;
  createdAt?: Date;
  category?: string;
  avatarImage?: string;
  onClickOpenUrl?: () => void;
};

export const FeedHeader: FC<FeedHeaderProps> = ({
  isProduct,
  createdBy,
  createdAt,
  category,
  avatarImage,
  onClickOpenUrl,
}) => {
  const { t } = useTranslation();
  return (
    <>
      <Container>
        <AvatarImage
          url={avatarImage}
          name={createdBy}
          customSize={42}
        ></AvatarImage>
        <Spacer x={8} />
        <div>
          <InlineWrapper>
            <Typography variant="h3" color="neutral">
              {createdBy}
            </Typography>
          </InlineWrapper>
          <Spacer y={4} />
          <InlineWrapper>
            <Typography variant="p-small-strong" color="neutral/90">
              {isProduct
                ? t("homepage.feedHeader.publishedAProduct")
                : t("homepage.feedHeader.publishedAnInsight")}
            </Typography>
            &nbsp;
            <Typography variant="p-small" color="neutral/90">
              {!!createdAt &&
                t("homepage.feedHeader.publishedOn", [
                  `${createdAt.getDate()} ${createdAt.toLocaleString(
                    "default",
                    {
                      month: "short",
                    }
                  )} ${createdAt.getFullYear()}`,
                ])}
            </Typography>
          </InlineWrapper>
        </div>
        <Filler />
        <InlineWrapper>
          {category && (
            <Label>
              <Typography color="neutral/60">{category}</Typography>
            </Label>
          )}
          <Spacer x={8} />
          {isProduct ? (
            <Icon type="cube" size="sm" color="neutral/80" />
          ) : (
            <Icon type="document" size="sm" color="neutral/80" />
          )}
          <Spacer x={8} />
          <IconContainer onClick={onClickOpenUrl}>
            <Icon type="external-url" size="sm" color="neutral/80" />
          </IconContainer>
        </InlineWrapper>
      </Container>
    </>
  );
};

export default FeedHeader;
