import React, { FC } from "react";

import {
  Frame,
  MainContainer,
  DottedBorder,
  BackgroundContainer,
  RocketContainer,
  ComingSoonContainer,
  FadeAwayBlock,
  DescriptionContainer,
} from "./styles";

import { Rocket, SpaceBackground, ComingSoon } from "../../";
import { Typography } from "../../../";

import { useTranslation } from "react-i18next";

export const EmptyState: FC = ({}) => {
  const { t } = useTranslation();
  return (
    <>
      <Frame>
        <BackgroundContainer>
          <DottedBorder />
          <SpaceBackground />
        </BackgroundContainer>
        <RocketContainer>
          <Rocket />
        </RocketContainer>
        <ComingSoonContainer>
          <ComingSoon />
          <FadeAwayBlock />
        </ComingSoonContainer>
      </Frame>
      <DescriptionContainer>
        <Typography variant="h4">
          {t("A new feature is landing in your world soon")}
        </Typography>
      </DescriptionContainer>
    </>
  );
};

export default EmptyState;
