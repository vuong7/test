import styled from "styled-components";

export const Frame = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const MainContainer = styled.div`
  position: absolute;
  border: 140px solid ${(props) => props.theme.system.light1};
  background-color: #ffffff00;
  border-radius: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 133px;
  height: 133px;
  z-index: 2;
  top: 0;
  left: 0;
`;

export const DottedBorder = styled.div`
  width: 151px;
  height: 151px;
  border: 1px dashed ${(props) => props.theme.highlight};
  border-radius: 50%;
  position: absolute;
  z-index: 2;
`;

export const BackgroundContainer = styled.div`
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid ${(props) => props.theme.system.light1};
  width: 133px;
  height: 133px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
`;

export const RocketContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-60%, -50%);
  z-index: 1;

  animation-name: diagonal;
  animation-duration: 1.3s;
  animation-timing-function: linear;

  @keyframes diagonal {
    0% {
      top: 70%;
      left: 32%;
    }
  }
`;

export const ComingSoonContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(10px, 14px);
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: center;

  animation-name: fadeIn;
  animation-duration: 1s;
  animation-timing-function: linear;
  border-radius: 36px;

  @keyframes fadeIn {
    0% {
      opacity: 0;
      background-color: #ed399500;
    }
  }
`;

export const FadeAwayBlock = styled.div`
  width: 0;
  height: 100%;
  background-color: #303030;
  opacity: 0.1;

  position: absolute;
  top: 0;
  right: 0;
  animation-name: fadeAway;
  animation-duration: 0.6s;
  animation-timing-function: ease-in;
  border-radius: 36px;

  @keyframes fadeAway {
    0% {
      width: 100%;
      opacity: 0.6;
    }

    5% {
      width: 95%;
      border-bottom-left-radius: 0px;
      border-top-left-radius: 0px;
    }
    100% {
      width: 15px;
    }
  }
`;

export const DescriptionContainer = styled.div`
  width: 190px;
  margin: 0 auto;
  margin-top: 20px;
  text-align: center;
  z-index: 2;

  animation-name: moveUp;
  animation-duration: 1.3s;
  animation-timing-function: linear;

  @keyframes moveUp {
    0% {
      top: 330px;
      opacity: 0;
    }
  }
`;
