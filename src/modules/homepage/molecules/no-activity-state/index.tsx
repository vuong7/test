import React, { FC } from "react";

import { Container } from "./styles";

import { Typography, Icon, InlineWrapper, Spacer } from "../../../";

import { useTranslation } from "react-i18next";

export const NoActivityState: FC = ({}) => {
  const { t } = useTranslation();
  return (
    <>
      <Container>
        <InlineWrapper justifyContent="center">
          <Icon type="alert-circle" color="neutral/80" />
        </InlineWrapper>
        <Spacer y={16} />
        <InlineWrapper justifyContent="center">
          <Typography variant="p">{t("No activity to report yet")}</Typography>
        </InlineWrapper>
        <Spacer y={4} />
        <InlineWrapper justifyContent="center">
          <Typography variant="titles">
            {t("Share your first {{0}}", ["Uplink"])}
          </Typography>
        </InlineWrapper>
      </Container>
    </>
  );
};

export default NoActivityState;
