import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { NoActivityState } from "..";

export default {
  title: "Homepage/NoActivityState",
  component: NoActivityState,
} as ComponentMeta<typeof NoActivityState>;

const Template: Story = (args) => (
  <NoActivityState {...args}>Example of No Activity State</NoActivityState>
);

export const Default = Template.bind({});
