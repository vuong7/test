import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { SpaceBackground } from "../";

export default {
  title: "Homepage/SpaceBackground",
  component: SpaceBackground,
} as ComponentMeta<typeof SpaceBackground>;

const Template: Story = (args) => (
  <SpaceBackground {...args}>Example of SpaceBackground</SpaceBackground>
);

export const Default = Template.bind({});
