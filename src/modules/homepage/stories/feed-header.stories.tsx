import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { FeedHeader, FeedHeaderProps } from "..";

export default {
  title: "Homepage/FeedHeader",
  component: FeedHeader,
} as ComponentMeta<typeof FeedHeader>;

const Template: Story<FeedHeaderProps> = (args) => (
  <FeedHeader {...args}>Example of Suggestion Widget</FeedHeader>
);

export const Product = Template.bind({});
Product.args = {
  isProduct: true,
  createdBy: "Test User",
  createdAt: new Date(),
  category: "category",
  avatarImage:
    "https://isaojose.com.br/wp-content/uploads/2020/12/blank-profile-picture-mystery-man-avatar-973460.jpg",
};

export const Insight = Template.bind({});
Insight.args = {
  createdBy: "Test User",
};
