import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Feed, FeedProps } from "..";

export default {
  title: "Homepage/Feed",
  component: Feed,
} as ComponentMeta<typeof Feed>;

const Template: Story<FeedProps> = (args) => (
  <Feed {...args}>Example of Suggestion Widget</Feed>
);

export const Default = Template.bind({});
Default.args = {
  feedData: {
    createdBy: "Test User",
    createdAt: new Date(),
    category: "category",
    title:
      "Protecting and investing in marine capital makes business protecting",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu justo, massa nec blandit tincidunt id. Blandit nunc nibh in quis elementum. Lorem blandit sagittis volutpat nulla dictumst augue tellus. Ut ultricies in egestas ultrices semper morbi vel, tristique nisl. Blandit sagittis volutp. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu justo, massa nec blandit tincidunt id. Blandit nunc nibh in quis elementum. Lorem blandit sagittis volutpat nulla dictumst augue tellus. Ut ultricies in egestas ultrices semper morbi vel, tristique nisl. Blandit sagittis volutp.",
    thumbnail:
      "https://images.unsplash.com/photo-1477959858617-67f85cf4f1df?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max",
  },
  isProduct: true,
  avatarImage:
    "https://isaojose.com.br/wp-content/uploads/2020/12/blank-profile-picture-mystery-man-avatar-973460.jpg",
};

export const LessData = Template.bind({});
LessData.args = {
  feedData: {
    createdBy: "Test User",
    title:
      "Protecting and investing in marine capital makes business protecting",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu justo, massa nec blandit tincidunt id. Blandit nunc nibh in quis elementum.",

    isProduct: true,
  },
};
