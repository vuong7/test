import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { ComingSoon } from "..";

export default {
  title: "Homepage/ComingSoon",
  component: ComingSoon,
} as ComponentMeta<typeof ComingSoon>;

const Template: Story = (args) => (
  <ComingSoon {...args}>Example of Coming Soon text</ComingSoon>
);

export const Default = Template.bind({});
