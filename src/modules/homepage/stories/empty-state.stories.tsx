import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { EmptyState } from "..";

export default {
  title: "Homepage/EmptyState",
  component: EmptyState,
} as ComponentMeta<typeof EmptyState>;

const Template: Story = (args) => (
  <EmptyState {...args}>Example of Empty State</EmptyState>
);

export const Default = Template.bind({});
