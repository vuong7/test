import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Rocket } from "../";

export default {
  title: "Homepage/Rocket",
  component: Rocket,
} as ComponentMeta<typeof Rocket>;

const Template: Story = (args) => <Rocket {...args}>Example of Rocket</Rocket>;

export const Default = Template.bind({});
