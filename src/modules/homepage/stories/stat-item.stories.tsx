import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { StatItem, StatItemProps } from "..";

export default {
  title: "Homepage/StatItem",
  component: StatItem,
} as ComponentMeta<typeof StatItem>;

const Template: Story<StatItemProps> = (args) => (
  <StatItem {...args}>Example of Stat Item</StatItem>
);

export const Default = Template.bind({});
Default.args = {
  title: "My first stat",
  statData: {
    current: 120,
    previous: 90,
  },
};

export const TimeStat = Template.bind({});
TimeStat.args = {
  title: "My first stat",
  statData: {
    current: 125,
    previous: 250,
  },
  isHigherBetter: false,
  isDataTime: true,
};

export const ComingSoon = Template.bind({});
ComingSoon.args = {
  title: "My first stat",
};

export const NoActivity = Template.bind({});
NoActivity.args = {
  title: "My first stat",
  statData: {
    current: 0,
    previous: 0,
  },
};
