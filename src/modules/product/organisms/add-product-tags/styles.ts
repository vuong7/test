import styled from "styled-components";
import { ScrollbarStyled } from "../../../";

type ClearAllContainerProps = {
  hasPrimary?: boolean;
};

type DropContainerProps = {
  isMaxReached?: boolean;
};

type FoundTagsWrapperProps = {
  hasTagsSelected?: boolean;
};

type SelectedTagsWrapper = {
  hasMaxHeight?: boolean;
};

export const Container = styled.div`
  background-color: ${(props) => props.theme.system.light1};
  box-shadow: 0px 10px 250px rgba(0, 0, 0, 0.1);
  border-radius: 16px;
  padding: 16px 24px 30px;
  width: 598px;
  min-height: 601px;
  display: flex;
  flex-direction: column;
`;

export const Header = styled.header`
  display: flex;
  justify-content: space-between;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;

export const InputsWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 12px;
`;

export const TagsContainer = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  overflow-y: hidden;
  min-height: 385px;
`;

export const LoadContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-grow: 1;
`;

export const NoTagResult = styled.div`
  flex-grow: 1;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const FoundTagsWrapper = styled.div<FoundTagsWrapperProps>`
  overflow-y: auto;
  display: flex;
  flex-wrap: wrap;
  row-gap: 12px;

  max-height: ${(props) => (props.hasTagsSelected ? "308px" : "385px")};

  ${ScrollbarStyled}
`;

export const ShowSelected = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const SelectedTagsWrapper = styled.div<SelectedTagsWrapper>`
  display: flex;
  flex-wrap: wrap;
  row-gap: 12px;

  ${(props) =>
    props.hasMaxHeight ? "overflow-y: auto; max-height: 250px;" : ""}
  ${ScrollbarStyled};
`;

export const ClearAllContainer = styled.div<ClearAllContainerProps>`
  opacity: ${(props) => (props.hasPrimary ? 1 : 0)};

  &:hover {
    cursor: ${(props) => (props.hasPrimary ? "pointer" : "grab")};
  }
`;

export const DropContainer = styled.div<DropContainerProps>`
  width: 100%;
  min-height: 78.84px;
  background-color: ${(props) => props.theme.neutralTs[10]};
  border: 1px dashed
    ${(props) =>
      props.isMaxReached ? props.theme.system.error : props.theme.neutral};
  box-sizing: border-box;
  border-radius: 8px;
  padding: 18px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  row-gap: 12px;

  overflow-y: auto;
  ${ScrollbarStyled}
`;

export const DropMessage = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`;

export const DraggableContainer = styled.div`
  box-sizing: border-box;
  height: fit-content;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;
