import React, { FC, useState, useEffect } from "react";
import {
  Container,
  Header,
  IconContainer,
  InputsWrapper,
  TagsContainer,
  LoadContainer,
  NoTagResult,
  FoundTagsWrapper,
  ShowSelected,
  SelectedTagsWrapper,
  ClearAllContainer,
  DropContainer,
  DropMessage,
  DraggableContainer,
  Filler,
} from "./styles";

import {
  Typography,
  InlineWrapper,
  Spacer,
  Icon,
  InputSelection,
  ButtonSaveCancel,
  Divider,
} from "../../..";
import {
  EmptyTagSearch,
  SearchField,
  DotLoader,
  ProductTag,
  ProductTagVariant,
} from "../../";

import { useTranslation } from "react-i18next";

export type TagCategory = {
  id: string;
  name: string;
};

export type Tag = {
  id: string;
  name: string;
  category: TagCategory;
};

export type TagSelected = Tag & {
  isPrimary?: boolean;
};

export type AddProductTagsProps = {
  tags: Tag[];
  tagsSelected?: TagSelected[];
  onClickClose(): void;
  isProduct?: boolean;
  onClickSave(selectedTags: TagSelected[]): void;
};

export const AddProductTags: FC<AddProductTagsProps> = ({
  tags,
  tagsSelected,
  onClickClose,
  isProduct,
  onClickSave,
}) => {
  const { t } = useTranslation();

  const [filterTagName, setFilterTagName] = useState<string>("");
  const [selectedCategory, setSelectedCategory] = useState<string>("");

  const [allTags, setAllTags] = useState<Tag[]>([]);
  const [currentTagsSelected, setCurrentTagsSelected] = useState<TagSelected[]>(
    []
  );

  const [isChoosingPrimaries, setIsChoosingPrimaries] =
    useState<boolean>(false);

  const [foundTags, setFoundTags] = useState<Tag[]>([]);

  const [isEditing, setIsEditing] = useState<boolean>(
    tagsSelected && tagsSelected.length > 0 ? true : false
  );
  const [countCategorySearch, setCountCategorySearch] = useState<number>(0);
  const [isCategoryChanging, setIsCategoryChanging] = useState<boolean>(
    tagsSelected && tagsSelected.length > 0 ? true : false
  );

  const [isCleaningInput, setIsCleaningInput] = useState<boolean>(false);

  const [isOnDropArea, setIsOnDropArea] = useState<boolean>(false);
  const [tagDragged, setTagDragged] = useState<Tag | undefined>(undefined);

  const [isShowMore, setIsShowMore] = useState<boolean>(false);

  const handleBackButton = () => {
    setFilterTagName("");
    setSelectedCategory("");
    setFoundTags([]);
    setIsCleaningInput(!isCleaningInput);
    if (currentTagsSelected.length > 0) {
      setIsCategoryChanging(true);
    }
  };

  useEffect(() => {
    setCurrentTagsSelected(tagsSelected || []);
    setAllTags(tags);
  }, [tagsSelected, tags]);

  useEffect(() => {
    if (!(filterTagName === "" && selectedCategory === "")) {
      setFoundTags(
        allTags.filter(
          (tag) =>
            tag.name.toLowerCase().includes(filterTagName.toLowerCase()) &&
            (tag.category.name === selectedCategory || selectedCategory === "")
        )
      );
      setIsEditing(true);
    }
  }, [filterTagName, selectedCategory]);

  useEffect(() => {
    setCountCategorySearch(countCategorySearch + 1);
    if (currentTagsSelected.length > 0) {
      setIsCategoryChanging(true);
    }
  }, [selectedCategory]);

  useEffect(() => {
    if (currentTagsSelected.length === 0) {
      setIsChoosingPrimaries(false);
      setIsShowMore(false);
    }
  }, [currentTagsSelected]);

  const hangleTags = (changingTag: Tag, currentStatus: ProductTagVariant) => {
    if (currentStatus === "unselected") {
      setCurrentTagsSelected([...currentTagsSelected, changingTag]);
    } else if (currentStatus === "selected" || currentStatus === "primary") {
      setCurrentTagsSelected([
        ...currentTagsSelected.filter((tag) => tag.id !== changingTag.id),
      ]);
    }
  };

  const handleVariant = (tag: Tag) => {
    const selectedTag = currentTagsSelected.find(
      (selected) => selected.id === tag.id
    );
    if (selectedTag) {
      return selectedTag.isPrimary ? "primary" : "selected";
    } else {
      return "unselected";
    }
  };

  const dragEnd = () => {
    if (isOnDropArea && tagDragged) {
      setCurrentTagsSelected([
        ...currentTagsSelected.map((tag) =>
          tag.id === tagDragged.id ? { ...tag, isPrimary: true } : tag
        ),
      ]);
    }
    setIsOnDropArea(false);
    setTagDragged(undefined);
  };

  const handlePrimaryTag = (tag: Tag) => {
    setCurrentTagsSelected([
      ...currentTagsSelected.map((selected) =>
        selected.id === tag.id ? { ...tag, isPrimary: false } : selected
      ),
    ]);
  };

  const clearAll = () => {
    setCurrentTagsSelected([
      ...currentTagsSelected.map((tag) => {
        return { ...tag, isPrimary: false };
      }),
    ]);
  };

  return (
    <Container onDragOver={(evt) => setIsOnDropArea(false)}>
      <Header>
        <Typography variant="h1" color="primary">
          {t("product.addTags.addTags")}
        </Typography>
        <IconContainer onClick={onClickClose}>
          <Icon type="close" color="neutral" />
        </IconContainer>
      </Header>
      <Spacer y={22} />
      {!isChoosingPrimaries ? (
        <>
          <Typography variant="p" color="neutral">
            {isProduct
              ? t("product.addTags.chooseTags", ["product"])
              : t("product.addTags.chooseTags", ["insight"])}
          </Typography>
          <Spacer y={24} />
          <InputsWrapper>
            <SearchField
              key={`searchfield${isCleaningInput}`}
              onChange={setFilterTagName}
              placeholder={t("product.addTags.typeSearch")}
            />
            <InputSelection
              key={`selection${isCleaningInput}`}
              handleSelection={setSelectedCategory}
              placeholder={t("product.addTags.selectCategory")}
              choices={allTags.map((tag) => tag.category.name)}
            />
          </InputsWrapper>
          <Spacer y={14} />
          <TagsContainer>
            {allTags.length === 0 ? (
              <LoadContainer>
                <DotLoader />
              </LoadContainer>
            ) : (
              <>
                {foundTags.length === 0 && (
                  <NoTagResult>
                    <EmptyTagSearch
                      isSearching={
                        selectedCategory !== "" || filterTagName !== ""
                      }
                    />
                  </NoTagResult>
                )}
              </>
            )}
            <FoundTagsWrapper
              hasTagsSelected={
                currentTagsSelected.length > 0 && isCategoryChanging
              }
            >
              {foundTags
                .filter((tag) =>
                  isCategoryChanging
                    ? !currentTagsSelected.some(
                        (selected) => selected.id === tag.id
                      )
                    : true
                )
                .map((tag, index) => (
                  <div key={index}>
                    <ProductTag
                      key={index}
                      tag={tag}
                      onClickChangeTag={hangleTags}
                      variant={handleVariant(tag)}
                    />
                    <Spacer x={12} />
                  </div>
                ))}
            </FoundTagsWrapper>
            {isCategoryChanging && currentTagsSelected.length > 0 && (
              <>
                <Divider marginVertical={20} />
                {isShowMore && (
                  <SelectedTagsWrapper>
                    {currentTagsSelected.map((tag, index) => (
                      <div key={index}>
                        <ProductTag
                          key={index}
                          tag={tag}
                          variant={tag.isPrimary ? "primary" : "selected"}
                          onClickChangeTag={hangleTags}
                        />
                        <Spacer x={12} />
                      </div>
                    ))}
                  </SelectedTagsWrapper>
                )}
                <Spacer y={13} />
                <InlineWrapper justifyContent="center">
                  <ShowSelected onClick={() => setIsShowMore(!isShowMore)}>
                    <Icon
                      type={isShowMore ? "arrow-up" : "arrow-down"}
                      color="highlight"
                    />
                    <Spacer x={8} />
                    <Typography color="highlight">
                      {isShowMore
                        ? t("product.addTags.hideSelected", [
                            currentTagsSelected.length,
                          ])
                        : t("product.addTags.showSelected", [
                            currentTagsSelected.length,
                          ])}
                    </Typography>
                  </ShowSelected>
                </InlineWrapper>
              </>
            )}
          </TagsContainer>
          <Spacer y={24} />
          <InlineWrapper justifyContent="flex-end">
            {isEditing && foundTags.length > 0 && (
              <ButtonSaveCancel
                variant="cancel"
                onClick={() => handleBackButton()}
              >
                {t("product.addTags.backButton")}
              </ButtonSaveCancel>
            )}
            <Spacer x={8} />
            <ButtonSaveCancel
              variant={"save"}
              onClick={() => setIsChoosingPrimaries(true)}
              isDisabled={currentTagsSelected.length === 0}
            >
              {t("product.addTags.nextButton")}
            </ButtonSaveCancel>
          </InlineWrapper>{" "}
        </>
      ) : (
        <>
          <Spacer y={20} />
          <Typography variant="small-titles" color="neutral">
            {t("product.addTags.selectedTags", [
              `${currentTagsSelected.length}`,
            ])}
          </Typography>
          <Spacer y={22} />
          <SelectedTagsWrapper hasMaxHeight={true}>
            {currentTagsSelected.map((tag, index) => (
              <DraggableContainer
                key={index}
                draggable
                onDragStart={(evt) => setTagDragged(tag)}
                onDragEnd={(evt) => dragEnd()}
              >
                <ProductTag
                  tag={tag}
                  variant={tag.isPrimary ? "primary" : "selected"}
                  onClickChangeTag={hangleTags}
                  customIcon="close-circle"
                />
                <Spacer x={12} />
              </DraggableContainer>
            ))}
          </SelectedTagsWrapper>
          <Spacer y={24} />
          <Typography variant="small-titles" color="neutral">
            {t("product.addTags.primaryTag")}
          </Typography>
          <Spacer y={11} />
          <Typography variant="p" color="neutral">
            {t("product.addTags.primaryTagsMessage")}
          </Typography>
          <InlineWrapper justifyContent="flex-end">
            <ClearAllContainer
              onClick={clearAll}
              hasPrimary={currentTagsSelected.some((tag) => tag.isPrimary)}
            >
              <Typography variant="p-semi-bold" color="highlight">
                {t("product.addTags.clearAll")}
              </Typography>
            </ClearAllContainer>
          </InlineWrapper>
          <Spacer y={4} />
          <DropContainer
            onDragOver={(evt) => {
              evt.stopPropagation();
              setIsOnDropArea(true);
            }}
            isMaxReached={
              // currentTagsSelected.filter((tag) => tag.isPrimary).length === 3 &&
              // isOnDropArea
              false
            }
          >
            {currentTagsSelected.some((tag) => tag.isPrimary) ? (
              currentTagsSelected
                .filter((tag) => tag.isPrimary)
                .map((tag, index) => (
                  <div key={index}>
                    <ProductTag
                      tag={tag}
                      variant={"primary"}
                      onClickChangeTag={handlePrimaryTag}
                    />
                    <Spacer x={12} />
                  </div>
                ))
            ) : (
              <DropMessage>
                <Typography variant="p" color="neutral">
                  {t("product.addTags.dropMessage")}
                </Typography>
              </DropMessage>
            )}
          </DropContainer>
          <Filler />
          <Spacer y={24} />
          <InlineWrapper justifyContent="flex-end">
            <ButtonSaveCancel
              variant="cancel"
              onClick={() => setIsChoosingPrimaries(false)}
            >
              {t("product.addTags.backButton")}
            </ButtonSaveCancel>
            <Spacer x={8} />
            <ButtonSaveCancel
              variant="save"
              onClick={() => onClickSave(currentTagsSelected)}
            >
              {t("product.addTags.saveButton")}
            </ButtonSaveCancel>
          </InlineWrapper>
        </>
      )}
    </Container>
  );
};

export default AddProductTags;
