import React, { FC, useEffect, useState, useCallback } from "react";
import {
  Container,
  IconContainer,
  AddExternalWrapper,
  AttachmentsWrapper,
  CardContainer,
  FieldWrapper,
  FieldControl,
  Filler,
  LoadingContainer,
} from "./styles";

import {
  Typography,
  InlineWrapper,
  Spacer,
  Icon,
  ButtonSaveCancel,
  InputError,
  DotLoader,
} from "../../..";

import { DropArea, AttachmentCard, FilterMenu } from "../../";

import { useTranslation } from "react-i18next";
import { v4 } from "uuid";

export type DocumentTypeData = {
  id?: string;
  name: string;
};

export type AttachmentData = {
  id?: string;
  file?: File;
  name?: string;
  extension?: string;
  fileType?: string;
  externalUrl?: string;
  uploadedAt: Date;
  documentType?: string;
  isPrivate?: boolean;
  isPrimary?: boolean;
  uuidKey?: string;
};

export type AttachmentWidgetProps = {
  onClickClose(): void;
  onSubmit(attachments: AttachmentData[]): Promise<void>;
  documentTypes: DocumentTypeData[];
  currentAttachments?: AttachmentData[];
};

export const AttachmentWidget: FC<AttachmentWidgetProps> = ({
  onClickClose,
  onSubmit,
  documentTypes,
  currentAttachments,
}) => {
  const { t } = useTranslation();

  const [attachments, setAttachments] = useState<AttachmentData[]>([]);
  const [draggedIndex, setDraggedIndex] = useState<number>(0);
  const [dropIndex, setDropIndex] = useState<number>(0);
  const [isValidDrop, setIsValidDrop] = useState<boolean>(false);

  const [isDragging, setIsDragging] = useState<boolean>(false);
  const [movingIndexes, setMovingIndexes] = useState<number[]>([]);

  const [currentFiles, setCurrentFiles] = useState<File[]>([]);
  const [hasSubmitFailed, setHasSubmitFailed] = useState<boolean>(false);
  const [filteredTypes, setFilteredTypes] = useState<string[]>([]);
  const [isUploading, setIsUploading] = useState<boolean>(false);
  const [docTypeNames, setDocTypeNames] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isChanging, setIsChanging] = useState<boolean>(false);

  useEffect(() => {
    setAttachments(
      currentAttachments
        ? [
            ...currentAttachments
              .filter((attachment) => attachment.isPrimary)
              .map((attachment) => {
                return {
                  ...attachment,
                  extension:
                    attachment.fileType === "EXTERNAL_LINK"
                      ? "EXTERNAL_LINK"
                      : attachment.name?.slice(
                          attachment.name?.lastIndexOf(".") + 1
                        ),
                  uuidKey: v4(),
                };
              }),
            ...currentAttachments
              .filter((attachment) => !attachment.isPrimary)
              .map((attachment) => {
                return {
                  ...attachment,
                  extension:
                    attachment.fileType === "EXTERNAL_LINK"
                      ? "EXTERNAL_LINK"
                      : attachment.name?.slice(
                          attachment.name?.lastIndexOf(".") + 1
                        ),
                  uuidKey: v4(),
                };
              }),
          ]
        : []
    );
  }, [currentAttachments]);

  useEffect(() => {
    setDocTypeNames(documentTypes.map((docType) => docType.name));
  }, [documentTypes]);

  const handleAttachments = (fileList: File[]) => {
    setAttachments([
      ...attachments,
      ...fileList.map((file) => {
        return {
          file: file,
          uploadedAt: new Date(),
          name: file.name,
          extension: file.name.slice(file.name.lastIndexOf(".") + 1),
          fileType: "FILE",
          isPrivate: false,
          uuidKey: v4(),
        };
      }),
    ]);
    setIsUploading(false);
  };

  useEffect(() => {
    const files = attachments
      .filter((attachment) => !!attachment.file)
      .map((attachment) => attachment.file);
    setCurrentFiles(files as File[]);
  }, [attachments]);

  const removeAttachment = (indexToRemove: number) => {
    setAttachments(
      attachments.filter((attachment, index) => index !== indexToRemove)
    );
    setIsChanging(!isChanging);
  };

  const handleDragStart = useCallback((originIndex: number) => {
    setDraggedIndex(originIndex);
    setDropIndex(originIndex);
    setIsDragging(true);
    setMovingIndexes([]);
  }, []);

  const handleDragOver = useCallback(
    (evt: React.DragEvent, destinyIndex: number) => {
      evt.stopPropagation();
      setIsValidDrop(true);
      setDropIndex(destinyIndex);
    },
    []
  );

  // const handleDragEnd = useCallback(() => {
  //   setMovingIndexes([]);
  //   setIsDragging(false);
  //   if (isValidDrop) {
  //     setAttachments([
  //       ...attachments.map((attachment, index, array) =>
  //         index === draggedIndex
  //           ? array[dropIndex]
  //           : index === dropIndex
  //           ? array[draggedIndex]
  //           : attachment
  //       ),
  //     ]);
  //   }
  //   setDraggedIndex(0);
  //   setDropIndex(0);
  // }, [isValidDrop, draggedIndex, dropIndex]);

  const handleDragEnd = useCallback(() => {
    setMovingIndexes([]);
    setIsDragging(false);
    if (isValidDrop) {
      const [draggedAttachment] = attachments.splice(draggedIndex, 1);
      attachments.splice(dropIndex, 0, draggedAttachment);
      setAttachments([...attachments]);
    }
    setDraggedIndex(0);
    setDropIndex(0);
  }, [isValidDrop, draggedIndex, dropIndex]);

  useEffect(() => {
    if (isDragging) {
      setMovingIndexes([draggedIndex, dropIndex]);
    }
  }, [isDragging, draggedIndex, dropIndex]);

  const handlePrivate = (newValue: boolean, indexToChange: number) => {
    setAttachments(
      attachments.map((attachment, index) =>
        index === indexToChange
          ? { ...attachment, isPrivate: newValue }
          : attachment
      )
    );
  };

  const handleRename = (
    newName: string,
    indexToChange: number,
    isFile: boolean
  ) => {
    if (isFile) {
      let attachmentToChange = attachments.find(
        (attachments, index) => index === indexToChange
      );
      const changedFile = attachmentToChange?.file
        ? new File([attachmentToChange?.file as File], newName, {
            type: attachmentToChange.file?.type,
          })
        : undefined;
      setAttachments(
        attachments.map((attachment, index) =>
          index === indexToChange
            ? { ...attachment, file: changedFile, name: newName }
            : attachment
        )
      );
    } else {
      setAttachments(
        attachments.map((attachment, index) =>
          index === indexToChange
            ? { ...attachment, externalUrl: newName }
            : attachment
        )
      );
    }
  };

  const handleChangeType = (newType: string, indexToChange: number) => {
    setAttachments(
      attachments.map((attachment, index) =>
        index === indexToChange
          ? { ...attachment, documentType: newType }
          : attachment
      )
    );
  };

  const addExternalUrl = () => {
    setAttachments([
      ...attachments,
      {
        externalUrl: "",
        uploadedAt: new Date(),
        extension: "EXTERNAL_LINK",
        fileType: "EXTERNAL_LINK",
        isPrivate: false,
        uuidKey: v4(),
      },
    ]);
  };

  const handleSubmit = async () => {
    const isValidUrls = attachments.every((attachment) => {
      if (attachment.fileType === "EXTERNAL_LINK") {
        try {
          let url = new URL(attachment.externalUrl as string);
          return true;
        } catch {
          return false;
        }
      } else {
        return true;
      }
    });
    const isValidTypes = attachments.every(
      (attachment) => !!attachment.documentType
    );
    // const isValidName = attachments.every((attachment) =>
    //   attachment.name ? attachment.name !== "" : true
    // );
    if (isValidUrls && isValidTypes) {
      setHasSubmitFailed(false);
      setIsLoading(true);
      try {
        await onSubmit(
          attachments.map((attachment, index) => {
            const { extension, uuidKey, ...data } = attachment;
            return { ...data, isPrimary: index === 0 };
          })
        );
      } catch (err) {
        console.error("Error on submit attachments.");
        console.log(err);
      } finally {
        setIsLoading(false);
      }
    } else {
      setHasSubmitFailed(true);
    }
    // console.log(
    //   attachments.map((attachment, index) => {
    //     const { extension, uuidKey, ...data } = attachment;
    //     return { ...data, isPrimary: index === 0 };
    //   })
    // );
  };

  const handleFilter = (idList: string[]) => {
    setFilteredTypes(idList);
  };

  return (
    <Container onMouseLeave={() => setIsValidDrop(false)}>
      <InlineWrapper justifyContent="space-between">
        <Typography variant="h1" color="primary">
          {t("product.attachments.attachments")}
        </Typography>
        <IconContainer onClick={onClickClose}>
          <Icon type="close" />
        </IconContainer>
      </InlineWrapper>
      <Spacer y={21} />
      <InlineWrapper>
        <Typography variant="p" color="neutral">
          {t("product.attachments.message")}
        </Typography>
      </InlineWrapper>
      <Spacer y={12} />
      <DropArea
        onDrop={handleAttachments}
        maxSize={250}
        accept={["JPG", "JPEG", "PNG", "GIF", "PDF", "MP4"]}
        currentFiles={currentFiles}
        handleLoading={(value: boolean) => setIsUploading(value)}
      />
      <Spacer y={28} />
      <InlineWrapper>
        <AddExternalWrapper onClick={addExternalUrl}>
          <Icon type="plus" color="primary" size="sm" />
          <Typography variant="h4" color="primary">
            {t("product.attachments.addExternal")}
          </Typography>
        </AddExternalWrapper>
      </InlineWrapper>
      <Spacer y={16} />
      <InlineWrapper>
        <Typography variant="p" color="neutral">
          {t("product.attachments.reorderMessage")}
        </Typography>
      </InlineWrapper>
      <Spacer y={16} />
      <InlineWrapper justifyContent="space-between">
        <Typography variant="h4" color="neutral">
          {t("product.attachments.yourAttachments", [attachments.length])}
        </Typography>
        <FilterMenu
          currentAttachments={attachments}
          documentTypes={docTypeNames}
          onChange={handleFilter}
        />
      </InlineWrapper>
      <Spacer y={6} />

      <AttachmentsWrapper>
        {attachments.length === 0 && (
          <>
            <Spacer y={10} />
            <InlineWrapper justifyContent="center">
              <Icon type="attachment" color="neutral/80" size="sm" />
            </InlineWrapper>
            <Spacer y={12} />
            <InlineWrapper justifyContent="center">
              <Typography variant="p">
                {t("product.attachments.noAttachments")}
              </Typography>
            </InlineWrapper>
          </>
        )}
        {attachments?.map((attachment, index) => (
          <FieldWrapper
            key={`wrapper-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
            draggable={true}
            onDragStart={() => handleDragStart(index)}
            onDragEnd={handleDragEnd}
            isFiltered={!filteredTypes.includes(attachment.documentType || "")}
          >
            <CardContainer
              key={`container-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
              isDragged={isDragging && movingIndexes[0] === index}
              isMovingUp={
                isDragging &&
                index !== movingIndexes[0] &&
                movingIndexes[0] > movingIndexes[1] &&
                index >= movingIndexes[1] &&
                index < movingIndexes[0]
              }
              isMovingDown={
                isDragging &&
                index !== movingIndexes[0] &&
                movingIndexes[0] < movingIndexes[1] &&
                index <= movingIndexes[1] &&
                index > movingIndexes[0]
              }
              distance={102}
            >
              <>
                <Spacer y={4} />
                <AttachmentCard
                  key={`attachment-${attachment.name}${attachment.uploadedAt}${attachment.uuidKey}`}
                  attachment={attachment}
                  isPrimary={index === 0}
                  onClickDelete={() => removeAttachment(index)}
                  onChangePrivate={(newValue: boolean) =>
                    handlePrivate(newValue, index)
                  }
                  onClickRename={(newName: string, isFile: boolean) =>
                    handleRename(newName, index, isFile)
                  }
                  documentTypes={docTypeNames}
                  onChangeType={(newType: string) =>
                    handleChangeType(newType, index)
                  }
                  hasSubmitFailed={hasSubmitFailed}
                />
                <Spacer y={4} />
              </>
            </CardContainer>
            {isDragging && (
              <FieldControl onDragOver={(evt) => handleDragOver(evt, index)} />
            )}
          </FieldWrapper>
        ))}
        {isUploading && (
          <LoadingContainer>
            <DotLoader />
          </LoadingContainer>
        )}
      </AttachmentsWrapper>
      <Spacer y={25} />
      <InlineWrapper>
        {hasSubmitFailed && (
          <InputError>{t("product.attachments.submitFailed")}</InputError>
        )}
        <Filler />
        <ButtonSaveCancel
          variant="save"
          onClick={handleSubmit}
          isDisabled={false}
        >
          {t("product.attachments.saveButton")}
        </ButtonSaveCancel>
      </InlineWrapper>
      {isLoading && (
        <LoadingContainer>
          <DotLoader />
        </LoadingContainer>
      )}
    </Container>
  );
};

export default AttachmentWidget;
