import styled from "styled-components";

type FieldWrapperProps = {
  isFiltered?: boolean;
};

type CardContainerProps = {
  isDragged?: boolean;
  isMovingUp?: boolean;
  isMovingDown?: boolean;
  distance?: number;
};

export const Container = styled.div`
  width: 526px;
  background-color: ${(props) => props.theme.system.light1};
  box-shadow: 0px 10px 250px rgba(0, 0, 0, 0.1);
  border-radius: 16px;
  padding: 16px 24px 40px;
  position: relative;
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const AddExternalWrapper = styled.div`
  display: flex;
  align-items: center;

  position: relative;

  &:hover {
    cursor: pointer;
  }
`;

export const AttachmentsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`;

export const CardContainer = styled.div<CardContainerProps>`
  position: relative;
  opacity: ${(props) => (props.isDragged ? 0 : 1)};

  ${(props) =>
    props.isMovingDown
      ? `transform: translateY(-${props.distance}px); transition: transform 0.5s;`
      : ""}
  ${(props) =>
    props.isMovingUp
      ? `transform: translateY(${props.distance}px); transition: transform 0.5s;`
      : ""}
`;

export const FieldWrapper = styled.div<FieldWrapperProps>`
  position: relative;
  ${(props) => (props.isFiltered ? "display: none;" : "")}
`;

export const FieldControl = styled.div`
  width: 100%;
  height: 80%;
  position: absolute;
  top: 70%;
  left: 0;
  background-color: #00000000;
  transform: translateY(-50%);
  z-index: 1;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const LoadingContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 1;
  background-color: #ffffff80;
  top: 0;
  left: 0;
  border-radius: 16px;
`;
