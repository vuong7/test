import styled from "styled-components";

type ContainerProps = {
  isShowMore?: boolean;
};

type TagContainerProps = {
  isLoading?: boolean;
  isShowMore?: boolean;
};

type ViewTagProps = {
  hasHover?: boolean;
  isShowMore?: boolean;
};

export const Container = styled.div<ContainerProps>`
  display: flex;
  justify-content: space-between;
  align-items: ${(props) => (props.isShowMore ? "flex-start" : "center")};
  height: ${(props) => (props.isShowMore ? "auto" : "49px")};
  white-space: nowrap;
  overflow-y: hidden;
  ${(props) => (props.isShowMore ? "padding-top: 15px" : "")}
`;

export const AddNew = styled.div`
  display: flex;
  align-items: center;

  &:hover {
    cursor: pointer;
  }
`;

export const TagsContainer = styled.div<TagContainerProps>`
  flex-grow: 1;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  row-gap: ${(props) => (props.isShowMore ? "12px" : "50px")};
  overflow-y: hidden;
  position: relative;
  opacity: ${(props) => (props.isLoading ? 0 : 1)};
`;

export const AditionalTags = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 7px 8px;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid ${(props) => props.theme.system.neutral};
  border-radius: 100px;

  &:hover {
    cursor: pointer;
  }
`;

export const TagsWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const ViewTag = styled.div<ViewTagProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 5px 8px 4px;
  background-color: ${(props) =>
    props.isShowMore
      ? props.theme.system.light1
      : props.theme.contactTags.systemAdded};
  border: 1px solid ${(props) => props.theme.system.neutral};
  border-radius: 100px;
  box-sizing: border-box;

  &:hover {
    cursor: ${(props) => (props.hasHover ? "pointer" : "grab")};
  }
`;
