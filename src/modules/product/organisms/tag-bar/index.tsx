import React, { useCallback, useEffect, useState, useRef } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  AddNew,
  TagsContainer,
  AditionalTags,
  ViewTag,
  TagsWrapper,
} from "./styles";

import { TagSelected } from "../add-product-tags";

import { Icon, Typography, Spacer, Tag, ProductTagVariant } from "../../../";
import { ProductTag } from "../../atoms";

export type TagBarProps = {
  tagsSelected?: TagSelected[];
  onClickAddNew(): void;
  onClickRemoveTag(tag: Tag): void;
  isViewMode?: boolean;
};

export const TagBar: React.FC<TagBarProps> = ({
  tagsSelected,
  onClickAddNew,
  onClickRemoveTag,
  isViewMode,
}) => {
  const { t } = useTranslation();

  const ref = useRef<HTMLHeadingElement>(null);

  const [tagsToRender, setTagsToRender] = useState<TagSelected[]>(
    tagsSelected || []
  );
  const [isChanging, setIsChanging] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [newTagsSelection, setNewTagsSelection] = useState<TagSelected[]>(
    tagsSelected || []
  );

  const [isShowMore, setIsShowMore] = useState<boolean>(false);

  useEffect(() => {
    setNewTagsSelection(tagsSelected || []);
    setTagsToRender(tagsSelected || []);
    setIsLoading(true);
    setIsChanging(!isChanging);
  }, [tagsSelected]);

  useEffect(() => {
    const height = ref.current ? ref.current.offsetHeight : 0;
    if (height > 55) {
      setTagsToRender([...tagsToRender.slice(0, tagsToRender.length - 1)]);
      setIsChanging(!isChanging);
    } else {
      // setTimeout(() => {
      setIsLoading(false);
      // }, 500);
    }
  }, [isChanging]);

  useEffect(() => {
    if (isShowMore) {
      setTagsToRender(tagsSelected || []);
    } else {
      setIsLoading(true);
      setIsChanging(!isChanging);
    }
  }, [isShowMore]);

  const handleChangeTag = (tag: Tag, status: ProductTagVariant) => {
    // setIsChanging(!isChanging);
    // setIsLoading(true);
    // setTagsToRender([
    //   ...newTagsSelection.filter((selected) => selected.id !== tag.id),
    // ]);
    // setNewTagsSelection([
    //   ...newTagsSelection.filter((selected) => selected.id !== tag.id),
    // ]);
    onClickRemoveTag(tag);
  };

  return (
    <Container isShowMore={isShowMore}>
      <Typography variant="h4">{`${t("product.addTags.tags")}${
        newTagsSelection.length > 0 ? ` (${newTagsSelection.length})` : ""
      }`}</Typography>
      <Spacer x={36} />
      <TagsContainer ref={ref} isLoading={isLoading} isShowMore={isShowMore}>
        {tagsToRender?.map((tag, index) => (
          <TagsWrapper key={index}>
            {!isViewMode ? (
              <ProductTag
                tag={tag}
                variant={tag.isPrimary ? "primary" : "selected"}
                onClickChangeTag={handleChangeTag}
                customIcon="close-circle"
              />
            ) : (
              <ViewTag>
                <Typography variant="p-small-strong">{tag.name}</Typography>
              </ViewTag>
            )}
            <Spacer x={7} />
          </TagsWrapper>
        ))}
        {tagsToRender.length !== newTagsSelection.length && (
          <>
            {!isViewMode ? (
              <AditionalTags onClick={onClickAddNew}>
                <Typography variant="titles" color="system/neutral">
                  {"+"}
                  {newTagsSelection.length - tagsToRender.length}
                </Typography>
              </AditionalTags>
            ) : (
              <ViewTag onClick={() => setIsShowMore(!isShowMore)} hasHover>
                <Typography variant="p-small-strong">
                  {"+"}
                  {newTagsSelection.length - tagsToRender.length}
                </Typography>
              </ViewTag>
            )}
          </>
        )}
        {isShowMore && (
          <ViewTag
            onClick={() => setIsShowMore(!isShowMore)}
            hasHover
            isShowMore
          >
            <Typography variant="p-small-strong">
              {t("product.addTags.hide")}
            </Typography>
          </ViewTag>
        )}
      </TagsContainer>
      <Spacer x={12} />
      {!isViewMode && (
        <AddNew onClick={onClickAddNew}>
          <Icon type="plus" size="sm" color="primary" />
          <Spacer x={7} />
          <Typography variant="h4" color="primary">
            {t("product.addTags.addNew")}
          </Typography>
          <Spacer x={22} />
        </AddNew>
      )}
    </Container>
  );
};

export default TagBar;
