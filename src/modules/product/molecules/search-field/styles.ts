import styled from "styled-components";

import { Input, InputLabel } from "../../../";

export const Container = styled.div`
  display: grid;
  justify-items: start;
  align-items: center;
  margin: 0px;

  grid-gap: 8px;
  grid-template-rows: min-content min-content;
  grid-template-areas:
    "label"
    "input";

  &:focus {
    outline: none;
  }
`;

export const GridInput = styled(Input)`
  grid-area: input;

  &:focus {
    outline: none;
  }
`;

export const GridInputLabel = styled(InputLabel)`
  grid-area: label;
  &:focus {
    outline: none;
  }
`;

export const InputWrapper = styled.div`
  position: relative;
  width: 100%;

  svg {
    position: absolute;
    top: 50%;
    left: 12px;
    transform: translateY(-50%);
  }

  input {
    width: 100%;
    padding-left: 39px;
    &:focus {
      outline: none;
    }
  }
  &:focus {
    outline: none;
  }
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
