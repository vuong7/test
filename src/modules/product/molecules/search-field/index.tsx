import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  GridInput,
  GridInputLabel,
  InputWrapper,
  IconContainer,
} from "./styles";

import { Input, Icon } from "../../../";

export type SearchFieldProps = {
  delay?: number;
  onChange(value: string): void;
  placeholder?: string;
};

export const SearchField: React.FC<SearchFieldProps> = ({
  delay = 300,
  onChange,
  placeholder = "Type to search",
}) => {
  const { t } = useTranslation();

  const [inputValue, setInputValue] = useState<string>("");

  const handleOnChange = useCallback((event) => {
    setInputValue(event.target.value);
  }, []);

  const handleClear = useCallback((event) => {
    setInputValue("");
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => onChange(inputValue), delay);
    return () => clearTimeout(timer);
    // eslint-disable-next-line
  }, [inputValue]);

  return (
    <Container>
      <InputWrapper>
        {inputValue === "" ? (
          <IconContainer>
            <Icon type="search" size="sm" color="neutral/90" />
          </IconContainer>
        ) : (
          <IconContainer onClick={handleClear}>
            <Icon type="close" size="sm" color="neutral/90" />
          </IconContainer>
        )}
        <GridInput
          placeholder={t(placeholder)}
          value={inputValue}
          onChange={handleOnChange}
        />
      </InputWrapper>
    </Container>
  );
};

export default SearchField;
