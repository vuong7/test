import React, { FC, useEffect, useState } from "react";

import {
  Container,
  FilterContainer,
  FilterOption,
  Filler,
  IconContainer,
} from "./styles";

import { AttachmentData } from "../../";

import { Icon, Typography, Spacer, useComponentVisible } from "../../../";
import { useTranslation } from "react-i18next";

export type FilterMenuProps = {
  currentAttachments: AttachmentData[];
  documentTypes: string[];
  onChange(idList: string[]): void;
};

export const FilterMenu: FC<FilterMenuProps> = ({
  currentAttachments,
  documentTypes,
  onChange,
}) => {
  const { t } = useTranslation();
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const [currentTypes, setCurrentTypes] = useState<string[]>([]);
  const [chosenTypes, setChosenTypes] = useState<string[]>([""]);

  useEffect(() => {
    const validTypes = documentTypes.filter((type) =>
      currentAttachments.some((attachment) => attachment.documentType === type)
    );
    const newTypes = validTypes.filter((type) => !currentTypes.includes(type));
    setCurrentTypes(validTypes);
    setChosenTypes([...chosenTypes, ...newTypes.map((type) => type)]);
  }, [documentTypes, currentAttachments]);

  useEffect(() => {
    onChange(chosenTypes);
  }, [chosenTypes]);

  const handleChosenTypes = (evt: React.MouseEvent, id: string) => {
    evt.stopPropagation();
    if (chosenTypes.includes(id)) {
      setChosenTypes(chosenTypes.filter((type) => type !== id));
    } else {
      setChosenTypes([...chosenTypes, id]);
    }
  };

  return (
    <Container
      ref={ref}
      onClick={(evt) => {
        evt.stopPropagation();
        setIsComponentVisible(!isComponentVisible);
      }}
    >
      <Icon type="sliders" size="sm" />
      {isComponentVisible && (
        <FilterContainer>
          {currentTypes.length === 0 ? (
            <FilterOption>
              <Typography variant="p-semi-bold" color="neutral">
                {t("product.attachments.noTypes")}
              </Typography>
            </FilterOption>
          ) : (
            <>
              <FilterOption onClick={(evt) => handleChosenTypes(evt, "")}>
                <Typography variant="p-semi-bold" color="neutral">
                  {`${t("product.attachments.unselected")} (${
                    currentAttachments.filter(
                      (count) => count.documentType === undefined
                    ).length
                  })`}
                </Typography>
                <Spacer x={30} />
                <Filler />
                <IconContainer isSelected={chosenTypes.includes("")}>
                  <Icon type="check" size="sm" color="highlight" />
                </IconContainer>
              </FilterOption>
              {currentTypes.map((type) => (
                <FilterOption onClick={(evt) => handleChosenTypes(evt, type)}>
                  <Typography
                    variant="p-semi-bold"
                    color="neutral"
                  >{`${type} (${
                    currentAttachments.filter(
                      (count) => count.documentType === type
                    ).length
                  })`}</Typography>
                  <Spacer x={30} />
                  <Filler />
                  <IconContainer isSelected={chosenTypes.includes(type)}>
                    <Icon type="check" size="sm" color="highlight" />
                  </IconContainer>
                </FilterOption>
              ))}
            </>
          )}
        </FilterContainer>
      )}
    </Container>
  );
};

export default FilterMenu;
