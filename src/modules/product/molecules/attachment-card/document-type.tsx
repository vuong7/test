import React, { FC, useEffect, useState } from "react";
import {
  SelectedType,
  OptionsContainer,
  Option,
  OptionsContainerWrapper,
} from "./styles";

import {
  Typography,
  InlineWrapper,
  Spacer,
  Icon,
  useComponentVisible,
} from "../../..";
import { useTranslation } from "react-i18next";

export type DocumentTypeProps = {
  types: string[];
  current?: string;
  onChange(value: string): void;
  hasSubmitFailed?: boolean;
};

export const DocumentType: FC<DocumentTypeProps> = ({
  types,
  current,
  onChange,
  hasSubmitFailed,
}) => {
  const { t } = useTranslation();

  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const [selectedType, setSelectedType] = useState<string>(current || "");
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    setSelectedType(current || "");
  }, [current]);

  const handleSelect = (type: string) => {
    setSelectedType(type);
    onChange(type);
  };

  useEffect(() => {
    setIsError(hasSubmitFailed ? selectedType === "" : false);
  }, [hasSubmitFailed, current, selectedType]);

  return (
    <SelectedType
      ref={ref}
      onClick={(evt) => {
        evt.stopPropagation();
        setIsComponentVisible(!isComponentVisible);
      }}
      isError={isError}
    >
      <Typography
        variant="p-small-strong"
        color={isError ? "error" : "system/neutral"}
      >
        {selectedType === ""
          ? t("product.attachments.selectType")
          : selectedType}
      </Typography>
      <Spacer x={10} />
      <Icon
        type="arrow-down"
        size="sm"
        color={isError ? "error" : "system/neutral"}
      />
      {isComponentVisible && (
        <OptionsContainerWrapper>
          <OptionsContainer>
            {types.map((type, index) => (
              <Option key={index} onClick={() => handleSelect(type)}>
                <Typography variant="p-semi-bold">{type}</Typography>
              </Option>
            ))}
          </OptionsContainer>
        </OptionsContainerWrapper>
      )}
    </SelectedType>
  );
};

export default DocumentType;
