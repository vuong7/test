import styled from "styled-components";

import { ScrollbarStyled } from "../../../core/utils/scrollbar";
import { Input } from "../../../core/atoms/input/index";

type ContainerProps = {
  isPrimary?: boolean;
};

type SelectedTypeProps = {
  isError?: boolean;
};

export const Container = styled.div<ContainerProps>`
  background-color: ${(props) => props.theme.system.light2};
  border-radius: 8px;
  border: 1px solid
    ${(props) => (props.isPrimary ? props.theme.highlight : "#00000000")};

  width: 100%;

  box-sizing: border-box;
  position: relative;
  padding: 10px 3px 12px 8px;
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const DragIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  &:hover {
    cursor: grab;
  }
`;

export const ThumNailContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 42px;
  min-width: 63px;
  width: 63px;
  background-color: ${(props) => props.theme.system.light1};

  border: 1px solid ${(props) => props.theme.neutralTs[10]};
  box-sizing: border-box;
  border-radius: 4px;
  position: relative;
`;

export const PrimaryContainer = styled.div`
  position: absolute;
  bottom: 0px;
  right: 3px;
  z-index: 1;
  width: fit-content;
  height: fit-content;
`;

export const FileName = styled.div`
  display: flex;
  align-items: flex-start;
  flex-grow: 1;
  height: 36px;
  overflow-wrap: anywhere;
  white-space: wrap;
  overflow: hidden;
  text-overflow: ellipsis;
  box-sizing: border-box;
  width: fit-content;
  flex-grow: 1;

  position: relative;

  &:hover :last-child {
    opacity: 1;
  }
`;

export const IconContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const IconOptionContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 31px;
  padding: 0px 12px;
  box-sizing: border-box;
  border-left: 1px solid ${(props) => props.theme.neutralTs[10]};
  position: relative;

  &:hover {
    cursor: pointer;
  }
`;

export const SelectedType = styled.div<SelectedTypeProps>`
  border: 1px solid
    ${(props) =>
      props.isError ? props.theme.system.error : props.theme.system.neutral};
  padding: 2px 8px;
  height: 20px;
  box-sizing: border-box;
  border-radius: 40px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  /* white-space: nowrap; */

  position: relative;
  background-color: ${(props) => props.theme.system.light1};

  &:hover {
    cursor: pointer;
  }
`;

export const OptionsContainerWrapper = styled.div`
  /* position: relative;
  top: 31px;
  right: 12px;
  background-color: red;
  z-index: 9;
  opacity: 1; */
  /* background-color: ${(props) => props.theme.system.light1}; */
`;

export const OptionsContainer = styled.div`
  position: absolute;
  top: 31px;
  right: 12px;
  /* padding: 18px; */
  box-sizing: border-box;
  outline: none;
  background-color: ${(props) => props.theme.system.light1};

  box-shadow: 0px 13px 48px #aaa2b4;
  border-radius: 8px;

  border: 1px solid rgba(205, 208, 227, 0.6);
  overflow-y: auto;
  max-height: 400px;
  opacity: 1;
  z-index: 9;

  ${ScrollbarStyled}
`;

export const Option = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 12px 28px 12px 18px;
  display: flex;
  align-items: center;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid #ffffff00;
  outline: none;
  box-sizing: border-box;
  white-space: nowrap;

  position: relative;

  &:first-child {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
  }
  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.contactTags.systemAdded};
  }
`;

export const NameInput = styled(Input)`
  box-sizing: border-box;
  width: fit-content;
  flex-grow: 1;
  position: relative;
`;

export const EditContainer = styled.div`
  opacity: 0;
  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;

  &:hover {
    cursor: pointer;
    opacity: 1;
  }
`;

export const InputUrlContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
`;
