import React, { FC, useState, useEffect } from "react";
import {
  Container,
  Wrapper,
  Filler,
  DragIcon,
  ThumNailContainer,
  PrimaryContainer,
  FileName,
  IconContainer,
  IconOptionContainer,
  OptionsContainer,
  Option,
  NameInput,
  EditContainer,
  InputUrlContainer,
} from "./styles";

import DocumentType from "./document-type";

import {
  Typography,
  Spacer,
  Icon,
  formatFullDateAndTime,
  useComponentVisible,
  InlineWrapper,
  InputError,
} from "../../..";

import { AttachmentData } from "../../";

import { IconTypes } from "../../../core/atoms/icon/svgs";
import { getFileType } from "../../../core/utils";

import { useTranslation } from "react-i18next";

export type AttachmentCardProps = {
  attachment: AttachmentData;

  isPrimary?: boolean;
  onClickDelete(): void;
  onChangePrivate(newValue: boolean): void;
  onClickRename(newName: string, isFile: boolean): void;
  hasSubmitFailed?: boolean;
  documentTypes: string[];
  onChangeType(newType: string): void;
};

export const AttachmentCard: FC<AttachmentCardProps> = ({
  attachment,

  isPrimary,
  onClickDelete,
  onChangePrivate,
  onClickRename,
  hasSubmitFailed,
  documentTypes,
  onChangeType,
}) => {
  const { t } = useTranslation();

  // const img = new Image(1, 1);
  // img.src =
  //   "https://icons-for-free.com/iconfiles/png/512/part+2+video-1320568372949772743.png";

  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const [fileName, setFileName] = useState<string>(
    attachment.fileType === "FILE"
      ? attachment.name || ""
      : attachment.externalUrl || ""
  );
  const [uploadedAt, setUploadedAt] = useState<string>(
    formatFullDateAndTime(attachment.uploadedAt)
  );

  const [isPrivate, setIsPrivate] = useState<boolean>(!!attachment.isPrivate);

  const [newName, setNewName] = useState<string>(
    attachment.fileType === "FILE"
      ? attachment.name || ""
      : attachment.externalUrl || ""
  );
  const [isEditingName, setIsEditingName] = useState<boolean>(
    attachment.externalUrl === "" ? true : false
  );
  const [isNameInvalid, setIsNameInvalid] = useState<boolean>(false);
  const [documentType, setDocumentType] = useState<string>();

  const [primaryIcon, setPrimaryIcon] = useState<boolean>(false);

  useEffect(() => {
    setFileName(
      attachment.fileType === "FILE"
        ? attachment.name || ""
        : attachment.externalUrl || ""
    );
    setNewName(
      attachment.fileType === "FILE"
        ? attachment.name || newName
        : attachment.externalUrl || newName
    );
    setIsEditingName(attachment.externalUrl === "" ? true : false);
    setDocumentType(attachment.documentType);
  }, [attachment]);

  const handlePrivate = (newValue: boolean) => {
    setIsPrivate(newValue);
    onChangePrivate(newValue);
  };

  const handleRename = () => {
    const isFile = attachment.fileType === "FILE";
    if (!isFile) {
      try {
        let url = new URL(newName);
        setIsEditingName(false);
        setIsNameInvalid(false);
      } catch {
        setIsNameInvalid(true);
      }
      onClickRename(newName, isFile);
    } else if (newName !== "") {
      setIsEditingName(false);
      const extension = fileName.slice(fileName.lastIndexOf("."));
      const newNameExtension = newName.endsWith(extension)
        ? newName
        : `${newName}${extension}`;
      onClickRename(newNameExtension, isFile);
    } else {
      setIsEditingName(false);
      onClickRename(fileName, isFile);
    }
  };

  const handleDocumentType = (newType: string) => {
    onChangeType(newType);
  };

  useEffect(() => {
    if (hasSubmitFailed) {
      handleRename();
    }
  }, [hasSubmitFailed]);

  useEffect(() => {
    setPrimaryIcon(!!isPrimary);
  }, [isPrimary]);

  const getExtensionIcon = () => {
    const mappedTypes: Record<string, string> = {
      VIDEO: "video-light",
      IMAGE: "image-light",
      PDF: "pdf-light",
      EXTERNAL_LINK: "external-url",
    };

    if (attachment.extension) {
      const type = getFileType(attachment.extension);
      return mappedTypes[type] || "attachment";
    }

    return "attachment";
  };

  return (
    <Container isPrimary={isPrimary}>
      <Wrapper>
        <Spacer x={4} />
        <Typography variant="p-small">
          {t("product.attachments.uploadedOn", [
            uploadedAt.slice(0, uploadedAt.lastIndexOf(" ")),
          ])}
        </Typography>
        <Filler />

        <Typography variant="p-small">
          {t("product.attachments.documentType")}
        </Typography>
        <Spacer x={12} />
        <DocumentType
          current={documentType}
          types={documentTypes}
          onChange={handleDocumentType}
          hasSubmitFailed={hasSubmitFailed}
        />
      </Wrapper>
      <Spacer y={8} />
      <Wrapper>
        <DragIcon>
          <Icon type="menu" size="sm" color="neutral/40" />
        </DragIcon>
        <Spacer x={8} />
        <ThumNailContainer>
          <Icon
            type={(getExtensionIcon() as IconTypes) || "attachment"}
            color="neutral/90"
          />
          {primaryIcon && (
            <PrimaryContainer>
              <Icon type="primary" color="highlight" size="sm" />
            </PrimaryContainer>
          )}
        </ThumNailContainer>
        <Spacer x={8} />
        {isEditingName ? (
          <NameInput
            value={newName}
            onChange={(evt) => setNewName(evt.target.value)}
            isError={isNameInvalid}
            onKeyDown={(evt) => (evt.key === "Enter" ? handleRename() : null)}
            onBlur={handleRename}
            autoFocus
            draggable={true}
            onDragStart={(evt) => {
              evt.stopPropagation();
              evt.preventDefault();
            }}
          />
        ) : (
          <>
            <FileName>
              <Typography variant="titles" color="neutral">
                {fileName}
              </Typography>
              <Spacer x={8} />
              <EditContainer onClick={() => setIsEditingName(true)}>
                <Icon type="edit" size="sm" color="neutral/80" />
              </EditContainer>
            </FileName>
          </>
        )}
        <Spacer x={8} />
        {!attachment.externalUrl &&
          attachment.externalUrl !== "" &&
          !isEditingName && (
            <IconOptionContainer
              ref={ref}
              onClick={(evt) => {
                evt.stopPropagation();
                setIsComponentVisible(!isComponentVisible);
              }}
            >
              <Icon
                type={isPrivate ? "lock" : "globe"}
                size="sm"
                color="neutral/80"
              />
              <Spacer x={5} />
              <Icon type="arrow-down" size="sm" color="neutral/80" />
              {isComponentVisible && (
                <OptionsContainer>
                  <Option onClick={() => handlePrivate(false)}>
                    <IconContainer>
                      <Icon type="globe" size="sm" color="neutral/80" />
                    </IconContainer>
                    <Spacer x={10} />
                    <Typography variant="p-semi-bold">
                      {t("product.attachments.public")}
                    </Typography>
                    <Spacer x={30} />
                    <Filler />
                    {!isPrivate && (
                      <IconContainer>
                        <Icon type="check" size="sm" color="highlight" />
                      </IconContainer>
                    )}
                  </Option>

                  <Option onClick={() => handlePrivate(true)}>
                    <IconContainer>
                      <Icon type="lock" size="sm" color="neutral/80" />
                    </IconContainer>
                    <Spacer x={10} />
                    <Typography variant="p-semi-bold">
                      {t("product.attachments.private")}
                    </Typography>
                    <Spacer x={30} />
                    <Filler />
                    {isPrivate && (
                      <IconContainer>
                        <Icon type="check" size="sm" color="highlight" />
                      </IconContainer>
                    )}
                  </Option>
                </OptionsContainer>
              )}
            </IconOptionContainer>
          )}
        {isEditingName && newName !== "" ? (
          <IconOptionContainer onClick={() => handleRename()}>
            <Icon type="check" color="neutral/80" />
          </IconOptionContainer>
        ) : (
          <IconOptionContainer onClick={onClickDelete}>
            <Icon type="close-circle" color="neutral/80" />
          </IconOptionContainer>
        )}
      </Wrapper>
      {isNameInvalid && (
        <InlineWrapper>
          <Spacer x={100} />
          <InputError>{t("product.attachments.invalidUrl")}</InputError>
        </InlineWrapper>
      )}
    </Container>
  );
};

export default AttachmentCard;
