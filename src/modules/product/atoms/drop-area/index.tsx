import React, { FC, useState, useEffect } from "react";
import { Container, DropInput, DropMessage } from "./styles";

import { Typography, InlineWrapper, Spacer, Icon } from "../../..";

import { useTranslation } from "react-i18next";

export type DropAreaProps = {
  accept?: string[];
  maxSize?: number;
  maxFiles?: number;
  onDrop(fileList: File[]): void;
  currentFiles?: File[];
  handleLoading?(value: boolean): void;
  variant?: "default" | "small";
};

export const DropArea: FC<DropAreaProps> = ({
  accept,
  maxSize,
  maxFiles,
  onDrop,
  currentFiles,
  handleLoading,
  variant = "default",
}) => {
  const { t } = useTranslation();

  const [currentCount, setCurrentCount] = useState<number>(0);
  const [currentSize, setCurrentSize] = useState<number>(0);

  const [isDragging, setIsDragging] = useState<boolean>(false);

  useEffect(() => {
    if (currentFiles) {
      setCurrentCount(currentFiles.length);
      setCurrentSize(
        currentFiles.reduce((acm, cv) => {
          return acm + cv.size / 1000000;
        }, 0)
      );
    } else {
      setCurrentCount(0);
      setCurrentSize(0);
    }
  }, [currentFiles]);

  const handleChange = (evt: any, files: FileList) => {
    if (handleLoading) {
      handleLoading(true);
    }

    const filesDropped = Array.from(files);
    const isTypesValid = accept
      ? filesDropped.every((file) =>
          accept.includes(file.type.split("/")[1].toUpperCase())
        )
      : true;
    // const isSizeValid = maxSize
    //   ? filesDropped.reduce((acm, cv) => {
    //       return acm + cv.size / 1000000;
    //     }, 0) +
    //       currentSize <=
    //     maxSize
    //   : true;
    const isSizeValid = maxSize
      ? filesDropped.every((file) => file.size / 1000000 <= maxSize)
      : true;
    const isCountValid = maxFiles
      ? currentCount + filesDropped.length <= maxFiles
      : true;

    if (isTypesValid && isSizeValid && isCountValid) {
      onDrop(filesDropped);
    } else if (handleLoading) {
      handleLoading(false);
    }
    evt.currentTarget.files = null;
  };

  const handleDrop = () => {
    setIsDragging(false);
  };

  return (
    <>
      <Container isDragging={isDragging} variant={variant}>
        {variant === "default" && (
          <>
            <InlineWrapper>
              <Icon type="image" color="neutral/80" />
            </InlineWrapper>
            <Spacer y={4} />
          </>
        )}
        <InlineWrapper>
          <Typography variant="p" color="neutral">
            {!isDragging
              ? t("product.attachments.dragDropMessage")
              : t("product.attachments.dropFiles")}
          </Typography>
          {!isDragging && (
            <>
              <Spacer x={4} />
              <Typography variant="p-semi-bold" color="primary">{`${t(
                "product.attachments.browseMessage"
              )}`}</Typography>
            </>
          )}
        </InlineWrapper>

        <DropInput
          multiple
          type="file"
          onDrop={(evt) => {
            handleDrop();
          }}
          onChange={(evt) => {
            handleChange(evt, evt.currentTarget.files as FileList);
          }}
          onDragOver={() => setIsDragging(true)}
          onDragLeave={() => setIsDragging(false)}
        />
      </Container>
      {variant === "default" && (
        <>
          <Spacer y={12} />
          <InlineWrapper>
            <Typography variant="input-validation" color="neutral/80">
              {accept && t("product.attachments.accepted", [accept.join(", ")])}{" "}
              {maxSize && t("product.attachments.maxSize", [`${maxSize}MB`])}
            </Typography>
          </InlineWrapper>
        </>
      )}
    </>
  );
};

export default DropArea;
