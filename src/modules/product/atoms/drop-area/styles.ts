import styled from "styled-components";

type ContainerProps = {
  isDragging?: boolean;
  variant: string;
};

export const Container = styled.div<ContainerProps>`
  background-color: ${(props) => props.theme.neutralTs[10]};
  border-radius: 8px;
  border: 1px dashed
    ${(props) =>
      props.isDragging ? props.theme.highlight : props.theme.neutral};
  box-sizing: border-box;

  width: 100%;
  height: ${(props) => (props.variant === "default" ? "116px" : "62px")};
  position: relative;
  overflow: hidden;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;

export const DropInput = styled.input`
  width: 100%;
  height: 100%;
  background-color: #10808080;
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;

  &:hover {
    cursor: pointer;
  }
`;

export const DropMessage = styled.div``;
