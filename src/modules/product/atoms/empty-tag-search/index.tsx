import React, { FC } from "react";
import { Container } from "./styles";

import { Typography, Spacer, Icon } from "../../..";

import { useTranslation } from "react-i18next";

export type EmptyTagSearchProps = {
  isSearching?: boolean;
};

export const EmptyTagSearch: FC<EmptyTagSearchProps> = ({ isSearching }) => {
  const { t } = useTranslation();

  return (
    <Container>
      <Icon
        type={isSearching ? "close-circle" : "tag"}
        size="lg"
        color="neutral/80"
      />
      <Spacer y={8} />
      <Typography>
        {isSearching
          ? t("product.addTags.noTagsFound")
          : t("product.addTags.startTagSearch")}
      </Typography>
    </Container>
  );
};

export default EmptyTagSearch;
