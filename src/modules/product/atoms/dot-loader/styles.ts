import styled, { keyframes } from "styled-components";
// background-color: ${props => props.theme.brandingColors?.primary};

const bouncedelay = (props: any) => keyframes`
  0%, 33.33%, 66.66%, 100% {
    bottom: 0;
    background-color: ${props.color || props.theme.brandingColors?.primary};
  }
  16.66%, 50%, 83.33% {
    bottom: 20px;
    background-color: ${props.color || props.theme.brandingColors?.primary};
  }
}`;

export const ThreeBalls = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: center;
  min-height: 30px;
`;

type BallProps = {
  color?: string;
};
const Ball = styled.div<BallProps>`
  position: relative;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  display: inline-block;
  animation: ${(props) => bouncedelay(props)} 3s infinite
    cubic-bezier(0.62, 0.28, 0.23, 0.99) both;
`;

export const Ball1 = styled(Ball)`
  -webkit-animation-delay: -0.14s;
  animation-delay: -0.14s;
  margin-right: 4px;
`;

export const Ball2 = styled(Ball)`
  -webkit-animation-delay: -0.08s;
  animation-delay: -0.08s;
`;

export const Ball3 = styled(Ball)`
  margin-left: 4px;
`;
