import React from "react";
import { ColorProps, useSelectColor } from "../../../";

import { ThreeBalls, Ball1, Ball2, Ball3 } from "./styles";

export const DotLoader: React.FC<ColorProps> = ({ color, customColor }) => {
  return (
    <ThreeBalls>
      <Ball1 color={useSelectColor(color, customColor)} />
      <Ball2 color={useSelectColor(color, customColor)} />
      <Ball3 color={useSelectColor(color, customColor)} />
    </ThreeBalls>
  );
};

export default DotLoader;
