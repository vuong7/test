import React, { FC } from "react";
import {
  Container,
  PrimaryTag,
  SelectedTag,
  UnselectedTag,
  IconContainer,
} from "./styles";

import { Typography, Spacer, Icon } from "../../../core";
import { IconTypes } from "../../../core/atoms/icon/svgs";

import { Tag } from "../../";

export type ProductTagVariant = "primary" | "selected" | "unselected";

const mappedStyles: Record<ProductTagVariant, any> = {
  primary: PrimaryTag,
  selected: SelectedTag,
  unselected: UnselectedTag,
};

export type ProductTagProps = {
  tag: Tag;
  variant?: ProductTagVariant;
  onClickChangeTag(tag: Tag, status: ProductTagVariant): void;
  customIcon?: IconTypes;
};

export const ProductTag: FC<ProductTagProps> = ({
  tag,
  variant = "unselected",
  onClickChangeTag,
  customIcon,
}) => {
  const Component: React.FC = mappedStyles[variant];

  const handleOnClick = () => {
    onClickChangeTag(tag, variant);
  };

  const iconType =
    variant === "unselected"
      ? "plus"
      : variant === "selected"
      ? "check"
      : "close-circle";

  return (
    <Container>
      <Component>
        <Typography
          variant="titles"
          color={variant === "unselected" ? "system/neutral" : "system/light1"}
        >
          {tag.name}
        </Typography>
        <Spacer x={8} />
        <IconContainer onClick={handleOnClick}>
          <Icon
            type={customIcon ? customIcon : iconType}
            size="sm"
            color={
              variant === "unselected" ? "system/neutral" : "system/light1"
            }
          />
        </IconContainer>
      </Component>
    </Container>
  );
};

export default ProductTag;
