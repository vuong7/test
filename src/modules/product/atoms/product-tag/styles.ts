import styled from "styled-components";

export const Container = styled.div`
  display: inline-block;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;

export const PrimaryTag = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 7px 8px;
  background-color: ${(props) => props.theme.primary};
  border: 1px solid ${(props) => props.theme.primary};
  border-radius: 100px;
`;

export const SelectedTag = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 7px 8px;
  background-color: ${(props) => props.theme.system.neutral};
  border: 1px solid ${(props) => props.theme.system.neutral};
  border-radius: 100px;
`;

export const UnselectedTag = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 7px 8px;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid ${(props) => props.theme.system.neutral};
  border-radius: 100px;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;
