import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { SearchField, SearchFieldProps } from "..";

export default {
  title: "Product/SearchField",
  component: SearchField,
} as ComponentMeta<typeof SearchField>;

const Template: Story<SearchFieldProps> = (args) => (
  <SearchField {...args}>Example of Nav Tags text</SearchField>
);

export const Default = Template.bind({});
Default.args = {
  placeholder: "Type to search for a tag",
  onChange: (value: string) => console.log(value),
};
