import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AttachmentWidget, AttachmentWidgetProps } from "..";
import { AttachmentData } from "../..";

export default {
  title: "Product/AttachmentWidget",
  component: AttachmentWidget,
} as ComponentMeta<typeof AttachmentWidget>;

const Template: Story<AttachmentWidgetProps> = (args) => (
  <AttachmentWidget {...args}>Example of Nav Tags text</AttachmentWidget>
);

export const Default = Template.bind({});
Default.args = {
  onClickClose: () => console.log("close"),
  onSubmit: (attachments: AttachmentData[]) => console.log(attachments),
  documentTypes: [
    { id: "1", name: "Termsheet" },
    { id: "2", name: "Legal" },
    { id: "3", name: "Brochure" },
    { id: "4", name: "Subscription Form" },
  ],
};

export const Current = Template.bind({});
Current.args = {
  onClickClose: () => console.log("close"),
  onSubmit: (attachments: AttachmentData[]) => console.log(attachments),
  documentTypes: [
    { id: "1", name: "Termsheet" },
    { id: "2", name: "Legal" },
    { id: "3", name: "Brochure" },
    { id: "4", name: "Subscription Form" },
  ],
  currentAttachments: [
    {
      id: "first",
      name: "investment-chart.pdf",
      fileType: "FILE",
      uploadedAt: new Date(),
      documentType: "Brochure",
      isPrivate: true,
      isPrimary: false,
      externalUrl: "NVFIUNUIRAWENFUIWEN543EUWI",
    },
    {
      id: "second",
      fileType: "EXTERNAL_LINK",
      uploadedAt: new Date(),
      externalUrl: "https://google.com",
      documentType: "Legal",
      isPrivate: false,
      isPrimary: true,
      name: "should-be-hidden.jpg",
    },
    {
      id: "third",
      name: "recommendations.jpg",
      fileType: "FILE",
      uploadedAt: new Date(),
      documentType: "Brochure",
      isPrivate: true,
      isPrimary: false,
      externalUrl: "NVFIUNUIRAWENFUIWENFIEUWI",
    },
  ],
};
