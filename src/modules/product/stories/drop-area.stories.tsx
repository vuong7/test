import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { DropArea, DropAreaProps } from "..";

export default {
  title: "Product/DropArea",
  component: DropArea,
} as ComponentMeta<typeof DropArea>;

const Template: Story<DropAreaProps> = (args) => (
  <DropArea {...args}>Example</DropArea>
);

export const Default = Template.bind({});
Default.args = {};

export const Small = Template.bind({});
Small.args = { variant: "small" };
