import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AttachmentCard, AttachmentCardProps } from "..";

export default {
  title: "Product/AttachmentCard",
  component: AttachmentCard,
} as ComponentMeta<typeof AttachmentCard>;

const Template: Story<AttachmentCardProps> = (args) => (
  <AttachmentCard {...args}>Example</AttachmentCard>
);

export const Default = Template.bind({});
Default.args = {
  attachment: {
    file: {
      name: "Global insurance market lorem ipsum dolor.mp4",
      type: "video/mp4",
    },
    uploadedAt: new Date(),
  },
  documentTypes: [
    { id: "1", name: "Termsheet" },
    { id: "2", name: "Legal" },
    { id: "3", name: "Brochure" },
    { id: "4", name: "Subscription Form" },
  ],
};

export const ExternalURL = Template.bind({});
ExternalURL.args = {
  attachment: {
    externalUrl: "https://www.jobhop.technology/",
    uploadedAt: new Date(),
  },
  isPrimary: true,
  documentTypes: [
    { id: "1", name: "Termsheet" },
    { id: "2", name: "Legal" },
    { id: "3", name: "Brochure" },
    { id: "4", name: "Subscription Form" },
  ],
};

export const CreatingUrl = Template.bind({});
CreatingUrl.args = {
  attachment: {
    externalUrl: "",
    uploadedAt: new Date(),
  },
  documentTypes: [
    { id: "1", name: "Termsheet" },
    { id: "2", name: "Legal" },
    { id: "3", name: "Brochure" },
    { id: "4", name: "Subscription Form" },
  ],
};
