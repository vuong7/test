import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { AddProductTags, AddProductTagsProps } from "..";

export default {
  title: "Product/AddProductTags",
  component: AddProductTags,
} as ComponentMeta<typeof AddProductTags>;

const Template: Story<AddProductTagsProps> = (args) => (
  <AddProductTags {...args}>Example of Nav Tags text</AddProductTags>
);

export const Default = Template.bind({});
Default.args = {
  tags: [
    {
      id: "1",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "3",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "5",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "6",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "7",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "8",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "9",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "10",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "11",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "12",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "13",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "24234",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "231",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "26",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1535",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "23",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "19",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2yr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hthr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2hrther",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1kyukykyu",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2mutmut",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1thrhrth",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "ehthr2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hyrtmytm",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2fntnft",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrtrnr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2ngfxn",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myytmrn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2rtmytehrhe",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrjtyrehr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2trhrtjrdh",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1rtjrthrh",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2trjrthrth",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrhrsheh",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2htrjrsh",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1jtrjsrtjrdj",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2jtrjrshjrs",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myufmftnn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hregreg",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2865i67j7",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1j76jdtjtdj6",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2j65ejjyj65",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "j65rjrj6he41",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
  ],
  onClickClose: () => console.log("close"),
  onClickSave: (tagsSelected) => console.log(tagsSelected),
};

export const SelectedStart = Template.bind({});
SelectedStart.args = {
  tags: [
    {
      id: "1",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "3",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "5",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "6",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "7",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "8",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "9",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "10",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "11",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "12",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "13",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "24234",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "231",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "26",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1535",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "23",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "19",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2yr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hthr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2hrther",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1kyukykyu",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2mutmut",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1thrhrth",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "ehthr2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hyrtmytm",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2fntnft",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrtrnr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2ngfxn",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myytmrn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2rtmytehrhe",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrjtyrehr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2trhrtjrdh",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1rtjrthrh",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2trjrthrth",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrhrsheh",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2htrjrsh",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1jtrjsrtjrdj",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2jtrjrshjrs",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myufmftnn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hregreg",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2865i67j7",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1j76jdtjtdj6",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2j65ejjyj65",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "j65rjrj6he41",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
  ],

  tagsSelected: [
    {
      id: "1",
      name: "Business",
      category: { id: "1", name: "Market" },
      isPrimary: true,
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
      isPrimary: true,
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
  ],
  onClickClose: () => console.log("close"),
  onClickSave: (tagsSelected) => console.log(tagsSelected),
};
