import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { DotLoader } from "..";

import { ColorProps } from "../../../";

export default {
  title: "Product/DotLoader",
  component: DotLoader,
} as ComponentMeta<typeof DotLoader>;

const Template: Story<ColorProps> = (args) => (
  <DotLoader {...args}>Example of Nav Tags text</DotLoader>
);

export const Default = Template.bind({});
Default.args = {
  color: "primary",
};
