import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { EmptyTagSearch, EmptyTagSearchProps } from "..";

export default {
  title: "Product/EmptyTagSearch",
  component: EmptyTagSearch,
} as ComponentMeta<typeof EmptyTagSearch>;

const Template: Story<EmptyTagSearchProps> = (args) => (
  <EmptyTagSearch {...args}>Example of Nav Tags text</EmptyTagSearch>
);

export const Default = Template.bind({});
Default.args = {};

export const Searching = Template.bind({});
Searching.args = { isSearching: true };
