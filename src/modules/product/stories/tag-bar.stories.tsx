import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { TagBar, TagBarProps } from "..";

export default {
  title: "Product/TagBar",
  component: TagBar,
} as ComponentMeta<typeof TagBar>;

const Template: Story<TagBarProps> = (args) => (
  <TagBar {...args}>Example of Nav Tags text</TagBar>
);

export const Default = Template.bind({});
Default.args = {};

export const SelectedStart = Template.bind({});
SelectedStart.args = {
  tagsSelected: [
    {
      id: "1",
      name: "Business",
      category: { id: "1", name: "Market" },
      isPrimary: true,
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
      isPrimary: true,
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
  ],
};

export const ManyTags = Template.bind({});
ManyTags.args = {
  tagsSelected: [
    {
      id: "1",
      name: "Business market plus",
      category: { id: "1", name: "Market" },
      isPrimary: true,
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
      isPrimary: true,
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1535",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "23",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "19",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2yr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hthr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2hrther",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1kyukykyu",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2mutmut",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1thrhrth",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "ehthr2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hyrtmytm",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2fntnft",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrtrnr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2ngfxn",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myytmrn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
  ],
};

export const ViewMode = Template.bind({});
ViewMode.args = {
  isViewMode: true,
  tagsSelected: [
    {
      id: "1",
      name: "Business market plus",
      category: { id: "1", name: "Market" },
      isPrimary: true,
    },
    {
      id: "256j5h54h4",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
      isPrimary: true,
    },
    {
      id: "2gerhtrjsr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1535",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "23",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "19",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2yr",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hthr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2hrther",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1kyukykyu",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2mutmut",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1thrhrth",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "ehthr2",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1hyrtmytm",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2fntnft",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1htrtrnr",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
    {
      id: "2ngfxn",
      name: "BitCoin",
      category: { id: "2", name: "Digital" },
    },
    {
      id: "1myytmrn",
      name: "Business",
      category: { id: "1", name: "Market" },
    },
  ],
};
