import React, { FC, useEffect, useState } from "react";

import {
  Container,
  FilterContainer,
  FilterOption,
  Filler,
  IconContainer,
  Triangle,
} from "./styles";

import { Icon, Typography, Spacer, useComponentVisible } from "../../../";
import { useTranslation } from "react-i18next";

export type OptionData = {
  name: string;
  count?: number;
};

export type DropDownFilterProps = {
  options: OptionData[];
  selected: string[];
  onSelect(option: string): void;
};

export const DropDownFilter: FC<DropDownFilterProps> = ({
  options,
  selected,
  onSelect,
}) => {
  const { t } = useTranslation();
  const { ref, isComponentVisible, setIsComponentVisible } =
    useComponentVisible(false);

  const [isFirstHover, setIsFirstHover] = useState<boolean>(false);

  useEffect(() => {
    setIsFirstHover(false);
  }, [isComponentVisible]);

  return (
    <Container
      ref={ref}
      onClick={(evt) => {
        evt.stopPropagation();
        setIsComponentVisible(!isComponentVisible);
        setIsFirstHover(false);
      }}
    >
      <Icon type="sliders" size="sm" />
      {isComponentVisible && (
        <>
          <FilterContainer>
            {options.map((option, index) => (
              <>
                <FilterOption
                  key={`${index}${option.name}`}
                  onClick={() => onSelect(option.name)}
                  onMouseOver={() =>
                    index === 0 ? setIsFirstHover(true) : null
                  }
                  onMouseOut={() =>
                    index === 0 ? setIsFirstHover(false) : null
                  }
                >
                  <Typography variant="p-semi-bold" color="neutral">
                    {option.count
                      ? `${option.name} (${option.count})`
                      : option.name}
                  </Typography>
                  <Spacer x={30} />
                  <Filler />
                  <IconContainer isSelected={selected.includes(option.name)}>
                    <Icon type="check" size="sm" color="highlight" />
                  </IconContainer>
                </FilterOption>
              </>
            ))}
          </FilterContainer>
          <Triangle hasHover={isFirstHover} />
        </>
      )}
    </Container>
  );
};

export default DropDownFilter;
