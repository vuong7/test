import styled from "styled-components";

import { ScrollbarStyled } from "../../../";

type IconContainerProps = {
  isSelected?: boolean;
};

type TriangleProps = {
  hasHover?: boolean;
};

export const Container = styled.div`
  background-color: ${(props) => props.theme.system.light2};

  position: relative;
  &:hover {
    cursor: pointer;
  }
`;

export const FilterContainer = styled.div`
  position: absolute;
  top: 31px;
  right: 12px;
  /* padding: 18px; */
  box-sizing: border-box;
  outline: none;
  background-color: ${(props) => props.theme.system.light1};

  box-shadow: 0px 13px 48px #aaa2b4;
  border-radius: 8px;
  z-index: 2;
  border: 1px solid rgba(205, 208, 227, 0.6);
  overflow-y: auto;
  max-height: 400px;
  ${ScrollbarStyled}
`;

export const FilterOption = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 12px 28px 12px 18px;
  display: flex;
  align-items: center;
  background-color: ${(props) => props.theme.system.light1};
  border: 1px solid #ffffff00;
  outline: none;
  box-sizing: border-box;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: clip;
  white-space: nowrap;
  position: relative;

  &:first-child {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
  }
  &:last-child {
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.contactTags.systemAdded};
  }
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const IconContainer = styled.div<IconContainerProps>`
  display: flex;
  justify-content: center;
  align-items: center;

  opacity: ${(props) => (props.isSelected ? 1 : 0)};
`;

export const Triangle = styled.div<TriangleProps>`
  position: absolute;
  top: 20px;
  right: 52px;
  z-index: 2;
  width: 0;
  height: 0;
  border-left: 8px solid transparent;
  border-right: 8px solid transparent;

  box-shadow: 0px 13px 48px #aaa2b4;

  border-bottom: 12px solid
    ${(props) =>
      props.hasHover
        ? props.theme.contactTags.systemAdded
        : props.theme.system.light1};
`;
