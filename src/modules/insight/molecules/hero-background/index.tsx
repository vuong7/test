import React from "react";

import { ProductBackground, InsightBackground } from "../../";

import { ColorProps, useSelectColor } from "../../../";

import { Wrapper } from "./styles";

type Props = ColorProps & {
  imageUrl?: string;
  variant?: "product" | "insight";
};
export const HeroBackground: React.FC<Props> = ({
  color = "primary",
  customColor,
  imageUrl,
  variant = "product",
}) => {
  const hexColor = useSelectColor(color, customColor);
  return (
    <Wrapper className="hero-background" image={imageUrl}>
      {!imageUrl && variant === "product" && (
        <ProductBackground color={hexColor} />
      )}
      {!imageUrl && variant === "insight" && (
        <InsightBackground color={hexColor} />
      )}
    </Wrapper>
  );
};

export default HeroBackground;
