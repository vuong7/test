import styled, { css } from "styled-components";

type WrapperProps = {
  image?: string;
};
export const Wrapper = styled.div<WrapperProps>`
  ${(props) =>
    props.image &&
    css`
      /* background-image: url(${props.image}); */
      background: linear-gradient(
          270.3deg,
          rgba(0, 0, 0, 0.38) 9.92%,
          rgba(0, 0, 0, 0) 87.19%
        ),
        linear-gradient(0deg, rgba(0, 0, 0, 0.43), rgba(0, 0, 0, 0.43)),
        no-repeat center center url(${props.image});
    `}
  background-size: cover;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  overflow: hidden;
`;
