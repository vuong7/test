import React, { FC, useState, useEffect } from "react";

import { IconContainer } from "./styles";

import { Icon } from "../../../";

export type FavoriteProps = {
  isFilled?: boolean;
  onClick(): void;
};

export const Favorite: FC<FavoriteProps> = ({ isFilled, onClick }) => {
  const [isStarFilled, setIsStarFilled] = useState<boolean>(false);

  useEffect(() => {
    setIsStarFilled((isStarFilled) => isFilled || isStarFilled);
  }, []);

  const handleFavorite = () => {
    onClick();
    setIsStarFilled(!isStarFilled);
  };

  return (
    <IconContainer onClick={() => handleFavorite()}>
      {isStarFilled ? (
        <Icon type="star-filled" size="sm" customColor="#FFA83E" />
      ) : (
        <Icon type="star" size="sm" color="system/light1" />
      )}
    </IconContainer>
  );
};

export default Favorite;
