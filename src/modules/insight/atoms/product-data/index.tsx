import React, { FC } from "react";

import { Container } from "./styles";

import { Typography, InlineWrapper, Spacer } from "../../../";

export type ProductDataProps = {
  data: string;
  subtitle: string;
};

export const ProductData: FC<ProductDataProps> = ({ data, subtitle }) => {
  return (
    <>
      <Container>
        <Typography variant="p-small-strong" color="neutral">
          {data}
        </Typography>
        <Spacer y={6} />
        <Typography variant="p-small" color="neutral/90">
          {subtitle}
        </Typography>
      </Container>
    </>
  );
};

export default ProductData;
