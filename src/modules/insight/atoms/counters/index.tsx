import React, { FC, useState, useEffect } from "react";

import { Container } from "./styles";

import { Icon, Typography, InlineWrapper, Spacer } from "../../../";

export type CountersProps = {
  variant: "views" | "users";
  count?: number;
};

export const Counters: FC<CountersProps> = ({ variant, count = 0 }) => {
  return (
    <>
      <Container>
        <InlineWrapper>
          <Icon
            type={variant === "views" ? "eye" : "users-two"}
            color="neutral/50"
            size="sm"
          />
          <Spacer x={4} />
          <Typography variant="p-small" color="neutral">
            {count}
          </Typography>
        </InlineWrapper>
      </Container>
    </>
  );
};

export default Counters;
