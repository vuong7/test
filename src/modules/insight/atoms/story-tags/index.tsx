import React, { FC } from "react";

import { Recommended, High, Medium, Low } from "./styles";

import { Typography } from "../../../";

import { useTranslation } from "react-i18next";

export type StoryTagVariant = "Recommended" | "Strong" | "Ok" | "Weak";

const mappedStyles: Record<StoryTagVariant, any> = {
  Recommended: Recommended,
  Strong: High,
  Ok: Medium,
  Weak: Low,
};

export type StoryTagsProps = {
  variant: StoryTagVariant;
};

export const StoryTags: FC<StoryTagsProps> = ({ variant }) => {
  const { t } = useTranslation();

  const Component = mappedStyles[variant];

  const mappedTexts: Record<StoryTagVariant, string> = {
    Recommended: t("insight.storyTags.recommended"),
    Strong: t("insight.storyTags.strong"),
    Ok: t("insight.storyTags.ok"),
    Weak: t("insight.storyTags.weak"),
  };

  return (
    <>
      <Component>
        <Typography variant="p-small-strong" color="system/light1">
          {mappedTexts[variant]}
        </Typography>
      </Component>
    </>
  );
};

export default StoryTags;
