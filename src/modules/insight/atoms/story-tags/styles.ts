import styled from "styled-components";

export const Recommended = styled.div`
  background-color: #ff3297;
  padding: 4px 8px;
  border-radius: 4px;
  display: inline-block;
`;

export const High = styled.div`
  background-color: ${(props) => props.theme.system.success};
  padding: 4px 8px;
  border-radius: 4px;
  display: inline-block;
`;

export const Medium = styled.div`
  background-color: ${(props) => props.theme.system.warning};
  padding: 4px 8px;
  border-radius: 4px;
  display: inline-block;
`;

export const Low = styled.div`
  background-color: ${(props) => props.theme.system.error};
  padding: 4px 8px;
  border-radius: 4px;
  display: inline-block;
`;
