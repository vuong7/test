import React from "react";

// import { Container } from './styles';

type Props = {
  color: string;
};

export const InsightBackground: React.FC<Props> = ({ color }) => {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 1920 1280"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      preserveAspectRatio="xMinYMin slice"
    >
      <g clipPath="url(#clip0_639_982)">
        <rect width="100%" height="100%" fill={color} />
        <g opacity="0.8">
          <path
            d="M-82.0011 902.999C-65.7721 911.32 -75.0392 932.795 -82.0011 946.892V957.467C-73.8032 951.701 -55.1202 935.625 -17.5001 902.999C152 755.999 648.678 596.936 937.999 902.999C1214.5 1195.5 2164.5 973.5 1995 463.5C1889.29 145.437 2377 320.999 2418.5 371.999C2451.7 412.799 2432.33 1067 2418.5 1389H-82.0011V957.467C-91.1602 963.909 -87.2311 957.482 -82.0011 946.892V902.999Z"
            fill="url(#paint0_linear_639_982)"
          />
          <path
            d="M1968.83 494.999C1952.6 503.32 1961.87 524.795 1968.83 538.892V549.467C1960.63 543.701 1941.95 527.625 1904.33 494.999C1734.83 347.999 1413.32 188.936 1124 494.999C847.5 787.5 -277.671 565.5 -108.17 55.4998C-2.46033 -262.563 -490.169 -87.0007 -531.669 -36.0007C-564.869 4.79926 -545.502 1023 -531.669 1345H1968.83V549.467C1977.99 555.909 1974.06 549.482 1968.83 538.892V494.999Z"
            fill="url(#paint1_linear_639_982)"
          />
          <path
            d="M-30.5 662V1292L1991 1419.5C2134.33 1085.67 2465.8 993.099 2075 975.499C1586.5 953.499 1737 454.499 1302.5 552.5C1057.32 607.799 971 188.5 672.5 454.499C400.64 696.759 55.6667 510.333 -30.5 662Z"
            fill="url(#paint2_linear_639_982)"
            fillOpacity="0.8"
          />
          <path
            d="M-808 1011.66V1641.66L1213.5 1769.16C1356.83 1435.33 1688.3 1342.76 1297.5 1325.16C809 1303.16 934.5 818.999 500 917C254.824 972.299 218.5 595.5 -137.5 733.5C-477.023 865.113 -721.833 859.996 -808 1011.66Z"
            fill="url(#paint3_linear_639_982)"
          />
        </g>
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_639_982"
          x1="1043.5"
          y1="516"
          x2="1177.42"
          y2="1389"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.14" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_639_982"
          x1="843.33"
          y1="108"
          x2="709.414"
          y2="981"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.14" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_639_982"
          x1="953.127"
          y1="615.148"
          x2="1083.48"
          y2="1417.24"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.14" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_639_982"
          x1="175.627"
          y1="964.811"
          x2="305.976"
          y2="1766.91"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.1" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <clipPath id="clip0_639_982">
          <rect width="100%" height="100%" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
};

export default InsightBackground;
