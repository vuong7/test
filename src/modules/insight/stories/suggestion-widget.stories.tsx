import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { SuggestionWidget, SuggestionWidgetProps } from "..";

export default {
  title: "Insight/SuggestionWidget",
  component: SuggestionWidget,
} as ComponentMeta<typeof SuggestionWidget>;

const Template: Story<SuggestionWidgetProps> = (args) => (
  <SuggestionWidget {...args}>Example of Suggestion Widget</SuggestionWidget>
);

export const Insight = Template.bind({});
Insight.args = {
  isRecommended: true,
  score: "Strong",
  isFavorite: true,
  onClickFavorite: () => console.log("favorite"),
  insightStory: {
    title: "My first insight story",
    thumbnail: "https://www.safewise.com/app/uploads/2020/01/facebook.jpg",
    category: "category",
    engagement: 140,
    globalView: 234,
    salesCloseDate: new Date(),
    text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore.",
    createdBy: "Credit Suisse",
  },
};

export const ProductEmpty = Template.bind({});
ProductEmpty.args = {
  score: "Weak",
  onClickFavorite: () => console.log("favorite"),
  productStory: {
    title: "Empty Product Story",
  },
};

export const Product = Template.bind({});
Product.args = {
  isRecommended: true,
  score: "Ok",
  isFavorite: true,
  onClickFavorite: () => console.log("favorite"),
  productStory: {
    title: "Product Story of the component, with all content available",
    thumbnail:
      "https://images.unsplash.com/photo-1477959858617-67f85cf4f1df?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max",
    category: "category",
    engagement: 140,
    globalView: 234,
    productMaturityDate: new Date(),
    logoImage:
      "https://www.brandland.in/wp-content/uploads/2018/12/dbs-logo-1.jpg",
    data: [
      { data: "US$10,000", subtitle: "Min. Investment" },
      { data: "US$10,000", subtitle: "Min. Investment" },
      { data: "Capital protected", subtitle: "Sub-type" },
    ],
  },
};

export const InsightEmpty = Template.bind({});
InsightEmpty.args = {
  onClickFavorite: () => console.log("favorite"),
  insightStory: {
    title: "My first insight story",
  },
};
