import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { StoryTellingSuggestions, StoryTellingSuggestionsProps } from "..";

export default {
  title: "Insight/StoryTellingSuggestions",
  component: StoryTellingSuggestions,
} as ComponentMeta<typeof StoryTellingSuggestions>;

const Template: Story<StoryTellingSuggestionsProps> = (args) => (
  <StoryTellingSuggestions {...args}>
    Example of Suggestion Widget
  </StoryTellingSuggestions>
);

export const Default = Template.bind({});
Default.args = {
  items: [
    {
      isRecommended: true,
      score: "Strong",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      insightStory: {
        id: "id1",
        title: "My first insight story",
        thumbnail: "https://www.safewise.com/app/uploads/2020/01/facebook.jpg",
        category: "category",
        engagement: 140,
        globalView: 234,
        salesCloseDate: new Date(),
        text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore.",
        createdBy: "Credit Suisse",
      },
    },

    {
      isRecommended: true,
      score: "Ok",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id2",
        title: "Product Story of the component, with all content available",
        thumbnail:
          "https://images.unsplash.com/photo-1477959858617-67f85cf4f1df?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max",
        category: "category",
        engagement: 140,
        globalView: 234,
        productMaturityDate: new Date(),
        logoImage:
          "https://www.brandland.in/wp-content/uploads/2018/12/dbs-logo-1.jpg",
        data: [
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "Capital protected", subtitle: "Sub-type" },
        ],
      },
    },
    {
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id3",
        title: "Empty Product Story",
      },
    },
    {
      isRecommended: true,
      score: "Weak",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      insightStory: {
        id: "id1",
        title: "My first insight story",
        thumbnail: "https://www.safewise.com/app/uploads/2020/01/facebook.jpg",
        category: "category",
        engagement: 140,
        globalView: 234,
        salesCloseDate: new Date(),
        text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore.",
        createdBy: "Credit Suisse",
      },
    },

    {
      isRecommended: true,
      score: "Ok",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id2",
        title: "Product Story of the component, with all content available",
        thumbnail:
          "https://images.unsplash.com/photo-1477959858617-67f85cf4f1df?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max",
        category: "category",
        engagement: 140,
        globalView: 234,
        productMaturityDate: new Date(),
        logoImage:
          "https://www.brandland.in/wp-content/uploads/2018/12/dbs-logo-1.jpg",
        data: [
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "Capital protected", subtitle: "Sub-type" },
        ],
      },
    },
    {
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id3",
        title: "Empty Product Story",
      },
    },
    {
      isRecommended: true,
      score: "Strong",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      insightStory: {
        id: "id1",
        title: "My first insight story",
        thumbnail: "https://www.safewise.com/app/uploads/2020/01/facebook.jpg",
        category: "category",
        engagement: 140,
        globalView: 234,
        salesCloseDate: new Date(),
        text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore.",
        createdBy: "Credit Suisse",
      },
    },

    {
      isRecommended: true,
      score: "Ok",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id2",
        title: "Product Story of the component, with all content available",
        thumbnail:
          "https://images.unsplash.com/photo-1477959858617-67f85cf4f1df?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max",
        category: "category",
        engagement: 140,
        globalView: 234,
        productMaturityDate: new Date(),
        logoImage:
          "https://www.brandland.in/wp-content/uploads/2018/12/dbs-logo-1.jpg",
        data: [
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "US$10,000", subtitle: "Min. Investment" },
          { data: "Capital protected", subtitle: "Sub-type" },
        ],
      },
    },
    {
      onClickFavorite: () => console.log("favorite"),
      productStory: {
        id: "id3",
        title: "Empty Product Story",
      },
    },
  ],
};

export const Smaller = Template.bind({});
Smaller.args = {
  isEditMode: true,
  items: [
    {
      isRecommended: true,
      score: "Strong",
      isFavorite: true,
      onClickFavorite: () => console.log("favorite"),
      insightStory: {
        id: "id1",
        title: "My first insight story",
        thumbnail: "https://www.safewise.com/app/uploads/2020/01/facebook.jpg",
        category: "category",
        engagement: 140,
        globalView: 234,
        salesCloseDate: new Date(),
        text: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga officia totam magnam amet ipsam laborum natus distinctio officiis soluta, ut, nesciunt, ex atque magni dolor nam odit vero deserunt inventore.",
        createdBy: "Credit Suisse",
      },
    },
  ],
};
