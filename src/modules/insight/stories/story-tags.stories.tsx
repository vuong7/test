import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { StoryTags, StoryTagsProps } from "..";

export default {
  title: "Insight/StoryTags",
  component: StoryTags,
} as ComponentMeta<typeof StoryTags>;

const Template: Story<StoryTagsProps> = (args) => (
  <StoryTags {...args}>Example of Suggestion Widget</StoryTags>
);

export const Recommended = Template.bind({});
Recommended.args = { variant: "Recommended" };

export const Strong = Template.bind({});
Strong.args = { variant: "Strong" };

export const Ok = Template.bind({});
Ok.args = { variant: "Ok" };

export const Weak = Template.bind({});
Weak.args = { variant: "Weak" };
