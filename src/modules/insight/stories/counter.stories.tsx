import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Counters, CountersProps } from "..";

export default {
  title: "Insight/Counters",
  component: Counters,
} as ComponentMeta<typeof Counters>;

const Template: Story<CountersProps> = (args) => (
  <Counters {...args}>Example of Suggestion Widget</Counters>
);

export const Views = Template.bind({});
Views.args = { variant: "views", count: 999 };

export const Users = Template.bind({});
Users.args = { variant: "users" };
