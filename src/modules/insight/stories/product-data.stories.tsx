import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { ProductData, ProductDataProps } from "..";

export default {
  title: "Insight/ProductData",
  component: ProductData,
} as ComponentMeta<typeof ProductData>;

const Template: Story<ProductDataProps> = (args) => (
  <ProductData {...args}>Example of Suggestion Widget</ProductData>
);

export const Default = Template.bind({});
Default.args = {
  data: "US$10,000",
  subtitle: "Min. investment",
};
