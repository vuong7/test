import React from "react";
import { ComponentMeta, Story } from "@storybook/react";

import { Favorite, FavoriteProps } from "..";

export default {
  title: "Insight/Favorite",
  component: Favorite,
} as ComponentMeta<typeof Favorite>;

const Template: Story<FavoriteProps> = (args) => (
  <Favorite {...args}>Example of Suggestion Widget</Favorite>
);

export const Default = Template.bind({});
Default.args = {
  onClick: () => console.log("favorite"),
};
