import React, { FC, useState, useEffect } from "react";

import {
  Container,
  ThumbContainer,
  TagsContainer,
  Filler,
  TitleContainer,
  LogoImage,
  CategoryTag,
  TextContainer,
} from "./styles";

import { Typography, InlineWrapper, Spacer, Divider } from "../../../";

import { useTranslation } from "react-i18next";
import {
  StoryTags,
  Favorite,
  Counters,
  ProductData,
  HeroBackground,
} from "../../";

export type InsightStoryContent = {
  id: string;
  title: string;
  thumbnail?: string;
  category?: string;
  engagement?: number;
  globalView?: number;
  salesCloseDate?: Date;
  text?: string;
  createdBy?: string;
};

export type ProductStoryContent = {
  id: string;
  title: string;
  thumbnail?: string;
  category?: string;
  engagement?: number;
  globalView?: number;
  productMaturityDate?: Date;
  logoImage?: string;
  data?: { data: string; subtitle: string }[];
};

export type SuggestionWidgetProps = {
  isRecommended?: boolean;
  score?: "Strong" | "Ok" | "Weak";
  isFavorite?: boolean;
  onClickFavorite(id: string): void;
  insightStory?: InsightStoryContent;
  productStory?: ProductStoryContent;
};

export const SuggestionWidget: FC<SuggestionWidgetProps> = ({
  isRecommended,
  score,
  isFavorite,
  onClickFavorite,
  insightStory,
  productStory,
}) => {
  const { t } = useTranslation();

  const [timeInfo, setTimeInfo] = useState<string>("");

  useEffect(() => {
    if (insightStory) {
      const salesDate = insightStory?.salesCloseDate;
      setTimeInfo(
        salesDate
          ? t("insight.card.createdOn", [
              `${salesDate.getDate()} ${salesDate.toLocaleString("default", {
                month: "short",
              })} ${salesDate.getFullYear()}`,
            ])
          : t("insight.sliderUplinkCards.noCloseDate")
      );
    }
    if (productStory) {
      const maturityDate = productStory?.productMaturityDate;
      setTimeInfo(
        maturityDate
          ? t("insight.card.createdOn", [
              `${maturityDate.getDate()} ${maturityDate.toLocaleString(
                "default",
                {
                  month: "short",
                }
              )} ${maturityDate.getFullYear()}`,
            ])
          : t("insight.sliderUplinkCards.noMaturityDate")
      );
    }
  }, []);

  const handleClickFavorite = () => {
    const id = productStory?.id || insightStory?.id || "";
    onClickFavorite(id);
  };

  return (
    <>
      <Container>
        <ThumbContainer>
          {insightStory?.thumbnail && (
            <HeroBackground
              color="primary"
              imageUrl={insightStory?.thumbnail}
            />
          )}
          {!!insightStory && !insightStory.thumbnail && (
            <HeroBackground color="primary" variant="insight" />
          )}
          {productStory?.thumbnail && (
            <HeroBackground
              color="primary"
              imageUrl={productStory?.thumbnail}
            />
          )}
          {!!productStory && !productStory.thumbnail && (
            <HeroBackground color="primary" variant="product" />
          )}
          <TagsContainer>
            {isRecommended && <StoryTags variant="Recommended" />}
            <Filler />
            {!!score && <StoryTags variant={score} />}
            <Spacer x={8} />
            <Favorite onClick={handleClickFavorite} isFilled={!!isFavorite} />
          </TagsContainer>
          <TitleContainer>
            <Typography variant="titles" color="system/light1">
              {productStory?.title || insightStory?.title}
            </Typography>
            {productStory?.logoImage && (
              <>
                <Spacer x={10} />
                <LogoImage src={productStory.logoImage} />
              </>
            )}
          </TitleContainer>
        </ThumbContainer>
        <Spacer y={12} />
        <InlineWrapper>
          <Spacer x={12} />
          <Typography>{timeInfo}</Typography>
          <Filler />
          <Counters
            variant="views"
            count={insightStory?.globalView || productStory?.globalView || 0}
          />
          <Spacer x={8} />
          <Counters
            variant="users"
            count={insightStory?.engagement || productStory?.engagement || 0}
          />
          <Spacer x={12} />
        </InlineWrapper>
        <Spacer y={13} />
        <InlineWrapper>
          <Spacer x={12} />
          {productStory?.category || insightStory?.category ? (
            <CategoryTag>
              <Typography variant="p-small-strong" color="neutral/60">
                {productStory?.category || insightStory?.category}
              </Typography>
            </CategoryTag>
          ) : (
            <Spacer y={21} />
          )}
          <Filler />
        </InlineWrapper>

        {insightStory && (
          <>
            <Spacer y={9} />
            <TextContainer>
              <Typography variant="p" color="neutral">
                {insightStory.text}
              </Typography>
            </TextContainer>
            <Spacer y={5} />
            <InlineWrapper>
              <Spacer x={12} y={12} />
              <Typography variant="p-small" color="neutral/90">
                {insightStory.createdBy
                  ? `${t("insight.card.createdBy", [insightStory.createdBy])}`
                  : ""}
              </Typography>
              <Spacer x={12} />
            </InlineWrapper>
            <Spacer y={16} />
          </>
        )}
        {productStory && (
          <>
            <Divider marginVertical={13} marginHorizontal={12} />
            <InlineWrapper justifyContent="space-between">
              <Spacer x={12} y={39} />
              {productStory.data &&
                productStory.data.map((info) => (
                  <ProductData data={info.data} subtitle={info.subtitle} />
                ))}
              <Spacer x={12} />
            </InlineWrapper>
            <Spacer y={14} />
          </>
        )}
      </Container>
    </>
  );
};

export default SuggestionWidget;
