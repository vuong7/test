import styled from "styled-components";

type Props = {
  opacity?: string;
};

export const Container = styled.div`
  width: 100%;
  height: 347px;
  border-radius: 8px;
  background-color: ${(props) => props.theme.system.light2};
  position: relative;
  box-sizing: border-box;
`;

export const ThumbContainer = styled.div<Props>`
  position: relative;
  width: 100%;
  height: 192px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border-radius: 8px 8px 0px 0px;
  box-sizing: border-box;

  .hero-background {
    border-radius: 8px 8px 0px 0px;
  }
`;

export const TagsContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 8px 12px;
  z-index: 1;
`;

export const Filler = styled.div`
  flex-grow: 1;
`;

export const TitleContainer = styled.div`
  padding: 16px 12px;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  z-index: 1;
`;

export const LogoImage = styled.img`
  width: 70px;
  border-radius: 4px;
`;

export const CategoryTag = styled.div`
  padding: 5px 8px 4px;
  border: 1px solid ${(props) => props.theme.neutralTs[60]};
  box-sizing: border-box;
  border-radius: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const TextContainer = styled.div`
  width: 100%;
  height: 43px;
  padding: 0px 12px;
  box-sizing: border-box;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  line-clamp: 2;
  -webkit-box-orient: vertical;
`;
