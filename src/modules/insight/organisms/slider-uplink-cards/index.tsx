import React, { FC, useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  Marker,
  ArrowContainer,
  CardsContainer,
  Card,
} from "./styles";
import { SuggestionWidget, SuggestionWidgetProps } from "../";

import { Icon, Typography, InlineWrapper, Spacer } from "../../..";

export type SliderUplinkCardsProps = {
  items: SuggestionWidgetProps[];

  cardWidth?: number;
  columnGap?: number;
};

export const SliderUplinkCards: FC<SliderUplinkCardsProps> = ({
  items,

  cardWidth = 306,
  columnGap = 12,
}) => {
  const { t } = useTranslation();

  const cardsRef = useRef<HTMLHeadingElement>(null);

  const [scrollPosition, setScrollPosition] = useState<number>(0);
  const [maxRange, setMaxRange] = useState<number>(0);
  const [isSlideOn, setIsSlideOn] = useState<boolean>(false);

  const cardSpace = cardWidth + columnGap;

  const handleScroll = (value: number) => {
    const newPosition = value === 0 ? 0 : scrollPosition + value;

    if (newPosition === 0) {
      setScrollPosition(0);
    } else if (newPosition < maxRange) {
      setScrollPosition(maxRange);
    } else {
      setScrollPosition(newPosition >= 0 ? 0 : newPosition);
    }
  };

  const handleMaxRange = () => {
    const newMaxRange = cardsRef.current?.clientWidth
      ? (items.length - 1) * -cardSpace +
        (cardsRef.current?.clientWidth - cardSpace + columnGap)
      : 0;
    if (maxRange > 0) {
      setMaxRange(0);
    } else {
      setMaxRange(newMaxRange);
    }
  };

  useEffect(() => {
    handleMaxRange();
  }, [cardsRef.current]);

  useEffect(() => {
    handleScroll(0);
    if (maxRange >= 0) {
      setIsSlideOn(false);
    } else {
      setIsSlideOn(true);
    }
  }, [maxRange]);

  useEffect(() => {
    window.addEventListener("resize", handleMaxRange);
    return () => window.removeEventListener("resize", handleMaxRange);
  }, []);

  return (
    <>
      <Container>
        <Marker />
        <InlineWrapper justifyContent="space-between">
          <Typography variant="h2" color="neutral">
            {t("insight.sliderUplinkCards.storySuggestions", [items.length])}
          </Typography>
          <InlineWrapper>
            {isSlideOn && (
              <>
                <ArrowContainer onClick={() => handleScroll(cardSpace)}>
                  <Icon type="arrow-left" color="primary" />
                </ArrowContainer>
                <ArrowContainer onClick={() => handleScroll(-cardSpace)}>
                  <Icon type="arrow-right" color="primary" />
                </ArrowContainer>
              </>
            )}
          </InlineWrapper>
        </InlineWrapper>
        <Spacer y={16} />
        <CardsContainer ref={cardsRef} columnGap={columnGap}>
          {items.map((card) => (
            <Card cardWidth={cardWidth} translateX={scrollPosition}>
              <SuggestionWidget
                isRecommended={card.isRecommended}
                score={card.score}
                isFavorite={card.isFavorite}
                onClickFavorite={card.onClickFavorite}
                insightStory={card.insightStory}
                productStory={card.productStory}
              />
            </Card>
          ))}
        </CardsContainer>
      </Container>
    </>
  );
};

export default SliderUplinkCards;
