import styled from "styled-components";

type CardsContainerProps = {
  columnGap: number;
};

type CardProps = {
  cardWidth: number;
  translateX?: number;
};

export const Container = styled.div`
  position: relative;
  padding: 21px 24px 20px;
  box-sizing: border-box;
  background-color: ${(props) => props.theme.system.light1};
  border-radius: 8px;
  width: 100%;
`;

export const Marker = styled.div`
  background-color: #ff3297;
  width: 6px;
  height: 20px;
  position: absolute;
  top: 21px;
  left: 0;
`;

export const ArrowContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;

export const CardsContainer = styled.div<CardsContainerProps>`
  display: flex;
  column-gap: ${(props) => `${props.columnGap}px`};
  overflow: hidden;

  box-sizing: border-box;
  height: fit-content;
`;

export const Card = styled.div<CardProps>`
  width: ${(props) => `${props.cardWidth}px`};
  min-width: ${(props) => `${props.cardWidth}px`};
  box-sizing: border-box;

  transform: ${(props) => `translateX(${props.translateX}px)`};
  transition: transform 1s;
  transition-timing-function: transform ease-out;
`;
