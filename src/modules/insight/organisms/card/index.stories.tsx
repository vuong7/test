import React from 'react'
import { ComponentMeta } from '@storybook/react';

import { InsightCard } from './index'

export default {
  title: 'Insight/Card',
  component: InsightCard,
} as ComponentMeta<typeof InsightCard>

// const Template: Story<InsightCardProps> = (args) => <InsightCard {...args} />

export const Default = () => <InsightCard data={{
  title: 'Title goes here',
  id: 'x',
  category: 'xx',
  score: 'MEDIUM',
  isShared: false,
  createdBy: 'Author name',
  createdAt: new Date()
}} />;