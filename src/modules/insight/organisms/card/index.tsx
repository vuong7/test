import React, { memo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { Typography } from '../../../core';
// import { Icon, Label, Typography } from 'modules/core/presenters';

// import { formatDateTime } from 'utils/date';

import { Image, Body, Footer, Header, Wrapper } from './styles';

export type InsightCardData = {
  id: string;
  title: string;
  category: string;
  thumbnail?: string;
  score: 'HIGH' | 'MEDIUM' | 'LOW';
  isShared: boolean;
  createdBy: string;
  createdAt: Date;
};

export type InsightCardProps = {
  data: InsightCardData;
};
export const InsightCard: React.FC<InsightCardProps> = memo(({ data }) => {
  const { t } = useTranslation();
  
  return (
    <Wrapper>
      <Header>
        <Typography variant="p-small">
          {t('insight.card.createdOn', [data.createdAt.toISOString()])}
          {/* {t('insight.card.createdOn', { date: data.createdAt.toISOString() })} */}
        </Typography>
        {/* <Label variant="squared" customColor="#1CC684">
          High
        </Label> */}
      </Header>
      <Body>
        <Image src={data.thumbnail} />
        <Typography color="primary" variant="titles">{data.title}</Typography>
      </Body>
      <Footer>
        <Typography variant="p-small">{t('insight.card.createdBy', [data.createdBy])}</Typography>
        {/* <Label variant="outline">{data.category || t('Category')}</Label> */}
      </Footer>
    </Wrapper>
  );
});

export default InsightCard;
