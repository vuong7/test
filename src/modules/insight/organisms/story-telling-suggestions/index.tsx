import React, { FC, useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import {
  Container,
  Marker,
  IconContainer,
  CardsContainer,
  Card,
  ShowMoreContainer,
  EmptyFilter,
} from "./styles";
import { SuggestionWidget, SuggestionWidgetProps } from "../";

import { Icon, Typography, InlineWrapper, Spacer } from "../../..";
import { DropDownFilter, OptionData } from "../../";

export type StoryTellingSuggestionsProps = {
  items: SuggestionWidgetProps[];
  cardWidth?: number;
  columnGap?: number;
  isEditMode?: boolean;
  onClickAddNew?(): void;
};

export const StoryTellingSuggestions: FC<StoryTellingSuggestionsProps> = ({
  items,
  cardWidth = 305,
  columnGap = 31,
  isEditMode,
  onClickAddNew,
}) => {
  const { t } = useTranslation();

  const cardsRef = useRef<HTMLHeadingElement>(null);

  const [hasShowMore, setHasShowMore] = useState<boolean>(false);
  const [isExpanded, setIsExpanded] = useState<boolean>(false);
  const [wasExpanded, setWasExpanded] = useState<boolean>(false);
  const [isChanging, setIsChanging] = useState<boolean>(false);

  const [filterOptions, setFilterOptions] = useState<OptionData[]>([]);
  const [filterSelected, setFilterSelected] = useState<string[]>([]);

  useEffect(() => {
    setIsExpanded(false);
    setIsChanging(!isChanging);
  }, [items.length, filterSelected]);

  useEffect(() => {
    if (!isExpanded) {
      const isHidden = cardsRef.current
        ? Math.floor(
            cardsRef.current?.scrollHeight - cardsRef.current?.offsetHeight
          ) > 1
        : false;

      setHasShowMore(isHidden);
      setIsExpanded(wasExpanded);
    }
  }, [isChanging, cardsRef.current]);

  useEffect(() => {
    const productsCount = items.filter((item) => !!item.productStory).length;
    const insightsCount = items.filter((item) => !!item.insightStory).length;
    let currentOptions = [];
    if (productsCount > 0) {
      currentOptions.push({ name: "products", count: productsCount });
    }
    if (insightsCount > 0) {
      currentOptions.push({ name: "insights", count: insightsCount });
    }
    setFilterOptions(currentOptions);
    setFilterSelected([...currentOptions.map((option) => option.name)]);
  }, [items]);

  const handleExpanded = () => {
    setIsExpanded(!isExpanded);
    setWasExpanded(!isExpanded);
  };

  const handleFilter = (name: string) => {
    if (filterSelected.includes(name)) {
      setFilterSelected([
        ...filterSelected.filter((option) => option !== name),
      ]);
    } else {
      setFilterSelected([...filterSelected, name]);
    }
  };

  return (
    <>
      <Container>
        <Marker />
        <InlineWrapper justifyContent="space-between">
          <Typography variant="h2" color="neutral">
            {t("insight.sliderUplinkCards.storySuggestions", [items.length])}
          </Typography>
          <InlineWrapper>
            {isEditMode ? (
              <>
                <IconContainer onClick={onClickAddNew}>
                  <Icon type="plus" size="sm" color="primary" />
                  <Typography variant="h4" color="primary">
                    {t("insight.storyTellingSuggestions.addNew")}
                  </Typography>
                </IconContainer>
              </>
            ) : (
              <DropDownFilter
                options={filterOptions}
                selected={filterSelected}
                onSelect={(name: string) => handleFilter(name)}
              />
            )}
          </InlineWrapper>
        </InlineWrapper>
        <Spacer y={16} />
        <CardsContainer
          ref={cardsRef}
          columnGap={columnGap}
          isExpanded={isExpanded}
        >
          {items
            .filter(
              (item) =>
                (!!item.insightStory && filterSelected.includes("insights")) ||
                (!!item.productStory && filterSelected.includes("products"))
            )
            .map((card, index) => (
              <Card
                key={`${index}${card.insightStory?.id}${card.productStory?.id}`}
                cardWidth={cardWidth}
              >
                <SuggestionWidget
                  isRecommended={card.isRecommended}
                  score={card.score}
                  isFavorite={card.isFavorite}
                  onClickFavorite={card.onClickFavorite}
                  insightStory={card.insightStory}
                  productStory={card.productStory}
                />
              </Card>
            ))}
        </CardsContainer>
        {hasShowMore && (
          <>
            <Spacer y={28} />
            <InlineWrapper justifyContent="center">
              <ShowMoreContainer onClick={() => handleExpanded()}>
                <IconContainer>
                  <Icon
                    type={isExpanded ? "arrow-up" : "arrow-down"}
                    color="highlight"
                  />
                </IconContainer>
                <Spacer x={8} />
                <Typography variant="p-semi-bold" color="highlight">
                  {isExpanded
                    ? t("insight.storyTellingSuggestions.showLess")
                    : t("insight.storyTellingSuggestions.showMore")}
                </Typography>
              </ShowMoreContainer>
            </InlineWrapper>
          </>
        )}
        {filterSelected.length === 0 && (
          <EmptyFilter>
            <InlineWrapper>
              <Icon type="alert-circle" color="neutral/80" size="lg" />
            </InlineWrapper>
            <Spacer y={10} />
            <InlineWrapper>
              <Typography variant="h3">
                {t("insight.storyTellingSuggestions.noFilterStories")}
              </Typography>
            </InlineWrapper>
          </EmptyFilter>
        )}
      </Container>
    </>
  );
};

export default StoryTellingSuggestions;
