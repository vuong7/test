import styled from "styled-components";

type CardsContainerProps = {
  columnGap: number;
  isExpanded?: boolean;
};

type CardProps = {
  cardWidth: number;
};

export const Container = styled.div`
  position: relative;
  padding: 21px 24px 20px;
  box-sizing: border-box;
  background-color: ${(props) => props.theme.system.light1};
  border-radius: 8px;
  width: 100%;
  overflow: hidden;
  min-height: 424px;
`;

export const Marker = styled.div`
  background-color: #ff3297;
  width: 6px;
  height: 20px;
  position: absolute;
  top: 21px;
  left: 0;
`;

export const IconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
  }
`;

export const CardsContainer = styled.div<CardsContainerProps>`
  display: flex;
  flex-wrap: wrap;

  column-gap: ${(props) => `${props.columnGap}px`};
  row-gap: 26px;

  box-sizing: border-box;
  max-height: ${(props) => (props.isExpanded ? "auto" : "720px")};
  overflow: hidden;
`;

export const Card = styled.div<CardProps>`
  width: ${(props) => `${props.cardWidth}px`};
  min-width: ${(props) => `${props.cardWidth}px`};
  box-sizing: border-box;
`;

export const ShowMoreContainer = styled.div`
  display: flex;
  align-items: center;
  &:hover {
    cursor: pointer;
  }
`;

export const EmptyFilter = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  position: absolute;
  top: 50%;
  left: 50%;

  transform: translate(-50%, -50%);
`;
