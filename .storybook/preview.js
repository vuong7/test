import React from 'react';
import { JobhopStorybookProvider } from '../src/contexts/provider'

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  options: {
    storySort: {
      order: ['Design System', 'Contact', 'Insight', 'Product', 'Uplink'],
      method: 'alphabetical',
    },
  }
}

export const decorators = [
  (Story) => (
    <JobhopStorybookProvider>
      <Story />
    </JobhopStorybookProvider>
  ),
];